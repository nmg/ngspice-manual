# Locations
The project and download pages of ngspice may be found at:
- [Ngspice homepage](http://ngspice.sourceforge.net/)
- [Project page at sourceforge](http://sourceforge.net/projects/ngspice/)
- [Download page at
  sourceforge](http://sourceforge.net/projects/ngspice/files/)
- [Git source download](http://sourceforge.net/scm/?type=cvs&group_id=38962)


# Status

This is a online version of a PDF work-in progress manual. The PDF can be found
at the [ngspice website](http://ngspice.sourceforge.net/docs.html). 

Some to-dos are listed in Chapt. 24.3. More is surely
needed. You are invited to report bugs, missing items, wrongly described items, bad English
style etc.

# How to use this manual

This manual is intended to follow the latest version of ngspice (Version 30 as
of this entry) and largely mirror the latest PDF manual. 

The manual is
intended to provide a complete description of the ngspice functionality, its features, commands,
or procedures. 

It is **not** a book about learning SPICE usage, but the novice user may find some
hints how to start using ngspice. 

Chapter 21.1 gives a short introduction how to set up and
simulate a small circuit. Chapter 32 is about compiling and installing ngspice from a tarball or
the actual Git source code, which you may find on the ngspice web pages. If you are running a
specific Linux distribution, you may check if it provides ngspice as part of the package. Some
are listed here.

# License
This document is covered by the Creative Commons Attribution Share-Alike (CC-BY-SA)
v4.0..
Part of chapters 12 and 25-27 are in the public domain.
Chapter 30 is covered by New BSD (chapt. 33.3.2).

# Contributors
Spice3 and CIDER were originally written at The University of California at Berkeley (USA).

XSPICE has been provided by Georgia Institute of Technology, Atlanta (USA).

Since then, there have been many people working on the software, most of them releasing
patches to the original code through the Internet.

The following people have contributed in some way:
- Vera Albrecht
- Cecil Aswell
- Giles C. Billingsley
- Phil Barker
- Steven Borley
- Stuart Brorson
- Mansun Chan
- Wayne A. Christopher
- Al Davis
- Glao S. Dezai
- Jon Engelbert
- Daniele Foci
- Noah Friedman
- David A. Gates
- Alan Gillespie
- John Heidemann
- Marcel Hendrix
- Jeffrey M. Hsu
- JianHui Huang
- S. Hwang
- Chris Inbody
- Gordon M. Jacobs
- Min-Chie Jeng
- Beorn Johnson
- Stefan Jones
- Kenneth H. Keller
- Francesco Lannutti
- Robert Larice
- Mathew Lew
- Robert Lindsell
- Weidong Liu
- Kartikeya Mayaram
- Richard D. McRoberts
- Manfred Metzger
- Wolfgang Muees
- Paolo Nenzi
- Gary W. Ng
- Hong June Park
- Stefano Perticaroli
- Arno Peters
- Serban-Mihai Popescu
- Georg Post
- Thomas L. Quarles
- Emmanuel Rouat
- Jean-Marc Routure
- Jaijeet S. Roychowdhury
- Lionel Sainte Cluque
- Takayasu Sakurai
- Amakawa Shuhei
- Kanwar Jit Singh
- Bill Swartz
- Hitoshi Tanaka
- Steve Tell
- Andrew Tuckey
- Andreas Unger
- Holger Vogt
- Dietmar Warning
- Michael Widlok
- Charles D.H. Williams
- Antony Wilson

 and many others...

If someone helped in the development and has not been inserted in this list then this omis-
sion was unintentional. 

If you feel you should be on this list then please write to <ngspice-devel@lists.sourceforge.net>. Do not be shy, we would like to make a list as complete as
possible.



