 
Applications that are exclusively analog can make use of all analysis modes
 with the exception of Code Model subsystem that do not implements Pole-Zero,
 Distortion, Sensitivity and Noise analyses.
 Event-driven applications that include digital and User-Defined Node types
 may make use of DC (operating point and DC sweep) and Transient only.
 


 
In order to understand the relationship between the different analyses and
 the two underlying simulation algorithms of ngspice, it is important to
 understand what is meant by each analysis type.
 This is detailed below.


