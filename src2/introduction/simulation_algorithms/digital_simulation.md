## Digital Simulation

Digital circuit simulation differs from analog circuit simulation in several
 respects.
 A primary difference is that a solution of Kirchhoff's laws is not required.
 Instead, the simulator must only determine whether a change in the logic
 state of a node has occurred and propagate this change to connected elements.
 Such a change is called an `event'.
 
When an event occurs, the simulator examines only those circuit elements
 that are affected by the event.
 As a result, matrix analysis is not required in digital simulators.
 By comparison, analog simulators must iteratively solve for the behavior
 of the entire circuit because of the forward and reverse transmission propertie
s of analog components.
 This difference results in a considerable computational advantage for digital
 circuit simulators, which is reflected in the significantly greater speed
 of digital simulations.


