## Mixed-Level Simulation


 
Ngspice can simulate numerical device models for diodes and transistors
 in two different ways, either through the integrated DSIM simulator or
 interfacing to GSS TCAD system.
 DSIM is an internal C-based device simulator that is part of the CIDER
 simulator, the mixed-level simulator based on SPICE3f5.
 CIDER within ngspice provides circuit analyses, compact models for semiconducto
r devices, and one- or two-dimensional numerical device models.
 


### CIDER (DSIM)


 
CIDER integrates the DSIM simulator with Spice3.
 It provides accurate, one- and two-dimensional numerical device models
 based on the solution of Poisson's equation, and the electron and hole
 current-continuity equations.
 DSIM incorporates many of the same basic physical models found in the Stanford
 two-dimensional device simulator PISCES.
 Input to CIDER consists of a SPICE-like description of the circuit and
 its compact models, and PISCES-like descriptions of the structures of numerical
ly modeled devices.
 As a result, CIDER should seem familiar to designers already accustomed
 to these two tools.
 The CIDER input format has great flexibility and allows access to physical
 model parameters.
 New physical models have been added to allow simulation of state-of-the-art
 devices.
 These include transverse field mobility degradation important in scaled-down
 MOSFETs and a polysilicon model for poly-emitter bipolar transistors.
 Temperature dependence has been included over the range from -50C to 150C.
 The numerical models can be used to simulate all the basic types of semiconduct
or devices: resistors, MOS capacitors, diodes, BJTs, JFETs and MOSFETs.
 BJTs and JFETs can be modeled with or without a substrate contact.
 Support has been added for the management of device internal states.
 Post-processing of device states can be performed using the ngnutmeg user
 interface.


### GSS TCAD


 
GSS is a TCAD software that enables two-dimensional numerical simulation
 of semiconductor device with well-known drift-diffusion and hydrodynamic
 method.
 GSS has Basic DDM (drift-diffusion method) solver, Lattice Temperature
 Corrected DDM solver, EBM (energy balance method) solver and Quantum corrected
 DDM solver based on density-gradient theory.
 The GSS program is directed via input statements by a user specified disk
 file.
 Supports triangle mesh generation and adaptive mesh refinement.
 Employs PMI (physical model interface) to support various materials, including
 compound semiconductor materials such as SiGe and AlGaAs.
 Supports DC sweep, transient and AC sweep calculations.
 The device can be stimulated by voltage or current source(s).
 


 
GSS is no longer updated, but is still available as open source as a limited
 edition of the commercial GENIUS TCAD tool.
 This interface has not been tested with actual ngspice versions and may
 need some maintainance efforts.

