## Analog Simulation
 
Analog simulation focuses on the linear and non-linear behavior of a circuit
 over a continuous time or frequency interval.
 The circuit response is obtained by iteratively solving Kirchhoff's Laws
 for the circuit at time steps selected to ensure the solution has converged
 to a stable value and that numerical approximations of integrations are
 sufficiently accurate.
 Since Kirchhoff's laws form a set of simultaneous equations, the simulator
 operates by solving a matrix of equations at each time point.
 This matrix processing generally results in slower simulation times when
 compared to digital circuit simulators.


 
The response of a circuit is a function of the applied sources.
 Ngspice offers a variety of source types including DC, sine-wave, and pulse.
 In addition to specifying sources, the user must define the type of simulation
 to be run.
 This is termed the `mode of analysis'.
 Analysis modes include DC analysis, AC analysis, and transient analysis.
 For DC analysis, the time-varying behavior of reactive elements is neglected
 and the simulator calculates the DC solution of the circuit.
 Swept DC analysis may also be accomplished with ngspice.
 This is simply the repeated application of DC analysis over a range of
 DC levels for the input sources.
 For AC analysis, the simulator determines the response of the circuit,
 including reactive elements to small-signal sinusoidal inputs over a range
 of frequencies.
 The simulator output in this case includes amplitudes and phases as a function
 of frequency.
 For transient analysis, the circuit response, including reactive elements,
 is analyzed to calculate the behavior of the circuit as a function of time.
 


