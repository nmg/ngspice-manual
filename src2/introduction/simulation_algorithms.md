# Simulation Algorithms


 
Computer-based circuit simulation is often used as a tool by designers,
 test engineers, and others who want to analyze the operation of a design
 without examining the physical circuit.
 Simulation allows you to change quickly the parameters of many of the circuit
 elements to determine how they affect the circuit response.
 Often it is difficult or impossible to change these parameters in a physical
 circuit.


 
However, to be practical, a simulator must execute in a reasonable amount
 of time.
 The key to efficient execution is choosing the proper level of modeling
 abstraction for a given problem.
 To support a given modeling abstraction, the simulator must provide appropriate
 algorithms.


 
Historically, circuit simulators have supported either an analog simulation
 algorithm or a digital simulation algorithm.
 Ngspice inherits the XSPICE framework and supports both analog and digital
 algorithms and is a `mixed-mode' simulator.


