# AC Small-Signal Analysis

AC analysis is limited to analog nodes and represents the small signal,
 sinusoidal solution of the analog system described at a particular frequency
 or set of frequencies.
 This analysis is similar to the DC analysis in that it represents the steady-st
ate behavior of the described system with a single input node 
*at a given set of stimulus frequencies*.


 
The program first computes the dc operating point of the circuit and determines
 linearized, small-signal models for all of the nonlinear devices in the
 circuit.
 The resultant linear circuit is then analyzed over a user-specified range
 of frequencies.
 The desired output of an ac small-signal analysis is usually a transfer
 function (voltage gain, transimpedance, etc).
 If the circuit has only one ac input, it is convenient to set that input
 to unity and zero phase, so that output variables have the same value as
 the transfer function of the output variable with respect to the input.


