# Small-Signal Distortion Analysis
The distortion analysis portion of Ngspice computes steady-state harmonic
 and intermodulation products for small input signal magnitudes.
 If signals of a single frequency are specified as the input to the circuit,
 the complex values of the second and third harmonics are determined at
 every point in the circuit.
 If there are signals of two frequencies input to the circuit, the analysis
 finds out the complex values of the circuit variables at the sum and difference
 of the input frequencies, and at the difference of the smaller frequency
 from the second harmonic of the larger frequency.
 Distortion analysis is supported for the following nonlinear devices: 
- Diodes (DIO)
- BJT
- JFET (level 1)
- MOSFETs (levels 1, 2, 3, 9, and BSIM1)
- MESFET (level 1)

All linear devices are automatically supported by distortion analysis.
 If there are switches present in the circuit, the analysis continues to
 be accurate provided the switches do not change state under the small excitatio
ns used for distortion calculations.

If a device model does not support direct small signal distortion analysis,
 please use the Fourier of FFT statements and evaluate the output per scripting.


