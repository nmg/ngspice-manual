# Transient Analysis

Transient analysis is an extension of DC analysis to the time domain.
 A transient analysis begins by obtaining a DC solution to provide a point
 of departure for simulating time-varying behavior.
 Once the DC solution is obtained, the time-dependent aspects of the system
 are reintroduced, and the two simulator algorithms incrementally solve
 for the time varying behavior of the entire system.
 Inconsistencies in node values are resolved by the two simulation algorithms
 such that the time-dependent waveforms created by the analysis are consistent
 across the entire simulated time interval.
 Resulting time-varying descriptions of node behavior for the specified
 time interval are accessible to you.


 
All sources that are not time dependent (for example, power supplies) are
 set to their dc value.
 The transient time interval is specified on a `.TRAN `control line.


