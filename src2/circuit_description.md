# Circuit Description

## General Structure and Conventions

### Input file structure
The circuit to be analyzed is described to ngspice by a set of element instance
 lines, which define the circuit topology and element instance values, and
 a set of control lines, which define the model parameters and the run controls.
 All lines are assembled in an input file to be read by ngspice.
 Two lines are essential:

- The first line in the input file must be the title, which is the only comment
 line that does not need any special character in the first place.

- The last line must be `.end`.
 

The order of the remaining lines is arbitrary (except, of course, that continuat
ion lines must immediately follow the line being continued).
 This feature in the ngspice input language dates back to the punched card
 times where elements were written on separate cards (and cards frequently
 fell off).
 Leading white spaces in a line are ignored, as well as empty lines.

The lines described in sections 2.1 to 2.12 are typically used in the core
 of the input file, outside of a `.control` section (see 
\begin_inset CommandInset ref
LatexCommand ref
reference "subsec:Interactive-mode-with"

\end_inset

).
 An exception is the `.include includefile`
 line (
\begin_inset CommandInset ref
LatexCommand ref
reference "sec:.INCLUDE"

\end_inset

) that may be placed anywhere in the input file.
 The contents of
\family typewriter
 includefile
\family default
 will be inserted exactly in place of the 
\family typewriter
.include
\family default
 line.
\end_layout

\begin_layout Subsection
\begin_inset CommandInset label
LatexCommand label
name "subsec:Circuit-elements-(device"

\end_inset

Circuit elements (device instances)
\end_layout

\begin_layout Standard
Each element in the circuit is a device instance specified by an
\series bold
 instance line
\series default
 that contains:
\end_layout

\begin_layout Itemize
the element instance name, 
\end_layout

\begin_layout Itemize
the circuit nodes to which the element is connected, 
\end_layout

\begin_layout Itemize
and the values of the parameters that determine the electrical characteristics
 of the element.
 
\end_layout

\begin_layout Standard
The first letter of the element instance name specifies the element type.
 The format for the ngspice element types is given in the following manual
 chapters.
 In the rest of the manual, the strings 
\family typewriter
XXXXXXX
\family default
, 
\family typewriter
YYYYYYY
\family default
, and 
\family typewriter
ZZZZZZZ
\family default
 denote arbitrary alphanumeric strings.
\end_layout

\begin_layout Standard
For example, a resistor instance name must begin with the letter 
\family typewriter
R
\family default
 and can contain one or more characters.
 Hence, 
\family typewriter
R
\family default
, 
\family typewriter
R1
\family default
,
\family typewriter
\series bold
 
\series default
RSE
\family default
, 
\family typewriter
ROUT
\family default
, and 
\family typewriter
R3AC2ZY
\family default
 are valid resistor names.
 Details of each type of device are supplied in a following section 
\begin_inset CommandInset ref
LatexCommand ref
reference "cha:Circuit-Elements-and"

\end_inset

.
 Table 
\begin_inset CommandInset ref
LatexCommand ref
reference "tab:ngspice-element-types"

\end_inset

 lists the element types available in ngspice, sorted by their first letter.
\end_layout

\begin_layout Standard
\begin_inset Float table
placement H
wide false
sideways false
status open

\begin_layout Plain Layout
\align center
\begin_inset Tabular
<lyxtabular version="3" rows="27" columns="3">
<features tabularvalignment="middle">
<column alignment="center" valignment="middle">
<column alignment="center" valignment="middle">
<column alignment="center" valignment="middle" width="4cm">
<row>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
First letter
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Element description 
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Comments, links
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
A
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
XSPICE code model
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset CommandInset ref
LatexCommand ref
reference "cha:Behavioral-Modeling"

\end_inset


\begin_inset Newline linebreak
\end_inset

analog (
\begin_inset CommandInset ref
LatexCommand ref
reference "sec:XSPICE-Analog-Models"

\end_inset

)
\begin_inset Newline linebreak
\end_inset

digital (
\begin_inset CommandInset ref
LatexCommand ref
reference "sec:XSPICE-Digital-Models"

\end_inset

)
\begin_inset Newline linebreak
\end_inset

mixed signal (
\begin_inset CommandInset ref
LatexCommand ref
reference "sec:XSPICE-Hybrid-Models"

\end_inset

)
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
B
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Behavioral (arbitrary) source
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset CommandInset ref
LatexCommand ref
reference "sec:B-source-(ASRC)"

\end_inset


\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
C
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Capacitor
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset CommandInset ref
LatexCommand ref
reference "subsec:Capacitors"

\end_inset


\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
D
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Diode
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset CommandInset ref
LatexCommand ref
reference "cha:DIODEs"

\end_inset


\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
E
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Voltage-controlled voltage source (VCVS)
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
linear (
\begin_inset CommandInset ref
LatexCommand ref
reference "subsec:Exxxx:-Linear-Voltage-Controlled"

\end_inset

), 
\begin_inset Newline linebreak
\end_inset

non-linear (
\begin_inset CommandInset ref
LatexCommand ref
reference "sec:E-source-(non-linear"

\end_inset

)
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
F
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Current-controlled current source (CCCs)
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
linear (
\begin_inset CommandInset ref
LatexCommand ref
reference "subsec:Fxxxx:-Linear-Current-Controlled"

\end_inset

)
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
G
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Voltage-controlled current source (VCCS)
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
linear (
\begin_inset CommandInset ref
LatexCommand ref
reference "subsec:Gxxxx:-Linear-Voltage-Controlled"

\end_inset

), 
\begin_inset Newline linebreak
\end_inset

non-linear (
\begin_inset CommandInset ref
LatexCommand ref
reference "sec:G-source-(non-linear"

\end_inset

)
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
H
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Current-controlled voltage source (CCVS)
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
linear (
\begin_inset CommandInset ref
LatexCommand ref
reference "subsec:Hxxxx:-Linear-Current-Controlled"

\end_inset

)
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
I
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Current source
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset CommandInset ref
LatexCommand ref
reference "sec:Independent-Sources-for"

\end_inset


\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
J
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Junction field effect transistor (JFET)
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset CommandInset ref
LatexCommand ref
reference "cha:JFETs"

\end_inset


\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
K
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Coupled (Mutual) Inductors
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset CommandInset ref
LatexCommand ref
reference "subsec:Coupled-(Mutual)-Inductors"

\end_inset


\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
L
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Inductor
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset CommandInset ref
LatexCommand ref
reference "subsec:Inductors"

\end_inset


\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
M
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Metal oxide field effect transistor (MOSFET)
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset CommandInset ref
LatexCommand ref
reference "cha:MOSFETs"

\end_inset

 
\begin_inset Newline linebreak
\end_inset

BSIM3 (
\begin_inset CommandInset ref
LatexCommand ref
reference "subsec:BSIM3-model"

\end_inset

) 
\begin_inset Newline linebreak
\end_inset

BSIM4 (
\begin_inset CommandInset ref
LatexCommand ref
reference "subsec:BSIM4-model"

\end_inset

)
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
N
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Numerical device for GSS
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset CommandInset ref
LatexCommand ref
reference "sec:GSS,-Genius"

\end_inset


\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
O
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Lossy transmission line
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset CommandInset ref
LatexCommand ref
reference "sec:Lossy-Transmission-Lines"

\end_inset


\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
P
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Coupled multiconductor line (CPL)
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset CommandInset ref
LatexCommand ref
reference "subsec:Coupled-Multiconductor-Line"

\end_inset


\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Q
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Bipolar junction transistor (BJT)
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset CommandInset ref
LatexCommand ref
reference "cha:BJTs"

\end_inset


\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
R
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Resistor
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset CommandInset ref
LatexCommand ref
reference "subsec:Resistors"

\end_inset


\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
S
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Switch (voltage-controlled)
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset CommandInset ref
LatexCommand ref
reference "subsec:Switches"

\end_inset


\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
T
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Lossless transmission line
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset CommandInset ref
LatexCommand ref
reference "sec:Lossless-Transmission-Lines"

\end_inset


\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
U
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Uniformly distributed RC line
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset CommandInset ref
LatexCommand ref
reference "sec:Uniform-Distributed-RC"

\end_inset


\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
V
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Voltage source
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset CommandInset ref
LatexCommand ref
reference "sec:Independent-Sources-for"

\end_inset


\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
W
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Switch (current-controlled)
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset CommandInset ref
LatexCommand ref
reference "subsec:Switches"

\end_inset


\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
X
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Subcircuit
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset CommandInset ref
LatexCommand ref
reference "subsec:Subcircuit-Calls"

\end_inset


\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Y
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Single lossy transmission line (TXL)
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset CommandInset ref
LatexCommand ref
reference "subsec:Single-Lossy-Transmission"

\end_inset


\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Z
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Metal semiconductor field effect transistor (MESFET)
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset CommandInset ref
LatexCommand ref
reference "cha:MESFETs"

\end_inset


\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
\begin_inset CommandInset label
LatexCommand label
name "tab:ngspice-element-types"

\end_inset

ngspice element types
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Subsection
Some naming conventions
\end_layout

\begin_layout Standard
Fields on a line are separated by one or more blanks, a comma, an equal
 (
\family typewriter
=
\family default
) sign, or a left or right parenthesis; extra spaces are ignored.
 A line may be continued by entering a `
\family typewriter
+
\family default
' (plus) in column 1 of the following line; ngspice continues reading beginning
 with column 2.
 A name field must begin with a letter (A through Z) and cannot contain
 any delimiters.
 A number field may be an integer field (12, -44), a floating point field
 (3.14159), either an integer or floating point number followed by an integer
 exponent (1e-14, 2.65e3), or either an integer or a floating point number
 followed by one of the following scale factors:
\end_layout

\begin_layout Standard
\begin_inset Float table
placement H
wide false
sideways false
status open

\begin_layout Plain Layout
\align center
\begin_inset Tabular
<lyxtabular version="3" rows="11" columns="3">
<features tabularvalignment="middle">
<column alignment="center" valignment="top">
<column alignment="center" valignment="top">
<column alignment="center" valignment="top">
<row>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Suffix
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Name
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Factor
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
T
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Tera
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $10^{12}$
\end_inset


\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
G
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Giga
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $10^{9}$
\end_inset


\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Meg
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Mega
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $10^{6}$
\end_inset


\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
K
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Kilo
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $10^{3}$
\end_inset


\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
mil
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Mil
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $25.4\times10^{-6}$
\end_inset


\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
m
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
milli
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $10^{-3}$
\end_inset


\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
u
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
micro
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $10^{-6}$
\end_inset


\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
n
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
nano
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $10^{-9}$
\end_inset


\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
p
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
pico
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $10^{-12}$
\end_inset


\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
f
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
femto
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $10^{-15}$
\end_inset


\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
Ngspice scale factors
\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout

\end_layout

\end_inset


\end_layout

\begin_layout Standard
Letters immediately following a number that are not scale factors are ignored,
 and letters immediately following a scale factor are ignored.
 Hence, 10, 10V, 10Volts, and 10Hz all represent the same number, and M,
 MA, MSec, and MMhos all represent the same scale factor.
 Note that 1000, 1000.0, 1000Hz, 1e3, 1.0e3, 1kHz, and 1k all represent the
 same number.
 Note that 
\emph on
`M'
\emph default
 or 
\emph on
`m'
\emph default
 denote `milli', i.e.
 
\begin_inset Formula $10^{-3}$
\end_inset

.
 Suffix 
\emph on
meg
\emph default
 has to be used for 
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none

\begin_inset Formula $10^{6}$
\end_inset

.
\end_layout

\begin_layout Standard
Nodes names may be arbitrary character strings and are case insensitive,
 if ngspice is used in batch mode (
\begin_inset CommandInset ref
LatexCommand ref
reference "subsec:Batch-mode"

\end_inset

).
 If in interactive (
\begin_inset CommandInset ref
LatexCommand ref
reference "subsec:Interactive-mode"

\end_inset

) or control (
\begin_inset CommandInset ref
LatexCommand ref
reference "subsec:Interactive-mode-with"

\end_inset

) mode, node names may either be plain numbers or arbitrary character strings,
 
\series bold
not
\series default
 starting with a number.
 The ground node must be named `
\family typewriter
\series medium
0
\family default
\series default
' (zero).
 For compatibility reason 
\family typewriter
gnd
\family default
 is accepted as ground node, and will internally be treated as a global
 node and be converted to `
\family typewriter
\series medium
0
\family default
\series default
'.
 If this is not feasible, you may switch the conversion off by setting
\family typewriter
 set no_auto_gnd
\family default
 in one of the configuration files spinit or .spiceinit.
 
\emph on
Each circuit has to have a ground node (gnd or 0)!
\emph default
 Note the difference in ngspice where the nodes are treated as character
 strings and not evaluated as numbers, thus `
\family typewriter
\series medium
0
\family default
\series default
' and 
\family typewriter
00
\family default
 are distinct nodes in ngspice but not in SPICE2.
 
\end_layout

\begin_layout Standard
Ngspice requires that the following topological constraints are satisfied:
\end_layout

\begin_layout Itemize
The circuit cannot contain a loop of voltage sources and/or inductors and
 cannot contain a cut-set of current sources and/or capacitors.
 
\end_layout

\begin_layout Itemize
Each node in the circuit must have a dc path to ground.
 
\end_layout

\begin_layout Itemize
Every node must have at least two connections except for transmission line
 nodes (to permit unterminated transmission lines) and MOSFET substrate
 nodes (which have two internal connections anyway).
\end_layout

\begin_layout Section
Basic lines
\end_layout

\begin_layout Subsection
.TITLE line
\end_layout

\begin_layout Standard
\begin_inset Box Frameless
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "100col%"
special "none"
height "1in"
height_special "totalheight"
thickness "0.4pt"
separation "3pt"
shadowsize "4pt"
framecolor "black"
backgroundcolor "none"
status open

\begin_layout Plain Layout
Examples:
\end_layout

\begin_layout LyX-Code
\begin_inset listings
lstparams "basicstyle={\ttfamily}"
inline false
status open

\begin_layout Plain Layout

POWER AMPLIFIER CIRCUIT
\end_layout

\begin_layout Plain Layout

* additional lines following
\end_layout

\begin_layout Plain Layout

*...
\end_layout

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout

Test of CAM cell
\end_layout

\begin_layout Plain Layout

* additional lines following
\end_layout

\begin_layout Plain Layout

*...
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
The title line must be the first in the input file.
 Its contents are printed verbatim as the heading for each section of output.
\end_layout

\begin_layout Standard
As an alternative you may place a
\family typewriter
 .TITLE <any title>
\family default
 line anywhere in your input deck.
 The first line of your input deck will be overridden by the contents of
 this line following the .TITLE statement.
 
\end_layout

\begin_layout Standard
\begin_inset Box Frameless
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "100col%"
special "none"
height "1in"
height_special "totalheight"
thickness "0.4pt"
separation "3pt"
shadowsize "4pt"
framecolor "black"
backgroundcolor "none"
status open

\begin_layout Plain Layout
.TITLE line example:
\end_layout

\begin_layout LyX-Code
\begin_inset listings
lstparams "basicstyle={\ttfamily}"
inline false
status open

\begin_layout Plain Layout

******************************
\end_layout

\begin_layout Plain Layout

* additional lines following
\end_layout

\begin_layout Plain Layout

*...
\end_layout

\begin_layout Plain Layout

.TITLE Test of CAM cell
\end_layout

\begin_layout Plain Layout

* additional lines following
\end_layout

\begin_layout Plain Layout

*...
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
will internally be replaced by
\end_layout

\begin_layout Standard
\begin_inset Box Frameless
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "100col%"
special "none"
height "1in"
height_special "totalheight"
thickness "0.4pt"
separation "3pt"
shadowsize "4pt"
framecolor "black"
backgroundcolor "none"
status open

\begin_layout Plain Layout
Internal input deck:
\end_layout

\begin_layout LyX-Code
\begin_inset listings
lstparams "basicstyle={\ttfamily}"
inline false
status open

\begin_layout Plain Layout

Test of CAM cell
\end_layout

\begin_layout Plain Layout

* additional lines following
\end_layout

\begin_layout Plain Layout

*...
\end_layout

\begin_layout Plain Layout

*TITLE Test of CAM cell
\end_layout

\begin_layout Plain Layout

* additional lines following
\end_layout

\begin_layout Plain Layout

*...
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Subsection
.END Line
\end_layout

\begin_layout Standard
\begin_inset Box Frameless
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "100col%"
special "none"
height "1in"
height_special "totalheight"
thickness "0.4pt"
separation "3pt"
shadowsize "4pt"
framecolor "black"
backgroundcolor "none"
status open

\begin_layout Plain Layout
Examples:
\end_layout

\begin_layout LyX-Code
\begin_inset listings
lstparams "basicstyle={\ttfamily}"
inline false
status open

\begin_layout Plain Layout

.end
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
The 
\family typewriter
.end
\family default
 line must always be the last in the input file.
 Note that the period is an integral part of the name.
\end_layout

\begin_layout Subsection
Comments
\end_layout

\begin_layout Standard
\begin_inset Box Frameless
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "100col%"
special "none"
height "1in"
height_special "totalheight"
thickness "0.4pt"
separation "3pt"
shadowsize "4pt"
framecolor "black"
backgroundcolor "none"
status open

\begin_layout Plain Layout
General Form:
\end_layout

\begin_layout LyX-Code
\begin_inset listings
lstparams "basicstyle={\ttfamily}"
inline false
status open

\begin_layout Plain Layout

* <any comment>
\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout
Examples:
\end_layout

\begin_layout LyX-Code
\begin_inset listings
lstparams "basicstyle={\ttfamily}"
inline false
status open

\begin_layout Plain Layout

* RF=1K Gain should be 100
\end_layout

\begin_layout Plain Layout

* Check open-loop gain and phase margin
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
The asterisk in the first column indicates that this line is a comment line.
 Comment lines may be placed anywhere in the circuit description.
 
\end_layout

\begin_layout Subsection
End-of-line comments
\end_layout

\begin_layout Standard
General Form:
\end_layout

\begin_layout LyX-Code
\begin_inset listings
lstparams "basicstyle={\ttfamily}"
inline false
status open

\begin_layout Plain Layout

<any command> $ <any comment>
\end_layout

\end_inset


\end_layout

\begin_layout Standard
Examples:
\end_layout

\begin_layout LyX-Code
\begin_inset listings
lstparams "basicstyle={\ttfamily}"
inline false
status open

\begin_layout Plain Layout

RF2=1K $ Gain should be 100
\end_layout

\begin_layout Plain Layout

C1=10p $ Check open-loop gain and phase margin
\end_layout

\begin_layout Plain Layout

.param n1=1 //new value
\end_layout

\end_inset


\end_layout

\begin_layout Standard
ngspice supports comments that begin with double characters `
\family typewriter
$ 
\family default
' (dollar plus space) or `
\family typewriter
//
\family default
'.
 For readability you should precede each comment character with a space.
 ngspice will accept the single character `
\family typewriter
$
\family default
'.
 
\end_layout

\begin_layout Standard
Please note that in 
\family typewriter
.control
\family default
 sections the `
\family typewriter
;
\family default
' character means `continuation' and can be used to put more than one statement
 on a line.
\end_layout

\begin_layout Section
\begin_inset CommandInset label
LatexCommand label
name "sec:Device-Models"

\end_inset

.MODEL Device Models
\end_layout

\begin_layout Standard
\begin_inset Box Frameless
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "100col%"
special "none"
height "1in"
height_special "totalheight"
thickness "0.4pt"
separation "3pt"
shadowsize "4pt"
framecolor "black"
backgroundcolor "none"
status open

\begin_layout Plain Layout
General form:
\end_layout

\begin_layout LyX-Code
\begin_inset listings
inline false
status open

\begin_layout Plain Layout

.model mname type(pname1=pval1 pname2=pval2 ...
 )
\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout
Examples:
\end_layout

\begin_layout LyX-Code
\begin_inset listings
inline false
status open

\begin_layout Plain Layout

.model MOD1 npn (bf=50 is=1e-13 vbf=50)
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
Most simple circuit elements typically require only a few parameter values.
 However, some devices (semiconductor devices in particular) that are included
 in ngspice require many parameter values.
 Often, many devices in a circuit are defined by the same set of device
 model parameters.
 For these reasons, a set of device model parameters is defined on a separate
 
\family typewriter
.model
\family default
 line and assigned a unique model name.
 The device element lines in ngspice then refer to the model name.
\end_layout

\begin_layout Standard
For these more complex device types, each device element line contains the
 device name, the nodes the device is connected to, and the device model
 name.
 In addition, other optional parameters may be specified for some devices:
 geometric factors and an initial condition (see the following section on
 Transistors (
\begin_inset CommandInset ref
LatexCommand ref
reference "cha:BJTs"

\end_inset

 to 
\begin_inset CommandInset ref
LatexCommand ref
reference "cha:MOSFETs"

\end_inset

) and Diodes (
\begin_inset CommandInset ref
LatexCommand ref
reference "cha:DIODEs"

\end_inset

) for more details).
 
\family typewriter
mname
\family default
 in the above is the model name, and type is one of the following fifteen
 types:
\end_layout

\begin_layout Standard
\begin_inset Float table
wide false
sideways false
status open

\begin_layout Plain Layout
\align center
\begin_inset Tabular
<lyxtabular version="3" rows="18" columns="2">
<features tabularvalignment="middle">
<column alignment="center" valignment="top">
<column alignment="center" valignment="top">
<row>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Code
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Model Type
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
R
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Semiconductor resistor model
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
C
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Semiconductor capacitor model
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
L
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Inductor model
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
SW
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Voltage controlled switch
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
CSW
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Current controlled switch
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
URC
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Uniform distributed RC model
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
LTRA
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Lossy transmission line model
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
D
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Diode model
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
NPN
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
NPN BJT model
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
PNP
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
PNP BJT model
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
NJF
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
N-channel JFET model
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
PJF
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
P-channel JFET model
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
NMOS
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
N-channel MOSFET model
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
PMOS
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
P-channel MOSFET model
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
NMF
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
N-channel MESFET model
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
PMF
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
P-channel MESFET model
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
VDMOS
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Power MOS model
\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
Ngspice model types
\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout

\end_layout

\end_inset


\end_layout

\begin_layout Standard
Parameter values are defined by appending the parameter name followed by
 an equal sign and the parameter value.
 Model parameters that are not given a value are assigned the default values
 given below for each model type.
 Models are listed in the section on each device along with the description
 of device element lines.
 Model parameters and their default values are given in Chapt.
 
\begin_inset CommandInset ref
LatexCommand ref
reference "cha:Model-and-Device"

\end_inset

.
\end_layout

\begin_layout Section
.SUBCKT Subcircuits
\end_layout

\begin_layout Standard
A subcircuit that consists of ngspice elements can be defined and referenced
 in a fashion similar to device models.
 Subcircuits are the way ngspice implements hierarchical modeling, but this
 is not entirely true because each subcircuit instance is flattened during
 parsing, and thus ngspice is not a hierarchical simulator.
\end_layout

\begin_layout Standard
The subcircuit is defined in the input deck by a grouping of element cards
 delimited by the 
\family typewriter
.subckt
\family default
 and the 
\family typewriter
.ends
\family default
 cards (or the keywords defined by the 
\family typewriter
substart
\family default
 and 
\family typewriter
subend
\family default
 options (see 
\begin_inset CommandInset ref
LatexCommand ref
reference "sec:Variables"

\end_inset

)); the program then automatically inserts the defined group of elements
 wherever the subcircuit is referenced.
 Instances of subcircuits within a larger circuit are defined through the
 use of an instance card that begins with the letter `
\family typewriter
X
\family default
'.
 A complete example of all three of these cards follows:
\end_layout

\begin_layout Standard
\begin_inset Box Frameless
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "100col%"
special "none"
height "1in"
height_special "totalheight"
thickness "0.4pt"
separation "3pt"
shadowsize "4pt"
framecolor "black"
backgroundcolor "none"
status open

\begin_layout Plain Layout
Example:
\end_layout

\begin_layout LyX-Code
\begin_inset listings
inline false
status open

\begin_layout LyX-Code

* The following is the instance card: 
\end_layout

\begin_layout LyX-Code

*
\end_layout

\begin_layout LyX-Code

xdiv1 10 7 0 vdivide
\end_layout

\begin_layout LyX-Code

\end_layout

\begin_layout LyX-Code

* The following are the subcircuit definition cards: 
\end_layout

\begin_layout LyX-Code

*
\end_layout

\begin_layout LyX-Code

.subckt vdivide 1 2 3 
\end_layout

\begin_layout LyX-Code

r1 1 2 10K 
\end_layout

\begin_layout LyX-Code

r2 2 3 5K 
\end_layout

\begin_layout LyX-Code

.ends
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
The above specifies a subcircuit with ports numbered `1', `2' and `3':
\end_layout

\begin_layout Itemize
Resistor `R1' is connected from port `1' to port `2', and has value 10 kOhms.
 
\end_layout

\begin_layout Itemize
Resistor `R2' is connected from port `2' to port `3', and has value 5 kOhms.
 
\end_layout

\begin_layout Standard
The instance card, when placed in an ngspice deck, will cause subcircuit
 port `1' to be equated to circuit node `10', while port `2' will be equated
 to node `7' and port `3' will equated to node `
\family typewriter
\series medium
0
\family default
\series default
'.
\end_layout

\begin_layout Standard
There is no limit on the size or complexity of subcircuits, and subcircuits
 may contain other subcircuits.
 An example of subcircuit usage is given in Chapt.
 
\begin_inset CommandInset ref
LatexCommand ref
reference "sec:MOS-Four-Bit"

\end_inset

.
\end_layout

\begin_layout Subsection
.SUBCKT Line
\end_layout

\begin_layout Standard
\begin_inset Box Frameless
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "100col%"
special "none"
height "1in"
height_special "totalheight"
thickness "0.4pt"
separation "3pt"
shadowsize "4pt"
framecolor "black"
backgroundcolor "none"
status open

\begin_layout Plain Layout
General form:
\end_layout

\begin_layout LyX-Code
\begin_inset listings
inline false
status open

\begin_layout LyX-Code

.SUBCKT subnam N1 <N2 N3 ...>
\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout
Examples:
\end_layout

\begin_layout LyX-Code
\begin_inset listings
inline false
status open

\begin_layout LyX-Code

.SUBCKT OPAMP 1 2 3 4
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
A circuit definition is begun with a 
\family typewriter
.SUBCKT
\family default
 line.
 
\family sans
subnam
\family default
 is the subcircuit name, and N1, N2, ...
 are the external nodes, which cannot be zero.
 The group of element lines that immediately follow the 
\family typewriter
.SUBCKT
\family default
 line define the subcircuit.
 The last line in a subcircuit definition is the 
\family typewriter
.ENDS
\family default
 line (see below).
 Control lines may not appear within a subcircuit definition; however, subcircui
t definitions may contain anything else, including other subcircuit definitions,
 device models, and subcircuit calls (see below).
 Note that any device models or subcircuit definitions included as part
 of a subcircuit definition are strictly local (i.e., such models and definitions
 are not known outside the subcircuit definition).
 Also, any element nodes not included on the 
\family typewriter
.SUBCKT
\family default
 line are strictly local, with the exception of 0 (ground) that is always
 global.
 If you use parameters, the 
\family typewriter
.SUBCKT
\family default
 line will be extended (see 
\begin_inset CommandInset ref
LatexCommand ref
reference "subsec:Subcircuit-parameters"

\end_inset

).
\end_layout

\begin_layout Subsection
.ENDS Line
\end_layout

\begin_layout Standard
\begin_inset Box Frameless
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "100col%"
special "none"
height "1in"
height_special "totalheight"
thickness "0.4pt"
separation "3pt"
shadowsize "4pt"
framecolor "black"
backgroundcolor "none"
status open

\begin_layout Plain Layout
General form:
\end_layout

\begin_layout LyX-Code
\begin_inset listings
inline false
status open

\begin_layout LyX-Code

.ENDS <SUBNAM>
\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout
Examples:
\end_layout

\begin_layout LyX-Code
\begin_inset listings
inline false
status open

\begin_layout LyX-Code

.ENDS OPAMP
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
The 
\family typewriter
.ENDS
\family default
 line must be the last one for any subcircuit definition.
 The subcircuit name, if included, indicates which subcircuit definition
 is being terminated; if omitted, all subcircuits being defined are terminated.
 The name is needed only when nested subcircuit definitions are being made.
\end_layout

\begin_layout Subsection
\begin_inset CommandInset label
LatexCommand label
name "subsec:Subcircuit-Calls"

\end_inset

Subcircuit Calls
\end_layout

\begin_layout Standard
\begin_inset Box Frameless
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "100col%"
special "none"
height "1in"
height_special "totalheight"
thickness "0.4pt"
separation "3pt"
shadowsize "4pt"
framecolor "black"
backgroundcolor "none"
status open

\begin_layout Plain Layout
General form:
\end_layout

\begin_layout LyX-Code
\begin_inset listings
inline false
status open

\begin_layout LyX-Code

XYYYYYYY N1 <N2 N3 ...> SUBNAM
\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout
Examples:
\end_layout

\begin_layout LyX-Code
\begin_inset listings
inline false
status open

\begin_layout LyX-Code

X1 2 4 17 3 1 MULTI
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
Subcircuits are used in ngspice by specifying pseudo-elements beginning
 with the letter X, followed by the circuit nodes to be used in expanding
 the subcircuit.
 If you use parameters, the subcircuit call will be modified (see 
\begin_inset CommandInset ref
LatexCommand ref
reference "subsec:Subcircuit-parameters"

\end_inset

).
\end_layout

\begin_layout Section
.GLOBAL
\end_layout

\begin_layout Standard
\begin_inset Box Frameless
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "100col%"
special "none"
height "1in"
height_special "totalheight"
thickness "0.4pt"
separation "3pt"
shadowsize "4pt"
framecolor "black"
backgroundcolor "none"
status open

\begin_layout Plain Layout
General form:
\end_layout

\begin_layout LyX-Code
\begin_inset listings
inline false
status open

\begin_layout LyX-Code

.GLOBAL nodename
\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout
Examples:
\end_layout

\begin_layout LyX-Code
\begin_inset listings
inline false
status open

\begin_layout LyX-Code

.GLOBAL gnd vcc
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
Nodes defined in the .GLOBAL statement are available to all circuit and subcircui
t blocks independently from any circuit hierarchy.
 After parsing the circuit, these nodes are accessible from top level.
\end_layout

\begin_layout Section
\begin_inset CommandInset label
LatexCommand label
name "sec:.INCLUDE"

\end_inset

.INCLUDE
\end_layout

\begin_layout Standard
\begin_inset Box Frameless
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "100col%"
special "none"
height "1in"
height_special "totalheight"
thickness "0.4pt"
separation "3pt"
shadowsize "4pt"
framecolor "black"
backgroundcolor "none"
status open

\begin_layout Plain Layout
General form:
\end_layout

\begin_layout LyX-Code
\begin_inset listings
inline false
status open

\begin_layout LyX-Code

.INCLUDE filename
\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout
Examples:
\end_layout

\begin_layout LyX-Code
\begin_inset listings
inline false
status open

\begin_layout LyX-Code

.INCLUDE /users/spice/common/bsim3-param.mod
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
Frequently, portions of circuit descriptions will be reused in several input
 files, particularly with common models and subcircuits.
 In any ngspice input file, the 
\family typewriter
.INCLUDE
\family default
 line may be used to copy some other file as if that second file appeared
 in place of the
\family typewriter
 .INCLUDE
\family default
 line in the original file.
\end_layout

\begin_layout Standard
There is no restriction on the file name imposed by ngspice beyond those
 imposed by the local operating system.
\end_layout

\begin_layout Section
\begin_inset CommandInset label
LatexCommand label
name "sec:.LIB"

\end_inset

.LIB
\end_layout

\begin_layout Standard
\begin_inset Box Frameless
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "100col%"
special "none"
height "1in"
height_special "totalheight"
thickness "0.4pt"
separation "3pt"
shadowsize "4pt"
framecolor "black"
backgroundcolor "none"
status open

\begin_layout Plain Layout
General form:
\end_layout

\begin_layout LyX-Code
\begin_inset listings
inline false
status open

\begin_layout LyX-Code

.LIB filename libname
\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout
Examples:
\end_layout

\begin_layout LyX-Code
\begin_inset listings
inline false
status open

\begin_layout LyX-Code

.LIB /users/spice/common/mosfets.lib mos1
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
The 
\family typewriter
.LIB
\family default
 statement allows to include library descriptions into the input file.
 Inside the *.lib file a library 
\series bold
libname
\series default
 will be selected.
 The statements of each library inside the *.lib file are enclosed in 
\family typewriter
.LIB libname <...> .ENDL
\family default
 statements.
\end_layout

\begin_layout Standard
If the compatibility mode (
\begin_inset CommandInset ref
LatexCommand ref
reference "sec:Compatibility"

\end_inset

) is set to 
\family typewriter
'ps'
\family default
 by 
\family typewriter
set ngbehavior=ps
\family default
 (
\begin_inset CommandInset ref
LatexCommand ref
reference "sec:Variables"

\end_inset

) in 
\family sans
spinit
\family default
 (
\begin_inset CommandInset ref
LatexCommand ref
reference "sec:Standard-configuration-file"

\end_inset

) or 
\family sans
.spiceinit
\family default
 (
\begin_inset CommandInset ref
LatexCommand ref
reference "sec:User-defined-configuration"

\end_inset

), then a simplified syntax 
\family typewriter
.LIB filename
\family default
 is available: a warning is issued and 
\family sans
filename
\family default
 is simply included as described in Chapt.
 
\begin_inset CommandInset ref
LatexCommand ref
reference "sec:.INCLUDE"

\end_inset

.
\end_layout

\begin_layout Section
.PARAM Parametric netlists
\end_layout

\begin_layout Standard
Ngspice allows for the definition of parametric attributes in the netlists.
 This is an enhancement of the ngspice front-end that adds arithmetic functional
ity to the circuit description language.
\end_layout

\begin_layout Subsection
\begin_inset CommandInset label
LatexCommand label
name "subsec:.param-line"

\end_inset

.param line
\end_layout

\begin_layout Standard
\begin_inset Box Frameless
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "100col%"
special "none"
height "1in"
height_special "totalheight"
thickness "0.4pt"
separation "3pt"
shadowsize "4pt"
framecolor "black"
backgroundcolor "none"
status open

\begin_layout Plain Layout
General form:
\end_layout

\begin_layout LyX-Code
\begin_inset listings
inline false
status open

\begin_layout LyX-Code

.param <ident> = <expr>  <ident> = <expr> ...
\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout
Examples:
\end_layout

\begin_layout LyX-Code
\begin_inset listings
inline false
status open

\begin_layout LyX-Code

.param pippo=5
\end_layout

\begin_layout LyX-Code

.param po=6 pp=7.8 pap={AGAUSS(pippo, 1, 1.67)}
\end_layout

\begin_layout LyX-Code

.param pippp={pippo + pp}
\end_layout

\begin_layout LyX-Code

.param p={pp}
\end_layout

\begin_layout LyX-Code

.param pop='pp+p'
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
This line assigns numerical values to identifiers.
 More than one assignment per line is possible using a separating space.
 Parameter identifier names must begin with an alphabetic character.
 The other characters must be either alphabetic, a number, or 
\family typewriter
! # $ % [ ]
\family default
 
\family typewriter
_
\family default
 as special characters.
 The variables
\series bold
 time
\series default
, 
\series bold
temper
\series default
, and 
\series bold
hertz
\series default
 (see 
\begin_inset CommandInset ref
LatexCommand ref
reference "subsec:Syntax-and-usage"

\end_inset

) are not valid identifier names.
 Other restrictions on naming conventions apply as well, see 
\begin_inset CommandInset ref
LatexCommand ref
reference "subsec:Reserved-words"

\end_inset

.
\end_layout

\begin_layout Standard
The 
\family typewriter
.param
\family default
 lines inside subcircuits are copied per call, like any other line.
 All assignments are executed sequentially through the expanded circuit.
 Before its first use, a parameter name must have been assigned a value.
 Expressions defining a parameter should be put within braces 
\family typewriter
{p+p2}
\family default
, or alternatively within single quotes 
\family typewriter
'AGAUSS(pippo, 1, 1.67)'
\family default
.
 An assignment cannot be self-referential, something like 
\family typewriter
.param pip = 'pip+3'
\family default
 will not work.
\end_layout

\begin_layout Standard
The current ngspice version does not always need quotes or braces in expressions
, especially when spaces are used sparingly.
 However, it is recommended to do so, as the following examples demonstrate.
\end_layout

\begin_layout Standard
\begin_inset Box Frameless
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "100col%"
special "none"
height "1in"
height_special "totalheight"
thickness "0.4pt"
separation "3pt"
shadowsize "4pt"
framecolor "black"
backgroundcolor "none"
status open

\begin_layout LyX-Code
\begin_inset listings
inline false
status open

\begin_layout LyX-Code

.param a = 123 * 3    b = sqrt(9) $ doesn't work, a <= 123 
\end_layout

\begin_layout LyX-Code

.param a = '123 * 3'  b = sqrt(9) $ ok.
\end_layout

\begin_layout LyX-Code

.param c = a + 123   $ won't work
\end_layout

\begin_layout LyX-Code

.param c = 'a + 123' $ ok.
 
\end_layout

\begin_layout LyX-Code

.param c = a+123     $ ok.
 
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Subsection
Brace expressions in circuit elements:
\end_layout

\begin_layout Standard
\begin_inset Box Frameless
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "100col%"
special "none"
height "1in"
height_special "totalheight"
thickness "0.4pt"
separation "3pt"
shadowsize "4pt"
framecolor "black"
backgroundcolor "none"
status open

\begin_layout Plain Layout
General form:
\end_layout

\begin_layout LyX-Code
\begin_inset listings
inline false
status open

\begin_layout LyX-Code

{ <expr> }
\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout
Examples:
\end_layout

\end_inset


\end_layout

\begin_layout Standard
These are allowed in 
\family typewriter
.model
\family default
 lines and in device lines.
 A SPICE number is a floating point number with an optional scaling suffix,
 immediately glued to the numeric tokens (see Chapt.
 
\begin_inset CommandInset ref
LatexCommand ref
reference "subsec:Syntax-of-expressions"

\end_inset

).
 Brace expressions ({..}) cannot be used to parametrize node names or parts
 of names.
 All identifiers used within an 
\family typewriter
<expr>
\family default
 must have known values at the time when the line is evaluated, else an
 error is flagged.
\end_layout

\begin_layout Subsection
\begin_inset CommandInset label
LatexCommand label
name "subsec:Subcircuit-parameters"

\end_inset

Subcircuit parameters
\end_layout

\begin_layout Standard
\begin_inset Box Frameless
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "100col%"
special "none"
height "1in"
height_special "totalheight"
thickness "0.4pt"
separation "3pt"
shadowsize "4pt"
framecolor "black"
backgroundcolor "none"
status open

\begin_layout Plain Layout
General form:
\end_layout

\begin_layout LyX-Code
\begin_inset listings
inline false
status open

\begin_layout LyX-Code

.subckt <identn> node node ...
  <ident>=<value> <ident>=<value> ...
\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout
Examples:
\end_layout

\begin_layout LyX-Code
\begin_inset listings
inline false
status open

\begin_layout LyX-Code

.subckt myfilter in out rval=100k cval=100nF
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard

\family typewriter
\series bold
<identn>
\family default
\series default
 is the name of the subcircuit given by the user.

\family typewriter
\series bold
 node
\family default
\series default
 is an integer number or an identifier, for one of the external nodes.
 The first 
\family typewriter
\series bold
<ident>=<value>
\family default
\series default
 introduces an optional section of the line.
 Each 
\family typewriter
\series bold
<ident>
\family default
\series default
 is a formal parameter, and each 
\family typewriter
\series bold
<value>
\family default
\series default
 is either a SPICE number or a brace expression.
 Inside the 
\family typewriter
.subckt
\family default
 ...
 
\family typewriter
.ends
\family default
 context, each formal parameter may be used like any identifier that was
 defined on a 
\family typewriter
.param
\family default
 control line.
 The 
\family typewriter
\series bold
<value>
\family default
\series default
 parts are supposed to be default values of the parameters.
 However, in the current version of , they are not used and each invocation
 of the subcircuit must supply the _exact_ number of actual parameters.
\end_layout

\begin_layout Standard
The syntax of a subcircuit call (invocation) is:
\end_layout

\begin_layout Standard
\begin_inset Box Frameless
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "100col%"
special "none"
height "1in"
height_special "totalheight"
thickness "0.4pt"
separation "3pt"
shadowsize "4pt"
framecolor "black"
backgroundcolor "none"
status open

\begin_layout Plain Layout
General form:
\end_layout

\begin_layout LyX-Code
\begin_inset listings
inline false
status open

\begin_layout LyX-Code

X<name> node node ...
 <identn> <ident>=<value> <ident>=<value> ...
\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout
Examples:
\end_layout

\begin_layout LyX-Code
\begin_inset listings
inline false
status open

\begin_layout LyX-Code

X1 input output myfilter rval=1k cval=1n
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard

\end_layout

\begin_layout Standard
Here 
\family typewriter
\series bold
<name>
\family default
\series default
 is the symbolic name given to that instance of the subcircuit, 
\family typewriter
\series bold
<identn>
\family default
\series default
 is the name of a subcircuit defined beforehand.
 
\family typewriter
\series bold
node node ...

\family default
\series default
 is the list of actual nodes where the subcircuit is connected.
 
\family typewriter
\series bold
<value>
\family default
\series default
 is either a SPICE number or a brace expression 
\family typewriter
\series bold
{ <expr> }
\family default
\series default
 .
 The sequence of 
\family typewriter
\series bold
<value>
\family default
\series default
 items on the 
\family typewriter
X
\family default
 line must exactly match the number and the order of formal parameters of
 the subcircuit.
\end_layout

\begin_layout Standard
\begin_inset Box Frameless
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "100col%"
special "none"
height "1in"
height_special "totalheight"
thickness "0.4pt"
separation "3pt"
shadowsize "4pt"
framecolor "black"
backgroundcolor "none"
status open

\begin_layout Plain Layout
Subcircuit example with parameters:
\end_layout

\begin_layout LyX-Code
\begin_inset listings
inline false
status open

\begin_layout LyX-Code

* Param-example 
\end_layout

\begin_layout LyX-Code

.param amplitude= 1V
\end_layout

\begin_layout LyX-Code

*
\end_layout

\begin_layout LyX-Code

.subckt myfilter in out rval=100k  cval=100nF 
\end_layout

\begin_layout LyX-Code

Ra in p1   {2*rval} 
\end_layout

\begin_layout LyX-Code

Rb p1 out  {2*rval} 
\end_layout

\begin_layout LyX-Code

C1 p1 0    {2*cval} 
\end_layout

\begin_layout LyX-Code

Ca in p2   {cval} 
\end_layout

\begin_layout LyX-Code

Cb p2 out  {cval} 
\end_layout

\begin_layout LyX-Code

R1 p2 0    {rval} 
\end_layout

\begin_layout LyX-Code

.ends myfilter
\end_layout

\begin_layout LyX-Code

*
\end_layout

\begin_layout LyX-Code

X1 input output myfilter rval=1k cval=1n 
\end_layout

\begin_layout LyX-Code

V1 input 0 AC {amplitude}
\end_layout

\begin_layout LyX-Code

.end
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Subsection
Symbol scope
\end_layout

\begin_layout Standard

\emph on
All subcircuit and model names are considered global and must be unique.

\emph default
 The 
\family typewriter
.param
\family default
 symbols that are defined outside of any 
\family typewriter
.subckt
\family default
 ...
 
\family typewriter
.ends
\family default
 section are global.
 Inside such a section, the pertaining 
\family typewriter
params:
\family default
 symbols and any 
\family typewriter
.param
\family default
 assignments are considered local: they mask any global identical names,
 until the 
\family typewriter
.ends
\family default
 line is encountered.
 You cannot reassign to a global number inside a 
\family typewriter
.subckt
\family default
, a local copy is created instead.
 Scope nesting works up to a level of 10.
 For example, if the main circuit calls A that has a formal parameter xx,
 A calls B that has a param.
 xx, and B calls C that also has a formal param.
 xx, there will be three versions of `xx' in the symbol table but only the
 most local one - belonging to C - is visible.
 
\end_layout

\begin_layout Subsection
\begin_inset CommandInset label
LatexCommand label
name "subsec:Syntax-of-expressions"

\end_inset

Syntax of expressions
\end_layout

\begin_layout LyX-Code
<expr> ( optional parts within [...] )
\end_layout

\begin_layout Standard
\begin_inset Box Frameless
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "100col%"
special "none"
height "1in"
height_special "totalheight"
thickness "0.4pt"
separation "3pt"
shadowsize "4pt"
framecolor "black"
backgroundcolor "none"
status open

\begin_layout Plain Layout
An expression may be one of:
\end_layout

\begin_layout LyX-Code
\begin_inset listings
inline false
status open

\begin_layout LyX-Code

<atom> where <atom> is either a spice number or an identifier 
\end_layout

\begin_layout LyX-Code

<unary-operator> <atom> 
\end_layout

\begin_layout LyX-Code

<function-name> ( <expr> [ , <expr> ...] ) 
\end_layout

\begin_layout LyX-Code

<atom> <binary-operator> <expr> 
\end_layout

\begin_layout LyX-Code

( <expr> )
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
As expected, atoms, built-in function calls and stuff within parentheses
 are evaluated before the other operators.
 The operators are evaluated following a list of precedence close to the
 one of the C language.
 For equal precedence binary ops, evaluation goes left to right.
 Functions operate on real values only!
\end_layout

\begin_layout Standard
\begin_inset Tabular
<lyxtabular version="3" rows="19" columns="4">
<features tabularvalignment="middle">
<column alignment="center" valignment="top">
<column alignment="center" valignment="top">
<column alignment="center" valignment="top">
<column alignment="center" valignment="top">
<row>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Operator
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Alias
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Precedence
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Description
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\family typewriter
-
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
1
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
unary -
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\family typewriter
!
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
1
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
unary not
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\family typewriter
**
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\family typewriter
^
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
2
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
power, like pwr
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\family typewriter
*
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
3
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
multiply
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\family typewriter
/
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
3
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
divide
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\family typewriter
%
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
3
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
modulo
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\family typewriter

\backslash

\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
3
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
integer divide
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\family typewriter
+
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
4
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
add
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\family typewriter
-
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
4
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
subtract
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\family typewriter
==
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
5
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
equality
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\family typewriter
!=
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\family typewriter
<>
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
5
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
non-equal
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\family typewriter
<=
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
5
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
less or equal
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\family typewriter
>=
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
5
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
greater or equal
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\family typewriter
<
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
5
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
less than
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\family typewriter
>
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
5
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
greater than
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\family typewriter
&&
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
6
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
boolean and
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\family typewriter
||
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
7
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
boolean or
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\family typewriter
c?x:y
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
8
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
ternary operator
\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\end_layout

\begin_layout Standard
The number zero is used to represent boolean False.
 Any other number represents boolean True.
 The result of logical operators is 1 or 0.
 An example input file is shown below:
\end_layout

\begin_layout Standard
\begin_inset Box Frameless
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "100col%"
special "none"
height "1in"
height_special "totalheight"
thickness "0.4pt"
separation "3pt"
shadowsize "4pt"
framecolor "black"
backgroundcolor "none"
status open

\begin_layout Plain Layout
Example input file with logical operators:
\end_layout

\begin_layout LyX-Code
\begin_inset listings
inline false
status open

\begin_layout LyX-Code

* Logical operators
\end_layout

\begin_layout LyX-Code

\end_layout

\begin_layout LyX-Code

v1or   1 0  {1 || 0}
\end_layout

\begin_layout LyX-Code

v1and  2 0  {1 && 0}
\end_layout

\begin_layout LyX-Code

v1not  3 0  {! 1}
\end_layout

\begin_layout LyX-Code

v1mod  4 0  {5 % 3}
\end_layout

\begin_layout LyX-Code

v1div  5 0  {5 
\backslash
 3}
\end_layout

\begin_layout LyX-Code

v0not  6 0  {! 0}  
\end_layout

\begin_layout LyX-Code

\end_layout

\begin_layout LyX-Code

.control
\end_layout

\begin_layout LyX-Code

op
\end_layout

\begin_layout LyX-Code

print allv
\end_layout

\begin_layout LyX-Code

.endc
\end_layout

\begin_layout LyX-Code

\end_layout

\begin_layout LyX-Code

.end
\end_layout

\begin_layout Plain Layout

\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Tabular
<lyxtabular version="3" rows="25" columns="2">
<features tabularvalignment="middle">
<column alignment="center" valignment="top">
<column alignment="center" valignment="top" width="10cm">
<row>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Built-in function
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Notes
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
sqrt(x)
\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\family typewriter
y = sqrt(x)
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
sin(x), cos(x), tan(x)
\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
sinh(x), cosh(x), tanh(x)
\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
asin(x), acos(x), atan(x)
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
asinh(x), acosh(x), atanh(x)
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
arctan(x)
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\family typewriter
atan(x)
\family default
, kept for compatibility 
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
exp(x)
\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
ln(x), log(x)
\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
abs(x)
\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
nint(x)
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Nearest integer, half integers towards even
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
int(x)
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Nearest integer rounded towards 0
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
floor(x)
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Nearest integer rounded towards 
\family typewriter
-∞
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
ceil(x)
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Nearest integer rounded towards 
\family typewriter
+∞
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
pow(x,y)
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
x raised to the power of y (pow from C runtime library)
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
pwr(x,y)
\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\family typewriter
pow(fabs(x), y)
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
min(x, y)
\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
max(x, y)
\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
sgn(x)
\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\family typewriter
1.0 for x > 0, 0.0 for x == 0, -1.0 for x < 0
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
ternary_fcn(x, y, z)
\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\family typewriter
x ? y : z
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
gauss(nom, rvar, sigma)
\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
nominal value plus variation drawn from Gaussian distribution with mean
 0 and standard deviation rvar (relative to nominal), divided by sigma
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
agauss(nom, avar, sigma)
\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
nominal value plus variation drawn from Gaussian distribution with mean
 0 and standard deviation avar (absolute), divided by sigma
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
unif(nom, rvar)
\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
nominal value plus relative variation (to nominal) uniformly distributed
 between +/-rvar
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
aunif(nom, avar)
\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
nominal value plus absolute variation uniformly distributed between +/-avar
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
limit(nom, avar)
\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
nominal value +/-avar, depending on random number in [-1, 1[ being 
\family typewriter
> 0
\family default
 or 
\family typewriter
< 0
\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\end_layout

\begin_layout Standard
The scaling suffixes (any decorative alphanumeric string may follow):
\end_layout

\begin_layout Standard
\begin_inset Tabular
<lyxtabular version="3" rows="9" columns="2">
<features tabularvalignment="middle">
<column alignment="center" valignment="top">
<column alignment="center" valignment="top">
<row>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
suffix
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
value
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
g
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
1e9
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
meg
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
1e6
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
k
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
1e3
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
m
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
1e-3
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
u
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
1e-6
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
n
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
1e-9
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
p
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
1e-12
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
f
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
1e-15
\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\end_layout

\begin_layout Standard
Note: there are intentional redundancies in expression syntax, e.g.
 
\family typewriter
x^y
\family default
 , 
\family typewriter
x**y
\family default
 and 
\family typewriter
pwr(x,y)
\family default
 all have nearly the same result.
 
\end_layout

\begin_layout Subsection
\begin_inset CommandInset label
LatexCommand label
name "subsec:Reserved-words"

\end_inset

Reserved words 
\end_layout

\begin_layout Standard
In addition to the above function names and to the verbose operators ( 
\family typewriter
not and or div mod
\family default
 ), other words are reserved and cannot be used as parameter names: 
\family typewriter
or
\family default
, 
\family typewriter
defined
\family default
, 
\family typewriter
sqr
\family default
, 
\family typewriter
sqrt
\family default
, 
\family typewriter
sin
\family default
, 
\family typewriter
cos
\family default
, 
\family typewriter
exp
\family default
, 
\family typewriter
ln
\family default
, 
\family typewriter
log, log10, arctan
\family default
, 
\family typewriter
abs
\family default
, 
\family typewriter
pwr
\family default
, 
\family typewriter
time
\family default
, 
\family typewriter
temper
\family default
, 
\family typewriter
hertz
\family default
.
 
\end_layout

\begin_layout Subsection
A word of caution on the three ngspice expression parsers
\end_layout

\begin_layout Standard
The historical parameter notation using 
\family typewriter
&
\family default
 as the first character of a line as equivalence to 
\family typewriter
.param
\family default
.
 is deprecated and will be removed in a coming release.
\end_layout

\begin_layout Standard
Confusion may arise in ngspice because of its multiple numerical expression
 features.
 The 
\family typewriter
.param
\family default
 lines and the brace expressions (see Chapt.
 
\begin_inset CommandInset ref
LatexCommand ref
reference "sec:func"

\end_inset

) are evaluated in the front-end, that is, just after the subcircuit expansion.
 (Technically, the X lines are kept as comments in the expanded circuit
 so that the actual parameters can be correctly substituted).
 Therefore, after the netlist expansion and before the internal data setup,
 all number attributes in the circuit are known constants.
 However, there are circuit elements in Spice that accept arithmetic expressions
 
\emph on
not
\emph default
 evaluated at this point, but only later during circuit analysis.
 These are the arbitrary current and voltage sources (B-sources, 
\begin_inset CommandInset ref
LatexCommand ref
reference "sec:Non-linear-Dependent-Sources"

\end_inset

), as well as E- and G-sources and R-, L-, or C-devices.
 The syntactic difference is that `compile-time' expressions are within
 braces, but `run-time' expressions have no braces.
 To make things more complicated, the back-end ngspice scripting language
 accepts arithmetic/logic expressions that operate only on its own scalar
 or vector data sets (
\begin_inset CommandInset ref
LatexCommand ref
reference "sec:Expressions,-Functions,-and"

\end_inset

).
 Please see Chapt.
 
\begin_inset CommandInset ref
LatexCommand ref
reference "sec:Parameters,-functions,-expressions,"

\end_inset

.
\end_layout

\begin_layout Standard
It would be desirable to have the same expression syntax, operator and function
 set, and precedence rules, for the three contexts mentioned above.
 In the current Numparam implementation, that goal is not achieved.
\end_layout

\begin_layout Section
\begin_inset CommandInset label
LatexCommand label
name "sec:func"

\end_inset

.FUNC
\end_layout

\begin_layout Standard
This keyword defines a function.
 The syntax of the expression is the same as for a 
\family typewriter
.param
\family default
 (
\begin_inset CommandInset ref
LatexCommand ref
reference "subsec:Syntax-of-expressions"

\end_inset

).
 
\end_layout

\begin_layout Standard
\begin_inset Box Frameless
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "100col%"
special "none"
height "1in"
height_special "totalheight"
thickness "0.4pt"
separation "3pt"
shadowsize "4pt"
framecolor "black"
backgroundcolor "none"
status open

\begin_layout Plain Layout
General form:
\end_layout

\begin_layout LyX-Code
\begin_inset listings
inline false
status open

\begin_layout LyX-Code

.func <ident> { <expr> }
\end_layout

\begin_layout LyX-Code

.func <ident> = { <expr> }
\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout
Examples:
\end_layout

\begin_layout LyX-Code
\begin_inset listings
inline false
status open

\begin_layout LyX-Code

.func icos(x) {cos(x) - 1}
\end_layout

\begin_layout LyX-Code

.func f(x,y) {x*y}
\end_layout

\begin_layout LyX-Code

.func foo(a,b) = {a + b}
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard

\family typewriter
.func
\family default
 will initiate a replacement operation.
 After reading the input files, and before parameters are evaluated, all
 occurrences of the 
\family typewriter
icos(x)
\family default
 function will be replaced by 
\family typewriter
cos(x)-1
\family default
.
 All occurrences of
\family typewriter
 f(x,y)
\family default
 will be replaced by 
\family typewriter
x*y
\family default
.
 Function statements may be nested to a depth of t.b.d..
\end_layout

\begin_layout Section
\begin_inset CommandInset label
LatexCommand label
name "sec:.csparam"

\end_inset

.CSPARAM
\end_layout

\begin_layout Standard
Create a constant vector (see 
\begin_inset CommandInset ref
LatexCommand ref
reference "subsec:Vectors"

\end_inset

) from a parameter in 
\family typewriter
plot
\family default
 (
\begin_inset CommandInset ref
LatexCommand ref
reference "sec:Plots"

\end_inset

) 
\family typewriter
const
\family default
.
\end_layout

\begin_layout Standard
\begin_inset Box Frameless
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "100col%"
special "none"
height "1in"
height_special "totalheight"
thickness "0.4pt"
separation "3pt"
shadowsize "4pt"
framecolor "black"
backgroundcolor "none"
status open

\begin_layout Plain Layout
General form:
\end_layout

\begin_layout LyX-Code
\begin_inset listings
inline false
status open

\begin_layout LyX-Code

.csparam <ident> = <expr>
\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout
Examples:
\end_layout

\begin_layout LyX-Code
\begin_inset listings
inline false
status open

\begin_layout LyX-Code

.param pippo=5
\end_layout

\begin_layout LyX-Code

.param pp=6
\end_layout

\begin_layout LyX-Code

.csparam pippp={pippo + pp}
\end_layout

\begin_layout LyX-Code

.param p={pp}
\end_layout

\begin_layout LyX-Code

.csparam pap='pp+p'
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
In the example shown, vectors pippp, and pap are added to the constants
 that already reside in 
\family typewriter
plot
\family default
 
\family typewriter
const
\family default
, having length one and real values.
 These vectors are generated during circuit parsing and thus cannot be changed
 later (same as with ordinary parameters).
 They may be used in ngspice scripts and 
\family typewriter
.control
\family default
 sections (see Chapt.
 
\begin_inset CommandInset ref
LatexCommand ref
reference "chap:Interactive-Interpreter"

\end_inset

).
 
\end_layout

\begin_layout Standard
The use of 
\family typewriter
.csparam
\family default
 is still experimental and has to be tested.
 A simple usage is shown below.
 
\end_layout

\begin_layout LyX-Code
* test csparam
\end_layout

\begin_layout LyX-Code
.param TEMPS = 27
\end_layout

\begin_layout LyX-Code
.csparam newt = {3*TEMPS} 
\end_layout

\begin_layout LyX-Code
.csparam mytemp = '2 + TEMPS'
\end_layout

\begin_layout LyX-Code
.control
\end_layout

\begin_layout LyX-Code
echo $&newt $&mytemp
\end_layout

\begin_layout LyX-Code
.endc
\end_layout

\begin_layout LyX-Code
.end 
\end_layout

\begin_layout Section
\begin_inset CommandInset label
LatexCommand label
name "sec:.temp"

\end_inset

.TEMP
\end_layout

\begin_layout Standard
Sets the circuit temperature in degrees Celsius.
\end_layout

\begin_layout Standard
\begin_inset Box Frameless
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "100col%"
special "none"
height "1in"
height_special "totalheight"
thickness "0.4pt"
separation "3pt"
shadowsize "4pt"
framecolor "black"
backgroundcolor "none"
status open

\begin_layout Plain Layout
General form:
\end_layout

\begin_layout LyX-Code
\begin_inset listings
inline false
status open

\begin_layout LyX-Code

.temp value
\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout
Examples:
\end_layout

\begin_layout LyX-Code
\begin_inset listings
inline false
status open

\begin_layout LyX-Code

.temp 27
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
This card overrides the circuit temperature given in an 
\family typewriter
.option
\family default
 line (
\begin_inset CommandInset ref
LatexCommand ref
reference "subsec:General-Options"

\end_inset

).
\end_layout

\begin_layout Section
.IF Condition-Controlled Netlist
\end_layout

\begin_layout Standard
A simple 
\family typewriter
.IF-.ELSE(IF)
\family default
 block allows condition-controlling of the netlist.
 
\family typewriter
boolean expression
\family default
 is any expression according to Chapt.
 
\begin_inset CommandInset ref
LatexCommand ref
reference "subsec:Syntax-of-expressions"

\end_inset

 that evaluates parameters and returns a boolean 1 or 0.
 The netlist block in between the 
\series bold
.if 
\series default
...
 
\series bold
.endif
\series default
 statements may contain device instances or 
\family typewriter
.model
\family default
 cards that are selected according to the logic condition.
\end_layout

\begin_layout Standard
\begin_inset Box Frameless
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "100col%"
special "none"
height "1in"
height_special "totalheight"
thickness "0.4pt"
separation "3pt"
shadowsize "4pt"
framecolor "black"
backgroundcolor "none"
status open

\begin_layout Plain Layout
General form:
\end_layout

\begin_layout LyX-Code
\begin_inset listings
inline false
status open

\begin_layout LyX-Code

.if(boolean expression)
\end_layout

\begin_layout LyX-Code

...
\end_layout

\begin_layout LyX-Code

.elseif(boolean expression)
\end_layout

\begin_layout LyX-Code

...
\end_layout

\begin_layout LyX-Code

.else
\end_layout

\begin_layout LyX-Code

...
\end_layout

\begin_layout LyX-Code

.endif
\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout
Example 1:
\end_layout

\begin_layout LyX-Code
\begin_inset listings
inline false
status open

\begin_layout LyX-Code

* device instance in IF-ELSE block
\end_layout

\begin_layout LyX-Code

.param ok=0 ok2=1
\end_layout

\begin_layout LyX-Code

\end_layout

\begin_layout LyX-Code

v1 1 0 1
\end_layout

\begin_layout LyX-Code

R1 1 0 2
\end_layout

\begin_layout LyX-Code

\end_layout

\begin_layout LyX-Code

.if (ok && ok2)
\end_layout

\begin_layout LyX-Code

R11 1 0 2
\end_layout

\begin_layout LyX-Code

.else
\end_layout

\begin_layout LyX-Code

R11 1 0 0.5   $ <-- selected
\end_layout

\begin_layout LyX-Code

.endif
\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout
Example 2:
\end_layout

\begin_layout LyX-Code
\begin_inset listings
inline false
status open

\begin_layout LyX-Code

* .model in IF-ELSE block
\end_layout

\begin_layout LyX-Code

.param m0=0 m1=1
\end_layout

\begin_layout LyX-Code

\end_layout

\begin_layout LyX-Code

M1 1 2 3 4 N1 W=1 L=0.5
\end_layout

\begin_layout LyX-Code

\end_layout

\begin_layout LyX-Code

.if(m0==1)
\end_layout

\begin_layout LyX-Code

.model N1 NMOS level=49 Version=3.1
\end_layout

\begin_layout LyX-Code

.elseif(m1==1)
\end_layout

\begin_layout LyX-Code

.model N1 NMOS level=49 Version=3.2.4  $ <-- selected
\end_layout

\begin_layout LyX-Code

.else
\end_layout

\begin_layout LyX-Code

.model N1 NMOS level=49 Version=3.3.0
\end_layout

\begin_layout LyX-Code

.endif
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
Nesting of 
\family typewriter
.IF-.ELSE(IF)-.ENDIF
\family default
 blocks is possible.
 Several 
\family typewriter
.elseif
\family default
 are allowed per block, of course only one 
\family typewriter
.else
\family default
 (please see example
\family sans
 ngspice/tests/regression/misc/if-elseif.cir
\family default
).
 However some restrictions apply, as the following netlist components are
 
\emph on
not
\emph default
 supported within the 
\family typewriter
.IF-.ENDIF
\family default
 block: 
\family typewriter
.SUBCKT
\family default
, 
\family typewriter
.INC
\family default
, 
\family typewriter
.LIB
\family default
, and
\family typewriter
 .PARAM
\family default
.
 
\end_layout

\begin_layout Section
\begin_inset CommandInset label
LatexCommand label
name "sec:Parameters,-functions,-expressions,"

\end_inset

Parameters, functions, expressions, and command scripts
\end_layout

\begin_layout Standard
In ngspice there are several ways to describe functional dependencies.
 In fact there are three independent function parsers, being active before,
 during, and after the simulation.
 So it might be due to have a few words on their interdependence.
\end_layout

\begin_layout Subsection
\begin_inset CommandInset label
LatexCommand label
name "subsec:Parameters"

\end_inset

Parameters
\end_layout

\begin_layout Standard
Parameters (Chapt.
 
\begin_inset CommandInset ref
LatexCommand ref
reference "subsec:.param-line"

\end_inset

) and functions, either defined within the
\family typewriter
 .param
\family default
 statement or with the
\family typewriter
 .func
\family default
 statement (Chapt.
 
\begin_inset CommandInset ref
LatexCommand ref
reference "sec:func"

\end_inset

) are evaluated 
\series bold
before
\series default
 any simulation is started, that is during the setup of the input and the
 circuit.
 Therefore these statements may not contain any simulation output (voltage
 or current vectors), because it is simply not yet available.
 The syntax is described in Chapt.
 
\begin_inset CommandInset ref
LatexCommand ref
reference "subsec:Syntax-of-expressions"

\end_inset

.
 During the circuit setup all functions are evaluated, all parameters are
 replaced by their resulting numerical values.
 Thus it will not be possible to get feedback from a later stage (during
 or after simulation) to change any of the parameters.
\end_layout

\begin_layout Subsection
Nonlinear sources
\end_layout

\begin_layout Standard
During the simulation, the B source (Chapt.
 
\begin_inset CommandInset ref
LatexCommand ref
reference "sec:Non-linear-Dependent-Sources"

\end_inset

) and their associated E and G sources, as well as some devices (R, C, L)
 may contain expressions.
 These expressions may contain parameters from above (evaluated immediately
 upon ngspice start up), numerical data, predefined functions, but also
 node voltages and branch currents resulting from the simulation.
 The source or device values are continuously updated 
\series bold
during
\series default
 the simulation.
 Therefore the sources are powerful tools to define non-linear behavior,
 you may even create new `devices' by yourself.
 Unfortunately the expression syntax (see Chapt.
 
\begin_inset CommandInset ref
LatexCommand ref
reference "sec:B-source-(ASRC)"

\end_inset

) and the predefined functions may deviate from the ones for parameters
 listed in 
\begin_inset CommandInset ref
LatexCommand ref
reference "subsec:.param-line"

\end_inset

.
\end_layout

\begin_layout Subsection
Control commands, Command scripts
\end_layout

\begin_layout Standard
Commands, as described in detail in Chapt.
 
\begin_inset CommandInset ref
LatexCommand ref
reference "sec:Commands"

\end_inset

, may be used interactively, but also as a command script enclosed in 
\family typewriter
.control ...
 .endc
\family default
 lines.
 The scripts may contain expressions (see Chapt.
 
\begin_inset CommandInset ref
LatexCommand ref
reference "sec:Expressions,-Functions,-and"

\end_inset

).
 The expressions may work upon simulation output vectors (of node voltages,
 branch currents), as well as upon predefined or user defined vectors and
 variables, and are invoked 
\series bold
after
\series default
 the simulation.
 Parameters from 
\begin_inset CommandInset ref
LatexCommand ref
reference "subsec:.param-line"

\end_inset

 defined by the 
\family typewriter
.param
\family default
 statement are not allowed in these expressions.
 However you may define such parameters with 
\family typewriter
.csparam
\family default
 (
\begin_inset CommandInset ref
LatexCommand ref
reference "sec:.csparam"

\end_inset

).
 Again the expression syntax (see Chapt.
 
\begin_inset CommandInset ref
LatexCommand ref
reference "sec:Expressions,-Functions,-and"

\end_inset

) will deviate from the one for parameters or B sources listed in 
\begin_inset CommandInset ref
LatexCommand ref
reference "subsec:.param-line"

\end_inset

 and 
\begin_inset CommandInset ref
LatexCommand ref
reference "sec:B-source-(ASRC)"

\end_inset

.
 
\end_layout

\begin_layout Standard
If you want to use parameters from 
\begin_inset CommandInset ref
LatexCommand ref
reference "subsec:.param-line"

\end_inset

 inside your control script, you may use 
\family typewriter
.csparam
\family default
 (
\begin_inset CommandInset ref
LatexCommand ref
reference "sec:.csparam"

\end_inset

) or apply a trick by defining a voltage source with the parameter as its
 value, and then have it available as a vector (e.g.
 after a transient simulation) with a then constant output (the parameter).
 A feedback from here back into parameters (
\begin_inset CommandInset ref
LatexCommand ref
reference "subsec:Parameters"

\end_inset

) is never possible.
 Also you cannot access non-linear sources of the preceding simulation.
 However you may start a first simulation inside your control script, then
 evaluate its output using expressions, change some of the element or model
 parameters with the 
\family typewriter
alter
\family default
 and 
\family typewriter
altermod
\family default
 statements (see Chapt.
 
\begin_inset CommandInset ref
LatexCommand ref
reference "subsec:Alter*:-Change-a"

\end_inset

) and then automatically start a new simulation.
\end_layout

\begin_layout Standard
Expressions and scripting are powerful tools within ngspice, and we will
 enhance the examples given in Chapt.
 
\begin_inset CommandInset ref
LatexCommand ref
reference "cha:Example-Circuits"

\end_inset

 continuously to describe these features.
 
\end_layout


