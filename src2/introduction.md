# Introduction

Ngspice is a general-purpose circuit simulation program for nonlinear and linear analyses.
Circuits may contain resistors, capacitors, inductors, mutual inductors, independent or dependent
voltage and current sources, loss-less and lossy transmission lines, switches, uniform distributed
RC lines, and the five most common semiconductor devices: diodes, BJTs, JFETs, MESFETs,
and MOSFETs.

Some introductory remarks on how to use ngspice may be found in Chapt. 21.

Ngspice is an update of Spice3f5, the last Berkeley’s release of Spice3 simulator family. Ngspice
is being developed to include new features to existing Spice3f5 and to fix its bugs. Improving
a complex software like a circuit simulator is a very hard task and, while some improvements
have been made, most of the work has been done on bug fixing and code refactoring.

Ngspice has built-in models for the semiconductor devices, and the user need specify only the
pertinent model parameter values. There are three models for bipolar junction transistors, all
based on the integral-charge model of Gummel and Poon; however, if the Gummel-Poon param-
eters are not specified, the basic model (BJT) reduces to the simpler Ebers-Moll model. In either
case and in either models, charge storage effects, ohmic resistances, and a current-dependent
output conductance may be included. The second bipolar model BJT2 adds dc current com-
putation in the substrate diode. The third model (VBIC) contains further enhancements for
advanced bipolar devices.

The semiconductor diode model can be used for either junction diodes or Schottky barrier
diodes. There are two models for JFET: the first (JFET) is based on the model of Shichman
and Hodges, the second (JFET2) is based on the Parker-Skellern model. All the original six
MOSFET models are implemented: MOS1 is described by a square-law I-V characteristic,
MOS2 [1] is an analytical model, while MOS3 [1] is a semi-empirical model; MOS6 [2] is a
simple analytic model accurate in the short channel region; MOS9, is a slightly modified Level
3 MOSFET model - not to confuse with Philips level 9; BSIM 1 [3, 4]; BSIM2 [5] are the
old BSIM (Berkeley Short-channel IGFET Model) models. MOS2, MOS3, and BSIM include
second-order effects such as channel-length modulation, subthreshold conduction, scattering-
limited velocity saturation, small-size effects, and charge controlled capacitances. The recent
MOS models for submicron devices are the BSIM3 (Berkeley BSIM3 web page) and BSIM4
(Berkeley BSIM4 web page) models. Silicon-on-insulator MOS transistors are described by the
SOI models from the BSIMSOI family (Berkeley BSIMSOI web page) and the STAG [18] one.
There is partial support for a couple of HFET models and one model for MESA devices.


Ngspice supports mixed-level simulation and provides a direct link between technology param-
eters and circuit performance. A mixed-level circuit and device simulator can provide greater
simulation accuracy than a stand-alone circuit or device simulator by numerically modeling the
critical devices in a circuit. Compact models can be used for all other devices. The mixed-
level extensions to ngspice is CIDER, a mixed-level circuit and device simulator integrated into
ngspice code.
Ngspice supports mixed-signal simulation through the integration of XSPICE code. XSPICE
software, developed as an extension to Spice3C1 by GeorgiaTech, has been enhanced and ported
to ngspice to provide ‘board’ level and mixed-signal simulation.
The XSPICE extension enables pure digital simulation as well.
New devices can be added to ngspice by several means: behavioral B-, E- or G-sources, the
XSPICE code-model interface for C-like device coding, and the ADMS interface based on
Verilog-A and XML.
Finally, numerous small bugs have been discovered and fixed, and the program has been ported
to a wider variety of computing platforms.

