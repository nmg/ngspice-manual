import os
import shutil


def is_heading(line):
    if line[0] == "#" and line[1] != "#" and line.strip() != "#":
        return True
    return False


def is_subheading(line):
    if line[0:2] == "##" and line[2] != "#" and len(line.strip()) > 2:
        return True
    return False


def is_subsubheading(line):
    if line[0:3] == "###" and len(line.strip()) > 3:
        return True
    return False


def begins_chapter(line):
    if line[:8] == "Chapter " and line[8:].strip().isdigit():
        return True
    return False


def line2dirname(line):
    table = line.maketrans(".()/\\:", "______", "'\"`*#\t\n ")
    return line.lower().translate(table).lstrip("_1234567890").strip("_")


def linkify(name, link, order):
    return order * "\t" + f"- [{name}]({link})\n"


def titlify(line):
    line = line.strip(" #\n\t").lstrip("1234567890.").strip(" ")
    line = (
        line.replace("(", "\\(")
        .replace(")", "\\)")
        .replace("[", "\\[")
        .replace("]", "\\]")
    )
    return line


os.mkdir("src")
shutil.copyfile("manual.md", "src/manual.md")
os.chdir("src")
with open("manual.md", "r") as manual, open("SUMMARY.md", "w") as summary:
    chap_name = ""
    subchap_name = ""
    subsubchap_name = ""
    is_preface = True
    chap_read_mode = False
    fp = open("preface.md", "a")
    summary.write(linkify("Preface", "preface.md", 0))
    for num, line in enumerate(manual):
        if begins_chapter(line):
            is_preface = False
            fp.close()
            chap_read_mode = True
        elif chap_read_mode and is_heading(line):
            chap_name = line2dirname(line)
            fp = open(chap_name + ".md", "a")
            summary.write(linkify(titlify(line), chap_name + ".md", 0))
            os.mkdir(chap_name)
            chap_read_mode = False
        if not chap_read_mode:
            if is_subheading(line) and not is_preface:
                fp.close()
                subchap_name = line2dirname(line)
                subchap_dir = os.path.join(chap_name, subchap_name)
                os.mkdir(subchap_dir)
                fp = open(subchap_dir + ".md", "a")
                summary.write(linkify(titlify(line), subchap_dir + ".md", 1))
                line = "# " + titlify(line) + "\n"

            elif is_subsubheading(line) and not is_preface:
                fp.close()
                filename = (
                    os.path.join(chap_name, subchap_name, line2dirname(line))
                    + ".md"
                )
                fp = open(filename, "a")
                summary.write(linkify(titlify(line), filename, 2))
                line = "# " + titlify(line) + "\n"

            fp.write(line)

    fp.close()
