# MISCELLANEOUS

C-shell type quoting with ' and ', and backquote substitution may be
used. Within single quotes, no further substitution (like history
substitution) is done, and within double quotes, the words are kept
together but further substitution is done. Any text between backquotes
is replaced by the result of executing the text as a command to the
shell.

History substitutions, similar to C-shell history substitutions, are
also available - see the C-shell manual page for all of the details. The
characters ~, @{, and @} have the same effects as they do in the
C-Shell, i.e., home directory and alternative expansion. It is possible
to use the wildcard characters \*, ?, \[, and \] also, but only if you
unset noglob first. This makes them rather useless for typing algebraic
expressions, so you should set noglob again after you are done with
wildcard expansion. Note that the pattern \[^abc\] matches all
characters except a, b, and c.

If X is being used, the cursor may be positioned at any point on the
screen when the window is up and characters typed at the keyboard are
added to the window at that point. The window may then be sent to a
printer using the xpr(1) program.

