# Scripts

Expressions, functions, constants, commands, variables, vectors, and
control structures may be assembled into scripts within a .control ...
.endc section of the input file. The script allows to automate any
ngspice task: simulations to perform, output data to analyze, repeat
simulations with modified parameters, assemble output plot vectors. The
ngspice scripting language is not very powerful, but well integrated
into the simulation flow.

The ngspice script input file contains the usual circuit netlist,
modelcards, and the actual script, enclosed in a .control .. .endc
section. Ngspice is started in interactive mode with the input file on
the command line (or sourced later with the **source** command). After
reading the input file the command sequence is immediately processed.
Variables or vectors set by previous commands may be referenced by the
commands following them. Data can be stored, plotted or grouped into new
vectors for either plotting or other means of data evaluation.

The input file may contain only the .control .. .endc section. To notify
ngspice about this (not mandatory), the script may start with
\*ng\_script in the first line.

