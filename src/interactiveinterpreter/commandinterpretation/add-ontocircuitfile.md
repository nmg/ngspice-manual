# Add-on to circuit file

The probably most common way to invoke the commands described in the
following Chapt. [17.5](#sec_Commands) is to add a .control ... .endc
section to the circuit input file (see
[16.4.3](#subsec_Interactive_mode_with)).

Example:

``` listings
.control
pre_set strict_errorhandling
unset ngdebug
*save outputs and specials
save x1.x1.x1.7 V(9) V(10) V(11) V(12) V(13)
run
display
* plot the inputs, use offset to plot on top of each other
plot  v(1) v(2)+4 v(3)+8 v(4)+12 v(5)+16 v(6)+20 v(7)+24 v(8)+28 
* plot the outputs, use offset to plot on top of each other
plot  v(9) v(10)+4 v(11)+8 v(12)+12 v(13)+16
.endc
```

