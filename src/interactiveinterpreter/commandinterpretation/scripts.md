# Scripts

If a word is typed as a command, and there is no built-in command with
that name, the directories in the sourcepath list are searched in order
for a file with the name given by the word. If it is found, it is read
in as a command file (as if it were sourced). Before it is read,
however, the variables **argc** and **argv** are set to the number of
words following the file-name on the command line, and a list of those
words respectively. After the file is finished, these variables are
unset. Note that if a command file calls another, it must save its
**argv** and **argc** since they are altered. Also, command files may
not be re-entrant since there are no local variables. Of course, the
procedures may explicitly manipulate a stack.... This way one can write
scripts analogous to shell scripts for ngnutmeg and ngspice.

Note that for the script to work with ngspice, it must begin with a
blank line (or whatever else, since it is thrown away) and then a line
with .control on it. This is an unfortunate result of the source command
being used for both circuit input and command file execution. Note also
that this allows the user to merely type the name of a circuit file as a
command and it is automatically run. The commands are executed
immediately, without running any analyses that may be specified in the
circuit (to execute the analyses before the script executes, include a
run command in the script).

There are various command scripts installed in
/usr/local/lib/spice/scripts (or whatever the path is on your machine),
and the default sourcepath includes this directory, so you can use these
command files (almost) like built-in commands.

