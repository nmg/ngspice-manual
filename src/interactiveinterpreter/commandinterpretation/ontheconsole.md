# On the console

On the ngspice console window (or into the Windows GUI) you may directly
type in any command from [17.5](#sec_Commands). Within a command
sequence Input/output redirection is available (see Chapt.
[17.8.8](#subsec_Output_redirection) for an example) - the symbols \>,
\>\>, \>&, \>\>&, and \< have the same effects as in the C-shell. This
I/O-redirection is internal to ngspice commands, and should not be mixed
up with the \`external' I/O-redirection offered by the usual shells
(Linux, MSYS etc.), see [17.5.69](#subsec_Shell__Call_the). You may type
multiple commands on one line, separated by semicolons.

