# Introduction

The simulation flow in ngspice (input, simulation, output) may be
controlled by dot commands (see Chapt. [15](#chap_Analyses_and_Output)
and [16.4.1](#subsec_Batch_mode)) in batch mode. There is, however, a
much more powerful control scheme available in ngspice, traditionally
coined \`Interactive Interpreter', but being much more than just that.
In fact there are several ways to use this feature, truly interactively
by typing commands to the input, but also running command sequences as
scripts or as part of your input deck in a quasi batch mode.

You may type in expressions, functions
([17.2](#sec_Expressions__Functions__and)) or commands
([17.5](#sec_Commands)) into the input console to elaborate on data
already achieved from the interactive simulation session.

Sequences of commands, functions and control structures
([17.6](#sec_Control_Structures)) may be assembled as a script
([17.8](#sec_SCRIPTS)) into a file, and then activated by just typing
the file name into the console input of an interactive ngspice session.

Finally, and most useful, is it to add a script to the input file, in
addition the the netlist and dot commands. This is achieved by enclosing
the script into .control ... .endc (see
[16.4.3](#subsec_Interactive_mode_with), and
[17.8.7](#subsec_Parameter_sweep) for an example). This feature enables
a wealth of control options. You may set internal
([17.7](#sec_Variables)) and other variables, start a simulation,
evaluate the simulation output, start a new simulation based on these
data, and finally make use of many options for outputting the data
(graphically or into output files).

Historical note: The final releases of Berkeley Spice introduced a
command shell and scripting possibilities. The former releases were not
interactive. The choice for the scripting language was an early version
of \`csh', the C-shell, which was *en vogue* back then as an improvement
over the ubiquitous Bourne Shell. Berkeley Spice incorporated a modified
csh source code that, instead of invoking the unix \`exec' system call,
executed internal SPICE C subroutines. Apart from bug fixes, this is
still how ngspice works.

The csh-like scripting language is active in .control sections. It works
on \`strings', and does string substitution of \`environment' variables.
You see the csh at work in ngspice with set foo = "bar"; set baz =
"bar$foo", and in if, repeat, for, ... constructs. However, ngspice
processes mainly numerical data, and support for this was not available
in the c-sh implementation. Therefore, Berkeley implemented an
additional type of variables, with different syntax, to access double
and complex double vectors (possibly of length 1). This new variable
type is modified with let, and can be used without special syntax in
places where a numerical expression is expected: let bar = 4 \* 5; let
zoo = bar \* 4 works. Unfortunately, occasionally one has to cross the
boundary between the numeric and the string domain. For this purpose the
$& construct is available – it queries a variable in the numerical let
domain, and expands it to a c-sh string denoting the value. This lets
you do do something like set another = "this is $\&bar". It is important
to remember that set can only operate on (c-sh) strings, and that let
operates only on numeric data. Convert from numeric to string with $&,
and from string to numeric with $.

