# Bugs

When defining aliases like alias pdb plot db( \!:1 - \!:2 ) you must be
careful to quote the argument list substitutions in this manner. If you
quote the whole argument it might not work properly.

In a user-defined function, the arguments cannot be part of a name that
uses the plot.vec syntax. For example: define check(v(1))
cos(tran1.v(1)) does not work.

