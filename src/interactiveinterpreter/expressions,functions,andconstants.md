# Expressions, Functions, and Constants

Ngspice and ngnutmeg store data in the form of vectors: time, voltage,
etc. Each vector has a type, and vectors can be operated on and combined
algebraically in ways consistent with their types. Vectors are normally
created as the output of a simulation, or when a data file (output raw
file) is read in again (ngspice, ngnutmeg, see the load command
[17.5.40](#subsec_Load__Load_rawfile)), or when the initial data-file is
loaded directly into ngnutmeg. They can also be created with the let
command 8[17.5.37](#subsec_Let__Assign_a)).

An expression is an algebraic formula involving vectors and scalars (a
scalar is a vector of length 1) and the following operations:

``` listings
+ - * / ^ % ,
```

% is the modulo operator, and the comma operator has two meanings: if it
is present in the argument list of a user definable function, it serves
to separate the arguments. Otherwise, the term x , y is synonymous with
x + j(y). Also available are the logical operations & (and), | (or), \!
(not), and the relational operations \<, \>, \>=, \<=, =, and \<\> (not
equal). If used in an algebraic expression they work like they would in
C, producing values of 0 or 1. The relational operators have the
following synonyms:

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-21275"></span>Operator
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21278"></span>Synonym
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-21281"></span>gt
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21284"></span>&gt;
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-21287"></span>lt
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21290"></span>&lt;
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-21293"></span>ge
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21296"></span>&gt;=
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-21299"></span>le
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21302"></span>&lt;=
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-21305"></span>ne
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21308"></span>&lt;&gt;
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-21311"></span>and
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21314"></span>&amp;
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-21317"></span>or
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21320"></span>|
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-21323"></span>not
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21326"></span>!
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-21329"></span>eq
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21332"></span>=
</div></td>
</tr>
</tbody>
</table>

The operators are useful when \< and \> might be confused with the
internal IO redirection (see [17.4](#sec_Command_Interpretation), which
is almost always happening). It is however safe to use \< and \> with
the **define** command ([17.5.15](#subsec_Define__Define_a)).

The following functions are available:

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-21404"></span>Name
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21407"></span>Function
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-21410"></span>mag(vector)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21413"></span>Magnitude of vector (same as abs(vector)).
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-21416"></span>ph(vector)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21419"></span>Phase of vector.
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-21422"></span>cph(vector)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21425"></span>Phase of vector. Continuous values, no discontinuity at ±<span class="math inline"><em>π</em></span>.
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-21428"></span>unwrap(vector)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21431"></span>Phase of vector. Continuous values, no discontinuity at ±<span class="math inline"><em>π</em></span>. Real phase vector in degrees as input.
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-21434"></span>j(vector)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21437"></span>i(sqrt(-1)) times vector.
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-21440"></span>real(vector
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21443"></span>The real component of vector.
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-21446"></span>imag(vector)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21449"></span>The imaginary part of vector.
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-21452"></span>db(vector)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21455"></span>20 log10(mag(vector)).
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-21458"></span>log10(vector)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21461"></span>The logarithm (base 10) of vector.
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-21464"></span>ln(vector)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21467"></span>The natural logarithm (base e) of vector.
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-21470"></span>exp(vector)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21473"></span>e to the vector power.
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-21476"></span>abs(vector)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21479"></span>The absolute value of vector (same as mag).
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-21482"></span>sqrt(vector)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21485"></span>The square root of vector.
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-21488"></span>sin(vector)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21491"></span>The sine of vector.
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-21494"></span>cos(vector)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21497"></span>The cosine of vector.
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-21500"></span>tan(vector)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21503"></span>The tangent of vector.
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-21506"></span>atan(vector)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21509"></span>The inverse tangent of vector.
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-21512"></span>sinh(vector)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21515"></span>The hyperbolic sine of vector.
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-21518"></span>cosh(vector)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21521"></span>The hyperbolic cosine of vector.
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-21524"></span>tanh(vector)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21527"></span>The hyperbolic tangent of vector.
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-21530"></span>floor(vector)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21533"></span>Largest integer that is less than or equal to vector.
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-21536"></span>ceil(vector)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21539"></span>Smallest integer that is greater than or equal to vector.
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-21542"></span>norm(vector)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21545"></span>The vector normalized to 1 (i.e, the largest magnitude of any component is 1).
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-21548"></span>mean(vector)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21551"></span>The result is a scalar (a length 1 vector) that is the mean of the elements of vector (elements values added, divided by number of elements).
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-21554"></span>avg(vector)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21557"></span>The average of a vector.<br />
Returns a vector where each element is the mean of the preceding elements of the input vector (including the actual element).
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-21560"></span>stddev(vector)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21563"></span>The result is a scalar (a length 1 vector) that is the standard deviation of the elements of vector .
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-21566"></span>group_delay(vector)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21569"></span>Calculates the group delay <span class="math inline">−<em>d</em><em>p</em><em>h</em><em>a</em><em>s</em><em>e</em>[<em>r</em><em>a</em><em>d</em>]/<em>d</em><em>ω</em>[<em>r</em><em>a</em><em>d</em>/<em>s</em>]</span>. Input is the complex vector of a system transfer function versus frequency, resembling damping and phase per frequency value. Output is a vector of group delay values (real values of delay times) versus frequency.
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-21572"></span>vector(number)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21575"></span>The result is a vector of length number, with elements 0, 1, ... number - 1. If number is a vector then just the first element is taken, and if it isn't an integer then the floor of the magnitude is used.
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-21578"></span>unitvec(number)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21581"></span>The result is a vector of length number, all elements having a value 1.
</div></td>
</tr>
</tbody>
</table>

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-21619"></span>Name
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21622"></span>Function
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-21625"></span>length(vector)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21628"></span>The length of vector.
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-21631"></span>interpolate(plot.vector)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21634"></span>The result of interpolating the named vector onto the scale of the current plot. This function uses the variable polydegree to determine the degree of interpolation.
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-21637"></span>deriv(vector)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21640"></span>Calculates the derivative of the given vector. This uses numeric differentiation by interpolating a polynomial and may not produce satisfactory results (particularly with iterated differentiation). The implementation only calculates the derivative with respect to the real component of that vector's scale.
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-21643"></span>vecd(vector)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21646"></span>Compute the differential of a vector.
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-21649"></span>vecmin(vector)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21652"></span>Returns the value of the vector element with minimum value. Same as minimum.
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-21655"></span>minimum(vector)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21658"></span>Returns the value of the vector element with minimum value. Same as vecmin.
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-21661"></span>vecmax(vector)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21664"></span>Returns the value of the vector element with maximum value. Same as maximum.
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-21667"></span>maximum(vector)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21670"></span>Returns the value of the vector element with maximum value. Same as vecmax.
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-21673"></span>fft(vector)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21676"></span>fast fourier transform (<a href="#subsec_fft__fast_Fourier">17.5.27</a>)
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-21679"></span>ifft(vector)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21682"></span>inverse fast fourier transform (<a href="#subsec_fft__fast_Fourier">17.5.27</a>)
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-21685"></span>sortorder(vector)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21688"></span>Returns a vector with the positions of the elements in a real vector after they have been sorted into increasing order using a stable method (qsort).
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-21691"></span>timer(vector)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21694"></span>Returns CPU-time minus the value of the first vector element.
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-21697"></span>clock(vector)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21700"></span>Returns wall-time minus the value of the first vector element.
</div></td>
</tr>
</tbody>
</table>

Several functions offering statistical procedures are listed in the
following table:

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-21723"></span>Name
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21726"></span>Function
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-21729"></span>rnd(vector)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21732"></span>A vector with each component a random integer between 0 and the absolute value of the input vector's corresponding integer element value.
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-21735"></span>sgauss(vector)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21738"></span>Returns a vector of random numbers drawn from a Gaussian distribution (real value, mean = 0 , standard deviation = 1). The length of the vector returned is determined by the input vector. The contents of the input vector will not be used. A call to sgauss(0) will return a single value of a random number as a vector of length 1..
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-21741"></span>sunif(vector)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21744"></span>Returns a vector of random real numbers uniformly distributed in the interval [-1 .. 1[. The length of the vector returned is determined by the input vector. The contents of the input vector will not be used. A call to sunif(0) will return a single value of a random number as a vector of length 1.
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-21747"></span>poisson(vector)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21750"></span>Returns a vector with its elements being integers drawn from a Poisson distribution. The elements of the input vector (real numbers) are the expected numbers <span class="math inline"><em>λ</em></span>. Complex vectors are allowed, real and imaginary values are treated separately.
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-21753"></span>exponential(vector)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21756"></span>Returns a vector with its elements (real numbers) drawn from an exponential distribution. The elements of the input vector are the respective mean values (real numbers). Complex vectors are allowed, real and imaginary values are treated separately.
</div></td>
</tr>
</tbody>
</table>

An input vector may be either the name of a vector already defined or a
floating-point number (a scalar). A scalar will result in an output
vector of length 1. A number may be written in any format acceptable to
ngspice, such as 14.6Meg or -1.231e-4. Note that you can either use
scientific notation or one of the abbreviations like MEG or G, but not
both. As with ngspice, a number may have trailing alphabetic characters.

The notation expr \[num\] denotes the num'th element of expr. For
multi-dimensional vectors, a vector of one less dimension is returned.
Also for multi-dimensional vectors, the notation expr\[m\]\[n\] will
return the nth element of the mth subvector. To get a subrange of a
vector, use the form expr\[lower, upper\]. To reference vectors in a
plot that is not the current plot (see the setplot command, below), the
notation plotname.vecname can be used. Either a plotname or a vector
name may be the wildcard all. If the plotname is all, matching vectors
from all plots are specified, and if the vector name is all, all vectors
in the specified plots are referenced. Note that you may not use binary
operations on expressions involving wildcards - it is not obvious what
all + all should denote, for instance. Thus some (contrived) examples of
expressions are:

Expressions examples:

``` listings
cos(TIME) + db(v(3))
sin(cos(log([1 2 3 4 5 6 7 8 9 10])))
TIME * rnd(v(9)) - 15 * cos(vin#branch) ^ [7.9e5 8]
not ((ac3.FREQ[32] & tran1.TIME[10]) gt 3)
(sunif(0) ge 0) ? 1.0 : 2.0
mag(fft(v(18)))
```

Vector names in ngspice may look like @dname\[param\], where dname is
either the name of a device instance or of a device model. The vector
contains the value of the parameter of the device or model. See
Appendix, Chapt. [31](#cha_Model_and_Device) for details of which
parameters are available. The returned value is a vector of length 1.
Please note that finding the value of device and device model parameters
can also be done with the show command (e.g. show v1 : dc).

There are a number of pre-defined constants in ngspice, which you may
use by their name. They are stored in plot ([17.3](#sec_Plots)) const
and are listed in the table below:

<table>
<colgroup>
<col width="33%" />
<col width="33%" />
<col width="33%" />
</colgroup>
<tbody>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-21825"></span>Name
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21828"></span>Description
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21831"></span>Value
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-21834"></span>pi
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21837"></span><span class="math inline"><em>π</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21840"></span>3.14159...
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-21843"></span>e
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21846"></span><span class="math inline"><em>e</em></span> (the base of natural logarithms)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21849"></span>2.71828...
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-21852"></span>c
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21855"></span><span class="math inline"><em>c</em></span> (the speed of light)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21858"></span>299,792,500 <span class="math inline">$\frac{m}{sec}$</span>
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-21861"></span>i
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21864"></span>i (the square root of -1)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21867"></span><span class="math inline">$\sqrt{- 1}$</span>
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-21870"></span>kelvin
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21873"></span>(absolute zero in centigrade)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21876"></span>-273.15<span class="math inline"><em>C</em></span>
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-21879"></span>echarge
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21882"></span>q (the charge of an electron)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21885"></span>1.60219e-19 C
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-21888"></span>boltz
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21891"></span>k (Boltzmann's constant)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21894"></span>1.38062e-23<span class="math inline">$\frac{J}{K}$</span>
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-21897"></span>planck
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21900"></span>h (Planck's constant)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21903"></span>6.62620e-34
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-21906"></span>yes
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21909"></span>boolean
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21912"></span>1
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-21915"></span>no
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21918"></span>boolean
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21921"></span>0
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-21924"></span>TRUE
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21927"></span>boolean
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21930"></span>1
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-21933"></span>FALSE
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21936"></span>boolean
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-21939"></span>0
</div></td>
</tr>
</tbody>
</table>

These constants are all given in MKS units. If you define another
variable with a name that conflicts with one of these then it takes
precedence.

Additional constants may be generated during circuit setup (see
.csparam, [2.10](#sec__csparam)).

