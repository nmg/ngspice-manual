# Internally predefined variables

The operation of both ngutmeg and ngspice may be affected by setting
variables with the set command ([17.5.63](#subsec_Set__Set_the)). In
addition to the variables mentioned below, the set command also affects
the behavior of the simulator via the options previously described under
the section on .OPTIONS ([15.1](#sec_Simulator_Variables)). You also may
define new variables or alter existing variables inside .control ...
.endc for later use in a user-defined script (see Chapt.
[17.8](#sec_SCRIPTS)).

The following list is in alphabetical order. All of these variables are
acknowledged by ngspice. Frontend variables (e.g. on circuits and
simulation) are not defined in ngnutmeg. The predefined variables that
may be set or altered by the set command are:

  - appendwrite  
    Append to the file when a write command is issued, if one already
    exists.
  - askquit  
    Check to make sure that there are circuits suspended or plots
    unsaved. ngspice warns the user when he tries to quit if this is the
    case.brief If set to FALSE, the netlist will be printed.
  - batchmode  
    Set by ngspice if run with the -b command line parameter. May be
    used in input files to suppress plotting if ngspice runs in batch
    mode.
  - colorN  
    These variables determine the colors used, if X is being run on a
    color display. N may be between 0 and 15. Color 0 is the background,
    color 1 is the grid and text color, and colors 2 through 15 are used
    in order for vectors plotted. The value of the color variables
    should be names of colors, which may be found in the file
    /usr/lib/rgb.txt. ngspice for Windows does support only white
    background (color0=”white” with black grid and text) or or
    color0=”black” with white grid and text. Colors of its graph
    traces are set automatically.
  - controlswait  
    (only available with shared ngspice, chapt.
    [19.4.1.4](#subsec_Using_a__control)) If the simulation is started
    with the background thread (command bg\_run), the .control section
    commands are executed immediately after bg\_run has been given, i.e.
    typically before the simulation has finished. This often is not very
    useful because you want to evaluate the simulation results. If
    controlswait is set in .spiceinit or spice.rc, the command execution
    is delayed until the background thread has returned (aka the
    simulation has finished). If set controlswait is given inside of the
    .control section, only the commands following this statement are
    delayed.
  - cpdebug  
    Print control debugging information.
  - curplot  
    (read only) Returns \<type\>\<no.\> of the current plot. Type is one
    of tran, ac, op, sp, dc, unknown, no. is a number, sequentially set
    internally. This information is used to uniquely identify each plot.
  - curplotdate  
    Sets the date of the current plot.
  - curplotname  
    Sets the name of the current plot.
  - curplottitle  
    Sets the title (a short description) of the current plot.
  - debug  
    If set then a lot of debugging information is printed.
  - device  
    The name (/dev/tty??) of the graphics device. If this variable isn't
    set then the user's terminal is used. To do plotting on another
    monitor you probably have to set both the device and term variables.
    (If device is set to the name of a file, nutmeg dumps the graphics
    control codes into this file – this is useful for saving plots.)
  - diff\_abstol  
    The relative tolerance used by the **diff** command (default is
    1e-12).
  - diff\_reltol  
    The relative tolerance used by the **diff** command (default is
    0.001).
  - diff\_vntol  
    The absolute tolerance for voltage type vectors used by the **diff**
    command (default is 1e-6).
  - echo  
    Print out each command before it is executed.
  - editor  
    The editor to use for the edit command.
  - filetype  
    This can be either **ascii** or **binary**, and determines the
    format of the raw file (compact binary or text editor readable
    ascii). The default is **binary**.
  - fourgridsize  
    How many points to use for interpolating into when doing Fourier
    analysis.
  - gridsize  
    If this variable is set to an integer, this number is used as the
    number of equally spaced points to use for the Y axis when plotting.
    Otherwise the current scale is used (which may not have equally
    spaced points). If the current scale isn't strictly monotonic, then
    this option has no effect.
  - gridstyle  
    Sets the grid during plotting with the plot command. Will be
    overridden by direct entry of gridstyle in the plot command. A
    linear grid is standard for both x and y axis. Allowed values are
    **lingrid loglog xlog ylog smith smithgrid polar nogrid**.
  - hcopydev  
    If this is set, when the hardcopy command is run the resulting file
    is automatically printed on the printer named hcopydev with the
    command lpr -Phcopydev -g file.
  - hcopyfont  
    This variable specifies the font name for hardcopy output plots. The
    value is device dependent.
  - hcopyfontsize  
    This is a scaling factor for the font used in hardcopy plots.
  - hcopydevtype  
    This variable specifies the type of the printer output to use in the
    hardcopy command. If **hcopydevtype** is not set, Postscript format
    is assumed. plot (5) is recognized as an alternative output format.
    When used in conjunction with **hcopydev**, **hcopydevtype** should
    specify a format supported by the printer.
  - hcopyscale  
    This is a scaling factor for the font used in hardcopy plots
    (between 0 and 10).
  - hcopywidth  
    Sets width of the hardcopy plot.
  - hcopyheight  
    Sets height of the hardcopy plot.
  - hcopypscolor  
    Sets the color of the hardcopy output. If not set, black & white
    plotting is assumed with different linestyles for each output
    vector. A valid color integer value yields a colored plot background
    (0: black 1: white, others see below). and colored solid lines. This
    is valid for Postscript only.
  - hcopypstxcolor  
    This variable sets the color of the text in the Postscript hardcopy
    output. If not set, black on white background is assumed, else it
    will be white on black background. Valid colors are 0: black 1:
    white 2: red 3: blue 4: orange 5: green 6: pink 7: brown 8: khaki 9:
    plum 10: orchid 11: violet 12: maroon 13: turquoise 14: sienna 15:
    coral 16: cyan 17: magenta 18: gray (for smith grid) 19: gray (for
    smith grid) 20: gray (for normal grid).
  - height  
    The length of the page for asciiplot and print col.
  - history  
    The number of events to save in the history list.
  - inputdir  
    The directory path of the last input file. It may be used to direct
    outputs into a directory relative to the input (even the into the
    same directory) by e.g. the command write $inputdir/outfile.raw vec1
    vec2.
  - interactive  
        If interactive is set, numparam error handling may be done manually
    with user input from the console. If not, ngspice will exit upon a
    numparam error.
  - lprplot5  
    This is a printf(3s) style format string used to specify the command
    to use for sending plot(5)-style plots to a printer or plotter. The
    first parameter supplied is the printer name, the second parameter
    is a file name containing the plot. Both parameters are strings.
  - lprps  
    This is a printf(3s) style format string used to specify the command
    to use for sending Postscript plots to a printer or plotter. The
    first parameter supplied is the printer name, the second parameter
    is the file name containing the plot. Both parameters are strings.
  - modelcard  
    The name of the model card (normally .MODEL)
  - moremode  
    If moremode is set, whenever a large amount of data is being printed
    to the screen (e.g, the print or asciiplot commands), the output is
    stopped every screenful and continues when a carriage return is
    typed. If moremode is unset, then data scrolls off the screen
    without pausing.
  - nfreqs  
    The number of frequencies to compute in the Fourier command.
    (Defaults to 10.)
  - ngbehavior  
    Sets the compatibility mode of ngspice. Default value is 'all'. To
    be set in spinit ([16.5](#sec_Standard_configuration_file)) or
    .spiceinit ([16.6](#sec_User_defined_configuration)). A value of
    'all' improves compatibility with commercial simulators. Full
    compatibility is however *not* the intention of ngspice\! The values
    'ps', 'hs' and 'spice3' are available. See Chapt.
    [16.13](#sec_Compatibility).
  - no\_auto\_gnd  
    Setting this boolean variable by set no\_auto\_gnd in spinit or
    .spiceinit, ngspice will refrain from replacing all nodes named gnd
    by node 0. In using this setting, you will have to take care of
    proper zeroing appropriate ground nodes. If you failes to do so,
    ngspice may crash, or deliver wrong results.
  - nobjthack  
    BJTs can have either 3 or 4 nodes, which makes it difficult for the
    subcircuit expansion routines to decide what to rename. If the
    fourth parameter has been declared as a model name, then it is
    assumed that there are 3 nodes, otherwise it is considered a node.
    To disable this, you can set the variable nobjthack and force BJTs
    to have 4 nodes (for the purposes of subcircuit expansion, at
    least).
  - nobreak  
    Don't have asciiplot and print col break between pages.
  - noasciiplotvalue  
    Don't print the first vector plotted to the left when doing an
    asciiplot.
  - nobjthack  
    Assume that BJTs have 4 nodes.
  - noclobber  
    Don't overwrite existing files when doing IO redirection.
  - noglob  
    Don't expand the global characters \`\*', \`?', \`\[', and \`\]'.
    This is the default.
  - nonomatch  
    If noglob is unset and a global expression cannot be matched, use
    the global characters literally instead of complaining.
  - noparse  
    Don't attempt to parse input files when they are read in (useful for
    debugging). Of course, they cannot be run if they are not parsed.
  - noprintscale  
    Don't print the scale in the leftmost column when a print col
    command is given.
  - nosort  
    Don't let display sort the variable names.
  - nosubckt  
    Don't expand subcircuits.
  - notrnoise  
    Switch off the transient noise sources (Chapt.
    [4.1.7](#subsec_Transient_noise_source)).
  - numdgt  
    The number of digits to use when printing tables of data (print
    col). The default precision is 6 digits. On the VAX, approximately
    16 decimal digits are available using double precision, so p should
    not be more than 16. If the number is negative, one fewer digit is
    printed to ensure constant widths in tables.
  - num\_threads  
    The number of of threads to be used if OpenMP (see Chapt.
    [16.10](#sec_Ngspice_on_multi_core)) is selected. The default value
    is 2.
  - plotstyle  
    This should be one of **linplot**, **combplot**, or **pointplot**.
    **linplot**, the default, causes points to be plotted as parts of
    connected lines. **combplot** causes a comb plot to be done. It
    plots vectors by drawing a vertical line from each point to the
    X-axis, as opposed to joining the points. **pointplot** causes each
    point to be plotted separately.
  - pointchars  
    Set a string as a list of characters to be used as points in a point
    plot. Standard is \`ox\*+\#abcdefhgijklmnpqrstuvwyz'. Some
    characters are forbidden.
  - polydegree  
    The degree of the polynomial that the plot command should fit to the
    data. If polydegree is N, then nutmeg fits a degree N polynomial to
    every set of N points and draws 10 intermediate points in between
    each end point. If the points aren't monotonic, then nutmeg tries to
    rotate the curve and reduce the degree until a fit is achieved.
  - polysteps  
    The number of points to interpolate between every pair of points
    available when doing curve fitting. The default is 10.
  - program  
    The name of the current program (argv\[0\]).
  - prompt  
    The prompt, with the character \`\!' replaced by the current event
    number. Single quotes ' ' are required around the specified string
    unless you *really* want it expanded.
  - rawfile  
    The default name for created rawfiles.
  - remote\_shell  
    Overrides the name used for generating rspice runs (default is rsh).
  - renumber  
    Renumber input lines when an input file has .includes.
  - rndseed  
    Seed value for random number generator (used by sgauss, sunif, and
    rnd functions). It is set by the option command 'option
    seed=val|random'.
  - rhost  
    The machine to use for remote ngspice runs, instead of the default
    one (see the description of the rspice command, below).
  - rprogram  
    The name of the remote program to use in the rspice command.
  - sharedmode  
    Variable is set when ngspice runs in its shared mode (from
    ngspice.dll or ngspice\_xx.so). May be used in universal input files
    to suppress plotting because a graphics interface is lacking.
  - sim\_status  
    will bet set to 0 when the simulation starts. If there is an error
    and the simulation fails with 'xx simulation(s) aborted', then
    sim\_status is set to 1. The variable can be used in scripted loops
    within a transient simulation to allow special handling e.g. in case
    of non-convergence.
  - sourcepath  
    A list of the directories to search when a source command is given.
    The default is the current directory and the standard ngspice
    library (/usr/local/lib/ngspice, or whatever LIBPATH is \#defined to
    in the ngspice source). The command  
    set sourcepath = ( e:/ D:/ . c:/spice/examples )  
        will overwrite the default. The search sequence now is: current
    directory, e:/, d:/, current directory (again due to .),
    c:/spice/examples. 'Current directory' is depending on the OS.
  - specwindow  
    Windowing for commands spec **** ([17.5.76](#subsec_Spec__Create_a))
    or** **fft ([17.5.27](#subsec_fft__fast_Fourier)). May be one of the
    following: bartlet blackman cosine gaussian hamming hanning none
    rectangular triangle.
  - specwindoworder  
    Integer value 2 - 8 (default 2), used by commands spec or** **fft.
  - spicepath  
    The program to use for the aspice command. The default is
    /cad/bin/spice.
  - sqrnoise  
    If set, noise data outputs will be given as \(V^{2}/Hz\) or
    \(A^{2}/Hz\), otherwise as the usual \(V/\left. \sqrt{}Hz \right.\)
    or \(A/\left. \sqrt{}Hz \right.\).
  - strict\_errorhandling  
    If set by the user, an error detected during circuit parsing will
    immediately lead ngspice to exit with exit code 1 (see
    [18.5](#sec_Error_handling)). May be set in files spinit
    ([16.5](#sec_Standard_configuration_file)) or .spiceinit
    ([16.6](#sec_User_defined_configuration)) only.
  - subend  
    The card to end subcircuits (normally .ends).
  - subinvoke  
    The prefix to invoke subcircuits (normally X).
  - substart  
    The card to begin subcircuits (normally .subckt).
  - term  
    The mfb name of the current terminal.
  - ticmarks  
    An integer value n, n tics (a small 'x') will be set on your graph.
  - ticlist  
    A list of integers, e.g. ( 4 14 24 ) to set tics (small 'x') on your
    graph.
  - units  
    If this is **degrees**, then all the trig functions will use degrees
    instead of radians.
  - unixcom  
    If a command isn't defined, try to execute it as a UNIX command.
    Setting this option has the effect of giving a rehash command,
    below. This is useful for people who want to use ngnutmeg as a login
    shell.
  - wfont  
    Set the font for the graphics plot in MS Windows. Typical fonts are
    courier, times, arial and all others found on your machine. Default
    is courier.
  - wfont\_size  
    The size of the windows font. The default depends on system
    settings.
  - width  
    The width of the page for asciiplot and print col (see also
    [15.6.7](#subsec__width)).
  - win\_console  
    is set when ngspice runs in a console under Windows.
  - x11lineararcs  
    Some X11 implementations have poor arc drawing. If you set this
    option, ngspice will plot using an approximation to the curve using
    straight lines.
  - xbrushwidth  
    Linewidth for grid, border and graph.
  - xfont  
    Set the font for the graphics plot in X11 (Linux, Cygwin, etc.).
    Input format still has to be checked.
  - xtrtol  
    Set trtol, e.g. to 7, to avoid the default speed reduction (accuracy
    increase) for XSPICE (see [16.9](#sec_Simulation_time)). Be aware of
    potential precision degradation or convergence issues using this
    option.

