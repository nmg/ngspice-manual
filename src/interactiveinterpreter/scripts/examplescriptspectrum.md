# Example script 'spectrum'

A typical example script named **spectrum** is delivered with the
ngspice distribution. Even if it is made obsolete by the internal spec
command (see [17.5.76](#subsec_Spec__Create_a)), and especially by the
much faster fft command (see [17.5.27](#subsec_fft__fast_Fourier)), it
is a good example for getting acquainted with the ngspice (or nutmeg)
post-processor language.

As a suitable input for spectrum you may run a ring-oscillator,
delivered with ngspice in e.g. test/bsim3soi/ring51\_41.cir. For an
adequate resolution a simulation time of 1\(\mu\)s is needed. A small
control script starts ngspice by loading the R.O. simulation data and
executing spectrum.

Small script to start ngspice, read the simulation data and start
spectrum:

``` listings
* test for script 'spectrum'
.control
load ring51_41.out
spectrum 10MEG 2500MEG 1MEG v(out25) v(out50)
.endc
```

