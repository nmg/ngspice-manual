# Vectors

Ngspice and ngnutmeg data is in the form of vectors: time, voltage, etc.
Each vector has a type, and vectors can be operated on and combined
algebraically in ways consistent with their types. Vectors are normally
created as a result of a transient or dc simulation. They are also
established when a data file is read in (see the load command
[17.5.40](#subsec_Load__Load_rawfile)), or they are created with the let
command [17.5.37](#subsec_Let__Assign_a) inside a script. If a variable
x is assigned something of the form $&word, then word has to be a
vector, and the numeric value of word is transferred into the variable
x.

