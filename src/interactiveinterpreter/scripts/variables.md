# Variables

Variables are defined and initialized with the set command
([17.5](#sec_Commands)). set output=10 defines the variable output and
sets it to the (real) number 10. Predefined variables, which are used
inside ngspice for specific purposes, are listed in Chapt.
[17.7](#sec_Variables). Variables are accessible globally. The values of
variables may be used in commands by writing $varname where the value of
the variable is to appear, e.g. $output. The special variable $$ refers
to the process ID of the program. With $\< a line of input is read from
the terminal. If a variable is assigned with to with $\&word, then word
must be a vector (see below), and word's numeric value is taken to be
the new value of the variable. If foo is a valid variable, and is of
type list, then the expression $foo\[low-high\] expands to a range of
elements. Either the upper or lower index may be left out, and in
addition to slicing also reversing of a list is possible through
$foo\[len-0\] (len is the length of the list, the first valid index is
always 1). Furthermore, the notation $?foo evaluates to 1 if the
variable foo is defined, 0 otherwise, and $\#foo evaluates to the number
of elements in foo if it is a list, 1 if it is a number or string, and 0
if it is a Boolean variable.

