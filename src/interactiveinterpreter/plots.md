# Plots

The output vectors of any analysis are stored in plots, a traditional
SPICE notion. A plot is a group of vectors. A first tran command will
generate several vectors within a plot tran1. A subsequent tran command
will store their vectors in tran2. Then a linearize command will
linearize all vectors from tran2 and store them in tran3, which then
becomes the current plot. A fft will generate a plot spec1, again now
the current plot. The display command always will show all vectors in
the current plot. Echo $plots followed by Return lists all plots
generated so far**. **Setplot followed by Return will show all plots and
ask for a (new) plot to become current. A simple Return will end the
command. Setplot name will change the current plot to 'name' (e.g.
setplot tran2 will make tran2 the current plot). A sequence
**name.vector** may be used to access the vector from a foreign plot.

You may generate plots by yourself: setplot new will generate a new plot
named unknown1, set curplottitle=”a new plot” will set a title, set
curplotname=myplot will set its name as a short description, set
curplotdate=”Sat Aug 28 10:49:42 2010” will set its date. Note that
strings with spaces have to be given with double quotes.

Of course the notion 'plot' will be used by this manual also in its more
common meaning, denoting a graphics plot or being a plot command. Be
careful to get the correct meaning.

