# Break

General Form:

``` listings
break
```

If there is a while, dowhile, or foreach block enclosing this statement,
control passes out of the block. Otherwise an error results.

Of course, control structures may be nested. When a block is entered and
the input is the terminal, the prompt becomes a number of \>'s
corresponding to the number of blocks the user has entered. The current
control structures may be examined with the debugging command cdump (see
[17.5.10](#subsec_Cdump__Dump_the)).

