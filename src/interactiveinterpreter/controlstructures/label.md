# Label

General Form:

``` listings
label word
```

If a statement of the form goto word is encountered, control is
transferred to this point, otherwise this is a no-op.

