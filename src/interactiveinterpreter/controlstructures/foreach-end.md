# Foreach - End

General Form:

``` listings
foreach var value ...
statement
...
end
```

The statements are executed once for each of the values, each time with
the variable **var** set to the current one. (**var** can be accessed by
the **$var** notation - see below).

