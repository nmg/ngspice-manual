# Dowhile - End

General Form:

``` listings
dowhile condition
statement
...
end
```

The same as while, except that the condition is tested after the
statements are executed.

