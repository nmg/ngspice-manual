# While - End

General Form:

``` listings
while condition
statement
...
end
```

While condition, an arbitrary algebraic expression, is true, execute the
statements.

