# Repeat - End

General Form:

``` listings
repeat [number]
statement
...
end
```

Execute the statements number times, or forever if no argument is given.

