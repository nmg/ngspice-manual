# Goto

General Form:

``` listings
goto word
```

If a statement of the form label word is present in the block or an
enclosing block, control is transferred there. Note that if the label is
at the top level, it must be before the goto statement (i.e, a forward
goto may occur only within a block). A block to just include goto on the
top level may look like the following example.

Example noop block to include forward goto on top level:

``` listings
if (1)
... 
goto gohere
...
label gohere 
end
```

