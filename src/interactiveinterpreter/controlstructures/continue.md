# Continue

General Form:

``` listings
continue
```

If there is a while, dowhile, or foreach block enclosing this statement,
control passes to the test, or in the case of foreach, the next value is
taken. Otherwise an error results.

