# If - Then - Else

General Form:

``` listings
if condition
statement
...
else
statement
...
end
```

If the condition is non-zero then the first set of statements are
executed, otherwise the second set. The else and the second set of
statements may be omitted.

