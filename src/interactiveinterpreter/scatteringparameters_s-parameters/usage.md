# Usage

Copy and then edit s-param.cir. You will find this file in directory
/examples/control\_structs of the ngspice distribution.

The reference resistance (often called characteristic impedance) for the
measurements is added as a parameter

.param Rbase=50

The bias voltages at the input and output ports of the circuit are set
as parameters as well:

.param Vbias\_in=1 Vbias\_out=2

Place your circuit at the appropriate place in the input file, e.g.
replacing the existing example circuits. The input port of your circuit
has two nodes **in, 0**. The output port has the two nodes **out, 0**.
The bias voltages are connected to your circuit via the resistances of
value **Rbase** at the input and output respectively. This may be of
importance for the operating point calculations if your circuit draws a
large dc current.

Now edit the ac commands (see [17.5.1](#subsec_Ac___Perform_an))
according to the circuit provided, e.g.

ac lin 100 2.5MEG 250MEG $ use for Tschebyschef 

Be careful to keep both ac lines in the .control ... .endc section the
same and only change both in equal measure\!

Select the plot commands (lin/log, or smithgrid) or the 'write to file'
commands (write, wrdata, or wrs2p) according to your needs.

Run ngspice in interactive mode

ngspice s-param.cir

