# Intro

A command line script, available from the ngspice distribution at
examples/control\_structs/s-param.cir, together with the command wrs2p
(see Chapt. [17.5.94](#subsec_Wrs2p__Write_scattering)) allows to
calculate, print and plot the scattering parameters S11, S21, S12, and
S22 of any two port circuit at varying frequencies.

The printed output using wrs2p is a **Touchstone® version 1** format
file. The file follows the format according to The Touchstone File
Format Specification, Version 2.0, available from
[here](http://www.eda.org/ibis/touchstone_ver2.0/). An example is given
as number 13 on page 15 of that specification.

