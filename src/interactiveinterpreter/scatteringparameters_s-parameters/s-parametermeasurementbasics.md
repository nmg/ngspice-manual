# S-parameter measurement basics

S-parameters allow a two-port description not just by permuting
\(I_{1}\), \(U_{1}\), \(I_{2}\), \(U_{2}\), but using a superposition,
leading to a power view of the port (We only look at two-ports here,
because multi-ports are not (yet?) implemented.).

You may start with the effective power, being negative or positive

\[\begin{array}{ll}
{P = u \cdot i} & \\
\end{array}\]

The value of \(P\) may be the difference of two real numbers, with \(K\)
being another real number.

\[\begin{array}{ll}
{ui = P = a^{2} - b^{2} = \left( {a + b} \right)\left( {a - b} \right) = \left( {a + b} \right)\left( {KK^{- 1}} \right)\left( {a - b} \right) = \left\{ {K\left( {a + b} \right)} \right\}\left\{ {K^{- 1}\left( {a - b} \right)} \right\}} & \\
\end{array}\]

Thus you get

\[\begin{array}{ll}
{K^{- 1}u = a + b} & \\
\end{array}\]

\[\begin{array}{ll}
{Ki = a - b} & \\
\end{array}\]

and finally

\[\begin{array}{ll}
{a = \frac{u + K^{2}i}{2K}} & \\
\end{array}\]

\[\begin{array}{ll}
{b = \frac{u - K^{2}i}{2K}} & \\
\end{array}\]

By introducing the reference resistance \(Z_{0}: = K^{2} > 0\) we get
finally the Heaviside transformation

\[\begin{array}{ll}
{a = \frac{u + Z_{0}i}{2\sqrt{Z_{0}}}, b = \frac{u - Z_{0}i}{2\sqrt{Z_{0}}}} & \\
\end{array}\]

In case of our two-port we subject our variables to a Heaviside
transformation

\[\begin{array}{ll}
{a_{1} = \frac{U_{1} + Z_{0}I_{1}}{2\sqrt{Z_{0}}} b_{1} = \frac{U_{1} - Z_{0}I_{1}}{2\sqrt{Z_{0}}}} & \\
\end{array}\]

\[\begin{array}{ll}
{a_{2} = \frac{U_{2} + Z_{0}I_{2}}{2\sqrt{Z_{0}}} b_{2} = \frac{U_{2} - Z_{0}I_{2}}{2\sqrt{Z_{0}}}} & \\
\end{array}\]

The s-matrix for a two-port then is

\[\begin{array}{ll}
{\left( \begin{array}{l}
b_{1} \\
b_{2} \\
\end{array} \right) = \left( \begin{array}{ll}
s_{11} & s_{12} \\
s_{21} & s_{22} \\
\end{array} \right)\left( \begin{array}{l}
a_{1} \\
a_{2} \\
\end{array} \right)} & \\
\end{array}\]

Two obtain \(s_{11}\) we have to set \(a_{2} = 0\). This is accomplished
by loading the output port exactly with the reference resistance
\(Z_{0},\) which sinks a current \(I_{2} = - U_{2}/Z_{0}\) from the
port.

\[\begin{array}{ll}
{s_{11} = \left( \frac{b_{1}}{a_{1}} \right)_{a_{2} = 0}} & \\
\end{array}\]

\[\begin{array}{ll}
{s_{11} = \frac{U_{1} - Z_{0}I_{1}}{U_{1} + Z_{0}I_{1}}} & \\
\end{array}\]

Loading the input port from an ac source \(U_{0}\) via a resistor with
resistance value \(Z_{0}\), we obtain the relation

\[\begin{array}{ll}
{U_{0} = Z_{0}I_{1} + U_{1}} & \\
\end{array}\]

Entering this into [77](#eq_), we get

\[\begin{array}{ll}
{s_{11} = \frac{2U_{1} - U_{0}}{U_{0}}} & \\
\end{array}\]

For \(s_{21}\) we obtain similarly

\[\begin{array}{ll}
{s_{21} = \left( \frac{b_{2}}{a_{1}} \right)_{a_{2} = 0}} & \\
\end{array}\]

\[\begin{array}{ll}
{s_{21} = \frac{U_{2} - Z_{0}I_{2}}{U_{1} + Z_{0}I_{1}} = \frac{2U_{2}}{U_{0}}} & \\
\end{array}\]

Equations [79](#eq__2) and [81](#eq__1) now tell us how to measure
\(s_{11}\) and \(s_{21}\): Measure \(U_{1}\) at the input port, multiply
by 2 using an E source, subtracting \(U_{0}\), which for simplicity is
set to 1, and divide by \(U_{0}\). At the same time measure \(U_{2}\) at
the output port, multiply by 2 and divide by \(U_{0}\). Biasing and
measuring is done by subcircuit S\_PARAM. To obtain \(s_{22}\) and
\(s_{12}\), you have to exchange the input and output ports of your
two-port and do the same measurement again. This is achieved by
switching resistors from low (\(1m\Omega\)) to high (\(1T\Omega\)) and
thus switching the input and output ports.

