# specwindow:

This variable is set to one of the following strings, which will
determine the type of windowing used for the Fourier transform in the
spec and fft command. If not set, the default is hanning.

  - none  
    No windowing
  - rectangular  
    Rectangular window
  - bartlet  
    Bartlett (also triangle) window
  - blackman  
    Blackman window
  - hanning  
    Hanning (also hann or cosine) window
  - hamming  
    Hamming window
  - gaussian  
    Gaussian window
  - flattop  
    Flat top window

![image:
0\_home\_nmg\_Documents\_gitroot\_ngspice-ngspice-manuals\_Images\_fft\_windows.png](0_home_nmg_Documents_gitroot_ngspice-ngspice-manuals_Images_fft_windows.png)
Figure 17.1: Spec and FFT window functions (Gaussian order = 4)

