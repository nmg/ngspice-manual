# Ac\*: Perform an AC, small-signal frequency response analysis

General Form:

``` listings
ac ( DEC | OCT | LIN ) N Fstart Fstop
```

Do an small signal ac analysis (see also Chapt.
[15.3.1](#subsec__AC__Small_Signal_AC)) over the specified frequency
range.

**DEC** decade variation, and **N** is the number of points per decade.

**OCT** stands for octave variation, and **N** is the number of points
per octave.

**LIN** stands for linear variation, and **N** is the number of points.

**fstart** is the starting frequency, and **fstop** is the final
frequency.

Note that in order for this analysis to be meaningful, at least one
independent source must have been specified with an ac value.

In this ac analysis all non-linear devices are linearized around their
actual dc operating point. All Ls and Cs get their imaginary value,
depending on the actual frequency step. Each output vector will be
calculated relative to the input voltage (current) given by the ac value
(Iin equals to 1 in the example below). The resulting node voltages (and
branch currents) are complex vectors. Therefore you have to be careful
using the plot command.

Example:

``` listings
* AC test
Iin 1 0 AC 1
R1 1 2 100
L1 2 0 1

.control
AC LIN 101 10 10K
plot v(2)       $ real part !
plot mag(v(2))  $ magnitude
plot db(v(2))   $ same as vdb(2)
plot imag(v(2)) $ imaginary part of v(2)
plot real(v(2)) $ same as plot v(2)
plot phase(v(2))  $ phase in rad
plot cph(v(2))  $ phase in rad, continuous beyond pi
plot 180/PI*phase(v(2)) $ phase in deg
.endc
.end
```

In addition to the plot examples given above you may use the variants of
vxx(node) described in Chapt. [15.6.2](#subsec__PRINT_Lines) like
vdb(2). An option to suppress OP analysis before AC may be set for
linear circuits ([15.1.3](#subsec_AC_Solution_Options)).

