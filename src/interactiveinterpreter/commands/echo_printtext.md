# Echo: Print text

General Form:

``` listings
echo [text...] [$variable] ["$&vector"]
```

Echos the given text, variable or vector to the screen. **echo** without
parameters issues a blank line.

