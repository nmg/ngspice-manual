# Devhelp: information on available devices

General Form:

``` listings
devhelp [[-csv] device_name [parameter]]
```

Devhelp command shows the user information about the devices available
in the simulator. If called without arguments, it simply displays the
list of available devices in the simulator. The name of the device is
the name used inside the simulator to access that device. If the user
specifies a device name, then all the parameters of that device (model
and instance parameters) will be printed. Parameter description includes
the internal ID of the parameter (id\#), the name used in the model card
or on the instance line (*Name*), the direction (*Dir*) and the
description of the parameter (*Description*). All the fields are
self-explanatory, except the \`*direction*'. Direction can be in, out or
inout and corresponds to a \`*write-only*', \`*read-only*' or a
\`*read/write*' parameter. Read-only parameters can be read but not set,
write only can be set but not read and *read/write* can be both set and
read by the user.

The -csv option prints the fields separated by a comma, for direct
import into a spreadsheet. This option is used to generate the simulator
documentation.

Example:

``` listings
devhelp 
devhelp resistor 
devhelp capacitor ic
```

