# Setseed: Set the seed value for the random number generator

General Form:

``` listings
setseed [number]
```

When this command is given, the seed value for the random number
generator is set to number. Number has to be an integer greater than 0.
The random numbers retrieved after this command are a sequence of pseudo
random numbers with a huge period. Setting the seed value will provide a
reproducible sequence of random numbers, i.e. the same seed results in
the same sequence. See also the options SEED and SEEDINFO in chapt.
[15.1.1](#subsec_General_Options)and chapt.
[22](#cha_Statistical_circuit_analysis) on statistical circuit
analysis..

