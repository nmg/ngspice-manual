# Sysinfo\*: Print system information

General Form:

``` listings
sysinfo
```

The command prints system information useful for sending bug report to
developers. Information consists of:

  - Name of the operating system,
  - CPU type,
  - Number of physical processors (not available under Windows OS),
    number of logical processors,
  - Total amount of DRAM available,
  - DRAM currently available.

The example below shows the use of this command.

``` listings
ngspice 1 -> sysinfo
OS: CYGWIN_NT-5.1 1.5.25(0.156/4/2) 2008-06-12 19:34 
CPU: Intel(R) Pentium(R) 4 CPU 3.40GHz 
Logical processors: 2 
Total DRAM available = 1535.480469 MB. 
DRAM currently available = 984.683594 MB.
ngspice 2 -> 
```

This command has been tested under Windows OS and Linux. It may not be
available in your operating system environment.

