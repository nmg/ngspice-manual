# Aspice\*: Asynchronous ngspice run

General Form:

``` listings
aspice input-file [output-file]
```

Start an ngspice run, and when it is finished load the resulting data.
The raw data is kept in a temporary file. If output-file is specified
then the diagnostic output is directed into that file, otherwise it is
thrown away.

