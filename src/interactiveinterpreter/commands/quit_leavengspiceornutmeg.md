# Quit: Leave Ngspice or Nutmeg

General Form:

``` listings
quit
quit [exitcode]
```

Quit ngnutmeg or ngspice. Ngspice will ask for an acknowledgment if
parameters have not been saved. If unset askquit is specified, ngspice
will terminate immediately.

The optional parameter exitcode is an integer that sets the exit code
for ngspice. This is useful to return a success/fail value to the
operating system.

