# Settype: Set the type of a vector

General Form:

``` listings
settype type vector ...
```

Change the type of the named vectors to type. Type names can be found in
the following table.

<table>
<colgroup>
<col width="20%" />
<col width="20%" />
<col width="20%" />
<col width="20%" />
<col width="20%" />
</colgroup>
<tbody>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-23917"></span>Type
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-23920"></span>Unit
</div></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-23926"></span>Type
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-23929"></span>Unit
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-23932"></span>notype
</div></td>
<td></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-23941"></span>pole
</div></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-23947"></span>time
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-23950"></span>s
</div></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-23956"></span>zero
</div></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-23962"></span>frequency
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-23965"></span>Hz
</div></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-23971"></span>s-param
</div></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-23977"></span>voltage
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-23980"></span>V
</div></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-23986"></span>temp-sweep
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-23989"></span>Celsius
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-23992"></span>current
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-23995"></span>A
</div></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-24001"></span>res-sweep
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-24004"></span>Ohms
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-24007"></span>onoise-spectrum
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-24010"></span>(V or A)/<span class="math inline">$\sqrt{Hz}$</span>
</div></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-24016"></span>impedance
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-24019"></span>Ohms
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-24022"></span>onoise-integrated
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-24025"></span>V or A
</div></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-24031"></span>admittance
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-24034"></span>Mhos
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-24037"></span>inoise-spectrum
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-24040"></span>(V or A)/<span class="math inline">$\sqrt{Hz}$</span>
</div></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-24046"></span>power
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-24049"></span>W
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-24052"></span>inoise-integrated
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-24055"></span>V or A
</div></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-24061"></span>phase
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-24064"></span>Degree
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-24067"></span>temperature
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-24070"></span>Celsius
</div></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-24076"></span>decibel
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-24079"></span>dB
</div></td>
</tr>
</tbody>
</table>

