# History: Review previous commands

General Form:

``` listings
history [-r] [number]
```

Print out the history, or the last (first if **-r** is specified) number
commands typed at the keyboard.

A history substitution enables you to reuse a portion of a previous
command as you type the current command. History substitutions save
typing. A history substitution normally starts with a '\!'. A history
substitution has three parts: an event that specifies a previous
command, a selector that selects one or more words of the event, and
some modifiers that modify the selected words. The selector and
modifiers are optional. A history substitution has the form
\!\[event\]\[:\]selector\[:modifier\] …\] The event is required unless
it is followed by a selector that does not start with a digit. The ':'
can be omitted before the selector if this selector does not begin with
a digit. History substitutions are interpreted before anything else —
even before quotations and command substitutions. The only way to quote
the '\!' of a history substitution is to escape it with a preceding
backslash. A '\!' need not be escaped if it is followed by whitespace,
'=', or '('.

Ngspice saves each command that you type on a history list, provided
that the command contains at least one word. The commands on the history
list are called events. The events are numbered, with the first command
that you issue when you start Ngspice being number one. The history
variable specifies how many events are retained on the history list.

These are the forms of an event in a history substitution:

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-22803"></span><span style="font-family:monospace;">!! </span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-22806"></span>The preceding event. Typing <span style="font-family:monospace;">'!!'</span> is an easy way to reissue the previous command.
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-22809"></span><span style="font-family:monospace;">!n</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-22812"></span> Event number <em>n.</em>
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-22815"></span><span style="font-family:monospace;">!-n</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-22818"></span>The <em>n</em><sup>th</sup> previous event. For example, <span style="font-family:monospace;">!-1</span> refers to the immediately preceding event and is equivalent to <span style="font-family:monospace;">!!.</span>
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-22825"></span><span style="font-family:monospace;">!str</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-22828"></span>The unique previous event whose name starts with <em>str.</em>
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-22831"></span><span style="font-family:monospace;">!?str[?]</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-22834"></span>The unique previous event containing the string <em>str</em>. The closing <span style="font-family:monospace;">'?'</span> can be omitted if it is followed by a newline.
</div></td>
</tr>
</tbody>
</table>

You can modify the words of an event by attaching one or more modifiers.
Each modifier must be preceded by a colon. The following modifiers
assume that the first selected word is a file name:

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-22863"></span><span style="font-family:monospace;">:r</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-22866"></span>Removes the trailing <span style="font-family:monospace;">.str</span> extension from the first selected word.
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-22869"></span><span style="font-family:monospace;">:h</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-22872"></span>Removes a trailing path name component from the first selected word.
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-22875"></span><span style="font-family:monospace;">:t</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-22878"></span>Removes all leading path name components from the first selected word.
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-22881"></span><span style="font-family:monospace;">:e</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-22884"></span>Remove all but the trailing suffix.
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-22887"></span><span style="font-family:monospace;">:p</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-22890"></span>Print the new command but do not execute it.
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-22893"></span><span style="font-family:monospace;">s/</span><em>old</em>/<em>new</em>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-22896"></span>Substitute <em>new</em> for the first occurrence of <em>old</em> in the event line. Any delimiter may be used in place of `<span style="font-family:monospace;">/</span>'. The delimiter may be quoted in <em>old</em> and <em>new</em> with a single backslash. If `<span style="font-family:monospace;">&amp;</span>' appears in <span style="font-family:monospace;">new</span>, it is replaced by <span style="font-family:monospace;">old</span>. A single backslash will quote the `<span style="font-family:monospace;">&amp;</span>'. The final delimiter is optional if it is the last character on the input line.
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-22899"></span><span style="font-family:monospace;">&amp;</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-22902"></span>Repeat the previous substitution.
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-22905"></span><span style="font-family:monospace;">g a</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-22908"></span>Cause changes to be applied over the entire event line. Used in conjunction with `<span style="font-family:monospace;">s</span>', as in <span style="font-family:monospace;">gs</span>/<em>old</em>/<em>new</em>/, or with `<span style="font-family:monospace;">&amp;</span>'.
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-22911"></span><span style="font-family:monospace;">G</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-22914"></span>Apply the following `<span style="font-family:monospace;">s</span>' modifier once to each word in the event.
</div></td>
</tr>
</tbody>
</table>

For example, if the command ls /usr/elsa/toys.txt has just been
executed, then the command echo \!\!^:r \!\!^:h \!\!^:t \!\!^:t:r
produces the output /usr/elsa/toys /usr/elsa toys.txt toys . The '^'
command is explained in the table below.

You can select a subset of the words of an event by attaching a selector
to the event. A history substitution without a selector includes all of
the words of the event. These are the possible selectors for selecting
words of the event:

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-22946"></span><span style="font-family:sans-serif;">:0</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-22949"></span>The command name
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-22952"></span><span style="font-family:sans-serif;">[:]^ </span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-22955"></span>The first argument
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-22958"></span><span style="font-family:sans-serif;">[:]$ </span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-22961"></span>The last argument
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-22964"></span><span style="font-family:sans-serif;">:n </span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-22967"></span>The <em>n</em><sup>th</sup> argument (<em>n</em> <span class="math inline">≥</span> 1)
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-22974"></span><span style="font-family:sans-serif;">:n1-n2 </span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-22977"></span>Words <em>n</em>1 through <em>n</em>2
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-22980"></span><span style="font-family:sans-serif;">[:]* </span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-22983"></span>Words 1 through $
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-22986"></span><span style="font-family:sans-serif;">:x* </span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-22989"></span>Words <em>x</em> through $
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-22992"></span><span style="font-family:sans-serif;">:x- </span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-22995"></span>Words <em>x</em> through ($ - 1)
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-22998"></span><span style="font-family:sans-serif;">[:]-x</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-23001"></span> Words 0 through <em>x</em>
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-23004"></span><span style="font-family:sans-serif;">[:]% </span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-23007"></span>The word matched by the last <span style="font-family:sans-serif;">?str?</span> search used
</div></td>
</tr>
</tbody>
</table>

The colon preceding a selector can be omitted if the selector does not
start with a digit.

The following additional special conventions provide abbreviations for
commonly used forms of history substitution:

  - An event specification can be omitted from a history substitution if
    it is followed by a selector that does not start with a digit. In
    this case the event is taken to be the event used in the most recent
    history reference on the same line if there is one, or the preceding
    event otherwise. For example, the command echo \!?qucs?^ \!$ echoes
    the first and last arguments of the most recent command containing
    the string qucs .
  - If the first non-blank character of an input line is '^', the '^' is
    taken as an abbreviation for \!:s^ . This form provides a convenient
    way to correct a simple spelling error in the previous line. For
    example, if by mistake you typed the command cat /etc/lasswd you
    could re-execute the command with lasswd changed to passwd by typing
    ^l^p .
  - You can enclose a history substitution in braces to prevent it from
    absorbing the following characters. In this case the entire
    substitution except for the starting '\!' must be within the braces.
    For example, suppose that you previously issued the command cp
    accounts ../money . Then the command \!cps looks for a previous
    command starting with cps while the command \!{cp}s turns into a
    command cp accounts ../moneys .

Some characters are handled specially as follows:

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-23035"></span><span style="font-family:sans-serif;">~</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-23038"></span>Expands to the home directory
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-23041"></span><span style="font-family:sans-serif;">*</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-23044"></span>Matches any string of characters in a filename
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-23047"></span><span style="font-family:sans-serif;">?</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-23050"></span>Matches any single character in a filename
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-23053"></span><span style="font-family:sans-serif;">[]</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-23056"></span>Matches any of the characters enclosed in a filename
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-23059"></span><span style="font-family:sans-serif;">-</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-23062"></span>Used within <span style="font-family:sans-serif;">[]</span> to specify a range of characters. For example, <span style="font-family:sans-serif;">[b-k]</span> matches on any character between and including <span style="font-family:sans-serif;">‘b’</span> through to <span style="font-family:sans-serif;">‘k’</span>.
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-23065"></span><span style="font-family:sans-serif;">^</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-23068"></span>If the <span style="font-family:sans-serif;">^</span> is included within <span style="font-family:sans-serif;">[]</span> as the first character, then it negates the following characters matching on anything but those. For example, <span style="font-family:sans-serif;">[^agm]</span> would match on anything other than <span style="font-family:sans-serif;">‘a’</span>, <span style="font-family:sans-serif;">‘g’</span> and <span style="font-family:sans-serif;">‘m’</span>. <span style="font-family:sans-serif;">[^a-zA-Z]</span> would match on anything other than an alphabetic character.
</div></td>
</tr>
</tbody>
</table>

The wildcard characters \*, ?, \[, and \] can be used, but only if you
unset noglob first. This makes them rather useless for typing algebraic
expressions, so you should set noglob again after you are done with
wildcard expansion.

When the environment variable HOME exists (on Unix, Linux, or CYGWIN),
history permanently stores previous command lines in the file
$HOME/.\_ngspice\_history. When this variable does not exist (typically
on Windows when the readline library is not officially installed), the
history file is called .history and put in the current working
directory.

The history command is part of the readline or editline package. The
readline program provides a command line editor that is configurable
through the file .inputrc. The path to this configuration file is either
found in the shell variable INPUTRC, or it is (on Unix/Linux/CYGWIN) the
file ~/.inputrc in the user's home directory. On Windows systems the
configuration file is /Users/\<username\>/.inputrc, unless the readline
library was officially installed. In that case the filename is taken
from the Windows registry and points to a location that the user
specified during installation. See
<https://cnswww.cns.cwru.edu/php/chet/readline/rltop.html> for detailed
documentation. Some useful commands are:

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-23099"></span><span style="font-family:monospace;">Left/Right arrow</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-23102"></span>Move one character to the left or right
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-23105"></span><span style="font-family:monospace;">Home/End</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-23108"></span>Move to beginning or end of line
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-23111"></span><span style="font-family:monospace;">Up/Down arrow</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-23114"></span>Cycle through the history buffer
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-23117"></span><span style="font-family:monospace;">C-_-</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-23120"></span>Undo last editing command
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-23123"></span><span style="font-family:monospace;">C-r</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-23126"></span>Incremental search backward
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-23129"></span><span style="font-family:monospace;">TAB</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-23132"></span>completion of a file name
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-23135"></span><span style="font-family:monospace;">C-ak</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-23138"></span>Erase the command line (kill)
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-23141"></span><span style="font-family:monospace;">C-y</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-23144"></span>Retrieve last kill (yank)
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-23147"></span><span style="font-family:monospace;">C-u</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-23150"></span>Erase from cursor to start of line
</div></td>
</tr>
</tbody>
</table>

