# Op\*: Perform an operating point analysis

General Form:

``` listings
op
```

Do an operating point analysis. See Chapt.
[15.3.5](#subsec__OP__Operating_Point) for more details.

