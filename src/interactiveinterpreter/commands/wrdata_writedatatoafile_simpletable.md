# Wrdata: Write data to a file \(simple table\)

General Form:

``` listings
<set wr_singlescale>
<set wr_vecnames>
<option numdgt=7>
...
wrdata [file] [vecs]
```

Writes out the vectors to file.

This is a very simple printout of data in array form. Variables are
written in pairs: scale vector, value vector. If variable is complex, a
triple is printed (scale, real, imag). If more than one vector is given,
the third column again is the default scale, the fourth the data of the
second vector. The default format is ASCII. All vectors have to stem
from the same plot, otherwise a segfault may occur. Setting
wr\_singlescale as variable, the scale vector will be printed only once,
if scale vectors are of the same length (you have to take care of that
yourself). Setting wr\_vecnames as variable, scale and data vector names
are printed on the first row. The number of significant digits is set
with option numdgt.

output example from two vectors:

``` listings
0.000000e+00  -1.845890e-06  0.000000e+00  0.000000e+00
7.629471e+06   4.243518e-06  7.629471e+06 -4.930171e-06
1.525894e+07  -5.794628e-06  1.525894e+07  4.769020e-06
2.288841e+07   5.086875e-06  2.288841e+07 -3.670687e-06
3.051788e+07  -3.683623e-06  3.051788e+07  1.754215e-06
3.814735e+07   1.330798e-06  3.814735e+07 -1.091843e-06
4.577682e+07  -3.804620e-07  4.577682e+07  2.274678e-06
5.340630e+07   9.047444e-07  5.340630e+07 -3.815083e-06
6.103577e+07  -2.792511e-06  6.103577e+07  4.766727e-06
6.866524e+07   5.657498e-06  6.866524e+07 -2.397679e-06
.... 
```

If variable appendwrite is set, the data may be added to an existing
file.

