# Circbyline\*: Enter a circuit line by line

General Form:

``` listings
circbyline line
```

Enter a circuit line by line. **line** is any circuit line, as found in
the \*.cir ngspice input files. The first line is a title line. The
entry will be finished by entering .end. Circuit parsing is then started
automatically.

Example:

``` listings
circbyline test circuit
circbyline v1 1 0 1
circbyline r1 1 0 1
circbyline .dc v1 0.5 1.5 0.1
circbyline .end
run
plot i(v1)
```

