# Define: Define a function

General Form:

``` listings
define function(arg1, arg2, ...) expression
```

Define the function with the name function and arguments arg1, arg2, ...
to be *expression*, which may involve the arguments. When the function
is later used, the arguments it is given are substituted for the formal
arguments when it was parsed. If expression is not present, any existing
definition for function is printed, and if there are no arguments then
all expressions for all currently active definitions are printed. Note
that you may have different functions defined with the same name but
different arities. Some useful definitions are:

Example:

``` listings
define max(x,y) (x > y) * x + (x <= y) * y
define min(x,y) (x < y) * x + (x >= y) * y
define limit(nom, avar) (nom + ((sgauss(0) >= 0) ? avar : -avar))  
```

