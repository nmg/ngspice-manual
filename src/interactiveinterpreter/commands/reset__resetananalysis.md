# Reset\*: Reset an analysis

General Form:

``` listings
reset
```

Throw out any intermediate data in the circuit (e.g, after a breakpoint
or after one or more analyses have been done), and re-parse the input
file. The circuit can then be re-run from it's initial state, overriding
the effect of any set or alter commands. These two need to be repated
after the reset command.

Reset may be required in simulation loops preceding any run (or tran
...) command.

Reset is required after an alterparam command
([17.5.5](#subsec_Alterparam___Change_value)) for making the parameter
change effective.

