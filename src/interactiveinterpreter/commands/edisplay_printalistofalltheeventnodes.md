# Edisplay: Print a list of all the event nodes

General Form:

``` listings
edisplay
```

Print the node names, node types, and number of events per node of all
event driven nodes generated or used by XSPICE 'A' devices. See eprint,
eprvcd, and [27.2.2](#subsec_Running_example_C3) for an example.

