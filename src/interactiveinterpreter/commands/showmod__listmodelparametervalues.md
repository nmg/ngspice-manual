# Showmod\*: List model parameter values

General Form:

``` listings
showmod models [ : parameters ] , ...
```

The showmod command operates like the show command (above) but prints
out model parameter values. The applicable forms for models are a single
letter specifying the device type letter (e.g. m, or c), a device name
(e.g. m.xbuf22.m4b), or \#modelname (e.g. \#p1).

Typical usage (e.g. for BSIM4 model):

``` listings
showmod #cmosn #cmosp : lkvth0 vth0
```

Note: there must be spaces around the \`:' that divides the device list
from the parameter list.

