# Psd: power spectral density of vectors

General Form:

``` listings
psd ave vector1 [vector2] ...
```

Calculate the single sided power spectral density of signals (vectors)
resulting from a transient analysis. Windowing is available as described
in the fft command ([17.5.27](#subsec_fft__fast_Fourier)). The FFT data
are squared, summarized, weighted and printed as total noise power up to
Nyquist frequency, and as noise voltage or current.

ave is the number of points used for averaging and smoothing in a
postprocess, useful for noisy data. A new plot vector is created that
holds the averaged results of the FFT, weighted by the frequency bin.
The result can be plotted and has the units V^2/Hz or A^2/Hz, depending
on the the input vector.

