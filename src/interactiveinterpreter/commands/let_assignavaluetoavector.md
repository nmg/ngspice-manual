# Let: Assign a value to a vector

General Form:

``` listings
let name = expr
```

Creates a new vector called name with the value specified by *expr*, an
expression as described above. If *expr* is \[\] (a zero-length vector)
then the vector becomes undefined. Individual elements of a vector may
be modified by appending a subscript to name (ex. name\[0\]). If there
are no arguments, let is the same as display.

The command **let** creates a vector in the current plot. Use
**setplot** [(17.5.65)](#subsec_Setplot__Switch_the) to create a new
plot.

There is no straightforward way to initialize a new vector. In general,
one might want let initialize a slice (i.e. name\[4:4,21:23\] = \[ 1 2 3
\]) of a multi-dimensional matrix of arbitrary type (i.e. real, complex
..), where all values and indexes are arbitrary expressions. This will
fail. The procedure is to first allocate a real vector of the
appropriate size with either vector(), unitvec(), or \[ n1 n2 n3 ... \].
The second step is to optionally change the type of the new vector (to
complex) with the j() function. The third step reshapes the dimensions,
and the final step (re)initializes the contents, like so:

> 
> 
> let a = j(vector(10))
> 
> reshape a \[2\]\[5\]
> 
> let a\[0\]\[0\] = (pi,pi)

Initialization of real vectors can be done quite efficiently with
compose:

> 
> 
> compose a values (pi, pi) (1,1) (2,sqrt(7)) (boltz,e)
> 
> reshape a \[2\]\[2\]

See also **unlet** [(17.5.88)](#subsec_Unlet__Delete_the), **compose**
[(17.5.13)](#subsec_Compose__Compose_a).

