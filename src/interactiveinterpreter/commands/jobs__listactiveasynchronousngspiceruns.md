# Jobs\*: List active asynchronous ngspice runs

General Form:

``` listings
jobs
```

Report on the asynchronous ngspice jobs currently running. Ngnutmeg
checks to see if the jobs are finished every time you execute a command.
If it is done then the data is loaded and becomes available.

