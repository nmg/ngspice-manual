# Stop\*: Set a breakpoint

General Form:

``` listings
stop [ after n] [ when value cond value ] ...
```

Set a breakpoint. The argument after **n** means stop after iteration
number \`n', and the argument **when value cond value** means stop when
the first value is in the given relation with the second value, the
possible relations being

<table>
<colgroup>
<col width="33%" />
<col width="33%" />
<col width="33%" />
</colgroup>
<tbody>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-24334"></span>Symbol
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-24337"></span>Alias
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-24340"></span>Meaning
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-24343"></span>=
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-24346"></span>eq
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-24349"></span>equal to
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-24352"></span>&lt;&gt;
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-24355"></span>ne
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-24358"></span>not equal
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-24361"></span>&gt;
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-24364"></span>gt
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-24367"></span>greater than
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-24370"></span>&lt;
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-24373"></span>lt
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-24376"></span>less than
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-24379"></span>&gt;=
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-24382"></span>ge
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-24385"></span>greater than or equal to
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-24388"></span>&lt;=
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-24391"></span>le
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-24394"></span>less than or equal to
</div></td>
</tr>
</tbody>
</table>

Symbol or alias may be used alternatively. All stop commands have to be
given in the control flow before the run command. The values above may
be node names in the running circuit, or real values. If more than one
condition is given, e.g.

stop after 4 when v(1) \> 4 when v(2) \< 2,

the conjunction of the conditions is implied. If the condition is met,
the simulation and control flow are interrupted, and ngspice waits for
user input.

In a transient simulation the \`=' or eq will only work with vector time
in commands like

stop when time = 200n.

Internally a breakpoint will be set at the time requested. Multiple
breakpoints may be set. If the first stop condition is met, the
simulation is interrupted, the commands following run or tran (e.g.
alter or altermod) are executed, then the simulation may continue at the
first resume command. The next breakpoint requires another resume to
continue automatically. Otherwise the simulation stops and ngspice waits
for user input.

If you try to stop at

stop when V(1) eq 1

(or similar) during a transient simulation, you probably will miss this
point, because it is not very likely that at any time step the vector
v(1) will have the exact value of 1. Then ngspice simply will not stop.

