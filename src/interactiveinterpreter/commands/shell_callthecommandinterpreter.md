# Shell: Call the command interpreter

General Form:

``` listings
shell [ command ]
```

Call the operating system's command interpreter; execute the specified
command or call for interactive use.

