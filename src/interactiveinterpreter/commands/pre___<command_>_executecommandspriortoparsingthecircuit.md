# Pre\_\<command\>: execute commands prior to parsing the circuit

General Form:

``` listings
pre_<command>
```

All commands in a .control ... .endc section are executed *after* the
circuit has been parsed. If you need command execution *before* circuit
parsing, you may add these commands to the general spinit or local
.spiceinit files. Another possibility is adding a leading pre\_ to a
command within the .control section of an ordinary input file, which
forces the command to be executed *before* circuit parsing. Basically
\<command\> may be any command listed in Chapt. [17.5](#sec_Commands),
however only a few commands are indeed useful here. Some examples are
given below:

Examples:

``` listings
pre_unset ngdebug
pre_set strict_errorhandling
pre_codemodel mymod.cm
```

pre\_\<command\> is available only in the *.control mode* (see
[16.4.3](#subsec_Interactive_mode_with)), *not* in *interactive mode*,
where the user may determine herself when a circuit is to be parsed,
using the source command ([17.5.75](#subsec_Source__Read_a)) .

