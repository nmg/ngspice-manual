# Trace\*: Trace nodes

General Form:

``` listings
trace [ node ...]
```

For every step of an analysis, the value of the node is printed. Several
traces may be active at once. Tracing is not applicable for all
analyses. To remove a trace, use the **delete**
([17.5.17](#subsec_Delete___Remove_a)) command.

