# Remcirc\*: Remove the current circuit

General Form:

``` listings
remcirc
```

This command removes the current circuit from the list of circuits
sourced into ngspice. To select a specific circuit, use setcirc
([17.5.64](#subsec_Setcirc___Change_the)). To load another circuit,
refer to **source** ([17.5.75](#subsec_Source__Read_a)). The new actual
circuit will be the circuit on top of the list of the remaining
circuits.

