# Version: Print the version of ngspice

General Form:

``` listings
version [-s | -f | <version id>]
```

Print out the version of ngnutmeg that is running, if invoked without
argument or with **-s** or **-f**. If the argument is a **\<version
id\>** (any string different from **-s** or **-f** is considered a
**\<version id\>** ), the command checks to make sure that the arguments
match the current version of ngspice. (This is mainly used as a Command:
line in rawfiles.)

Options description:

  - No option: The output of the command is the message you can see when
    running ngspice from the command line, no more no less.
  - **-s**(hort): A shorter version of the message you see when calling
    ngspice from the command line.
  - **-f**(ull): You may want to use this option if you want to know
    what extensions are included into the simulator and what compilation
    switches are active. A list of compilation options and included
    extensions is appended to the normal (not short) message. May be
    useful when sending bug reports.

The following example shows what the command returns in some situations:

Use of the version command:

``` listings
ngspice 10 -> version
******
** ngspice-24 : Circuit level simulation program
** The U. C. Berkeley CAD Group
** Copyright 1985-1994, Regents of the University of California.
** Please get your ngspice manual from
           http://ngspice.sourceforge.net/docs.html
** Please file your bug-reports at
           http://ngspice.sourceforge.net/bugrep.html
** Creation Date: Jan  1 2011   13:36:34
******
ngspice 2 -> 
ngspice 11 -> version 14 
Note: rawfile is version 14 (current version is 24)
ngspice 12 -> version 24
ngspice 13 -> 
```

> 
> 
> Note for developers: The option listing returned when version is
> called with the **-f** flag is built at compile time using \#ifdef
> blocks. When new compile switches are added, if you want them to
> appear on the list, you have to modify the code in misccoms.c.

