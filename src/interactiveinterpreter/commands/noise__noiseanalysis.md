# Noise\*: Noise analysis

See the .NOISE analysis ([15.3.4](#subsec__NOISE__Noise_Analysis)) for
details.

The** noise** command will generate two plots (typically named noise1
and noise2) with Noise Spectral Density Curves and Integrated Noise
data. To write these data into output file(s), you may use the following
command sequence:

Command sequence for writing noise data to file(s):

``` listings
.control 
tran 1e-6 1e-3 
write test_tran.raw 
noise V(out) vinp dec 333 1 1e8 16 
print inoise_total onoise_total 
*first option to get all of the output (two files) 
setplot noise1 
write test_noise1.raw all 
setplot noise2 
write test_noise2.raw all 
* second option (all in one raw-file) 
write testall.raw noise1.all noise2.all 
.endc
```

