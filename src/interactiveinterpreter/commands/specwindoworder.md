# specwindoworder:

This can be set to an integer in the range 2-8. This sets the order when
the Gaussian window is used in the spec and fft commands. If not set,
order 2 is used.

