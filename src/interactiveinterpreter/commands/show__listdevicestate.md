# Show\*: List device state

General Form:

``` listings
show devices [ : parameters ] , ...
```

The show command prints out tables summarizing the operating condition
of selected devices. If devices is missing, a default set of devices are
listed, if devices is a single letter, devices of that type are listed.
A device's full name may be specified to list only that device. Finally,
devices may be selected by model by using the form \#modelname.

If no parameters are specified, the values for a standard set of
parameters are listed. If the list of parameters contains a \`+', the
default set of parameters is listed along with any other specified
parameters.

For both devices and parameters, the word all has the obvious meaning.

Note: there must be spaces around the \`:' that divides the device list
from the parameter list.

