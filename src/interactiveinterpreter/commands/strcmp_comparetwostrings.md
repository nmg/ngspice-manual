# Strcmp: Compare two strings

General Form:

``` listings
strcmp _flag $string1 "string2"
```

The command compares two strings, either given by a variable (string1)
or as a string in quotes (\`string2'). \_flag is set as an output
variable to '0', if both strings are equal. A value greater than zero
indicates that the first character that does not match has a greater
value in str1 than in str2; and a value less than zero indicates the
opposite (like the C strcmp function).

