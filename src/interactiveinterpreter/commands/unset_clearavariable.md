# Unset: Clear a variable

General Form:

``` listings
unset [word ...]
```

Clear the value of the specified variable(s) (word).

