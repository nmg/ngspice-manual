# Diff: Compare vectors

General Form:

``` listings
diff plot1 plot2 [vec ...]
```

Compare all the vectors in the specified plots, or only the named
vectors if any are given. If there are different vectors in the two
plots, or any values in the vectors differ significantly, the difference
is reported. The variables diff\_abstol, diff\_reltol, and diff\_vntol
are used to determine a significant difference.

