# Unalias: Retract an alias

General Form:

``` listings
unalias [word ...]
```

Removes any aliases present for the words.

