# Rusage: Resource usage

General Form:

``` listings
rusage [resource ...]
```

Print resource usage statistics. If any resources are given, just print
the usage of that resource. Most resources require that a circuit be
loaded. Currently valid resources are:

  - time  
    Total Analysis Time
  - cputime  
    The amount of time elapsed since the last rusage cputime call.
  - totalcputime  
    Total elapsed time used so far.
  - decklineno  
    Number of lines in deck
  - netloadtime  
    Nelist loading time
  - netparsetime  
    Netlist parsing time
  - faults  
    Number of page faults and context switches (BSD only).
  - space  
    Data space used (output is depending on the operating system).
  - temp  
    Operating temperature.
  - tnom  
    Temperature at which device parameters were measured.
  - equations  
    Number of circuit equations
  - totiter  
    Total iterations
  - accept  
    Accepted time-points
  - rejected  
    Rejected time-points
  - loadtime  
    Time spent loading the circuit matrix and RHS.
  - reordertime  
    Matrix reordering time
  - lutime  
    L-U decomposition time
  - solvetime  
    Matrix solve time
  - trantime  
    Transient analysis time
  - tranpoints  
    Transient time-points
  - traniter  
    Transient iterations
  - trancuriters  
    Transient iterations for the last time point
  - tranlutime  
    Transient L-U decomposition time
  - transolvetime  
    Transient matrix solve time
  - everything  
    All of the above.
  - all  
    All of the above.

If rusage is given without any parameter, a sequence of outputs is
printed using the following rusage parameters: time, totalcputime,
space.

