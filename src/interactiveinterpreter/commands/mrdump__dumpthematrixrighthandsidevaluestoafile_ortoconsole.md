# Mrdump\*: Dump the matrix right hand side values to a file \(or to console\)

General Form:

``` listings
mrdump <filename>
```

If \<filename\> is given, the output will be appended to file
\<filename\>, otherwise dumped to your console.

Example usage after ngspice has started:

``` listings
* Dump matrix and RHS values after 10 and 20 steps
* of a transient simulation
source rc.cir
step 10
mdump m1.txt
mrdump mr1.txt
step 10
mdump m2.txt
mrdump mr2.txt
* just to continue to the end
step 10000
```

You may create a loop using the control structures (Chapt.
[17.6](#sec_Control_Structures)).

