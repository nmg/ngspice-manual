# Step\*: Run a fixed number of time-points

General Form:

``` listings
step [ number ]
```

Iterate number times, or once, and then stop.

