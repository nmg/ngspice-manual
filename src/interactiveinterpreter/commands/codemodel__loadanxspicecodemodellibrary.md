# Codemodel\*: Load an XSPICE code model library

General Form:

``` listings
codemodel [library file]
```

Load a XSPICE code model shared library file (e.g. analog.cm ...). Only
available if ngspice is compiled with the XSPICE option
(--enable-xspice) or with the Windows executable distributed since
ngspice21. This command has to be called from spinit (see Chapt.
[16.5](#sec_Standard_configuration_file)) (or .spiceinit for personal
code models, [16.6](#sec_User_defined_configuration)).

