# Setscale: Set the scale vector for the current plot

General Form:

``` listings
setscale [vector]
```

Defines the scale vector for the current plot. If no argument is given,
the current scale vector is printed. The scale vector delivers the
values for the x-axis in a 2D plot.

