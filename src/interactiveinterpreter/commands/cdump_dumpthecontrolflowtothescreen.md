# Cdump: Dump the control flow to the screen

General Form:

``` listings
cdump
```

Dumps the control sequence to the screen (all statements inside the
.control ... .endc structure before the line with cdump). Indentations
show the structure of the sequence. The example below is printed if you
add **cdump** to /examples/Monte\_Carlo/MonteCarlo.sp.

Example (abbreviated):

``` listings
let mc_runs=5
let run=0
...
define agauss(nom, avar, sig) (nom + avar/sig * sgauss(0))
define limit(nom, avar) (nom + ((sgauss(0) >=0) ? avar : -avar))
dowhile run < mc_runs
  alter c1=unif(1e-09, 0.1)
...
  ac oct 100 250k 10meg
  meas ac bw trig vdb(out) val=-10 rise=1 targ vdb(out)
+ val=-10 fall=1
  set run="$&run"
...
  let run=run + 1
end
plot db({$scratch}.allv)
echo
print {$scratch}.bwh
cdump  
```

