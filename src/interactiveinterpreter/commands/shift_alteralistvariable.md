# Shift: Alter a list variable

General Form:

``` listings
shift [varname] [number]
```

If varname is the name of a list variable, it is shifted to the left by
number elements (i.e, the number leftmost elements are removed). The
default varname is argv, and the default number is 1.

