# Resume\*: Continue a simulation after a stop

General Form:

``` listings
resume
```

Resume a simulation after a stop or interruption (control-C).

