# Sens\*: Run a sensitivity analysis

General Form:

``` listings
sens output_variable
sens output_variable ac ( DEC | OCT | LIN ) N Fstart Fstop
```

Perform a Sensitivity analysis. output\_variable is either a node
voltage (ex. v(1) or v(A,out)) or a current through a voltage source
(e.g. i(vtest)). The first form calculates DC sensitivities, the second
form AC sensitivities. The output values are in dimensions of change in
output per unit change of input (as opposed to percent change in output
or per percent change of input).

