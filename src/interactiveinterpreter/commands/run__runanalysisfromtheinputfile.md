# Run\*: Run analysis from the input file

General Form:

``` listings
run [rawfile]
```

Run the simulation as specified in the input file. If there were any of
the control lines .ac, .op, .tran, or .dc, they are executed. The output
is put in rawfile if it was given, in addition to being available
interactively.

