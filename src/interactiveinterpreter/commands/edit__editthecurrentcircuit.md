# Edit\*: Edit the current circuit

General Form:

``` listings
edit [ file-name ]
```

Print the current ngspice input file into a file, call up the editor on
that file and allow the user to modify it, and then read it back in,
replacing the original file. If a file-name is given, then edit that
file and load it, making the circuit the current one. The editor may be
defined in **** .spiceinit or spinit by a command line like 

set editor=emacs

Using MS Windows, to allow the **edit** command calling an editor, you
will have to add the editor's path to the PATH variable of the command
prompt windows (see
[here](http://en.wikipedia.org/wiki/Environment_variable#Examples_of_DOS_environment_variables)).
**edit** then calls cmd.exe with e.g. notepad++ and file-name as
parameter, if you have set

set editor=notepad++.exe

in .spiceinit or spinit.

