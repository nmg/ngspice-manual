# Load: Load rawfile data

General Form:

``` listings
load [filename] ...
```

Loads either binary or ascii format rawfile data from the files named.
The default file-name is rawspice.raw, or the argument to the -r flag if
there was
one.

