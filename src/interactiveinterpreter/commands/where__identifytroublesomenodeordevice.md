# Where\*: Identify troublesome node or device

General Form:

``` listings
where
```

When performing a transient or operating point analysis, the name of the
last node or device to cause non-convergence is saved. The where command
prints out this information so that you can examine the circuit and
either correct the problem or generate a bug report. You may do this
either in the middle of a run or after the simulator has given up on the
analysis. For transient simulation, the iplot command can be used to
monitor the progress of the analysis. When the analysis slows down
severely or hangs, interrupt the simulator (with control-C) and issue
the where command. Note that only one node or device is printed; there
may be problems with more than one node.

