# Alterparam\*: Change value of a global parameter

General form:

``` listings
alterparam paramname=pvalue
alterparam subname paramname=pvalue
```

Example (global, top level parameter):

``` listings
.param npar = 5
...
alterparam npar = 7 $ change npar from 5 to 7
reset
```

Example (parameter in a subcircuit):

``` listings
.subckt sname
.param subpar = 13
...
.ends
...
alterparam sname subpar = 11 $ change subpar from 13 to 11
reset
```

**Alterparam** operates on global parameters or on parameters in a
subcircuit defined by the .param ... statement. A subsequent call to
reset ([17.5.55](#subsec_Reset___Reset_an)) is required for the
parameter value change to become effective.

