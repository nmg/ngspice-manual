# Save\*: Save a set of outputs

General Form:

``` listings
save [all | outvec ...]
```

Save a set of outputs, discarding the rest (if not keyword all is
given). Maybe used to dramatically reduce memory (RAM) requirements if
only a few useful node voltages or branch currents are saved.

Node voltages may be saved by giving the nodename or v(nodename).
Currents through an independent voltage source are given by
i(sourcename) or *sourcename\#branch*. Internal device data
([31.1](#sec_Accessing_internal_device)) are accepted as @dev\[param\].
The syntax is identical to the .save command
([15.6.1](#subsec__SAVE_Lines)).

Note: In the .control .... .endc section save *must* occur before the
run or tran command to become effective.

If a node has been mentioned in a save command, it appears in the
working plot after a run has completed, or in the rawfile written by the
write ([17.5.93](#subsec_Write__Write_data)) command. For backward
compatibility, if there are no save commands given, all outputs are
saved. If you want to trace ([17.5.83](#subsec_Trace___Trace_nodes)) or
plot ([17.5.48](#subsec_Plot__Plot_values)) a node, you have to save it
explicitly, except for all given or no save command at all.

When the keyword all appears in the save command, all node voltages,
voltage source currents and inductor currents are saved in addition to
any other vectors listed.

Save voltage and current:

``` listings
save vd_node vs#branch v(vs_node) i(vs2)
```

Save allows to store and later access internal device parameters. e.g.
in a command like

Save internal parameters:

``` listings
save all @mn1[gm]
```

saves all standard analysis output data plus gm of transistor mn1 to
internal memory (see also [31.1](#sec_Accessing_internal_device)).

save may store data from nodes or devices residing inside of a
subcircuit:

Save voltage on node 3 (top level), node 8 (from inside subcircuit x2)
and current through vmeas (from subcircuit x1):

``` listings
save 3 x1.x2.x1.x2.8 v.x1.x1.x1.vmeas#branch
```

Save internal parameters within subcircuit:

``` listings
save @m.xmos3.mn1[gm]
```

Use commands listing expand ([17.5.39](#subsec_Listing___Print_a),
before the simulation) or display
([17.5.21](#subsec_Display__List_known), after simulation) to obtain a
list of all nodes and currents available. Please see Chapt.
[31](#cha_Model_and_Device) for an explanation of the syntax for
internal parameters.

Entering several save lines in a single .control section will accumulate
the nodes and parameters to be saved. If you want to exclude a node, you
have to get its number by calling status
([17.5.77](#subsec_Status___Display_breakpoint)) and then calling delete
number ([17.5.17](#subsec_Delete___Remove_a)).

Don't save anything:

``` listings
save none
```

Useful if shared ngspice library is used and data are immediately
tranferred to the caller via the sharedngspice interface.

