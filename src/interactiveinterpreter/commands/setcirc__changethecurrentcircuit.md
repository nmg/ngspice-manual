# Setcirc\*: Change the current circuit

General Form:

``` listings
setcirc [circuit number]
```

The current circuit is the one that is used for the simulation commands
below. When a circuit is loaded with the source command (see below,
[17.5.75](#subsec_Source__Read_a)) it becomes the current circuit.

**Setcirc** followed by 'return' without any parameters lists all
circuits loaded.

