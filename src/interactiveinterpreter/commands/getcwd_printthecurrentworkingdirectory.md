# Getcwd: Print the current working directory

General Form:

``` listings
getcwd
```

Print the current working directory.

