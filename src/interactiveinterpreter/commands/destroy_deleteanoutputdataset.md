# Destroy: Delete an output data set

General Form:

``` listings
destroy [plotnames | all]
```

Release the memory holding the output data (the given plot or all plots)
for the specified runs.

