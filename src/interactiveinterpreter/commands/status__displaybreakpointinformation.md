# Status\*: Display breakpoint information

General Form:

``` listings
status
```

Display all of the saved nodes and parameters, traces and breakpoints
currently in effect.

