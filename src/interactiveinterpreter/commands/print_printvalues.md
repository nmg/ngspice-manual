# Print: Print values

General Form:

``` listings
print [col] [line] expr ...
```

Prints the vector(s) described by the expression expr. If the col
argument is present, print the vectors named side by side. If line is
given, the vectors are printed horizontally. col is the default, unless
all the vectors named have a length of one, in which case line is the
default. The options width (default 80) and height (default 24) are
effective for this command (see asciiplot
[17.5.6](#subsec_Asciiplot__Plot_values)). The 'more' mode is the
standard mode if printing to the screen, that is after a number of lines
given by height, and after a page break printing stops with request for
answering the prompt by \<return\> (print next page), 'c' (print rest)
or 'q' (quit printing). If everything shall be printed without stopping,
put the command set nomoremode into .spiceinit
[16.6](#sec_User_defined_configuration) (or spinit
[16.5](#sec_Standard_configuration_file)). If the expression is all, all
of the vectors available are printed. Thus print col all \> filename
prints everything into the file filename in SPICE2 format. The scale
vector (time, frequency) is always in the first column unless the
variable noprintscale is true. You may use the vectors alli, allv, ally
with the print command, but then the scale vector will not be printed.

Examples:

``` listings
print all
set width=300
print v(1) > outfile.out
```

