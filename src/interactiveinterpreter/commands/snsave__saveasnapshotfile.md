# Snsave\*: Save a snapshot file

General Form:

``` listings
snsave file
```

If you run a transient simulation and interrupt it by e.g. a **stop**
breakpoint ([17.5.79](#subsec_Stop___Set_a)), you may resume simulation
immediately ([17.5.57](#subsec_Resume___Continue_a)) or store the
intermediate status in a snapshot file by snsave for resuming simulation
later (using snload ([17.5.73](#subsec_Snload___List_model))), even with
a new instance of ngspice.

Typical usage:

``` listings
Example input file for snsave
* load a circuit (including transistor models and .tran command)
* starts transient simulation until stop point
* store intermediate data to file
* begin with editing the file location
* to be run with 'ngspice adder_mos.cir'

.include adder_mos_circ.cir

.control
*cd to where all files are located
cd D:\Spice_general\ngspice\examples\snapshot
unset askquit
set noinit
*interrupt condition for the simulation
stop when time > 500n
* simulate
run
* store snapshot to file
snsave adder500.snap
quit
.endc

.END
```

adder\_mos\_circ.cir is a circuit input file, including the netlist,
.model and .tran statements.

Unfortunately snsave/snload will not work if you have XSPICE devices (or
V/I sources with polynomial statements) in your input deck.

