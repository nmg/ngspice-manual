# Source: Read a ngspice input file

General Form:

``` listings
source infile
```

For ngspice: read the ngspice input file **infile**, containing a
circuit netlist. Ngnutmeg and ngspice commands may be included in the
file, and must be enclosed between the lines .control and .endc. These
commands are executed immediately after the circuit is loaded, so a
control line of ac ... works the same as the corresponding .ac card. The
first line in any input file is considered a title line and not parsed
but kept as the name of the circuit. Thus, a ngspice command script in
**infile** must begin with a blank line and then with a .control line.
Also, any line starting with the string \`\*\#' is considered as a
control line (.control and .endc is placed around this line
automatically.). The exception to these rules are the files spinit
([16.5](#sec_Standard_configuration_file)) and .spiceinit
([16.6](#sec_User_defined_configuration)).

For ngutmeg: reads commands from the file **infile**. Lines beginning
with the character \`\*' are considered comments and are ignored.

The following search path is executed to find **infile**: current
directory (OS dependent), \<prefix\>/share/ngspice/scripts, env.
variable NGSPICE\_INPUT\_DIR (if defined), see
[16.7](#sec_Environmental_variables). This sequence may be overridden by
setting the internal sourcepath variable (see [17.7](#sec_Variables))
before calling source infile.

