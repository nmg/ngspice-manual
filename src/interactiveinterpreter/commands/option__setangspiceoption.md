# Option\*: Set a ngspice option

General Form:

``` listings
option [option=val] [option=val] ...
```

Set any of the simulator variables as listed in Chapt.
[15.1](#sec_Simulator_Variables). See this chapter also for more
information on the available options. The **option** command without any
argument lists the actual options set in the simulator (to be verified).
Multiple options may be set in a single line.

The following example demonstrates a control section, which may be added
to your circuit file to test the influence of variable trtol on the
number of iterations and on the simulation time.

Command sequence for testing option trtol:

``` listings
.control
set noinit

option trtol=1
echo 
echo trtol=1 
run
rusage traniter trantime
reset
option trtol=3
echo
echo trtol=3 
run
rusage traniter trantime
reset
option trtol=5
echo
echo trtol=5 
run
rusage traniter trantime
reset
option trtol=7
echo
echo trtol=7
run
rusage traniter trantime
plot tran1.v(out25) tran1.v(out50) v(out25)  v(out50)
.endc
```

