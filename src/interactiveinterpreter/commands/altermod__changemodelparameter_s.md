# Altermod\*: Change model parameter\(s\)

General form:

``` listings
altermod mod param = <expression>
altermod @mod[param] = <expression>
```

Example:

``` listings
altermod nc1 tox = 10e-9
altermod @nc1[tox] = 10e-9
```

**Altermod** operates on models and is used to change model parameters.
The above example will change the parameter tox in all devices using the
model nc1, which is defined as

\*\*\* BSIM3v3 model

.MODEL nc1 nmos LEVEL=8 version = 3.2.2

\+ acm = 2 mobmod = 1 capmod = 1 noimod = 1

\+ rs = 2.84E+03 rd = 2.84E+03 rsh = 45

\+ tox = 20E-9 xj = 0.25E-6 nch = 1.7E+17

\+ ...

If you invoke the model by the MOS device

M1 d g s b nc1 w=10u l=1u

you might also insert the device name M1 for mod as in

altermod M1 tox = 10e-9

The model parameter tox will be modified, however not only for device
M1, but for all devices using the associated MOS model nc1\!

If you want to run corner simulations within a single simulation flow,
the following option of altermod may be of help. The parameter set with
name modn may be overrun by the altermod command specifying a model
file. All parameter values fitting to the existing model modn will be
modified. As usual the 'reset' command (see
[17.5.55](#subsec_Reset___Reset_an)) restores the original values. The
model file (see [2.3](#sec_Device_Models)) has to use the standard
specifications for an input file, the .model section is the relevant
part. However the first line in the model file will be ignored by the
input parser, so it should contain only some title information. The
.model statement should appear then in the second or any later line.
More than one .model section may reside in the file.

General form:

``` listings
altermod mod1 [mod2 .. mod15] file = <model file name>
altermod mod1 [mod2 .. mod15] file  <model file name>
```

Example:

``` listings
altermod nch file = BSIM3_nmos.mod
altermod pch nch file BSIM4_mos.mod
```

Be careful that the new model file corresponds to the existing model
selected by modn. The existing models are defined during circuit setup
at start up of ngspice. Models have been included by .model statements
([2.3](#sec_Device_Models)) in your input file or included by the
.include command. In the example given above, the models nch (or nch and
pch) have to be already available before calling altermod. If they are
not found in the active circuit, ngspice will terminate with an error
message. There is no checking however of the version and level
parameters\! So you have to be responsible for offering model data of
the same model level (e.g. level 8 for BSIM3). Thus no new model is
selectable by altermod, but the parameters of the existing model(s) may
be changed (partially, completely, temporarily).

