# Inventory: Print circuit inventory

General Form:

``` listings
inventory
```

This commands accepts no argument and simply prints the number of
instances of a particular device in a loaded netlist.

