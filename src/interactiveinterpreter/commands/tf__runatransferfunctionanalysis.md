# Tf\*: Run a Transfer Function analysis

General Form:

``` listings
tf output_node input_source
```

The tf command performs a transfer function analysis, returning:

  - the transfer function (output/input),
  - output resistance,
  - and input resistance

between the given output node and the given input source. The analysis
assumes a small-signal DC (slowly varying) input. The following example
file

Example input file:

``` listings
* Tf test circuit
vs    1    0    dc 5
r1    1    2    100
r2    2    3    50
r3    3    0    150
r4    2    0    200

.control
tf v(3,5) vs
print all
.endc

.end
```

will yield the following output:

transfer\_function = 3.750000e-001

output\_impedance\_at\_v(3,5) = 6.662500e+001

vs\#input\_impedance = 2.000000e+002

