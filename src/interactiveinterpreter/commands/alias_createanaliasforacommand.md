# Alias: Create an alias for a command

General Form:

``` listings
alias [word] [text ...]
```

Causes word to be aliased to text. History substitutions may be used, as
in C-shell aliases.

