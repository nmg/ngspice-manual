# Linearize\*: Interpolate to a linear scale

General Form:

``` listings
linearize vec ...
```

Create a new plot with all of the vectors in the current plot, or only
those mentioned as arguments to the command, all data linearized onto an
equidistant time scale.

How to compute the fft from a transient simulation output:

``` listings
ngspice 8 -> setplot tran1
ngspice 9 -> linearize V(2)
ngspice 9 -> set specwindow=blackman
ngspice 10 -> fft V(2)
ngspice 11 -> plot mag(V(2))tstep
```

Linearize will redo the vectors **vec** or renew all vectors of the
current plot (e.g. tran3) if no arguments are given and store them into
a new plot (e.g. tran4). The new vectors are interpolated onto a linear
time scale, which is determined by the values of tstep, tstart, and
tstop in the currently active transient analysis. The currently loaded
input file must include a transient analysis (a tran command may be run
interactively before the last reset, alternately), and the current plot
must be from this transient analysis. The length of the new vector is
(tstop - tstart) / tstep + 1.5. This command is needed for example if
you want to do a fft analysis ([17.5.27](#subsec_fft__fast_Fourier)).
Please note that the parameter tstep of your transient analysis (see
Chapt. [15.3.9](#subsec__TRAN__Transient_Analysis)) has to be small
enough to get adequate resolution, otherwise the command linearize will
do sub-sampling of your signal. If no circuit is loaded and the data
have been acquired by the load ([17.5.40](#subsec_Load__Load_rawfile))
command, Linearize will take time data from transient analysis scale
vector.

