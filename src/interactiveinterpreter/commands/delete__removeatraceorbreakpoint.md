# Delete\*: Remove a trace or breakpoint

General Form:

``` listings
delete [ debug-number ... ]
```

Delete the specified saved nodes and parameters, breakpoints and traces.
The debug numbers are those shown by the status command (unless you do
status \> file, in which case the debug numbers are not printed).

