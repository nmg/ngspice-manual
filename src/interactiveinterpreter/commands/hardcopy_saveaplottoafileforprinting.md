# Hardcopy: Save a plot to a file for printing

General Form:

``` listings
hardcopy file plotargs
```

Just like plot, except that it creates a file called file containing the
plot. The file is a postscript image. As an alternative the plot(5)
format is available by setting the hcopydevtype variable to **plot5**,
and can be printed by either the plot(1) program or lpr with the -g
flag. See also Chapt. [18.6](#sec_Printing_options) for more details
(color etc.).

