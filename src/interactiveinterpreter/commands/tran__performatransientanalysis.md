# Tran\*: Perform a transient analysis

General Form:

``` listings
tran Tstep Tstop [ Tstart [ Tmax ] ] [ UIC ]
```

Perform a transient analysis. See Chapt.
[15.3.9](#subsec__TRAN__Transient_Analysis) of this manual for more
details.

An interactive transient analysis may be interrupted by issuing a
**ctrl-c** (control-C) command. The analysis then can be resumed by the
**resume** command ([17.5.57](#subsec_Resume___Continue_a)). Several
options may be set to control the simulation
([15.1.4](#subsec_Transient_Analysis_Options)).

