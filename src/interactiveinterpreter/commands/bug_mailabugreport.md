# Bug: Mail a bug report

General Form:

``` listings
bug
```

Send a bug report. Please include a short summary of the problem, the
version number and name of the operating system that you are running,
the version of ngspice that you are running, and the relevant ngspice
input file. (If you have defined BUGADDR, the mail is delivered to
there.)

