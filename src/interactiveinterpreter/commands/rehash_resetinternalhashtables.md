# Rehash: Reset internal hash tables

General Form:

``` listings
rehash
```

Recalculate the internal hash tables used when looking up UNIX commands,
and make all UNIX commands in the user's PATH available for command
completion. This is useless unless you have set unixcom first (see
above).

