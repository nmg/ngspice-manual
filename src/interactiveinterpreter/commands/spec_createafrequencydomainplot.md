# Spec: Create a frequency domain plot

General Form:

``` listings
spec start_freq stop_freq step_freq vector [vector ...] 
```

Calculates a new complex vector containing the Fourier transform of the
input vector (typically the linearized result of a transient analysis).
The default behavior is to use a Hanning window, but this can be changed
by setting the variables specwindow and specwindoworder appropriately.

Typical usage:

``` listings
ngspice 13 -> linearize 
ngspice 14 -> set specwindow = "blackman" 
ngspice 15 -> spec 10 1000000 1000 v(out) 
ngspice 16 -> plot mag(v(out))
```

Possible values for specwindow are: none, hanning, cosine, rectangular,
hamming, triangle, bartlet, blackman, gaussian and flattop. In the case
of a Gaussian window specwindoworder is a number specifying its order.
For a list of window functions see [17.5.27](#subsec_fft__fast_Fourier).

