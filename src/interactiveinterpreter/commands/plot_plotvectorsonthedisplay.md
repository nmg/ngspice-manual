# Plot: Plot vectors on the display

General Form:

``` listings
plot exprs [ylimit ylo yhi] [xlimit xlo xhi] [xindices xilo xihi]
[xcompress comp] [xdelta xdel] [ydelta ydel]
[xlog] [ylog] [loglog] [nogrid] [vs xname_expr]
[linplot] [combplot] [pointplot] [nointerp] [noretraceplot]
[xlabel word] [ylabel word] [title word] [samep] [linear]
```

Plot the given vectors or exprs on the screen (if you are on a graphics
terminal). The xlimit and ylimit arguments determine the high and low x-
and y-limits of the axes, respectively. The xindices arguments determine
what range of points are to be plotted - everything between the xilo'th
point and the xihi'th point is plotted. The xcompress argument specifies
that only one out of every comp points should be plotted. If an xdelta
or a ydelta parameter is present, it specifies the spacing between grid
lines on the X- and Y-axis. These parameter names may be abbreviated to
xl, yl, xind, xcomp, xdel, and ydel respectively.

The xname\_expr argument is an expression to use as the scale on the
x-axis. If xlog or ylog are present then the X or Y scale, respectively,
are logarithmic (loglog is the same as specifying both). The xlabel and
ylabel arguments cause the specified labels to be used for the X and Y
axes, respectively.

If samep is given, the values of the other parameters (other than
xname\_expr) from the previous plot, hardcopy, or asciiplot command are
used unless re-defined on the command line.

The title argument is used in the headline of the plot window and
replaces the default text, which is \`actual plot: first line of input
file'.

The linear keyword is used to override a default logscale plot (as in
the output for an AC analysis).

The keywords linplot, combplot and pointplot select different plot
styles. The keyword nointerp turns off interpolation of the vector data,
nogrid suppresses the drawing of gridlines, and noretraceplot may be
helpful in some cases where the plot generates an unwanted visible trace
from one line's end point to the start point of the next line.

Finally, the keyword polar generates a polar plot. To produce a smith
plot, use the keyword smith. Note that the data is transformed, so for
smith plots you will see the data transformed by the function
(x-1)/(x+1). To produce a polar plot with a smith grid but without
performing the smith transform, use the keyword smithgrid.

If you specify plot all, all vectors (including the scale vector) are
plotted versus the scale vector (see commands display
([17.5.21](#subsec_Display__List_known)) or setscale
([17.5.66](#subsec_Setscale__Set_the)) on viewing the vectors of the
current plot). The command plot ally will not plot the scale vector, but
all other 'real' y values. The command plot alli selects all current
vectors, the command plot allv all voltage vectors.

If the vector name to be plotted contains - , / or other tokens that may
be taken for operators of an expression, and plotting fails, try
enclosing the name in double quotes, e.g. plot “/vout”.

Plotting of complex vectors, as may occur after an ac simulation,
requires special considerations. Please see Chapt.
[17.5.1](#subsec_Ac___Perform_an) for
details.

