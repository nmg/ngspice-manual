# Compose: Compose a vector

General Form:

``` listings
compose name values value1 [ value2 ... ]      
compose name param = val [ param = val ... ] 
```

The first form takes the values and creates a new vector, where the
*values* may be arbitrary expressions.

The second form has the following possible parameters:

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-22365"></span><span style="font-family:monospace;">start</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-22368"></span>The value of <em>name</em>[0]
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-22371"></span><span style="font-family:monospace;">stop</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-22374"></span>The last value of <em>name</em>
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-22377"></span><span style="font-family:monospace;">step</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-22380"></span>The difference between successive elements of the created vector
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-22383"></span><span style="font-family:monospace;">lin</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-22386"></span>How many linearly spaced elements the new vector should have
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-22389"></span><span style="font-family:monospace;">log</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-22392"></span>The number of points, logarithmically spaced (<em>not working</em>)
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-22395"></span><span style="font-family:monospace;">dec</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-22398"></span>The number of points per decade, logarithmically spaced (<em>not working</em>)
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-22401"></span><span style="font-family:monospace;">center</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-22404"></span>Where to center the range of points (<em>not working</em>)
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-22407"></span><span style="font-family:monospace;">span</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-22410"></span>The size of the range of points (<em>not working</em>)
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-22413"></span><span style="font-family:monospace;">gauss</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-22416"></span>The nominal value for the used Gaussian distribution
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-22419"></span><span style="font-family:monospace;">sd</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-22422"></span>The standard deviation for the used Gaussian distribution
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-22425"></span><span style="font-family:monospace;">sigma</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-22428"></span>The sigma for the used Gaussian distribution
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-22431"></span><span style="font-family:monospace;">random</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-22434"></span>The nominal value for a uniform random distribution
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-22437"></span><span style="font-family:monospace;">rvar</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-22440"></span>The percentage variation for the uniform random distribution
</div></td>
</tr>
</tbody>
</table>

