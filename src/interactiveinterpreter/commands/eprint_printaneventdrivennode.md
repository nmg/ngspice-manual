# Eprint: Print an event driven node

General Form:

``` listings
eprint node [node]
eprint node [node] > nodeout.txt $ output redirected
```

Print an event driven node generated or used by an XSPICE 'A' device.
These nodes are vectors not organized in plots. See edisplay, eprvcd,
and Chapt. [27.2.2](#subsec_Running_example_C3) for an example. Output
redirection into a file is available.

