# Rspice\*: Remote ngspice submission

General Form:

``` listings
rspice <input file>
```

Runs a ngspice remotely taking the input file as a ngspice input file,
or the current circuit if no argument is given. Ngnutmeg or ngspice
waits for the job to complete, and passes output from the remote job to
the user's standard output. When the job is finished the data is loaded
in as with aspice. If the variable rhost is set, ngnutmeg connects to
this host instead of the default remote ngspice server machine. This
command uses the rsh command and thereby requires authentication via a
.rhosts file or other equivalent method. Note that rsh refers to the
\`remote shell' program, which may be remsh on your system; to override
the default name of rsh, set the variable remote\_shell. If the variable
rprogram is set, then rspice uses this as the pathname to the program to
run on the remote system.

Note: rspice will not acknowledge elements that have been changed via
the alter or altermod commands.

