# Undefine: Retract a definition

General Form:

``` listings
undefine function
```

Definitions for the named user-defined functions are deleted.

