# Meas\*: Measurements on simulation data

General Form (example):

``` listings
MEAS {DC|AC|TRAN|SP} result TRIG trig_variable VAL=val <TD=td> 
<CROSS=# | CROSS=LAST> <RISE=#|RISE=LAST> <FALL=#|FALL=LAST> 
<TRIG AT=time> TARG targ_variable VAL=val <TD=td>
<CROSS=# | CROSS=LAST> <RISE=#|RISE=LAST>
<FALL=#|FALL=LAST> <TRIG AT=time>
```

Most of the input forms found in [15.4](#subsec__MEAS) may be used here
with the command meas instead of .meas(ure). Using meas inside the
.control ... .endc section offers additional features compared to the
.meas use. meas will print the results as usual, but in addition will
store its measurement result (typically the token **result** given in
the command line) in a vector. This vector may be used in following
command lines of the script as an input value of another command. For
details of the command see Chapt. [15.4](#subsec__MEAS). The measurement
type SP is only available here, because a fft command will prepare the
data for SP measurement. Option autostop
([15.1.4](#subsec_Transient_Analysis_Options)) is not available.

Unfortunately par(*'expression'***)**
([15.6.6](#subsec_par__expression____Algebraic_expressions)) will not
work here, i.e. inside the .control section. You may use an expression
by the **let** command instead, giving let vec\_new = *expression*.

Replacement for par(*'expression'*) in meas inside the .control section

``` listings
let vdiff = v(n1)-v(n0) 
meas tran vtest find vdiff at=0.04e-3 
*the following will not do here:
*meas tran vtest find par('v(n1)-v(n0)') at=0.04e-3
```

