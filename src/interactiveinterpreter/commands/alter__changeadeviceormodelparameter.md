# Alter\*: Change a device or model parameter

Alter changes the value for a device or a specified parameter of a
device or model.

General Form:

``` listings
alter dev = <expression>
alter dev param = <expression>
alter @dev[param] = <expression>
```

\<*expression*\> must be real (complex isn't handled right now, integer
is fine though, but no strings. For booleans, use 0/1).

Old style (pre 3f4):

``` listings
alter device value
alter device parameter value [ parameter value ]
```

Using the old style, its first form is used by simple devices that have
one principal value (resistors, capacitors, etc.) where the second form
is for more complex devices (bjt's, etc.). Model parameters can be
changed with the second form if the name contains a \`\#'. For
specifying a list of parameters as values, start it with \`\[', followed
by the values in the list, and end with \`\]'. Be sure to place a space
between each of the values and before and after the \`\[' and \`\]'.

Some examples are given below:

Examples (Spice3f4 style):

``` listings
alter vd = 0.1
alter vg dc = 0.6
alter @m1[w]= 15e-06
alter  @vg[sin] [ -1 1.5 2MEG ]
alter @Vi[pwl] = [ 0 1.2 100p 0 ] 
```

**alter** may have vectors ([17.8.2](#subsec_Vectors)) or variables
([17.8.1](#subsec_Variables)) as parameters.

Examples (vector or variable in parameter list):

``` listings
let newfreq = 10k
alter  @vg[sin] [ -1 1.5 $&newfreq ]  $ vector
set newperiod = 150u
alter @Vi[pwl] = [ 0 1.2 $newperiod 0 ] $ variable
```

You may change a parameter of a device residing in a subcircuit, e.g. of
MOS transistor msub1 in subcircuit xm1 (see also Chapt.
[31.1](#sec_Accessing_internal_device)).

Examples (parameter of device in subcircuit):

``` listings
alter m.xm1.msub1 w = 20u
alter @m.xm1.msub1[w] = 20u
```

