# Deftype: Define a new type for a vector or plot

General Form:

``` listings
deftype [v | p] typename abbrev
```

defines types for vectors and plots. abbrev will be used to parse things
like abbrev(name) and to label axes with M\<abbrev\>, instead of
numbers. It may be omitted. Also, the command \`deftype p plottype
pattern ...' will assign plottype as the name to any plot with one of
the patterns in its Name: field.

Example:

``` listings
deftype v capacitance F
settype capacitance moscap 
plot moscap vs v(cc)
```

