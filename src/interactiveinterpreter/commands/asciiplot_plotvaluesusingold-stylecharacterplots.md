# Asciiplot: Plot values using old-style character plots

General Form:

``` listings
asciiplot plotargs
```

Produce a line printer plot of the vectors. The plot is sent to the
standard output, or you can put it into a file with asciiplot args ...
\> file. The set options width, height, and nobreak determine the width
and height of the plot, and whether there are page breaks, respectively.
The 'more' mode is the standard mode if printing to the screen, that is
after a number of lines given by height, and after a page break printing
stops with request for answering the prompt by \<return\>, 'c' or 'q'.
If everything shall be printed without stopping, put the command set
nomoremode into .spiceinit [16.6](#sec_User_defined_configuration) (or
spinit [16.5](#sec_Standard_configuration_file)). Note that you will
have problems if you try to asciiplot something with an X-scale that
isn't monotonic (i.e, something like sin(TIME) ), because asciiplot uses
a simple-minded linear interpolation. The asciiplot command doesn't deal
with log scales or the delta keywords.

