# Reshape: Alter the dimensionality or dimensions of a vector

General Form:

``` listings
reshape vector vector ...
or
reshape vector vector ... [ dimension, dimension, ... ]
or
reshape vector vector ... [ dimension ][ dimension ] ...
```

This command changes the dimensions of a vector or a set of vectors. The
final dimension may be left off and it will be filled in automatically.
If no dimensions are specified, then the dimensions of the first vector
are copied to the other vectors. An error message of the form
'dimensions of x were inconsistent' can be ignored.

Example:

``` listings
* generate vector with all (here 30) elements
let newvec=vector(30)
* reshape vector to format 3 x 10
reshape newvec [3][10]
* access elements of the reshaped vector
print newvec[0][9]
print newvec[1][5]
let newt = newvec[2][4]
```

