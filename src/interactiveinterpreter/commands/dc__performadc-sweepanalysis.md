# Dc\*: Perform a DC-sweep analysis

General Form:

``` listings
dc Source Vstart Vstop Vincr [ Source2 Vstart2 Vstop2 Vincr2 ]
```

Do a dc transfer curve analysis. See the previous Chapt.
[15.3.2](#subsec__DC__DC_Transfer) for more details. Several options may
be set ([15.1.2](#subsec_DC_Solution_Options)).

