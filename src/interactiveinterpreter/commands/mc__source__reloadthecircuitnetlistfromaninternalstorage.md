# Mc\_source\*: Reload the circuit netlist from an internal storage

General Form:

``` listings
mc_source
```

Upon reading a netlist, after its preprocessing is finished, the
modified netlist is stored internally. This command will reload this
netlist and create a new circuit inside ngspice. This command is used in
conjunction with the alterparam command.

