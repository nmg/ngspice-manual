# Eprvcd: Dump event nodes in VCD format

General Form:

``` listings
eprvcd node1 node2 .. noden [ > filename ]
```

Dump the data of the specified event driven nodes to a .vcd file. Such
files may be viewed with an vcd viewer, for example
[gtkwave](http://gtkwave.sourceforge.net/). See edisplay, eprint,
eprvcd, and [27.2.2](#subsec_Running_example_C3) for an example.

