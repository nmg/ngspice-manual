# Mdump\*: Dump the matrix values to a file \(or to console\)

General Form:

``` listings
mdump <filename>
```

If \<filename\> is given, the output will be stored in file
\<filename\>, otherwise dumped to your
console.

