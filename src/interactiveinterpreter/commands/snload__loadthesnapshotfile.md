# Snload\*: Load the snapshot file

General Form:

``` listings
snload circuit-file file 
```

**snload** reads the snapshot file generated by snsave
([17.5.74](#subsec_Snsave___List_model)). circuit-file is the original
circuit input file. After reading, the simulation may be continued by
resume ([17.5.57](#subsec_Resume___Continue_a)).

An input script for loading circuit and intermediate data, resuming
simulation and plotting is shown below:

Typical usage:

``` listings
*  SCRIPT: ADDER - 4 BIT BINARY
* script to reload circuit and continue the simulation
* begin with editing the file location
* to be started with 'ngspice adder_snload.script'

.control
* cd to where all files are located
cd D:\Spice_general\ngspice\examples\snapshot
* load circuit and snpashot file
snload adder_mos_circ.cir adder500.snap
* continue simulation
resume
* plot some node voltages
plot v(10) v(11) v(12)
.endc
```

Due to a bug we currently need the term 'script' in the title line
(first line) of the script.

