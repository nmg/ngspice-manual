# Unlet: Delete the specified vector\(s\)

General Form:

``` listings
unlet vector [ vector ... ]
```

Delete the specified vector(s). See also **let**
[(17.5.37)](#subsec_Let__Assign_a).

