# Gnuplot: Graphics output via gnuplot

General Form:

``` listings
gnuplot file plotargs
```

Like plot, but using gnuplot for graphics output and further data
manipulation. ngspice creates a file called file.plt containing the
gnuplot command sequence, a file called file.data containing the data to
be plotted, and a file called either file.eps (Postscript, this is the
default) or file.png (the compressed binary png format, when the
variable gnuplot\_terminal is set to png). It is possible to suppress
the latter hardcopy file by using a file name that starts with 'np\_'.
On Linux gnuplot is called via xterm, and offers a Gnuplot console to
manipulate the data. On Windows a plot window is opened and the command
console window is available with a mouse click. Of course you have to
have gnuplot installed on your system.

