# Cd: Change directory

General Form:

``` listings
cd [directory]
```

Change the current working directory to directory, or to the user's home
directory (Linux: HOME, MS Windows: USERPROFILE), if none is given.

