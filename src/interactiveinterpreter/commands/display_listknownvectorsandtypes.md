# Display: List known vectors and types

General Form:

``` listings
display [varname ...]
```

Prints a summary of currently defined vectors, or of the names
specified. The vectors are sorted by name unless the variable nosort is
set. The information given is the name of the vector, the length, the
type of the vector, and whether it is real or complex data.
Additionally, one vector is labeled \[scale\]. When a command such as
plot is given without a vs argument, this scale is used for the X-axis.
It is always the first vector in a rawfile, or the first vector defined
in a new plot. If you undefine the scale (i.e, let TIME = \[\]), one of
the remaining vectors becomes the new scale (which one is
unpredictable). You may set the scale to another vector of the plot with
the command ** **setscale ([17.5.66](#subsec_Setscale__Set_the)).

