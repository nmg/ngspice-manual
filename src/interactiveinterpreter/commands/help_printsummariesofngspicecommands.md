# Help: Print summaries of Ngspice commands

Prints help. This help information, however, is spice3f5-like, stemming
from 1991 and thus is outdated. If commands are given, descriptions of
those commands are printed. Otherwise help for only a few major commands
is printed. On Windows this **help** command is no longer available.
Spice3f5 compatible help may be found in the [Spice 3 User
manual](http://newton.ex.ac.uk/teaching/CDHW/Electronics2/userguide/).
For ngspice please use this manual.

