# Set: Set the value of a variable

General Form:

``` listings
set [word]
set [word = value] ...
```

Set the value of word to value, if it is present. You can set any word
to be any value, numeric or string. If no value is given then the value
is the Boolean \`true'. If you enter a string, you have to enclose it in
double quotes. Set save the lower case version of a word string.

The value of word may be inserted into a command by writing $word. If a
variable is set to a list of values that are enclosed in parentheses
(which must be separated from their values by white space), the value of
the variable is the list.

The variables used by ngspice are listed in section
[17.7](#sec_Variables).

Set entered without any parameter will list all variables set, and their
values, if applicable.

Be advised that set sets the lower case variant of word. An exceptions
to this rule is the variable sourcepath.

