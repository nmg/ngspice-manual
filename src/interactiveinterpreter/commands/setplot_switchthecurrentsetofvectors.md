# Setplot: Switch the current set of vectors

General Form:

``` listings
setplot
setplot [plotname]
setplot previous
setplot next
setplot new
```

Set the current plot to the plot with the given name, or if no name is
given, prompt the user with a list of all plots generated so far. (Note
that the plots are named as they are loaded, with names like tran1 or
op2. These names are shown by the setplot and display commands and are
used by diff, below.) If the \`New' item is selected, a new plot is
generated that has no vectors defined.

Note that here the word plot refers to a group of vectors that are the
result of one ngspice run. When more than one file is loaded in, or more
than one plot is present in one file, ngspice keeps them separate and
only shows you the vectors in the current plot with the display
([17.5.21](#subsec_Display__List_known)) command. setplot previous will
show the previous plot in the plot list, if available, setplot next the
next plot. If not avaialble, a warning is issued and the current plot
stays active.

