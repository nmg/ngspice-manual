# Write: Write data to a file \(Spice3f5 format\)

General Form:

``` listings
write [file] [exprs]
```

Writes out the expressions to file.

First vectors are grouped together by plots, and written out as such
(i.e. if the expression list contained three vectors from one plot and
two from another, then two plots are written, one with three vectors and
one with two). Additionally, if the scale for a vector isn't present it
is automatically written out as well.

The default format is a compact binary, but this can be changed to ASCII
with the set filetype=ascii command. The default file name is either
rawspice.raw or the argument of the optional **-r** flag on the command
line, and the default expression list is all.

If variable **appendwrite** is set, the data may be added to an existing
file.

