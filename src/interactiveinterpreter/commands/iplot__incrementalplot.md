# Iplot\*: Incremental plot

General Form:

``` listings
iplot [ node ...]
```

Incrementally plot the values of the nodes while ngspice runs. The iplot
command can be used with the where command to find trouble spots in a
transient simulation.

The @name\[param\] notation ([31.1](#sec_Accessing_internal_device))
might not work yet.

