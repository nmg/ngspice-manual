# AVG|MIN|MAX|PP|RMS|MIN\_AT|MAX\_AT

General form 7:

``` listings
.MEASURE {DC|AC|TRAN|SP} result
+ {AVG|MIN|MAX|PP|RMS|MIN_AT|MAX_AT}
+ out_variable <TD=td> <FROM=val> <TO=val>
```

Measure statements:

.measure tran ymax MAX v(2) from=2m to=3m

returns the maximum value of v(2) inside the time interval between 2 ms
and 3 ms.

.measure tran tymax MAX\_AT v(2) from=2m to=3m

returns the time point of the maximum value of v(2) inside the time
interval between 2 ms and 3 ms.

.measure tran ypp PP v(1) from=2m to=4m

returns the peak to peak value of v(1) inside the time interval between
2 ms and 4 ms.

.measure tran yrms RMS v(1) from=2m to=4m

returns the root mean square value of v(1) inside the time interval
between 2 ms and 4 ms.

.measure tran yavg AVG v(1) from=2m to=4m

returns the average value of v(1) inside the time interval between 2 ms
and 4 ms.

