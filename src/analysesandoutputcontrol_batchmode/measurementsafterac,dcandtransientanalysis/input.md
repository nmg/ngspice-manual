# Input

In the following lines you will get some explanation on the .measure
commands. A simple simulation file with two sines of different
frequencies may serve as an example. The transient simulation delivers
time as the independent variable and two voltages as output (dependent
variables).

Input file:

``` listings
File: simple-meas-tran.sp
* Simple .measure examples
* transient simulation of two sine
* signals with different frequencies
vac1 1 0 DC 0 sin(0 1 1k 0 0)
vac2 2 0 DC 0 sin(0 1.2 0.9k 0 0)
.tran 10u 5m
*
.measure tran ... $ for the different inputs see below!
*
.control
run
plot v(1) v(2)
.endc
.end
```

After displaying the general syntax of the .measure statement, some
examples are posted, referring to the input file given above.

