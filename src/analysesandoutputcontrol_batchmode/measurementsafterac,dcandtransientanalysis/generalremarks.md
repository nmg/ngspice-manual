# General remarks

The measure type {DC|AC|TRAN|SP} depends on the data that is to be
evaluated, either originating from a dc analysis, an ac analysis, or a
transient simulation. The type SP to analyze a spectrum from the spec or
fft commands is only available when executed in a meas command, see
[17.5.42](#subsec_Meas___Mesurements_on).

**result** will be a vector containing the result of the measurement.
trig\_variable, targ\_variable, and out\_variable are vectors stemming
from the simulation, e.g. a voltage vector v(out).

VAL**=**val expects a real number val. It may be as well a parameter
delimited by '' or {} expanding to a real number.

TD**=**td and AT**=**time expect a time value if measure type is tran.
For ac and sp AT will be a frequency value, TD is ignored. For dc
analysis AT is a voltage (or current), TD is ignored as well.

CROSS**=\#** requires an integer number \#. CROSS**=**LAST is possible
as well. The same is expected by RISE and FALL.

Frequency and time values may start at 0 and extend to positive real
numbers. Voltage (or current) inputs for the independent (scale) axis in
a dc analysis may start or end at arbitrary real valued numbers.

Please note that not all of the .measure commands have been implemented.

