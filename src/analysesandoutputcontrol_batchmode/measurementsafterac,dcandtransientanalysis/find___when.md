# Find ... When

The FIND and WHEN functions allow to measure any dependent or
independent time, frequency, or dc parameter, when two signals cross
each other or a signal crosses a given value. Measurements start after a
delay TD and may be restricted to a range between FROM and TO.

General form 2:

``` listings
.MEASURE {DC|AC|TRAN|SP} result WHEN out_variable=val
+ <TD=td> <FROM=val> <TO=val> <CROSS=# | CROSS=LAST>
+ <RISE=# | RISE=LAST> <FALL=# | FALL=LAST>
```

Measure statement:

.measure tran teval WHEN v(2)=0.7 CROSS=LAST

measures the time point when v(2) crosses 0.7 V for the last time (any
slope).

General form 3:

``` listings
.MEASURE {DC|AC|TRAN|SP} result
+ WHEN out_variable=out_variable2
+ <TD=td> <FROM=val> <TO=val> <CROSS=# | CROSS=LAST>
+ <RISE=# | RISE=LAST> <FALL=# | FALL=LAST>
```

Measure statement:

.measure tran teval WHEN v(2)=v(1) RISE=LAST

measures the time point when v(2) and v(1) are equal, v(2) rising for
the last time.

General form 4:

``` listings
.MEASURE {DC|AC|TRAN|SP} result FIND out_variable
+ WHEN out_variable2=val <TD=td> <FROM=val> <TO=val>
+ <CROSS=# | CROSS=LAST> <RISE=# | RISE=LAST>
+ <FALL=# | FALL=LAST>
```

Measure statement:

.measure tran yeval FIND v(2) WHEN v(1)=-0.4 FALL=LAST

returns the dependent (y) variable drawn from v(2) at the time point
when v(1) equals a value of -0.4, v(1) falling for the last time.

General form 5:

``` listings
.MEASURE {DC|AC|TRAN|SP} result FIND out_variable
+ WHEN out_variable2=out_variable3  <TD=td>
+ <CROSS=# | CROSS=LAST>
+ <RISE=#|RISE=LAST> <FALL=#|FALL=LAST>
```

Measure statement:

.measure tran yeval FIND v(2) WHEN v(1)=v(3) FALL=2

returns the dependent (y) variable drawn from v(2) at the time point
when v(1) crosses v(3), v(1) falling for the second time.

General form 6:

``` listings
.MEASURE {DC|AC|TRAN|SP} result FIND out_variable AT=val
```

Measure statement:

.measure tran yeval FIND v(2) AT=2m

returns the dependent (y) variable drawn from v(2) at the time point 2
ms (given by AT=time).

