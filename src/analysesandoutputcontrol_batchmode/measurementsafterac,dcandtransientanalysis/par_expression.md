# par\(*'expression'***\)**

The par(*'expression'*) option
([15.6.6](#subsec_par__expression____Algebraic_expressions)) allows to
use algebraic expressions on the .measure lines. Every out\_variable may
be replaced by par(*'expression'*) using the general forms 1…9 described
above. Internally par(*'expression'*) is substituted by a vector
according to the rules of the B source (Chapt.
[5.1](#sec_B_source__ASRC_)). A typical example of the general form is
shown below:

General form 10:

``` listings
.MEASURE {DC|TRAN|AC|SP} result
+ FIND par('expression') AT=val
```

The measure statement

.measure tran vtest find par('(v(2)\*v(1))') AT=2.3m

returns the product of the two voltages at time point 2.3 ms.

Note that a B-source, and therefore the par('...') feature, operates on
values of type complex in AC analysis mode.

