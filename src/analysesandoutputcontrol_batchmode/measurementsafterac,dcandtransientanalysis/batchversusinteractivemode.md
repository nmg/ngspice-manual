# batch versus interactive mode

.meas analysis may not be used in batch mode (**-b** command line
option), if an output file (rawfile) is given at the same time (**-r**
rawfile command line option). In this batch mode ngspice will write its
simulation output data directly to the output file. The data is not kept
in memory, thus is no longer available for further analysis. This is
made to allow a very large output stream with only a relatively small
memory usage. For .meas to be active you need to run the batch mode with
a .plot or .print command. A better alternative may be to start ngspice
in interactive mode.

If you need batch like operation, you may add a .control ... .endc
section to the input file:

Example:

``` listings
*input file
...
.tran 1ns 1000ns
...
*********************************
.control
run
write outputfile data 
.endc
*********************************
.end
```

and start ngspice in interactive mode, e.g. by running the command

ngspice inputfile .

.meas\<ure\> then prints its user-defined data analysis to the standard
output. The analysis includes propagation, delay, rise time, fall time,
peak-to-peak voltage, minimum or maximum voltage, the integral or
derivative over a specified period and several other user defined
values.

