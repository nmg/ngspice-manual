# param

General form 9:

``` listings
.MEASURE {DC|AC|TRAN|SP} result  param='expression'
```

Measure statement:

.param fval=5

.measure tran yadd param='fval + 7'

will evaluate the given expression fval + 7 and return the value 12.

*'Expression'* is evaluated according to the rules given in Chapt.
[2.8.5](#subsec_Syntax_of_expressions) during start up of ngspice. It
may contain parameters defined with the .param statement. It may also
contain parameters resulting from preceding .meas statements.

.param vout\_diff=50u 

...

.measure tran tdiff TRIG AT=1m TARG v(2) VAL=-0.8 CROSS=3

.meas tran bw\_chk param='(tdiff \< vout\_diff) ? 1 : 0'

will evaluate the given ternary function and return the value 1 in
bw\_chk, if tdiff measured is smaller than parameter vout\_diff.

The expression may not contain vectors like v(10), e.g. anything
resulting directly from a simulation. This may be handled with the
following .meas command option.

