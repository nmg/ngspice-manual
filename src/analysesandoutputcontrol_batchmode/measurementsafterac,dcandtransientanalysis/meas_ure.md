# .meas\(ure\)

The **.meas** or **.measure** statement (and its equivalent **meas**
command, see Chapt. [17.5.42](#subsec_Meas___Mesurements_on)) are used
to analyze the output data of a tran, ac, or dc simulation. The command
is executed immediately after the simulation has finished.

