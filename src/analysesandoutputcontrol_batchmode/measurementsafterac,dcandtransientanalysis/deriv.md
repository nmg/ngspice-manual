# Deriv

General form:

``` listings
.MEASURE {DC|AC|TRAN|SP} result DERIV<ATIVE> out_variable 
+ AT=val

.MEASURE {DC|AC|TRAN|SP} result DERIV<ATIVE> out_variable
+ WHEN out_variable2=val <TD=td>
+ <CROSS=# | CROSS=LAST> <RISE=#|RISE=LAST>
+ <FALL=#|FALL=LAST>

.MEASURE {DC|AC|TRAN|SP} result DERIV<ATIVE> out_variable
+ WHEN out_variable2=out_variable3
+ <TD=td> <CROSS=# | CROSS=LAST>
+ <RISE=#|RISE=LAST> <FALL=#|FALL=LAST>
```

