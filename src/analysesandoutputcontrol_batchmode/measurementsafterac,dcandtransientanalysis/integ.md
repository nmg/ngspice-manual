# Integ

General form 8:

``` listings
.MEASURE {DC|AC|TRAN|SP} result INTEG<RAL> out_variable
+ <TD=td> <FROM=val> <TO=val>
```

Measure statement:

.measure tran yint INTEG v(2) from=2m to=3m

returns the area under v(2) inside the time interval between 2 ms and 3
ms.

