# .SAVE: Name vector\(s\) to be saved in raw file

General form:

``` listings
.save vector vector vector ...
```

Examples:

``` listings
.save i(vin) node1 v(node2)
.save @m1[id] vsource#branch
.save all @m2[vdsat]
```

The vectors listed on the .SAVE line are recorded in the rawfile for use
later with ngspice or ngnutmeg (ngnutmeg is just the data-analysis half
of ngspice, without the ability to simulate). The standard vector names
are accepted. Node voltages may be saved by giving the *nodename* or
v(*nodename*). Currents through an independent voltage source are given
by i(*sourcename*) or *sourcename*\#*branch*. Internal device data are
accepted as @*dev*\[*param*\].

If no .SAVE line is given, then the default set of vectors is saved
(node voltages and voltage source branch currents). If .SAVE lines are
given, only those vectors specified are saved. For more discussion on
internal device data, e.g. @m1\[id\], see Appendix, Chapt.
[31.1](#sec_Accessing_internal_device). If you want to save internal
data in addition to the default vector set, add the parameter **all** to
the additional vectors to be saved. If the command .save vm(out) is
given, and you store the data in a rawfile, only the original data
v(*out*) are stored. The request for storing the magnitude is ignored,
because this may be added later during rawfile data evaluation with
ngnutmeg or ngspice. See also the section on the interactive command
interpreter (Chapt. [17.5](#sec_Commands)) for information on how to use
the rawfile.

