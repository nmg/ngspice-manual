# .PLOT Lines

.plot creates a printer plot output.

General form:

``` listings
.plot pltype ov1 <(plo1, phi1)> <ov2 <(plo2, phi2)> ... ov8>
```

Examples:

``` listings
.plot dc v(4) v(5) v(1)
.plot tran v(17, 5) (2, 5) i(vin) v(17) (1, 9)
.plot ac vm(5) vm(31, 24) vdb(5) vp(5)
.plot disto hd2 hd3(R) sim2
.plot tran v(5, 3) v(4) (0, 5) v(7) (0, 10)
```

The .plot line defines the contents of one plot of from one to eight
output variables. pltype is the type of analysis (DC, AC, TRAN, NOISE,
or DISTO) for which the specified outputs are desired. The syntax for
the ov*i* is identical to that for the .print line and for the plot
command in the interactive mode.

The overlap of two or more traces on any plot is indicated by the letter
\`X'. When more than one output variable appears on the same plot, the
first variable specified is printed as well as plotted. If a printout of
all variables is desired, then a companion .print line should be
included. There is no limit on the number of .plot lines specified for
each type of analysis. The par(*'expression'*) option
([15.6.6](#subsec_par__expression____Algebraic_expressions)) allows to
use algebraic expressions in the .plot lines.

