# .PRINT Lines

General form:

``` listings
.print prtype ov1 <ov2 ... ov8>
```

Examples:

``` listings
.print tran v(4) i(vin)
.print dc v(2) i(vsrc) v(23, 17)
.print ac vm(4, 2) vr(7) vp(8, 3)
```

The .print line defines the contents of a tabular listing of one to
eight output variables. prtype is the type of the analysis (DC, AC,
TRAN, NOISE, or DISTO) for which the specified outputs are desired. The
form for voltage or current output variables is the same as given in the
previous section for the print command; Spice2 restricts the output
variable to the following forms (though this restriction is not enforced
by ngspice):

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-20254"></span><span style="font-family:monospace;">V(N1&lt;,N2&gt;)</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-20257"></span>specifies the voltage difference between nodes N1 and N2. If N2 (and the preceding comma) is omitted, ground (0) is assumed. See the <span style="font-family:monospace;">print</span> command in the previous section for more details. For compatibility with SPICE2, the following five additional values can be accessed for the ac analysis by replacing the `V' in V(N1,N2) with:
</div></td>
</tr>
<tr class="even">
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-20263"></span>
<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-20282"></span>VR
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-20285"></span>Real part
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-20288"></span>VI
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-20291"></span>Imaginary part
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-20294"></span>VM
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-20297"></span>Magnitude
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-20300"></span>VP
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-20303"></span>Phase
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-20306"></span>VDB
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-20309"></span><span class="math inline">20<em>l</em><em>o</em><em>g</em>10(<em>m</em><em>a</em><em>g</em><em>n</em><em>i</em><em>t</em><em>u</em><em>d</em><em>e</em>)</span>
</div></td>
</tr>
</tbody>
</table>
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-20312"></span><span style="font-family:monospace;">I(VXXXXXXX)</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-20315"></span>specifies the current flowing in the independent voltage source named VXXXXXXX. Positive current flows from the positive node, through the source, to the negative node. (Not yet implemented: For the ac analysis, the corresponding replacements for the letter <span style="font-family:monospace;">I</span> may be made in the same way as described for voltage outputs.)
</div></td>
</tr>
</tbody>
</table>

Output variables for the noise and distortion analyses have a different
general form from that of the other analyses. There is no limit on the
number of .print lines for each type of analysis. The
par**(***'expression'***)** option
([15.6.6](#subsec_par__expression____Algebraic_expressions)) allows to
use algebraic expressions in the .print lines. **.width**
([15.6.7](#subsec__width)) selects the maximum number of characters per
line.

