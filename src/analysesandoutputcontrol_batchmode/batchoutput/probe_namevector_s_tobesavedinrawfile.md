# .PROBE: Name vector\(s\) to be saved in raw file

General form:

``` listings
.probe vector <vector vector ...>
```

Examples:

``` listings
.probe i(vin) input output
.probe @m1[id]
```

Same as .SAVE (see Chapt. [15.6.1](#subsec__SAVE_Lines)).

