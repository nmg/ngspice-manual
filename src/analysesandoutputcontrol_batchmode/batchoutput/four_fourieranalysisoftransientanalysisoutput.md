# .FOUR: Fourier Analysis of Transient Analysis Output

General form:

``` listings
.four freq ov1 <ov2 ov3 ...>
```

Examples:

``` listings
.four 100K v(5)
```

The .four (or Fourier) line controls whether ngspice performs a Fourier
analysis as a part of the transient analysis. freq is the fundamental
frequency, and ov1 is the desired vector to be analyzed. The Fourier
analysis is performed over the interval \<TSTOP-period, TSTOP\>, where
TSTOP is the final time specified for the transient analysis, and period
is one period of the fundamental frequency. The dc component and the
first nine harmonics are determined. For maximum accuracy, TMAX (see the
.tran line) should be set to period/100.0 (or less for very high-Q
circuits). The par(*'expression'*) option
([15.6.6](#subsec_par__expression____Algebraic_expressions)) allows to
use algebraic expressions in the .four lines.

