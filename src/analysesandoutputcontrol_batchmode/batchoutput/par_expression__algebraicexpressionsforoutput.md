# par\(*'expression'***\): Algebraic expressions for output**

General form:

``` listings
par('expression')
output=par('expression')  $ not in .measure ac
```

Examples:

``` listings
.four 1001 sq1=par('v(1)*v(1)')
.measure tran vtest find par('(v(2)*v(1))') AT=2.3m
.print tran output=par('v(1)/v(2)') v(1) v(2)
.plot dc v(1) diff=par('(v(4)-v(2))/0.01') out222
```

With the output lines .four, .plot, .print, .save and in .measure
evaluation it is possible to add algebraic expressions for output, in
addition to vectors. All of these output lines accept
par**(***'expression'***)**, where expression is any expression valid
for a B source (see Chapt. [5.1](#sec_B_source__ASRC_)). Thus expression
may contain predefined functions, numerical values, constants, simulator
output like v(n1) or i(vdb), parameters predefined by a .param
statement, and the variables hertz, temper, and time. Note that a
B-source, and therefore the par('...') feature, operates on values of
type complex in AC analysis mode.

Internally the expression is replaced by a generated voltage node that
is the output of a B source, one node, and the B source implementing
par**(**'...'**)**. Several par('...') are allowed in each line, up to
99 per input file. The internal nodes are named pa\_00 to pa\_99. An
error will occur if the input file contains any of these reserved node
names.

In .four, .plot, .print, .save, but not in .measure, an alternative
syntax  
output**=**par(*'expression'***)** is possible. par(*'expression'***)**
may be used as described above. output is the name of the new node to
replace the expression. So output has to be unique and a valid node
name.

The syntax of output**=**par(*expression***)** is strict, no spaces
between par and (', or between ( and ' are allowed, (' and ') both are
required. Also there is not much error checking on your input, if there
is a typo, for example, an error may pop up at an unexpected place.

