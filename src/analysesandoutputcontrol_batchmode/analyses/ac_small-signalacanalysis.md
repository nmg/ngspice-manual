# .AC: Small-Signal AC Analysis

General form:

``` listings
.ac dec nd fstart fstop
.ac oct no fstart fstop
.ac lin np fstart fstop
```

Examples:

``` listings
.ac dec 10 1 10K
.ac dec 10 1K 100MEG
.ac lin 100 1 100HZ
```

**dec** stands for decade variation, and **nd** is the number of points
per decade. **oct** stands for octave variation, and **no** is the
number of points per octave. **lin** stands for linear variation, and
**np** is the number of points. **fstart** is the starting frequency,
and **fstop** is the final frequency. If this line is included in the
input file, Ngspice performs an AC analysis of the circuit over the
specified frequency range. Note that in order for this analysis to be
meaningful, at least one independent source must have been specified
with an ac value. Typically it does not make much sense to specify more
than one ac source. If you do, the result will be a superposition of all
sources and difficult to interpret.

Example:

``` listings
Basic RC circuit
r 1 2 1.0
c 2 0 1.0
vin 1 0 dc 0 ac 1  $ <--- the ac source
.options noacct
.ac dec 10 .01 10
.plot ac  vdb(2) xlog
.end
```

In this AC (or 'small signal') analysis all non-linear devices are
linearized around their actual DC operating point. All L and C devices
get their imaginary value that depends on the actual frequency step.
Each output vector will be calculated relative to the input voltage
(current) given by the AC value (\(V_{in}\) equals 1 in the example
above). The resulting node voltages (and branch currents) are complex
vectors. Therefore one has to be careful using the plot command,
specifically, one may use the variants of vxx(node) described in Chapt.
[15.6.2](#subsec__PRINT_Lines) like vdb(2) (see also the above example).

