# .DISTO: Distortion Analysis

General form:

``` listings
.disto dec nd fstart fstop <f2overf1>
.disto oct no fstart fstop <f2overf1>
.disto lin np fstart fstop <f2overf1>
```

Examples:

``` listings
.disto dec 10 1kHz 100MEG
.disto dec 10 1kHz 100MEG 0.9
```

The .disto line does a small-signal distortion analysis of the circuit.
A multi-dimensional Volterra series analysis is done using
multi-dimensional Taylor series to represent the nonlinearities at the
operating point. Terms of up to third order are used in the series
expansions.

If the optional parameter **f2overf1** is not specified, .disto does a
harmonic analysis - i.e., it analyses distortion in the circuit using
only a single input frequency \(F_{1}\), which is swept as specified by
arguments of the .disto command exactly as in the .ac command. Inputs at
this frequency may be present at more than one input source, and their
magnitudes and phases are specified by the arguments of the **distof1**
keyword in the input file lines for the input sources (see the
description for independent sources). (The arguments of the **distof2**
keyword are not relevant in this case).

The analysis produces information about the AC values of all node
voltages and branch currents at the harmonic frequencies \(2F_{1}\) and
, vs. the input frequency \(F_{1}\) as it is swept. (A value of 1 (as a
complex distortion output) signifies
\(\cos\left( {2\pi\left( {2F_{1}} \right)t} \right)\) at \(2F_{1}\) and
\(\cos\left( {2\pi\left( {3F_{1}} \right)t} \right)\) at \(3F_{1}\),
using the convention that 1 at the input fundamental frequency is
equivalent to \(\cos\left( {2\pi F_{1}t} \right)\).) The distortion
component desired (\(2F_{1}\) or \(3F_{1}\)) can be selected using
commands in ngnutmeg, and then printed or plotted. (Normally, one is
interested primarily in the magnitude of the harmonic components, so the
magnitude of the AC distortion value is looked at). It should be noted
that these are the AC values of the actual harmonic components, and are
not equal to HD2 and HD3. To obtain HD2 and HD3, one must divide by the
corresponding AC values at \(F_{1}\), obtained from an .ac line. This
division can be done using ngnutmeg commands.

If the optional **f2overf1** parameter is specified, it should be a real
number between (and not equal to) 0.0 and 1.0; in this case, .disto does
a spectral analysis. It considers the circuit with sinusoidal inputs at
two different frequencies \(F_{1}\) and \(F_{2}\). \(F_{1}\) is swept
according to the .disto control line options exactly as in the .ac
control line. \(F_{2}\) is kept fixed at a single frequency as \(F_{1}\)
sweeps - the value at which it is kept fixed is equal to f2overf1 times
fstart. Each independent source in the circuit may potentially have two
(superimposed) sinusoidal inputs for distortion, at the frequencies
\(F_{1}\) and \(F_{2}\). The magnitude and phase of the \(F_{1}\)
component are specified by the arguments of the **distof1** keyword in
the source's input line (see the description of independent sources);
the magnitude and phase of the \(F_{2}\) component are specified by the
arguments of the **distof2** keyword. The analysis produces plots of all
node voltages/branch currents at the intermodulation product frequencies
\(F_{1} + F_{2}\), \(F_{1} - F_{2}\), and
\(\left( {2F_{1}} \right) - F_{2}\), vs the swept frequency \(F_{1}\).
The IM product of interest may be selected using the setplot command,
and displayed with the print and plot commands. It is to be noted as in
the harmonic analysis case, the results are the actual AC voltages and
currents at the intermodulation frequencies, and need to be normalized
with respect to .ac values to obtain the IM parameters.

If the **distof1** or **distof2** keywords are missing from the
description of an independent source, then that source is assumed to
have no input at the corresponding frequency. The default values of the
magnitude and phase are 1.0 and 0.0 respectively. The phase should be
specified in degrees.

It should be carefully noted that the number **f2overf1** should ideally
be an irrational number, and that since this is not possible in
practice, efforts should be made to keep the denominator in its
fractional representation as large as possible, certainly above 3, for
accurate results (i.e., if **f2overf1** is represented as a fraction
\(\frac{A}{B}\), where \(A\) and \(B\) are integers with no common
factors, \(B\) should be as large as possible; note that \(A < B\)
because **f2overf1** is constrained to be \(< 1\)). To illustrate why,
consider the cases where **f2overf1** is 49/100 and 1/2. In a spectral
analysis, the outputs produced are at \(F_{1} + F_{2}\),
\(F_{1} - F_{2}\) and \(2F_{1} - F_{2}\). In the latter case,
\(F_{1} - F_{2} = F_{2}\), so the result at the \(F_{1} - F_{2}\)
component is erroneous because there is the strong fundamental \(F_{2}\)
component at the same frequency. Also,
\(F_{1} + F_{2} = 2F_{1} - F_{2}\) in the latter case, and each result
is erroneous individually. This problem is not there in the case where
**f2overf1** = 49/100, because \(F_{1} - F_{2} = 51/100\)
\(F_{1} < > 49/100\) \(F_{1} = F_{2}\). In this case, there are two very
closely spaced frequency components at \(F_{2}\) and \(F_{1} - F_{2}\).
One of the advantages of the Volterra series technique is that it
computes distortions at mix frequencies expressed symbolically (i.e.
\(nF_{1} + mF_{2}\)), therefore one is able to obtain the strengths of
distortion components accurately even if the separation between them is
very small, as opposed to transient analysis for example. The
disadvantage is of course that if two of the mix frequencies coincide,
the results are not merged together and presented (though this could
presumably be done as a postprocessing step). Currently, the interested
user should keep track of the mix frequencies himself or herself and add
the distortions at coinciding mix frequencies together should it be
necessary.

Only a subset of the ngspice nonlinear device models supports distortion
analysis. These are

  - Diodes (DIO),
  - BJT,
  - JFET (level 1),
  - MOSFETs (levels 1, 2, 3, 9, and BSIM1),
  - MESFET (level 1).

