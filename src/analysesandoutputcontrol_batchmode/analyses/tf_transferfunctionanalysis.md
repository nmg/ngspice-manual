# .TF: Transfer Function Analysis

General form:

``` listings
.tf outvar insrc
```

Examples:

``` listings
.tf v(5, 3) VIN
.tf i(VLOAD) VIN
```

The .tf line defines the small-signal output and input for the dc
small-signal analysis. **outvar** is the small signal output variable
and **insrc** is the small-signal input source. If this line is
included, ngspice computes the dc small-signal value of the transfer
function (output/input), input resistance, and output resistance. For
the first example, ngspice would compute the ratio of V(5, 3) to VIN,
the small-signal input resistance at VIN, and the small signal output
resistance measured across nodes 5 and 3.

