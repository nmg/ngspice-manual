# .TRAN: Transient Analysis

General form:

``` listings
.tran tstep tstop <tstart <tmax>> <uic>
```

Examples:

``` listings
.tran 1ns 100ns
.tran 1ns 1000ns 500ns
.tran 10ns 1us
```

**tstep** is the printing or plotting increment for line-printer output.
For use with the post-processor, **tstep** is the suggested computing
increment. **tstop** is the final time, and **tstart** is the initial
time. If **tstart** is omitted, it is assumed to be zero. The transient
analysis always begins at time zero. In the interval \<zero,
**tstart**\>, the circuit is analyzed (to reach a steady state), but no
outputs are stored. In the interval \<**tstart**, **tstop**\>, the
circuit is analyzed and outputs are stored. **tmax** is the maximum
stepsize that ngspice uses; for default, the program chooses either
**tstep** or (**tstop**-**tstart**)/50.0, whichever is smaller. **tmax**
is useful when one wishes to guarantee a computing interval that is
smaller than the printer increment, **tstep**.

An initial transient operating point at time zero is calculated
according to the following procedure: all independent voltages and
currents are applied with their time zero values, all capacitances are
opened, inductances are shorted, the non linear device equations are
solved iteratively.

**uic** (use initial conditions) is an optional keyword that indicates
that the user does not want ngspice to solve for the quiescent operating
point before beginning the transient analysis. If this keyword is
specified, ngspice uses the values specified using IC=... on the various
elements as the initial transient condition and proceeds with the
analysis. If the .ic control line has been specified (see
[15.2.2](#subsec__IC__Set_Initial)), then the node voltages on the .ic
line are used to compute the initial conditions for the devices. IC=...
will take precedence over the values given in the .ic control line. If
neither IC=... nor the .ic control line is given for a specific node,
node voltage zero is assumed.

Look at the description on the .ic control line
([15.2.2](#subsec__IC__Set_Initial)) for its interpretation when **uic**
is not specified.

