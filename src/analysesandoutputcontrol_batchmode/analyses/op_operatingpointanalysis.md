# .OP: Operating Point Analysis

General form:

``` listings
.op
```

The inclusion of this line in an input file directs ngspice to determine
the dc operating point of the circuit with inductors shorted and
capacitors opened.

Note: a DC analysis is automatically performed prior to a transient
analysis to determine the transient initial conditions, and prior to an
AC small-signal, Noise, and Pole-Zero analysis to determine the
linearized, small-signal models for nonlinear devices (see the
KEEPOPINFO variable [15.1.2](#subsec_DC_Solution_Options)).

