# .DC: DC Transfer Function

General form:

``` listings
.dc srcnam vstart vstop vincr [src2 start2 stop2 incr2]
```

Examples:

``` listings
.dc VIN 0.25 5.0 0.25
.dc VDS 0 10 .5 VGS 0 5 1
.dc VCE 0 10 .25 IB 0 10u 1u
.dc RLoad 1k 2k 100
.dc TEMP -15 75 5
```

The .dc line defines the dc transfer curve source and sweep limits (with
capacitors open and inductors shorted). **srcnam** is the name of an
independent voltage or current source, a resistor, or the circuit
temperature. **vstart**, **vstop**, and **vincr** are the starting,
final, and incrementing values, respectively. The first example causes
the value of the voltage source \(V_{IN}\) to be swept from 0.25 Volts
to 5.0 Volts with steps of 0.25 Volt. A second source (**src2**) may
optionally be specified with its own associated sweep parameters. In
such a case the first source is swept over its own range for each value
of the second source. This option is useful for obtaining semiconductor
device output characteristics. See the example on transistor
characterization ([21.3](#sec_MOSFET_Characterization)).

