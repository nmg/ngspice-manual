# .NOISE: Noise Analysis

General form:

``` listings
.noise v(output <,ref>) src ( dec | lin | oct ) pts fstart fstop
+ <pts_per_summary>
```

Examples:

``` listings
.noise v(5) VIN dec 10 1kHz 100MEG
.noise v(5,3) V1 oct 8 1.0 1.0e6 1
```

The .noise line does a noise analysis of the circuit. **output** is the
node at which the total output noise is desired; if **ref** is
specified, then the noise voltage **v(output) - v(ref)** is calculated.
By default, **ref** is assumed to be ground. **src** is the name of an
independent source to which input noise is referred. **pts**, **fstart**
and **fstop** are .ac type parameters that specify the frequency range
over which plots are desired. **pts\_per\_summary** is an optional
integer; if specified, the noise contributions of each noise generator
is produced every **pts\_per\_summary** frequency points. The .noise
control line produces two plots, which can selected by setplot command:

  - one for the Voltage or Current Noise Spectral Density (in
    \(\frac{V}{\sqrt{Hz}}\) or \(\frac{A}{\sqrt{Hz}}\) respective the
    input is a voltage or current source) curves (e.g. after setplot
    noise1). There are two vectors over frequency:
      - **onoise\_spectrum**: This is the output noise voltage or
        current divided by \(\sqrt{Hz}\).
      - **inoise\_spectrum**: This the equivalent input noise = output
        noise divided by the gain of the circuit.
  - one for the Total Integrated Noise (in \(V\) or \(A\)) over the
    specified frequency range (e.g. after setplot noise2). There are two
    vectors which are in reality scalars:
      - **onoise\_total**: This is the output noise voltage over the
        specified frequency range
      - **inoise\_total**: This the equivalent input noise over the
        specified frequency range = output noise divided by the gain of
        the circuit.

The units of all result vectors can be changed by using control variable
**sqrnoise:**

  - **set sqrnoise:** will deliver results in squared form, means the
    unit is \(\frac{V^{2}}{Hz}\) or \(\frac{A^{2}}{Hz}\) . This value
    refers more to the convinient Power Spectral Density.

Default setting of ngspice is **unset sqrnoise,** which delivers Voltage
or Current Noise Spectral Density. This is more practical from designers
point of view.

