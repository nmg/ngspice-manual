# .SENS: DC or Small-Signal AC Sensitivity Analysis

General form:

``` listings
.SENS OUTVAR
.SENS OUTVAR AC DEC ND FSTART FSTOP
.SENS OUTVAR AC OCT NO FSTART FSTOP
.SENS OUTVAR AC LIN NP FSTART FSTOP
```

Examples:

``` listings
.SENS V(1,OUT)
.SENS V(OUT) AC DEC 10 100 100k
.SENS I(VTEST)
```

The sensitivity of OUTVAR to all non-zero device parameters is
calculated when the SENS analysis is specified. OUTVAR is a circuit
variable (node voltage or voltage-source branch current). The first form
calculates sensitivity of the DC operating-point value of OUTVAR. The
second form calculates sensitivity of the AC values of OUTVAR. The
parameters listed for AC sensitivity are the same as in an AC analysis
(see .AC above). The output values are in dimensions of change in output
per unit change of input (as opposed to percent change in output or per
percent change of input).

