# Diode SOA model parameter

1.  Bv\_max:if |Vj| exceeds Bv\_max, SOA warning is issued.
2.  Fv\_max:if |Vf| exceeds Fv\_max, SOA warning is issued.

