# BJT SOA model parameter

1.  Vbe\_max:if |Vbe| exceeds Vbe\_max, SOA warning is issued.
2.  Vbc\_max:if |Vbc| exceeds Vbc\_max, SOA warning is issued.
3.  Vce\_max:if |Vce| exceeds Vce\_max, SOA warning is issued.
4.  Vcs\_max:if |Vcs| exceeds Vcs\_max, SOA warning is issued.

