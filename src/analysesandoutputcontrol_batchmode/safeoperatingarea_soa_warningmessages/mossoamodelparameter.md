# MOS SOA model parameter

1.  Vgs\_max:if |Vgs| exceeds Vgs\_max, SOA warning is issued.
2.  Vgd\_max:if |Vgd| exceeds Vgd\_max, SOA warning is issued.
3.  Vgb\_max:if |Vgb| exceeds Vgb\_max, SOA warning is issued.
4.  Vds\_max:if |Vds| exceeds Vds\_max, SOA warning is issued.
5.  Vbs\_max:if |Vbs| exceeds Vbs\_max, SOA warning is issued.
6.  Vbd\_max:if |Vbd| exceeds Vbd\_max, SOA warning is issued.

