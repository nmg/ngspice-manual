# .NODESET: Specify Initial Node Voltage Guesses

General form:

``` listings
.nodeset v(nodnum)=val v(nodnum)=val ...
.nodeset all=val
```

Examples:

``` listings
.nodeset v(12)=4.5 v(4)=2.23
.nodeset all=1.5
```

The .nodeset line helps the program find the DC or initial transient
solution by making a preliminary pass with the specified nodes held to
the given voltages. The restrictions are then released and the iteration
continues to the true solution. The .nodeset line may be necessary for
convergence on bistable or a-stable circuits. .nodeset all=val allows to
set all starting node voltages (except for the ground node) to the same
value. In general, the .nodeset line should not be necessary.

