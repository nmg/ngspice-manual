# DC Solution Options

The following options controls properties pertaining to DC analysis and
algorithms. Since transient analysis is based on DC many of the options
affect the latter one.

  - ABSTOL=x  
    resets the absolute current error tolerance of the program. The
    default value is 1 pA.
  - GMIN=x  
    resets the value of GMIN, the minimum conductance allowed by the
    program. The default value is 1.0e-12.
  - ITL1=x  
    resets the dc iteration limit. The default is 100.
  - ITL2=x  
    resets the dc transfer curve iteration limit. The default is 50.
  - KEEPOPINFO  
    Retain the operating point information when either an AC,
    Distortion, or Pole-Zero analysis is run. This is particularly
    useful if the circuit is large and you do not want to run a
    (redundant) .OP analysis.
  - NOOPITER  
    Go directly to gmin stepping, skipping the first iteration.
  - PIVREL=x  
    resets the relative ratio between the largest column entry and an
    acceptable pivot value. The default value is 1.0e-3. In the
    numerical pivoting algorithm the allowed minimum pivot value is
    determined by
    \(\mathtt{EPSREL} = \mathtt{AMAX1}\left( {\mathtt{PIVREL} \cdot \mathtt{MAXVAL},\mathtt{PIVTOL}} \right)\)
    where MAXVAL is the maximum element in the column where a pivot is
    sought (partial pivoting).
  - PIVTOL=x  
    resets the absolute minimum value for a matrix entry to be accepted
    as a pivot. The default value is 1.0e-13.
  - RELTOL=x  
    resets the relative error tolerance of the program. The default
    value is 0.001 (0.1%).
  - RSHUNT=x  
    introduces a resistor from each analog node to ground. The value of
    the resistor should be high enough to not interfere with circuit
    operations. The XSPICE option has to be enabled (see
    [32.1.7](#subsec_Advanced_Install)) .
  - VNTOL=x  
    resets the absolute voltage error tolerance of the program. The
    default value is 1 \(\mu V\).

