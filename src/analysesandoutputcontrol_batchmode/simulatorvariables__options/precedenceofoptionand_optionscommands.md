# Precedence of option and .options commands

There are various ways to set the above mentioned options in Ngspice. If
no option or .options lines are set by the user, internal default values
are given for each of the simulator variables.

You may set options in the init files spinit or .spiceinit via the
option command (see Chapt. [17.5.47](#subsec_Option__)). The values
given there will supersede the default values. If you set options via
the .options line in your input file, their values will supersede the
default and init file data. Finally, if you set options inside a
.control ... .endc section, these values will again supersede any
simulator variables given so far.

