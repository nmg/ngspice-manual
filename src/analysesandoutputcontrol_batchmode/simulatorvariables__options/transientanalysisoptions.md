# Transient Analysis Options

  - AUTOSTOP  
    stops a transient analysis after successfully calculating all
    functions ([15.4](#subsec__MEAS)) specified with the dot command
    .meas. Autostop is not available with the meas
    ([17.5.42](#subsec_Meas___Mesurements_on)) command used in control
    mode.
  - CHGTOL=x  
    resets the charge tolerance of the program. The default value is
    1.0e-14.
  - CONVSTEP=x  
    relative step limit applied to code models.
  - CONVABSSTEP=x  
    absolute step limit applied to code models.
  - GMINSTEPS=x  
    \[\*\] sets the number of Gmin steps to be attempted. If the value
    is set to zero, the standard gmin stepping algorithm is skipped. The
    standard behavior is that gmin stepping is tried before going to the
    source stepping algorithm.
  - INTERP  
    interpolates output data onto fixed time steps on a TSTEP grid
    ([15.3.9](#subsec__TRAN__Transient_Analysis)). Uses linear
    interpolation between previous and next time values. Simulation
    itself is not influenced by this option. This option can be used in
    all simulation modes (batch, control or interactive,
    [16.4](#sec_Starting_options)). It may drastically reduce memory
    requirements in control mode, and file size in batch mode, but care
    is needed not to undersample the output data. See also the command
    linearize ([17.5.38](#subsec_Linearize___Interpolate_to)) that
    achieves a similar result by post-processing the data in control
    mode. The Ngspice/examples/xspice/delta-sigma/delta-sigma-1.cir
    example demonstrates how INTERP reduces memory requirements and
    speeds up plotting.
  - ITL3=x  
    resets the lower transient analysis iteration limit. The default
    value is 4. (Note: not implemented in Spice3).
  - ITL4=x  
    resets the transient analysis time-point iteration limit. The
    default is 10.
  - ITL5=x  
    resets the transient analysis total iteration limit. The default is
    5000. Set ITL5=0 to omit this test. (Note: not implemented in
    Spice3).
  - ITL6=x  
    \[\*\] synonym for SRCSTEPS.
  - MAXEVITER=x  
    sets the maximum number of event iterations per analysis point.
  - MAXOPALTER=x  
    specifies the maximum number of analog/event alternations that the
    simulator will use to solve a hybrid circuit.
  - MAXORD=x  
    \[\*\] specifies the maximum order for the numerical integration
    method used by SPICE. Possible values for the Gear method are from 2
    (the default) to 6. Using the value 1 with the trapezoidal method
    specifies backward Euler integration.
  - METHOD=name  
    sets the numerical integration method used by SPICE. Possible names
    are \`Gear' or \`trapezoidal' (or just \`trap'). The default is
    trapezoidal.
  - NOOPALTER=TRUE|FALSE  
    if set to false, alternations between analog/event are enabled.
  - RAMPTIME=x  
    During source stepping, this option sets the rate of change of
    independent supplies. It also affects code model inductors and
    capacitors that have initial conditions specified.
  - SRCSTEPS=x  
    \[\*\] a non-zero value causes SPICE to use a source-stepping method
    to find the DC operating point. The value specifies the number of
    steps.
  -  TRTOL=x  
    resets the transient error tolerance. The default value is 7. This
    parameter is an estimate of the factor by which SPICE overestimates
    the actual truncation error. If XSPICE is configured and 'A' devices
    are included, the value is internally set to 1 for higher precision.
    This slows down transient analysis with a factor of two.
  - XMU=x  
    sets the damping factor for trapezoidal integration. The default
    value is XMU=0.5. A value \< 0.5 may be chosen. Even a small
    reduction, e.g. to 0.495, may already suppress trap ringing. The
    reduction has to be set carefully in order not to excessively damp
    circuits that are prone to ringing or oscillation, which might lead
    the user to believe that the circuit is stable.

