# AC Solution Options

  - NOOPAC  
    Do not run an operating point (OP) analysis prior to an AC analysis.
    This option requires that the circuit is linear, *i.e.* consists
    only of R, L, and C devices, independent V, I sources and linear
    dependent E, G, H, and F sources (without poly statement,
    non-behavioral). If a non-linear device is detected, the OP analysis
    is executed automatically. This option is of interest *e.g.* in
    nested LC circuits where no series resistance for L devices is
    present. During the OP analysis an ill-formed matrix may be
    encountered, causing the simulator to abort with an error message.

