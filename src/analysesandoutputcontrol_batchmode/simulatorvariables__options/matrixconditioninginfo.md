# Matrix Conditioning info

In SPICE-based simulators, specific problems arise with certain circuit
topologies. One issue is the absence of a DC path to ground at some
node. This may happen when two capacitors are connected in series with
no other connection at the common node, or when code models are
cascaded. The result is an ill-conditioned or nearly singular matrix
that prevents the simulation from completing. Configuring with XSPICE
introduces the rshunt option to help eliminate this problem. The option
inserts resistors to ground at all the analog nodes in the circuit. In
general, the value of rshunt is set to some high resistance (e.g.
\(1000\mathsf{M}\Omega\) or greater) so that the operation of the
circuit is essentially unaffected but the matrix problems are corrected.
If a \`no DC path to ground' or a \`matrix is nearly singular' error
message is encountered, add the following .option card to the circuit
deck:

.option rshunt = 1.0e12

Usually a value of \(1\mathsf{ T}\Omega\) is sufficient to correct the
problem. In bad cases one can try lowering the value to
\(10\mathsf{ G}\Omega\) or even \(1\mathsf{G}\Omega\).

A different matrix conditioning problem occurs if an inductor is placed
in parallel to a voltage source. The AC simulation will fail, because it
is preceded by an OP analysis. Option NOOPAC
([15.1.3](#subsec_AC_Solution_Options)) will help if the circuit is
linear. However, if the circuit is non-linear the OP analysis is
essential. In such a case, adding a small resistor (e.g.
\(0.1\mathsf{m}\Omega\)) in series to the inductor will help to obtain
convergence.

.option rseries = 1.0e-4

adds a series resistor to each inductor in the circuit. Be careful when
using behavioral inductors (see
[3.2.12](#subsec_Inductors__dependent_on)), as the result may become
unpredictable.

