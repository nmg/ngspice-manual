# ELEMENT Specific options

  - BADMOS3  
    Use the older version of the MOS3 model with the \`kappa'
    discontinuity.
  - DEFAD=x  
    resets the value for MOS drain diffusion area; the default is 0.
  - DEFAS=x  
    resets the value for MOS source diffusion area; the default is 0.
  - DEFL=x  
    resets the value for MOS channel length; the default is 100
    \(\mu m\).
  - DEFW=x  
    resets the value for MOS channel width; the default is 100
    \(\mu m\).
  - SCALE=x  
    set the element scaling factor for geometric element parameters
    whose default unit is meters. As an example: scale=1u and a MOSFET
    instance parameter W=10 will result in a width of 10\(\mu m\) for
    this device. An area parameter AD=20 will result in
    \(\mathsf{20e - 12 m^{2}}\). Following instance parameters are
    scaled:

<!-- end list -->

  - Resistors and Capacitors: W, L
  - Diodes: W, L, Area
  - JFET, MESFET: W, L, Area
  - MOSFET: W, L, AS, AD, PS, PD, SA, SB, SC, SD

