# Transmission Lines Specific Options

  - TRYTOCOMPACT  
    Applicable only to the LTRA model (see
    [6.2.1](#subsec_Lossy_Transmission_Line)). When specified, the
    simulator tries to condense an LTRA transmission line's past history
    of input voltages and currents.

