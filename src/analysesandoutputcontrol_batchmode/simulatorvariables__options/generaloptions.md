# General Options

  - ACCT  
    causes accounting and run time statistics to be printed.
  - NOACCT  
    no printing of statistics, no printing of the Initial Transient
    Solution.
  - NOINIT  
    suppresses only printing of the Initial Transient Solution, maybe
    combined with ACCT.
  - LIST  
    causes the summary listing of the input data to be printed.
  - NOMOD  
    suppresses the printout of the model parameters.
  - NOPAGE  
    suppresses page ejects.
  - NODE  
    causes the printing of the node table.
  - OPTS  
    causes the option values to be printed.
  - SEED=val|random  
    Sets the seed value of the random number generator. **val** may be
    any integer number greater than 0. As an alternative **random** will
    set the seed value to the time in seconds since 1.1.1970.
  - SEEDINFO  
    will print the seed value when it has been set to a new integer
    number.
  - TEMP=x  
    Resets the operating temperature of the circuit. The default value
    is 27 \(C\) (300K). TEMP can be overridden per device by a
    temperature specification on any temperature dependent instance. May
    also be generally overridden by a .TEMP card ([2.11](#sec__temp)).
  - TNOM=x  
    resets the nominal temperature at which device parameters are
    measured. The default value is 27 \(C\) (300 deg K). TNOM can be
    overridden by a specification on any temperature dependent device
    model.
  - WARN=1|0  
    enables or turns of SOA (Safe Operating Area) voltage warning
    messages (default: 0).
  - MAXWARNS=x  
    specifies the maximum number of SOA (Safe Operating Area) warning
    messages per model (default: 5).
  - SAVECURRENTS  
    save currents through all terminals of the following devices: M, J,
    Q, D, R, C, L, B, F, G, W, S, I (see
    [2.1.2](#subsec_Circuit_elements__device)). Recommended only for
    small circuits, because otherwise memory requirements explode and
    simulation speed suffers. See [15.7](#sec_Measuring_current_in) for
    more details.

