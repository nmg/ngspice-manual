# Safe Operating Area \(SOA\) warning messages

By setting .option warn=1 the Safe Operation Area check algorithm is
enabled. In this case for .op, .dc and .tran analysis warning messages
are issued if the branch voltages of devices (Resistors, Capacitors,
Diodes, BJTs and MOSFETs) exceed limits that are specified by model
parameters. All these parameters are positive with default value of
infinity.

The check is executed after Newton-Raphson iteration is finished i.e. in
transient analysis in each time step. The user can specify an additional
.option maxwarns (default: 5) to limit the count of messages.

The output goes on default to stdout or alternatively to a file
specified by command line option --soa-log=filename.

