# Batch Output

The following commands .print ([15.6.2](#subsec__PRINT_Lines)), .plot
([15.6.3](#subsec__PLOT_Lines)) and .four
([15.6.4](#subsec__FOUR__Fourier_Analysis)) are valid only if ngspice is
started in batch mode (see [16.4.1](#subsec_Batch_mode)), whereas .save
and the equivalent .probe are aknowledged in all operating modes.

If you start ngspice in batch mode using the -b command line option, the
outputs of .print, .plot, and .four are printed to the console output.
You may use the output redirection of your shell to direct this printout
into a file (not available with MS Windows GUI). As an alternative you
may extend the ngspice command by specifying an output file:

ngspice -b -o output.log input.cir

If you however add the command line option -r to create a rawfile,
.print and .plot are ignored. If you want to involve the graphics plot
output of ngspice, use the control mode
([16.4.3](#subsec_Interactive_mode_with)) instead of the -b batch mode
option.

