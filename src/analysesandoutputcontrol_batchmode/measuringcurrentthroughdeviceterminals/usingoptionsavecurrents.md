# Using option 'savecurrents'

Current measurement with series voltage source

``` listings
*measure current through R1 and R2
V1 1 0 1
R1 1 0 5
R2 1 0 10
.options savecurrents
```

The option **savecurrents** will add .save lines
([15.6.1](#subsec__SAVE_Lines)) like

.save @r1\[i\]

.save @r2\[i\]

to your input file information read during circuit parsing. These newly
created vectors contain the terminal currents of the devices R1 and R2.

You will find information of the nomenclature in Chapt.
[31](#cha_Model_and_Device), also how to plot these vectors. The
following devices are supported: M, J, Q, D, R, C, L, B, F, G, W, S, I
(see [2.1.2](#subsec_Circuit_elements__device)). For M only MOSFET
models MOS1 to MOS9 are included so far. Devices in subcircuits are
supported as well. Be careful when choosing this option in larger
circuits, because 1 to 4 additional output vectors are created per
device and this may consume lots of memory.

