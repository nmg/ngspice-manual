# Adding a voltage source in series

Originally the ngspice matrix solver delivers node voltages and currents
through independent voltage sources. So to measure the currents through
a resistor you may add a voltage source in series with dc voltage 0.

Current measurement with series voltage source

``` listings
*measure current through R1
V1 1 0 1
R1 1 0 5
R2 1 0 10
* will become
V1 1 0 1
R1 1 11 5
Vmess 11 0 dc 0
R2 1 0 10
```

