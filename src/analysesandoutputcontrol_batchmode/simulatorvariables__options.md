# Simulator Variables \(.options\)

Various parameters of the simulations available in Ngspice can be
altered to control the accuracy, speed, or default values for some
devices. These parameters may be changed via the option command
(described in Chapt. [17.5.47](#subsec_Option__)) or via the .options
line:

General form:

``` listings
.options opt1 opt2 ... (or opt=optval ...)
```

Examples:

``` listings
.options reltol=.005 trtol=8
```

The options line allows the user to reset program control and user
options for specific simulation purposes. Options specified to Ngspice
via the **option** command (see Chapt. ) are also passed on as if
specified on a .options line. Any combination of the following options
may be included, in any order. \`x' (below) represents some positive
number.

