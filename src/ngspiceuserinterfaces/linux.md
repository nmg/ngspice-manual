# Linux

The standard user interface is a console for input and the X11 graphics
system for output with the interactive plot
([17.5.48](#subsec_Plot__Plot_values)) command. If ngspice is compiled
with the –without-x flag for ./configure, a console application without
graphical interface results. For more sophisticated input user
interfaces please have a look at Chapt.
[18.8](#sec_Integration_with_CAD).

