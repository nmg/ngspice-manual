# CygWin

The CygWin interface is similar to the Linux interface
([18.3](#sec_LINUX)), i.e. console input and X11 graphics output. To
avoid the warning of a missing graphical user interface, you have to
start the X11 window manager by issuing the commands

$ export DISPLAY=:0.0

$ xwin -multiwindow -clipboard &

inside of the CygWin window before starting ngspice.

