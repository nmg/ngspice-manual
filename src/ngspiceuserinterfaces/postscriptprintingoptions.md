# Postscript printing options

This info is compiled from Roger L. Traylor's [web
page](http://web.engr.oregonstate.edu/~traylor/ece391/ngspice_printing).
All the commands and variables you can set are described in Chapt.
[17.5](#sec_Commands). The corresponding input file for the examples
given below is listed in Chapt. [21.1](#sec_AC_coupled_transistor). Just
add the .control section to this file and run in interactive mode by

$ ngspice xspice\_c1\_print.cir

One way is to setup your printing like this:

.control

set hcopydevtype=postscript

op

run

plot vcc coll emit

hardcopy temp.ps vcc coll emit

.endc

Then print the postscript file temp.ps to a postscript printer.

You can add color traces to it if you wish:

.control

set hcopydevtype=postscript

\* allow color and set background color if set to value \> 0

set hcopypscolor=1

\*color0 is background color

\*color1 is the grid and text color

\*colors 2-15 are for the vectors

set color0=rgb:f/f/f

set color1=rgb:0/0/0

op

run

hardcopy temp.ps vcc coll emit

.endc

Then print the postscript file temp.ps to a postscript printer.

You can also direct your output directly to a designated printer (not
available in MS Windows):

.control

set hcopydevtype=postscript

\*send output to the printer kec3112-clr

set hcopydev=kec3112-clr

hardcopy out.tmp vcc coll emit

