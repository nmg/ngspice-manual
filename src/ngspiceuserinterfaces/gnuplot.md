# Gnuplot

Install Gnuplot (on Linux available from the distribution, on Windows
available [here](http://www.tatsuromatsuoka.com/gnuplot/Eng/winbin/)).
On Windows expand the zip file to a directory of your choice, add the
path \<any directory\>/gnuplot/bin to the PATH variable, and go... The
command to invoke Gnuplot ([17.5.30](#subsec_Gnuplot__Graphics_output))
is limited however to x/y plots (no polar etc.).

