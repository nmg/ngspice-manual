# Error handling

Error messages and error handling in ngspice have grown over the years,
include a lot of \`traditional' behavior and thus are not very
systematic and consistent.

Error messages may occur with the token \`Error:'. Often the errors are
non-recoverable and will lead to exiting ngspice with error code 1.
Sometimes, however, you will get an error message, but ngspice will
continue, and may either bail out later because the error has propagated
into the simulation, sometimes ngspice will continue, deliver wrong
results and exit with error code 0 (no error detected\!).

In addition ngspice may issue warning messages like \`Warning: ...'.
These should cover recoverable errors only.

So there is still work to be done to define a consistent error
messaging, recovery or exiting. A first step is the user definable
variable **strict\_errorhandling**. This variable may be set in files
spinit ([16.5](#sec_Standard_configuration_file)) or .spiceinit
([16.6](#sec_User_defined_configuration)) to immediately stop ngspice,
after an error is detected during parsing the circuit. An error message
is sent, the ngspice exit code is 1. This behavior deviates from
traditional SPICE error handling and thus is introduced as an option
only.

XSPICE error messages are explained in Chapt.
[29](#chap_Error_Messages).

