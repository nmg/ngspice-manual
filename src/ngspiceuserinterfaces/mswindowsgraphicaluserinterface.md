# MS Windows Graphical User Interface

If compiled properly (e.g. using the --with-wingui flag for ./configure
under MINGW), ngspice for Windows offers a simple graphical user
interface. In fact this interface does not offer much more for data
input than a console would offer, e.g. command line inputs, command
history and program text output. First of all it applies the Windows api
for data plotting. If you run the sample input file given below, you
will get an output as shown in Fig. [18.1](#fig_MS_Windows_GUI).

Input file:

``` listings
***** Single NMOS Transistor For BSIM3V3.1
***** general purpose check (Id-Vd) ***
* 
*** circuit description *** 
m1 2 1 3 0 n1 L=0.6u W=10.0u 
vgs 1 0 3.5  
vds 2 0 3.5  
vss 3 0 0 
* 
.dc vds 0 3.5 0.05 vgs 0 3.5 0.5  
* 
.control 
run 
plot vss#branch 
.endc 
* 
* UCB parameters BSIM3v3.2 
.include ../Exam_BSIM3/Modelcards/modelcard.nmos 
.include ../Exam_BSIM3/Modelcards/modelcard.pmos 
* 
.end 
```

The GUI consists of an I/O port (lower window) and a graphics window,
created by the plot command.

![image:
1\_home\_nmg\_Documents\_gitroot\_ngspice-ngspice-manuals\_Images\_ng-win-out.png](1_home_nmg_Documents_gitroot_ngspice-ngspice-manuals_Images_ng-win-out.png)
Figure 18.1:  MS Windows GUI

The output window displays messages issued by ngspice. You may scroll
the window to get more of the text. The input box (white box) may be
activated by a mouse click to accept any of the valid ngspice commends.
The lower left output bar displays the actual input file. ngspice
progress during setup and simulation is shown in the progress window
(--ready--). The Quit button allow to interrupt ngspice. If ngspice is
actively simulating, due to using only a single thread, this interrupt
has to wait until the window is accessible from within ngspice, e.g.
during an update of the progress window.

In the plot window there is the upper left button, which activated a
drop down menu. You may select to print the plot window shown (a very
simple printer interface, to be improved), set up any of the printers
available on your computer, or issue a postscript file of the actual
plot window, either black\&white or colored.

Instead of plotting with black background, you may set the background to
any other color, preferably to \`white' using the command shown below.

Input file modification for white background:

``` listings
.control 
run 
* white background
set color0=white 
* black grid and text (only needed with X11, automatic with MS Win)
set color1=black 
* wider grid and plot lines
set xbrushwidth=2 
plot vss#branch 
.endc 
```

![image:
2\_home\_nmg\_Documents\_gitroot\_ngspice-ngspice-manuals\_Images\_ng-win-out-white.png](2_home_nmg_Documents_gitroot_ngspice-ngspice-manuals_Images_ng-win-out-white.png)
Figure 18.2: Plotting with white background

