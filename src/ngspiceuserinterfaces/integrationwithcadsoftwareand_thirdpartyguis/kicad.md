# KiCad

[KiCad](http://kicad-pcb.org/) is a cross platform and open source
electronics design automation suite. Its schematic editor Eeschema fully
integrates shared ngspice (see Chapt. [19](#chap_ngspice_as_shared)) as
the simulation tool. Whereas this feature is not yet part of the actual
KiCad release, the code is already available in the master branch, and
also compiled as a nightly build for MS Windows.

