# GNU Octave

[GNU Octave](http://www.gnu.org/software/octave) is a high-level
language, primarily intended for numerical computations. An interface to
ngspice is available
[here](https://www.dsprelated.com/showarticle/707.php).

