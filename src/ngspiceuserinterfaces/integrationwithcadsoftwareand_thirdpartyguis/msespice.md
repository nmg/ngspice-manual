# MSEspice

A [graphical front end](http://sourceforge.net/projects/mseuniverse/) to
ngspice, using the Free Pascal cross platform RAD environment
[MSEide+MSEgui](http://mseide-msegui.sourceforge.net/).

