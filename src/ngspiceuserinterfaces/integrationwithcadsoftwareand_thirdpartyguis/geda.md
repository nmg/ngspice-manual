# GEDA

The [gEDA project](http://www.gpleda.org/) is developing a full GPL‘d
suite and toolkit of Electronic Design Automation tools for use with a
Linux. Ngspice may be integrated into the development flow. Two web
sites offer tutorials using gschem and gnetlist with ngspice:

<http://geda-project.org/wiki/geda:csygas>

<http://geda-project.org/wiki/geda:ngspice_and_gschem>

