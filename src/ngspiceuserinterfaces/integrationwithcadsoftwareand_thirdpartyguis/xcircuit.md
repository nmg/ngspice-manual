# XCircuit

CYGWIN and especially Linux users may find
[XCircuit](http://opencircuitdesign.com/xcircuit/) valuable to establish
a development flow including [schematic
capture](http://opencircuitdesign.com/xcircuit/tutorial/tutorial2.html)
and circuit simulation.

