# MS Windows Console

If the --with-wingui flag for ./configure under MINGW is omitted (see
[32.2.5](#subsec_ngspice_mingw_or)) or console\_debug or
console\_release is selected in the MS Visual Studio configuration
manager, then ngspice will compile without any internal graphical input
or output capability. This may be useful if you apply ngspice in a pipe
inside the MSYS window, or use it being called from another program, and
just generating output files from a given input. The plot
([17.5.48](#subsec_Plot__Plot_values)) command will not do and leads to
an error message.

Only on the ngspice console binary in MS Windows input/output
redirection is possible, if ngspice is called (e.g. within a MSYS shell
or from a shell script) like

$ ngspice \< input.

This feature is used in the new CMC model test suite (to be described
elsewhere), thus requires a console binary.

You still may generate graphics output plots or prints by gnuplot
([17.5.30](#subsec_Gnuplot__Graphics_output)), if installed properly
([18.7](#sec_Gnuplot)), or by selecting a suitable printing option
([18.6](#sec_Printing_options)).

