# Integration with CAD software and \`third party' GUIs

In this chapter you will find some links and comments on GUIs for
ngspice offered from other projects and on the integration of ngspice
into a circuit development flow. The data given rely mostly on
information available from the web and thus is out of our control. It
also may be far from complete. For a list of actual links with more than
20 entries please have a look at the [ngspice web
pages](http://ngspice.sourceforge.net/resources.html). Some open source
tools are listed here. The GUIs MSEspice and GNUSpiceGUI help you to
navigate the commands to need to perform your simulation. XCircuit and
the GEDA tools gschem and gnetlist offer integrating schematic capture
and simulation. KiCAD offers a complete design environment for
electronic circuits.

