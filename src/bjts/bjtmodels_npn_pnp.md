# BJT Models \(NPN/PNP\)

Ngspice provides three BJT device models, which are selected by the
.model card.

.model QMOD1 BJT level=2

This is the minimal version, further optional parameters listed in the
table below may replace the ngspice default parameters. The** level**
keyword specifies the model to be used:

  - level=1: This is the original SPICE BJT model, and it is the default
    model if the **level** keyword is not specified on the .model line.
  - level=2: This is a modified version of the original SPICE BJT that
    models both vertical and lateral devices and includes temperature
    corrections of collector, emitter and base resistors.
  - level=4: Advanced VBIC model (see
    <http://www.designers-guide.org/VBIC/> for details)

The bipolar junction transistor model in ngspice is an adaptation of the
integral charge control model of Gummel and Poon. This modified
Gummel-Poon model extends the original model to include several effects
at high bias levels. The model automatically simplifies to the simpler
Ebers-Moll model when certain parameters are not specified. The
parameter names used in the modified Gummel-Poon model have been chosen
to be more easily understood by the user, and to reflect better both
physical and circuit design thinking.

The dc model is defined by the parameters **is**, **bf**, **nf**,
**ise**, **ikf**, and **ne,** which determine the forward current gain
characteristics, **is**, **br**, **nr**, **isc**, **ikr**, and **nc**,
which determine the reverse current gain characteristics, and **vaf**
and **var**, which determine the output conductance for forward and
reverse regions.

The level 1 model has among the standard temperature parameters an
extension compatible with most foundry provided process design kits (see
parameter table below **tlev**).

The level 1 and 2 models include the substrate saturation current
**iss**. Three ohmic resistances **rb**, **rc**, and **re** are
included, where **rb** can be high current dependent. Base charge
storage is modeled by forward and reverse transit times, **tf** and
**tr**, where the forward transit time **tf** can be bias dependent if
desired. Nonlinear depletion layer capacitances are defined with
**cje**, **vje**, and **nje** for the B-E junction, **cjc**, **vjc**,
and **njc** for the B-C junction and **cjs**, **vjs**, and **mjs** for
the C-S (collector-substrate) junction.

The level 1 and 2 model support a substrate capacitance that is
connected to the device's base or collector, to model lateral or
vertical devices dependent on the parameter **subs**. The temperature
dependence of the saturation currents, **is** and **iss** (for the level
2 model), is determined by the energy-gap, **eg**, and the saturation
current temperature exponent, **xti**.

In the new model, additional base current temperature dependence is
modeled by the beta temperature exponent **xtb**. The values specified
are assumed to have been measured at the temperature **tnom**, which can
be specified on the .options control line or overridden by a
specification on the .model line.

The level 4 model (VBIC) has the following improvements beyond the GP
models: improved Early effect modeling, quasi-saturation modeling,
parasitic substrate transistor modeling, parasitic fixed (oxide)
capacitance modeling, includes an avalanche multiplication model,
improved temperature modeling, base current is decoupled from collector
current, electrothermal modeling, smooth and continuous mode.

The BJT parameters used in the modified Gummel-Poon model are listed
below. The parameter names used in earlier versions of SPICE2 are still
accepted.

