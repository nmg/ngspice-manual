# Gummel-Poon BJT Parameters \(incl. model extensions\)

<table style="width:100%;">
<colgroup>
<col width="16%" />
<col width="16%" />
<col width="16%" />
<col width="16%" />
<col width="16%" />
<col width="16%" />
</colgroup>
<tbody>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-8583"></span>Name
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8586"></span>Parameters
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8589"></span>Units
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8592"></span>Default
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8595"></span>Example
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8598"></span>Scale factor
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-8601"></span>SUBS
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8604"></span>Substrate connection: for vertical geometry, -1 for lateral geometry (level 2 only).
</div></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-8610"></span>1
</div></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-8619"></span>IS
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8622"></span>Transport saturation current.
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8625"></span><span class="math inline"><em>A</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8628"></span>1.0e-16
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8631"></span>1.0e-15
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8634"></span>area
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-8637"></span>ISS
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8640"></span>Reverse saturation current, substrate-to-collector for vertical device or substrate-to-base for lateral (level 2 only).
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8643"></span><span class="math inline"><em>A</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8646"></span>1.0e-16
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8649"></span>1.0e-15
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8652"></span>area
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-8655"></span>BF
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8658"></span>Ideal maximum forward beta.
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8661"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8664"></span>100
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8667"></span>100
</div></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-8673"></span>NF
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8676"></span>Forward current emission coefficient.
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8679"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8682"></span>1.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8685"></span>1
</div></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-8691"></span>VAF (VA)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8694"></span>Forward Early voltage.
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8697"></span><span class="math inline"><em>V</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8700"></span><span class="math inline">∞</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8703"></span>200
</div></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-8709"></span>IKF
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8712"></span>Corner for forward beta current roll-off.
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8715"></span><span class="math inline"><em>A</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8718"></span><span class="math inline">∞</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8721"></span>0.01
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8724"></span>area
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-8727"></span>NKF
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8730"></span>High current Beta rolloff exponent
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8733"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8736"></span>0.5
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8739"></span>0.58
</div></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-8745"></span>ISE
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8748"></span>B-E leakage saturation current.
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8751"></span><span class="math inline"><em>A</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8754"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8757"></span>1e-13
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8760"></span>area
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-8763"></span>NE
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8766"></span>B-E leakage emission coefficient.
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8769"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8772"></span>1.5
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8775"></span>2
</div></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-8781"></span>BR
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8784"></span>Ideal maximum reverse beta.
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8787"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8790"></span>1
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8793"></span>0.1
</div></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-8799"></span>NR
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8802"></span>Reverse current emission coefficient.
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8805"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8808"></span>1
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8811"></span>1
</div></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-8817"></span>VAR (VB)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8820"></span>Reverse Early voltage.
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8823"></span><span class="math inline"><em>V</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8826"></span><span class="math inline">∞</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8829"></span>200
</div></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-8835"></span>IKR
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8838"></span>Corner for reverse beta high current roll-off.
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8841"></span><span class="math inline"><em>A</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8844"></span><span class="math inline">∞</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8847"></span>0.01
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8850"></span>area
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-8853"></span>ISC
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8856"></span>B-C leakage saturation current (area is `areab' for vertical devices and `areac' for lateral).
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8859"></span><span class="math inline"><em>A</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8862"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8865"></span>1e-13
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8868"></span>area
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-8871"></span>NC
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8874"></span>B-C leakage emission coefficient.
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8877"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8880"></span>2
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8883"></span>1.5
</div></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-8889"></span>RB
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8892"></span>Zero bias base resistance.
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8895"></span><span class="math inline"><em>Ω</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8898"></span>0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8901"></span>100
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8904"></span>area
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-8907"></span>IRB
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8910"></span>Current where base resistance falls halfway to its min value.
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8913"></span><span class="math inline"><em>A</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8916"></span><span class="math inline">∞</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8919"></span>0.1
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8922"></span>area
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-8925"></span>RBM
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8928"></span>Minimum base resistance at high currents.
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8931"></span><span class="math inline"><em>Ω</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8934"></span>RB
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8937"></span>10
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8940"></span>area
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-8943"></span>RE
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8946"></span>Emitter resistance.
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8949"></span><span class="math inline"><em>Ω</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8952"></span>0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8955"></span>1
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8958"></span>area
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-8961"></span>RC
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8964"></span>Collector resistance.
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8967"></span><span class="math inline"><em>Ω</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8970"></span>0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8973"></span>10
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8976"></span>area
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-8979"></span>CJE
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8982"></span>B-E zero-bias depletion capacitance.
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8985"></span><span class="math inline"><em>F</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8988"></span>0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8991"></span>2pF
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-8994"></span>area
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-8997"></span>VJE (PE)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9000"></span>B-E built-in potential.
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9003"></span><span class="math inline"><em>V</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9006"></span>0.75
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9009"></span>0.6
</div></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-9015"></span>MJE (ME)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9018"></span>B-E junction exponential factor.
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9021"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9024"></span>0.33
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9027"></span>0.33
</div></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-9033"></span>TF
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9036"></span>Ideal forward transit time.
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9039"></span>sec
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9042"></span>0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9045"></span>0.1ns
</div></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-9051"></span>XTF
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9054"></span>Coefficient for bias dependence of TF.
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9057"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9060"></span>0
</div></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-9069"></span>VTF
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9072"></span>Voltage describing VBC dependence of TF.
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9075"></span><span class="math inline"><em>V</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9078"></span><span class="math inline">∞</span>
</div></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-9087"></span>ITF
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9090"></span>High-current parameter for effect on TF.
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9093"></span><span class="math inline"><em>A</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9096"></span>0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9099"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9102"></span>area
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-9105"></span>PTF
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9108"></span>Excess phase at freq=<span class="math inline">$\frac{1}{2\pi TF}$</span> Hz.
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9111"></span>deg
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9114"></span>0
</div></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-9123"></span>CJC
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9126"></span>B-C zero-bias depletion capacitance (area is `areab' for vertical devices and `areac' for lateral).
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9129"></span><span class="math inline"><em>F</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9132"></span>0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9135"></span>2pF
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9138"></span>area
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-9141"></span>VJC (PC)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9144"></span>B-C built-in potential.
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9147"></span><span class="math inline"><em>V</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9150"></span>0.75
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9153"></span>0.5
</div></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-9159"></span>MJC
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9162"></span>B-C junction exponential factor.
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9165"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9168"></span>0.33
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9171"></span>0.5
</div></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-9177"></span>XCJC
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9180"></span>Fraction of B-C depletion capacitance connected to internal base node.
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9183"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9186"></span>1
</div></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-9195"></span>TR
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9198"></span>Ideal reverse transit time.
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9201"></span>sec
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9204"></span>0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9207"></span>10ns
</div></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-9213"></span>CJS
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9216"></span>Zero-bias collector-substrate capacitance (area is `areac' for vertical devices and `areab' for lateral).
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9219"></span><span class="math inline"><em>F</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9222"></span>0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9225"></span>2pF
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9228"></span>area
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-9231"></span>VJS (PS)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9234"></span>Substrate junction built-in potential.
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9237"></span><span class="math inline"><em>V</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9240"></span>0.75
</div></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-9249"></span>MJS (MS)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9252"></span>Substrate junction exponential factor.
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9255"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9258"></span>0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9261"></span>0.5
</div></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-9267"></span>XTB
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9270"></span>Forward and reverse beta temperature exponent.
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9273"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9276"></span>0
</div></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-9285"></span>EG
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9288"></span>Energy gap for temperature effect on IS.
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9291"></span><span class="math inline"><em>e</em><em>V</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9294"></span>1.11
</div></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-9303"></span>XTI
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9306"></span>Temperature exponent for effect on IS.
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9309"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9312"></span>3
</div></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-9321"></span>KF
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9324"></span>Flicker-noise coefficient.
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9327"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9330"></span>0
</div></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-9339"></span>AF
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9342"></span>Flicker-noise exponent.
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9345"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9348"></span>1
</div></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-9357"></span>FC
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9360"></span>Coefficient for forward-bias depletion capacitance formula.
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9363"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9366"></span>0.5
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9369"></span>0
</div></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-9375"></span>TNOM (TREF)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9378"></span>Parameter measurement temperature.
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9381"></span><span class="math inline"><em>C</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9384"></span>27
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9387"></span>50
</div></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-9393"></span>TLEV
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9396"></span>BJT temperature equation selector
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9399"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9402"></span>0
</div></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-9411"></span>TLEVC
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9414"></span>BJT capac. temperature equation selector
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9417"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9420"></span>0
</div></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-9429"></span>TRE1
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9432"></span>1st order temperature coefficient for RE.
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9435"></span><span class="math inline">$\frac{1}{C}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9438"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9441"></span>1e-3
</div></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-9447"></span>TRE2
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9450"></span>2nd order temperature coefficient for RE.
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9453"></span><span class="math inline">$\frac{1}{C^{2}}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9456"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9459"></span>1e-5
</div></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-9465"></span>TRC1
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9468"></span>1st order temperature coefficient for RC .
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9471"></span><span class="math inline">$\frac{1}{C}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9474"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9477"></span>1e-3
</div></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-9483"></span>TRC2
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9486"></span>2nd order temperature coefficient for RC.
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9489"></span><span class="math inline">$\frac{1}{C^{2}}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9492"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9495"></span>1e-5
</div></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-9501"></span>TRB1
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9504"></span>1st order temperature coefficient for RB.
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9507"></span><span class="math inline">$\frac{1}{C}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9510"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9513"></span>1e-3
</div></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-9519"></span>TRB2
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9522"></span>2nd order temperature coefficient for RB.
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9525"></span><span class="math inline">$\frac{1}{C^{2}}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9528"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9531"></span>1e-5
</div></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-9537"></span>TRBM1
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9540"></span>1st order temperature coefficient for RBM
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9543"></span><span class="math inline">$\frac{1}{C}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9546"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9549"></span>1e-3
</div></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-9555"></span>TRBM2
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9558"></span>2nd order temperature coefficient for RBM
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9561"></span><span class="math inline">$\frac{1}{C^{2}}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9564"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9567"></span>1e-5
</div></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-9573"></span>TBF1
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9576"></span>1st order temperature coefficient for BF
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9579"></span><span class="math inline">$\frac{1}{C}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9582"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9585"></span>1e-3
</div></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-9591"></span>TBF2
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9594"></span>2nd order temperature coefficient for BF
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9597"></span><span class="math inline">$\frac{1}{C^{2}}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9600"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9603"></span>1e-5
</div></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-9609"></span>TBR1
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9612"></span>1st order temperature coefficient for BR
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9615"></span><span class="math inline">$\frac{1}{C}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9618"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9621"></span>1e-3
</div></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-9627"></span>TBR2
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9630"></span>2nd order temperature coefficient for BR
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9633"></span><span class="math inline">$\frac{1}{C^{2}}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9636"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9639"></span>1e-5
</div></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-9645"></span>TIKF1
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9648"></span>1st order temperature coefficient for IKF
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9651"></span><span class="math inline">$\frac{1}{C}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9654"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9657"></span>1e-3
</div></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-9663"></span>TIKF2
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9666"></span>2nd order temperature coefficient for IKF
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9669"></span><span class="math inline">$\frac{1}{C^{2}}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9672"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9675"></span>1e-5
</div></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-9681"></span>TIKR1
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9684"></span>1st order temperature coefficient for IKR
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9687"></span><span class="math inline">$\frac{1}{C}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9690"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9693"></span>1e-3
</div></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-9699"></span>TIKR2
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9702"></span>2nd order temperature coefficient for IKR
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9705"></span><span class="math inline">$\frac{1}{C^{2}}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9708"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9711"></span>1e-5
</div></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-9717"></span>TIRB1
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9720"></span>1st order temperature coefficient for IRB
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9723"></span><span class="math inline">$\frac{1}{C}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9726"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9729"></span>1e-3
</div></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-9735"></span>TIRB2
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9738"></span>2nd order temperature coefficient for IRB
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9741"></span><span class="math inline">$\frac{1}{C^{2}}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9744"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9747"></span>1e-5
</div></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-9753"></span>TNC1
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9756"></span>1st order temperature coefficient for NC
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9759"></span><span class="math inline">$\frac{1}{C}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9762"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9765"></span>1e-3
</div></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-9771"></span>TNC2
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9774"></span>2nd order temperature coefficient for NC
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9777"></span><span class="math inline">$\frac{1}{C^{2}}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9780"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9783"></span>1e-5
</div></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-9789"></span>TNE1
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9792"></span>1st order temperature coefficient for NE
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9795"></span><span class="math inline">$\frac{1}{C}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9798"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9801"></span>1e-3
</div></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-9807"></span>TNE2
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9810"></span>2nd order temperature coefficient for NE
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9813"></span><span class="math inline">$\frac{1}{C^{2}}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9816"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9819"></span>1e-5
</div></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-9825"></span>TNF1
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9828"></span>1st order temperature coefficient for NF
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9831"></span><span class="math inline">$\frac{1}{C}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9834"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9837"></span>1e-3
</div></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-9843"></span>TNF2
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9846"></span>2nd order temperature coefficient for NF
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9849"></span><span class="math inline">$\frac{1}{C^{2}}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9852"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9855"></span>1e-5
</div></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-9861"></span>TNR1
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9864"></span>1st order temperature coefficient for IKF
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9867"></span><span class="math inline">$\frac{1}{C}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9870"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9873"></span>1e-3
</div></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-9879"></span>TNR2
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9882"></span>2nd order temperature coefficient for IKF
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9885"></span><span class="math inline">$\frac{1}{C^{2}}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9888"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9891"></span>1e-5
</div></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-9897"></span>TVAF1
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9900"></span>1st order temperature coefficient for VAF
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9903"></span><span class="math inline">$\frac{1}{C}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9906"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9909"></span>1e-3
</div></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-9915"></span>TVAF2
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9918"></span>2nd order temperature coefficient for VAF
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9921"></span><span class="math inline">$\frac{1}{C^{2}}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9924"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9927"></span>1e-5
</div></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-9933"></span>TVAR1
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9936"></span>1st order temperature coefficient for VAR
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9939"></span><span class="math inline">$\frac{1}{C}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9942"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9945"></span>1e-3
</div></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-9951"></span>TVAR2
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9954"></span>2nd order temperature coefficient for VAR
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9957"></span><span class="math inline">$\frac{1}{C^{2}}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9960"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9963"></span>1e-5
</div></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-9969"></span>CTC
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9972"></span>1st order temperature coefficient for CJC
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9975"></span><span class="math inline">$\frac{1}{C}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9978"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9981"></span>1e-3
</div></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-9987"></span>CTE
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9990"></span>1st order temperature coefficient for CJE
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9993"></span><span class="math inline">$\frac{1}{C}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9996"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-9999"></span>1e-3
</div></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-10005"></span>CTS
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10008"></span>1st order temperature coefficient for CJS
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10011"></span><span class="math inline">$\frac{1}{C}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10014"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10017"></span>1e-3
</div></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-10023"></span>TVJC
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10026"></span>1st order temperature coefficient for VJC
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10029"></span><span class="math inline">$\frac{1}{C^{2}}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10032"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10035"></span>1e-5
</div></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-10041"></span>TVJE
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10044"></span>1st order temperature coefficient for VJE
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10047"></span><span class="math inline">$\frac{1}{C}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10050"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10053"></span>1e-3
</div></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-10059"></span>TITF1
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10062"></span>1st order temperature coefficient for ITF
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10065"></span><span class="math inline">$\frac{1}{C}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10068"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10071"></span>1e-3
</div></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-10077"></span>TITF2
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10080"></span>2nd order temperature coefficient for ITF
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10083"></span><span class="math inline">$\frac{1}{C^{2}}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10086"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10089"></span>1e-5
</div></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-10095"></span>TTF1
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10098"></span>1st order temperature coefficient for TF
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10101"></span><span class="math inline">$\frac{1}{C}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10104"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10107"></span>1e-3
</div></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-10113"></span>TTF2
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10116"></span>2nd order temperature coefficient for TF
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10119"></span><span class="math inline">$\frac{1}{C^{2}}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10122"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10125"></span>1e-5
</div></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-10131"></span>TTR1
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10134"></span>1st order temperature coefficient for TR
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10137"></span><span class="math inline">$\frac{1}{C}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10140"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10143"></span>1e-3
</div></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-10149"></span>TTR2
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10152"></span>2nd order temperature coefficient for TR
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10155"></span><span class="math inline">$\frac{1}{C^{2}}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10158"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10161"></span>1e-5
</div></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-10167"></span>TMJE1
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10170"></span>1st order temperature coefficient for MJE
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10173"></span><span class="math inline">$\frac{1}{C}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10176"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10179"></span>1e-3
</div></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-10185"></span>TMJE2
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10188"></span>2nd order temperature coefficient for MJE
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10191"></span><span class="math inline">$\frac{1}{C^{2}}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10194"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10197"></span>1e-5
</div></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-10203"></span>TMJC1
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10206"></span>1st order temperature coefficient for MJC
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10209"></span><span class="math inline">$\frac{1}{C}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10212"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10215"></span>1e-3
</div></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-10221"></span>TMJC2
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10224"></span>2nd order temperature coefficient for MJC
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10227"></span><span class="math inline">$\frac{1}{C^{2}}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10230"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10233"></span>1e-5
</div></td>
<td></td>
</tr>
</tbody>
</table>

