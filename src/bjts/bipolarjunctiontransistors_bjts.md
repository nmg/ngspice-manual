# Bipolar Junction Transistors \(BJTs\)

General form:

``` listings
QXXXXXXX nc nb ne <ns> mname <area=val> <areac=val>
+ <areab=val> <m=val> <off> <ic=vbe,vce> <temp=val>
+ <dtemp=val>
```

Examples:

``` listings
Q23 10 24 13 QMOD IC=0.6, 5.0
Q50A 11 26 4 20 MOD1
```

**nc**,** nb**, and **ne** are the collector, base, and emitter nodes,
respectively. **ns** is the (optional) substrate node. When unspecified,
ground is used. **mname** is the model name, **area**, **areab**,
**areac** are the area factors (emitter, base and collector
respectively), and **off** indicates an (optional) initial condition on
the device for the dc analysis. If the area factor is omitted, a value
of 1.0 is assumed.

The (optional) initial condition specification using **ic=vbe,vce** is
intended for use with the **uic** option on a .tran control line, when a
transient analysis is desired to start from other than the quiescent
operating point. See the .ic control line description for a better way
to set transient initial conditions. The (optional) **temp** value is
the temperature where this device is to operate, and overrides the
temperature specification on the .option control line. Using the
**dtemp** option one can specify the instance's temperature relative to
the circuit temperature.

