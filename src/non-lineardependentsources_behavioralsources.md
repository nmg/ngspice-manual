#  Non-linear Dependent Sources (Behavioral Sources)

The non-linear dependent sources B ( see Chapt.
[5.1](#sec_B_source__ASRC_)), E (see [5.2](#sec_E_source__non_linear)),
G see ([5.3](#sec_G_source__non_linear)) described in this chapter allow
to generate voltages or currents that result from evaluating a
mathematical expression. Internally E and G sources are converted to the
more general B source. All three sources may be used to introduce
behavioral modeling and analysis.

