# User-Defined Nodes

Support is provided for User Defined Nodes that operate with the
event-driven algorithm. These nodes allow the passing of arbitrary data
structures among models. The real and integer node types supplied with
XSPICE are actually predefined User-Defined Node types.

