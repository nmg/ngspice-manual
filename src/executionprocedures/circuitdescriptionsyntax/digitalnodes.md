# Digital Nodes

Support is included for digital nodes that are simulated by an
event-driven algorithm. Because the event-driven algorithm is faster
than the standard SPICE matrix solution algorithm, and because all
digital, real, int and User-Defined Node types make use of the
event-driven algorithm, reduced simulation time for circuits that
include these models can be anticipated compared to simulation of the
same circuit using analog code models and nodes.

