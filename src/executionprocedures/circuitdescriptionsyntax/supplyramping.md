# Supply Ramping

A supply ramping function is provided by the simulator as an option to a
transient analysis to simulate the turn-on of power supplies to a board
level circuit. To enable this option, the compile time flag
**XSPICE\_EXP** has to be set, e.g. by adding CFLAGS="-DXSPICE\_EXP" to
the ./configure command line. The supply ramping function linearly ramps
the values of all independent sources and the capacitor and inductor
code models (code model extension) with initial conditions toward their
final value at a rate that you define. A complete ngspice deck example
of usage of the **ramptime** option is shown below.

Example:

``` listings
Supply ramping option
* 
* This circuit demonstrates the use of the option 
* "ramptime" that ramps independent sources and the 
* capacitor and inductor initial conditions from 
* zero to their final value during the time period 
* specified. 
* 
*
.tran 0.1 5 
.option ramptime=0.2 
* a1 1 0 cap 
.model cap capacitor (c=1000uf ic=1) 
r1 1 0 1k 
* 
a2 2 0 ind 
.model ind inductor (l=1H ic=1) 
r2 2 0 1.0 
* 
v1 3 0 1.0 
r3 3 0 1k 
* 
i1 4 0 1e-3 
r4 4 0 1k 
*
v2 5 0 0.0 sin(0 1 0.3 0 0 45.0) 
r5 5 0 1k 
*
.end
```

