# XSPICE Syntax Extensions

In the preceding discussion, NGSPICE syntax was reviewed, and those
features of NGSPICE that are specifically supported by the XSPICE
simulator were enumerated. In addition to these features, there exist
extensions to the NGSPICE capabilities that provide much more
flexibility in describing and simulating a circuit. The following
sections describe these capabilities, as well as the syntax required to
make use of them.

