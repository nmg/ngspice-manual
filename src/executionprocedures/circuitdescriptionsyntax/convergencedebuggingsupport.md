# Convergence Debugging Support

When a simulation is failing to converge, the simulator can be asked to
return convergence diagnostic information that may be useful in
identifying the areas of the circuit in which convergence problems are
occurring. When running through the Nutmeg user interface, these
messages are written directly to the terminal.

