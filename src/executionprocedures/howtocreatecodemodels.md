# How to create code models

The following instruction to create an additional code model uses the
ngspice infrastructure and some 'intelligent' copy and paste. As an
example an extra code model d\_xxor is created in the xtradev shared
library, reusing the existing d\_xor model from the digital library.
More detailed information will be made available in Chapt.
[28](#cha_Code_Models_and).

You should have downloaded ngspice, either the most recent distribution
or from the Git repository, and compiled and installed it properly
according to your operating system and the instructions given in Chapt.
[32](#cha_Compilation_notes) of the Appendix, especially Chapt.
[32.1.4](#subsec_Compilation_using_an) (for Linux users), or Chapt.
[32.2.2](#subsec_How_to_make) for MINGW and MS Windows (MS Visual Studio
will not do, because we not yet have integrated the code model generator
into this compiler\! You may however use all code models later with any
ngspice executable.) . Then change into directory
ngspice/src/xspice/icm/xtradev.

Create a new directory

mkdir d\_xxor

Copy the two files cfunc.mod and ifspec.ifs from
ngspice/src/xspice/icm/digital/d\_xor to
ngspice/src/xspice/icm/xtradev/d\_xxor. These two files may serve as a
template for your new model\!

For simplicity reasons we do only a very simple editing to these files
here, in fact the functionality is not changed, just the name translated
to a new model. Edit the new cfunc.mod: In lines 5, 28, 122, 138, 167,
178 replace the old name (d\_xor) by the new name d\_xxor. Edit the new
ifspec.ifs: In lines 16, 23, 24 replace cm\_d\_xor by cm\_d\_xxor and
d\_xor by d\_xxor.

Make ngspice aware of the new code model by editing file  
ngspice/src/xspice/icm/xtradev/modpath.lst:

Add a line with the new model name d\_xxor.

Redo ngspice by entering directory ngspice/release, and issuing the
commands:

make

sudo make install

**And that's it\!** In ngspice/release/src/xspice/icm/xtradev/ you now
will find cfunc.c and ifspec.c and the corresponding object files. The
new code model d\_xxor resides in the shared library xtradev.cm, and is
available after ngspice is started. As a test example you may edit  
ngspice/src/xspice/examples/digital\_models1.deck, and change line 60 to
the new model:

.model d\_xor1 d\_xxor (rise\_delay=1.0e-6 fall\_delay=2.0e-6
input\_load=1.0e-12)

The complete input file follows:

``` listings
Code Model Test: new xxor
*
*** analysis type ***
.tran .01s 4s
*
*** input sources ***
*
v2 200 0 DC PWL( (0 0.0) (2 0.0) (2.0000000001 1.0) (3 1.0) ) 
*
v1 100 0 DC PWL( (0 0.0) (1.0 0.0) (1.0000000001 1.0) (2 1.0) 
+     (2.0000000001 0.0) (3 0.0) (3.0000000001 1.0) (4 1.0) )
*
*** resistors to ground ***
r1 100 0 1k
r2 200 0 1k
*
*** adc_bridge blocks ***
aconverter [200 100] [2 1] adc_bridge1
.model adc_bridge1 adc_bridge (in_low=0.1 in_high=0.9 
+                  rise_delay=1.0e-12 fall_delay=1.0e-12)
*
*** xor block ***
a7 [1 2] 70 d_xor1 
.model d_xor1 d_xxor (rise_delay=1.0e-6 fall_delay=2.0e-6
+                    input_load=1.0e-12)
*
*** dac_bridge blocks ****
abridge1 [70] [out] dac1
.model dac1 dac_bridge(out_low = 0.7 out_high = 3.5
+ out_undef = 2.2 input_load = 5.0e-12 t_rise = 50e-9
+ t_fall = 20e-9)
*
*** simulation and plotting ***
.control
run
plot allv
.endc
*
.end
```

An analog input, delivered by the pwl voltage sources, is transformed
into the digital domain by an adc\_bridge, processed by the new code
model d\_xxor, and then translated back into the analog domain.

If you want to change the functionality of the new model, you have to
edit ifspec.ifs for the code model interface and cfunc.mod for the
detailed functionality of the new model. Please see Chapt.
[28](#cha_Code_Models_and), especially Chapt.
[28.6](#subsec_Interface_Specification_File) ff. for any details. And of
course you may take the existing analog, digital, mixed signal and other
existing code models (to be found in the subdirectories to
ngspice/release/src/xspice/icm) as stimulating examples for your work.

