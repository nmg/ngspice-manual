# XSPICE Code Models

In the previous example, the specification of the amplifier was
accomplished by using a NGSPICE Voltage Controlled Voltage Source
device. This is an idealization of the actual amplifier. Practical
amplifiers include numerous non-ideal effects, such as offset error
voltages and non-ideal input and output impedances. The accurate
simulation of complex, real-world components can lead to cumbersome
subcircuit files, long simulation run times, and difficulties in
synthesizing the behavior to be modeled from a limited set of internal
devices known to the simulator.

To address these problems, XSPICE allows you to create Code Models that
simulate complex, non-ideal effects without the need to develop a
subcircuit design. For example, the following file provides simulation
of the circuit in Fig. [26.2](#cap_Example_Circuit_2), but with the
subcircuit amplifier replaced with a Code Model called \`Amp' that
models several non-ideal effects including input and output impedance
and input offset voltage.

``` listings
Small Signal Amplifier
*
* This circuit simulates a small signal amplifier
* with a diode limiter.
*
.dc Vin -1 1 .05

Vin       Input 0             DC 0
R_source  Input Amp_In        100
D_Neg 0   Amp_In              1n4148
D_Pos     Amp_In 0            1n4148
C1        Amp_In 0            1uF
A1        Amp_In 0 Amp_Out    Amplifier
R_Load    Amp_Out 0           1000

.model 1n4148 D (is=2.495e-09 rs=4.755e-01 n=1.679e+00
+ tt=3.030e-09 cjo=1.700e-12 vj=1 m=1.959e-01 bv=1.000e+02
+ ibv=1.000e-04)

.model Amplifier Amp (gain = -10 in_offset = 1e-3
+                     rin = 1meg rout = 0.4)
.end
```

A statement with a device label beginning with \`A' alerts the simulator
that the device uses a Code Model. The model statement is similar in
form to the one used to specify the diode. The model label \`Amp'
directs XSPICE to use the code model with that name. Parameter
information has been added to specify a gain of -10, an input offset of
1 millivolt, an input impedance of 1 meg ohm, and an output impedance of
0.4 ohm. Subsequent sections of this document detail the steps required
to create such a Code Model and include it in the XSPICE simulator.

