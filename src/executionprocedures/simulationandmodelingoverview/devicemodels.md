# Device Models

Device models allow you to specify, when required, many of the
parameters of the devices being simulated. In this example, model
statements are used to define the silicon diodes. Electrically, the
diodes serve to limit the voltage at the amplifier input to values
between about \(\pm\)700 millivolts. The diode is simulated by first
declaring the \`instance' of each diode with a device statement. Instead
of attempting to provide parameter information separately for both
diodes, the label \`1n4148' alerts the simulator that a separate model
statement is included in the file that provides the necessary electrical
specifications for the device (\`1n4148' is the part number for the type
of diode the model is meant to simulate).

The model statement that provides this information is:

``` listings
.model 1n4148 D (is=2.495e-09 rs=4.755e-01 n=1.679e+00
+ tt=3.030e-09 cjo=1.700e-12 vj=1 m=1.959e-01
+ bv=1.000e+02 ibv=1.000e-04)
```

The model statement always begins with the string .model followed by an
identifier and the model type (D for diode, NPN for NPN transistors,
etc).

The optional parameters (\`is', \`rs', \`n', …) shown in this example
configure the simulator's mathematical model of the diode to match the
specific behavior of a particular part (e.g. a \`1n4148').

