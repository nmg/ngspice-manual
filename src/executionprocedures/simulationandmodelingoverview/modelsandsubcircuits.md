# Models and Subcircuits

The file discussed in the previous section illustrated the most basic
syntax rules of a circuit description file. However, ngspice (and other
SPICE-based simulators) include many other features that allow for
accurate modeling of semiconductor devices such as diodes and
transistors and for grouping elements of a circuit into a macro or
\`subcircuit' description that can be reused throughout a circuit
description. For instance, the file shown below is a representation of
the schematic shown in Fig. [26.2](#cap_Example_Circuit_2).

Examples for control of a behavioral resistor:

``` listings
Small Signal Amplifier with Limit Diodes
*
* This circuit simulates a small signal amplifier
* with a diode limiter.
*
.dc Vin -1 1 .05

Vin       Input 0 DC            0
R_source  Input Amp_In        100
D_Neg     0 Amp_In            1n4148
D_Pos     Amp_In 0            1n4148
C1        Amp_In 0            1uF
X1        Amp_In 0 Amp_Out    Amplifier
R_Load    Amp_Out 0           1000

.model 1n4148 D (is=2.495e-09 rs=4.755e-01 n=1.679e+00
+ tt=3.030e-09 cjo=1.700e-12 vj=1 m=1.959e-01 bv=1.000e+02
+ ibv=1.000e-04)

.subckt Amplifier Input Common Output
E1        (Output Common) (Input Common) -10
R_Input   Input           Common 1meg
.ends Amplifier

.end
```

![image:
11\_home\_nmg\_Documents\_gitroot\_ngspice-ngspice-manuals\_Images\_Example\_Circuit\_2.png](11_home_nmg_Documents_gitroot_ngspice-ngspice-manuals_Images_Example_Circuit_2.png)

Figure 26.2:  Example Circuit 2

This is the same basic circuit as in the initial example, with the
addition of two components and some changes to the simulation file. The
two diodes have been included to illustrate the use of device models,
and the amplifier is implemented with a subcircuit. Additionally, this
file shows the use of the swept DC control card.

