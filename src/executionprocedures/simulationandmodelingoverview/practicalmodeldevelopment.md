# Practical Model Development

In practice, developing models often involves using a combination of
NGSPICE passive devices, device models, subcircuits, and XSPICE Code
Models. XSPICE's Code Models may be seen as an extension to the set of
device models offered in standard NGSPICE. The collection of over 40
predefined Code Models included with XSPICE provides you with an
enriched set of modeling primitives with which to build subcircuit
models. In general, you should first attempt to construct your models
from these available primitives. This is often the quickest and easiest
method.

If you find that you cannot easily design a subcircuit to accomplish
your goal using the available primitives, then you should turn to the
code modeling approach. Because they are written in a general purpose
programming language (C), code models enable you to simulate virtually
any behavior for which you can develop a set of equations or algorithms.

