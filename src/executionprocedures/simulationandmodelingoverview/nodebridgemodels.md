# Node Bridge Models

When a mixed-mode simulator is used, some method must be provided for
translating data between the different simulation algorithms. XSPICE's
Code Model support allows you to develop models that work under the
analog simulation algorithm, the event-driven simulation algorithm, or
both at once.

In XSPICE, models developed for the express purpose of translating
between the different algorithms or between different User-Defined Node
types are called \`Node Bridge' models. For translations between the
built-in analog and digital types, predefined node bridge models are
included in the XSPICE Code Model Library.

