# Subcircuits

In some applications, describing a device by embedding the required
elements in the main circuit file, as is done for the amplifier in Fig.
[26.1](#cap_Example_Circuit_1), is not desirable. A hierarchical
approach may be taken by using subcircuits. An example of a subcircuit
statement is shown in the second circuit file:

``` listings
X1 Amp_In 0 Amp_Out
```

Amplifier Subcircuits are always identified by a device label beginning
with \`X'. Just as with other devices, all of the connected nodes are
specified. Notice, in this example, that three nodes are used. Finally,
the name of the subcircuit is specified. Elsewhere in the circuit file,
the simulator looks for a statement of the form:

``` listings
.subckt <Name> <Node1> <Node2> <Node3> ...
```

This statement specifies that the lines that follow are part of the
Amplifier subcircuit, and that the three nodes listed are to be treated
wherever they occur in the subcircuit definition as referring,
respectively, to the nodes on the main circuit from which the subcircuit
was called. Normal device, model, and comment statements may then
follow. The subcircuit definition is concluded with a statement of the
form:

``` listings
.ends <Name>
```

