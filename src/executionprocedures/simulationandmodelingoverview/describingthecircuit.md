# Describing the Circuit

This section provides an overview of the circuit description syntax
expected by the XSPICE simulator. A general understanding of circuit
description syntax will be helpful to you should you encounter problems
with your circuit and need to examine the simulator's error messages, or
should you wish to develop your own models.

This section will introduce you to the creation of circuit description
input files using the Nutmeg user interface. Note that this material is
presented in an overview form. Details of circuit description syntax are
given later in this chapter and in the previous chapters of this manual.

