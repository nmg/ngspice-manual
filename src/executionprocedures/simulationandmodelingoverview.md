# Simulation and Modeling Overview

This section introduces the concepts of circuit simulation and modeling.
It is intended primarily for users who have little or no previous
experience with circuit simulators, and also for those who have not used
circuit simulators recently. However, experienced SPICE users may wish
to scan the material presented here since it provides background for new
Code Model and User-Defined Node capabilities of the XSPICE option.

