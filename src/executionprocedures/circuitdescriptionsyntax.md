# Circuit Description Syntax

If you need to debug a simulation, if you are planning to develop your
own models, or if you are using the XSPICE simulator through the Nutmeg
user interface, you will need to become familiar with the circuit
description language.

The previous sections presented example circuit description input files.
The following sections provide more detail on XSPICE circuit
descriptions with particular emphasis on the syntax for creating and
using models. First, the language and syntax of the NGSPICE simulator
are described and references to additional information are given. Next,
XSPICE extensions to the ngspice syntax are detailed. Finally, various
enhancements to NGSPICE operation are discussed including polynomial
sources, arbitrary phase sources, supply ramping, matrix conditioning,
convergence options, and debugging support.

