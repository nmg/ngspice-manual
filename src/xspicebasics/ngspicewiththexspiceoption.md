# ngspice with the XSPICE option

The XSPICE option allows you to add event-driven simulation capabilities
to NGSPICE. NGSPICE now is the main software program that performs
mathematical simulation of a circuit specified by you, the user. It
takes input in the form of commands and circuit descriptions and
produces output data (e.g. voltages, currents, digital states, and
waveforms) that describe the circuit’s behavior.

Plain NGSPICE is designed for analog simulation and is based exclusively
on matrix solution techniques. The XSPICE option adds even-driven
simulation capabilities. Thus, designs that contain significant portions
of digital circuitry can be efficiently simulated together with analog
components. NGSPICE with XSPICE option also includes a \`User-Defined
Node' capability that allows event-driven simulations to be carried out
with any type of data.

The XSPICE option has been developed by the Computer Science and
Information Technology Laboratory at Georgia Tech Research Institute of
the Georgia Institute of Technology, Atlanta, Georgia 30332 at around
1990 and enhanced by the NGSPICE team. The manual is based on the
original XSPICE user's manual, no longer available from Georgia Tech,
but from the [ngspice web
site](http://ngspice.sourceforge.net/literature.html).

In the following, the term \`XSPICE' may be read as \`NGSPICE with
XSPICE code model subsystem enabled'. You may enable the option by
adding --enable-xspice to the ./configure command. The MS Windows
distribution already contains the XSPICE option.

