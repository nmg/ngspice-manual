# XSPICE Top-Level Diagram

A top-level diagram of the XSPICE system integrated into ngspice is
shown in Fig. [25.1](#cap_ngspice_XSPICE_Top_Level_Diagram). The XSPICE
Simulator is made up of the NGSPICE core, the event-driven algorithm,
circuit description syntax parser extensions, a loading routine for code
models, and the Nutmeg user interface. The XSPICE Code Model Subsystem
consists of the Code Model Generator, 5 standard code model library
sources with more than 40 code models, the sources for Node Type
Libraries, and all the interfaces to User-Defined Code Models and to
User-Defined Node Types.

![image:
9\_home\_nmg\_Documents\_gitroot\_ngspice-ngspice-manuals\_Images\_XSPICE-Toplevel.png](9_home_nmg_Documents_gitroot_ngspice-ngspice-manuals_Images_XSPICE-Toplevel.png)

Figure 25.1:  ngspice/XSPICE Top-Level Diagram

