# The XSPICE Code Model Subsystem

The new component of ngspice, the Code Model Subsystem, provides the
tools needed to model the various parts of your system. While NGSPICE is
targeted primarily at integrated circuit (IC) analysis, XSPICE includes
features to model and simulate board-level and system-level designs as
well. The Code Model Subsystem is central to this new capability,
providing XSPICE with an extensive set of models to use in designs and
allowing you to add your own models to this model set.

The NGSPICE simulator at the core of XSPICE includes built-in models for
discrete components commonly found within integrated circuits. These
\`model primitives' include components such as resistors, capacitors,
diodes, and transistors. The XSPICE Code Model Subsystem extends this
set of primitives in two ways. First, it provides a library of over 40
additional primitives, including summers, integrators, digital gates,
controlled oscillators, s-domain transfer functions, and digital state
machines. See Chapt. [12](#cha_Behavioral_Modeling) for a description of
the library entries. Second, it adds a code model generator to ngspice
that provides a set of programming utilities to make it easy for you to
create your own models by writing them in the C programming language.

The code models are generated upon compiling ngspice. They are stored in
shared libraries, which may be distributed independently from the
ngspice sources. Upon runtime ngspice will load the code model libraries
and make the code model instances available for simulation.

