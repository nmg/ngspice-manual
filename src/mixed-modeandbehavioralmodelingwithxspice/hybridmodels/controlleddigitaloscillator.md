# Controlled Digital Oscillator

NAME\_TABLE:

C\_Function\_Name: cm\_d\_osc

Spice\_Model\_Name: d\_osc

Description: "controlled digital oscillator"

PORT\_TABLE:

Port Name: cntl\_in out

Description: "control input" "output"

Direction: in out

Default\_Type: v d

Allowed\_Types: \[v,vd,i,id\] \[d\]

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: no no

PARAMETER\_TABLE:

Parameter\_Name: cntl\_array freq\_array

Description: "control array" "frequency array"

Data\_Type: real real

Default\_Value: 0.0 1.0e6

Limits: - \[0 -\]

Vector: yes yes

Vector\_Bounds: \[2 -\] cntl\_array

Null\_Allowed: no no

PARAMETER\_TABLE:

Parameter\_Name: duty\_cycle init\_phase

Description: "duty cycle" "initial phase of output"

Data\_Type: real real

Default\_Value: 0.5 0

Limits: \[1e-6 0.999999\] \[-180.0 +360.0\]

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: yes yes

PARAMETER\_TABLE:

Parameter\_Name: rise\_delay fall\_delay

Description: "rise delay" "fall delay"

Data\_Type: real real

Default\_Value: 1e-9 1e-9

Limits: \[0 -\] \[0 -\]

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: yes yes

  - Description:  
    The digital oscillator is a hybrid model that accepts as input a
    voltage or current. This input is compared to the
    voltage-to-frequency transfer characteristic specified by the
    cntl\_array/freq\_array coordinate pairs, and a frequency is
    obtained that represents a linear interpolation or extrapolation
    based on those pairs. A digital time-varying signal is then produced
    with this fundamental frequency.  
    The output waveform, which is the equivalent of a digital clock
    signal, has rise and fall delays that can be specified
    independently. In addition, the duty cycle and the phase of the
    waveform are also variable and can be set by you.

Example SPICE Usage:

a5 1 8 var\_clock

.model var\_clock d\_osc(cntl\_array = \[-2 -1 1 2\]

\+ freq\_array = \[1e3 1e3 10e3 10e3\]

\+ duty\_cycle = 0.4 init\_phase = 180.0

\+ rise\_delay = 10e-9 fall\_delay=8e-9)

