# Node bridge from real to analog voltage

NAME\_TABLE:

Spice\_Model\_Name: real\_to\_v

C\_Function\_Name: ucm\_real\_to\_v

Description: "Node bridge from real to analog voltage"

PORT\_TABLE:

Port\_Name: in out

Description: "input" "output"

Direction: in out

Default\_Type: real v

Allowed\_Types: \[real\] \[v, vd, i, id\]

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: no no

PARAMETER\_TABLE:

Parameter\_Name: gain transition\_time

Description: "gain" "output transition time"

Data\_Type: real real

Default\_Value: 1.0 1e-9

Limits: - \[1e-15 -\]

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: yes yes

