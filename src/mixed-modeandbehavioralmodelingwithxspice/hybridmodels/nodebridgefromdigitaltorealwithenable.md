# Node bridge from digital to real with enable

NAME\_TABLE:

Spice\_Model\_Name: d\_to\_real

C\_Function\_Name: ucm\_d\_to\_real

Description: "Node bridge from digital to real with enable"

PORT\_TABLE:

Port\_Name: in enable out

Description: "input" "enable" "output"

Direction: in in out

Default\_Type: d d real

Allowed\_Types: \[d\] \[d\] \[real\]

Vector: no no no

Vector\_Bounds: - - -

Null\_Allowed: no yes no

PARAMETER\_TABLE:

Parameter\_Name: zero one delay

Description: "value for 0" "value for 1" "delay"

Data\_Type: real real real

Default\_Value: 0.0 1.0 1e-9

Limits: - - \[1e-15 -\]

Vector: no no no

Vector\_Bounds: - - -

Null\_Allowed: yes yes yes

