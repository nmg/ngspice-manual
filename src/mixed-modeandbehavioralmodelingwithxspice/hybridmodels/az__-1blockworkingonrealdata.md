# A Z\*\*-1 block working on real data

NAME\_TABLE:

Spice\_Model\_Name: real\_delay

C\_Function\_Name: ucm\_real\_delay

Description: "A Z \*\* -1 block working on real data"

PORT\_TABLE:

Port\_Name: in clk out

Description: "input" "clock" "output"

Direction: in in out

Default\_Type: real d real

Allowed\_Types: \[real\] \[d\] \[real\]

Vector: no no no

Vector\_Bounds: - - -

Null\_Allowed: no no no

PARAMETER\_TABLE:

Parameter\_Name: delay

Description: "delay from clk to out"

Data\_Type: real

Default\_Value: 1e-9

Limits: \[1e-15 -\]

Vector: no

Vector\_Bounds: -

Null\_Allowed: yes

