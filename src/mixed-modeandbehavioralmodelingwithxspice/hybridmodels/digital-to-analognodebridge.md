# Digital-to-Analog Node Bridge

NAME\_TABLE:

C\_Function\_Name: cm\_dac\_bridge

Spice\_Model\_Name: dac\_bridge

Description: "digital-to-analog node bridge"

PORT\_TABLE:

Port Name: in out

Description: "input" "output"

Direction: in out

Default\_Type: d v

Allowed\_Types: \[d\] \[v,vd,i,id,d\]

Vector: yes yes

Vector\_Bounds: - -

Null\_Allowed: no no

PARAMETER\_TABLE:

Parameter\_Name: out\_low

Description: "0-valued analog output"

Data\_Type: real

Default\_Value: 0.0

Limits: -

Vector: no

Vector\_Bounds: -

Null\_Allowed: yes

PARAMETER\_TABLE:

Parameter\_Name: out\_high

Description: "1-valued analog output"

Data\_Type: real

Default\_Value: 1.0

Limits: -

Vector: no

Vector\_Bounds: -

Null\_Allowed: yes

PARAMETER\_TABLE:

Parameter\_Name: out\_undef input\_load

Description: "U-valued analog output" "input load (F)"

Data\_Type: real real

Default\_Value: 0.5 1.0e-12

Limits: - -

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: yes yes

PARAMETER\_TABLE:

Parameter\_Name: t\_rise t\_fall

Description: "rise time 0-\>1" "fall time 1-\>0"

Data\_Type: real real

Default\_Value: 1.0e-9 1.0e-9

Limits: - -

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: yes yes

  - Description:  
    The dac\_bridge is the first of two node bridge devices designed to
    allow for the ready transfer of digital information to analog values
    and back again. The second device is the adc\_bridge (which takes an
    analog value and maps it to a digital one).The dac\_bridge takes as
    input a digital value from a digital node. This value by definition
    may take on only one of the values \`0', \`1' or \`U'. The
    dac\_bridge then outputs the value out\_low, out\_high or
    out\_undef, or ramps linearly toward one of these \`final' values
    from its current analog output level. The speed at which this
    ramping occurs depends on the values of t\_rise and t\_fall. These
    parameters are interpreted by the model such that the rise or fall
    slope generated is always constant. *Note that the* dac\_bridge
    *includes test code in its cfunc.mod file for determining the
    presence of the out\_undef parameter. If this parameter is not
    specified by you, and if* out\_high *and* out\_low *values are
    specified, then out\_undef is assigned the value of the arithmetic
    mean of* out\_high *and* out\_low**.** This simplifies coding of
    output buffers, where typically a logic family will include an
    out\_low and out\_high voltage, but not an out\_undef value. This
    model also posts an input load value (in farads) based on the
    parameter input load.

Example SPICE Usage:

abridge1 \[7\] \[2\] dac1

.model dac1 dac\_bridge(out\_low = 0.7 out\_high = 3.5 out\_undef = 2.2

\+ input\_load = 5.0e-12 t\_rise = 50e-9

\+ t\_fall = 20e-9)

