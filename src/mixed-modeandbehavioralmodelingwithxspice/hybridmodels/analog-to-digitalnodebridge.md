# Analog-to-Digital Node Bridge

NAME\_TABLE:

C\_Function\_Name: cm\_adc\_bridge

Spice\_Model\_Name: adc\_bridge

Description: "analog-to-digital node bridge"

PORT\_TABLE:

Port Name: in out

Description: "input" "output"

Direction: in out

Default\_Type: v d

Allowed\_Types: \[v,vd,i,id,d\] \[d\]

Vector: yes yes

Vector\_Bounds: - -

Null\_Allowed: no no

PARAMETER\_TABLE:

Parameter\_Name: in\_low

Description: "maximum 0-valued analog input"

Data\_Type: real

Default\_Value: 1.0

Limits: -

Vector: no

Vector\_Bounds: -

Null\_Allowed: yes

PARAMETER\_TABLE:

Parameter\_Name: in\_high

Description: "minimum 1-valued analog input"

Data\_Type: real

Default\_Value: 2.0

Limits: -

Vector: no

Vector\_Bounds: -

Null\_Allowed: yes

PARAMETER\_TABLE:

Parameter\_Name: rise\_delay fall\_delay

Description: "rise delay" "fall delay"

Data\_Type: real real

Default\_Value: 1.0e-9 1.0e-9

Limits: \[1.0e-12 -\] \[1.0e-12 -\]

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: yes yes

  - Description:  
    The adc\_bridge is one of two node bridge devices designed to allow
    for the ready transfer of analog information to digital values and
    back again. The second device is the dac\_bridge (which takes a
    digital value and maps it to an analog one). The adc\_bridge takes
    as input an analog value from an analog node. This value by
    definition may be in the form of a voltage, or a current. If the
    input value is less than or equal to in\_low, then a digital output
    value of \`0' is generated. If the input is greater than or equal to
    in\_high, a digital output value of \`1' is generated. If neither of
    these is true, then a digital \`UNKNOWN' value is output. Note that
    unlike the case of the dac\_bridge, no ramping time or delay is
    associated with the adc\_bridge. Rather, the continuous ramping of
    the input value provides for any associated delays in the digitized
    signal.

Example SPICE Usage:

abridge2 \[1\] \[8\] adc\_buff

.model adc\_buff adc\_bridge(in\_low = 0.3 in\_high = 3.5)

