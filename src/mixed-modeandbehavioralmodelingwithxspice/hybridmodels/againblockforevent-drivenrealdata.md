# A gain block for event-driven real data

NAME\_TABLE:

Spice\_Model\_Name: real\_gain

C\_Function\_Name: ucm\_real\_gain

Description: "A gain block for event-driven real data"

PORT\_TABLE:

Port\_Name: in out

Description: "input" "output"

Direction: in out

Default\_Type: real real

Allowed\_Types: \[real\] \[real\]

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: no no

PARAMETER\_TABLE:

Parameter\_Name: in\_offset gain out\_offset

Description: "input offset" "gain" "output offset"

Data\_Type: real real real

Default\_Value: 0.0 1.0 0.0

Limits: - - -

Vector: no no no

Vector\_Bounds: - - -

Null\_Allowed: yes yes yes

PARAMETER\_TABLE:

Parameter\_Name: delay ic

Description: "delay" "initial condition"

Data\_Type: real real

Default\_Value: 1.0e-9 0.0

Limits: - -

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: yes yes

