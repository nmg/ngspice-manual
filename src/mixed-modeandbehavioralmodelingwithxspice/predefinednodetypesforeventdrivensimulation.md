# Predefined Node Types for event driven simulation

The following pre-written node types are included with the XSPICE
simulator. These should provide you not only with valuable event-driven
modeling capabilities, but also with examples to use for guidance in
creating new UDN (user defined node) types. You may access these node
data by the plot ([17.5.48](#subsec_Plot__Plot_values)) or eprint
([17.5.25](#subsec_Eprint___Print_an)) commands.

