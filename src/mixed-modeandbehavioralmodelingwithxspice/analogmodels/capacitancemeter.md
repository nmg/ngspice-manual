# Capacitance Meter

NAME\_TABLE:

C\_Function\_Name: cm\_cmeter

Spice\_Model\_Name: cmeter

Description: "capacitance meter"

PORT\_TABLE:

Port Name: in out

Description: "input" "output"

Direction: in out

Default\_Type: v v

Allowed\_Types: \[v,vd,i,id\] \[v,vd,i,id\]

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: no no

PARAMETER\_TABLE:

Parameter\_Name: gain

Description: "gain"

Data\_Type: real

Default\_Value: 1.0

Limits: -

Vector: no

Vector\_Bounds: -

Null\_Allowed: yes

  - Description:  
    The capacitance meter is a sensing device that is attached to a
    circuit node and produces as an output a scaled value equal to the
    total capacitance seen on its input multiplied by the gain
    parameter. This model is primarily intended as a building block for
    other models that must sense a capacitance value and alter their
    behavior based upon it.

Example SPICE Usage:

atest1 1 2 ctest

.model ctest cmeter(gain=1.0e12)

