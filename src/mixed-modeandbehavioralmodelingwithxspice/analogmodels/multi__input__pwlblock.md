# multi\_input\_pwl block

NAME\_TABLE:

C\_Function\_Name: cm\_multi\_input\_pwl

Spice\_Model\_Name: multi\_input\_pwl

Description: "multi\_input\_pwl block"

PORT\_TABLE:

Port\_Name: in out

Description: "input array" "output"

Direction: in out

Default\_Type: vd vd

Allowed\_Types: \[vd,id\] \[vd,id\]

Vector: yes no

Vector\_Bounds: \[2 -\] -

Null\_Allowed: no no

PARAMETER\_TABLE:

Parameter\_Name: x y

Description: "x array" "y array"

Data\_Type: real real

Default\_Value: 0.0 0.0

Limits: - -

Vector: yes yes

Vector\_Bounds: \[2 -\] \[2 -\]

Null\_Allowed: no no

PARAMETER\_TABLE:

Parameter\_Name: model

Description: "model type"

Data\_Type: string

Default\_Value: "and"

Limits: -

Vector: no

Vector\_Bounds: -

Null\_Allowed: yes

  - Description:  
    Multi-input gate voltage controlled voltage source that supports
    **and** or **or** gating. The x's and y's represent the piecewise
    linear variation of output (y) as a function of input (x). The type
    of gate is selectable by the parameter model. In case the model is
    **and**, the smallest input determines the output value (i.e. the
    and function). In case the model is **or**, the largest input
    determines the output value (i.e. the or function). The inverse of
    these functions (i.e. nand and nor) is constructed by complementing
    the y array.

Example SPICE Usage:

a82 \[1 0 2 0 3 0\] 7 0 pwlm

.

.

.model pwlm multi\_input\_pwl((x=\[-2.0 -1.0 2.0 4.0 5.0\]

\+ y=\[-0.2 -0.2 0.1 2.0 10.0\]

\+ model="and")

