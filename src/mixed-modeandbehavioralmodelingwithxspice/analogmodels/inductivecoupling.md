# Inductive Coupling

NAME\_TABLE:

C\_Function\_Name: cm\_lcouple

Spice\_Model\_Name: lcouple

Description: "inductive coupling (for use with 'core' model)"

PORT\_TABLE:

Port\_Name: l mmf\_out

Description: "inductor" "mmf output (in ampere-turns)"

Direction: inout inout

Default\_Type: hd hd

Allowed\_Types: \[h,hd\] \[hd\]

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: no no

PARAMETER\_TABLE:

Parameter\_Name: num\_turns

Description: "number of inductor turns"

Data\_Type: real

Default\_Value: 1.0

Limits: -

Vector: no

Vector\_Bounds: -

Null\_Allowed: yes

  - Description:  
    This function is a conceptual model that is used as a building block
    to create a wide variety of inductive and magnetic circuit models.
    This function is normally used in conjunction with the core model,
    but can also be used with resistors, hysteresis blocks, etc. to
    build up systems that mock the behavior of linear and nonlinear
    components.  
    The lcouple takes as an input (on the \`l' port), a current. This
    current value is multiplied by the num\_turns value, N, to produce
    an output value (a voltage value that appears on the mmf\_out port).
    The mmf\_out acts similar to a magnetomotive force in a magnetic
    circuit; when the lcouple is connected to the core model, or to some
    other resistive device, a current will flow. This current value
    (which is modulated by whatever the lcouple is connected to) is then
    used by the lcouple to calculate a voltage \`seen' at the l port.
    The voltage is a function of the derivative with respect to time of
    the current value seen at mmf\_out.  
    The most common use for lcouples will be as a building block in the
    construction of transformer models. To create a transformer with a
    single input and a single output, you would require two lcouple
    models plus one core model. The process of building up such a
    transformer is described under the description of the core model,
    below.

Example SPICE Usage:

a150 (7 0) (9 10) lcouple1

.model lcouple1 lcouple(num\_turns=10.0)

