# PWL Controlled Source

NAME\_TABLE:

C\_Function\_Name: cm\_pwl

Spice\_Model\_Name: pwl

Description: "piecewise linear controlled source"

PORT\_TABLE:

Port\_Name: in out

Description: "input" "output"

Direction: in out

Default\_Type: v v

Allowed\_Types: \[v,vd,i,id,vnam\] \[v,vd,i,id\]

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: no no

PARAMETER\_TABLE:

Parameter\_Name: x\_array y\_array

Description: "x-element array" "y-element array"

Data\_Type: real real

Default\_Value: - -

Limits: - -

Vector: yes yes

Vector\_Bounds: \[2 -\] \[2 -\]

Null\_Allowed: no no

PARAMETER\_TABLE:

Parameter\_Name: input\_domain fraction

Description: "input sm. domain" "smoothing %/abs switch"

Data\_Type: real boolean

Default\_Value: 0.01 TRUE

Limits: \[1e-12 0.5\] -

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: yes yes

STATIC\_VAR\_TABLE:

Static\_Var\_Name: last\_x\_value

Data\_Type: pointer

Description: "iteration holding variable for limiting"

  - Description:  
    The Piece-Wise Linear Controlled Source is a single input, single
    output function similar to the Gain Block. However, the output of
    the PWL Source is not necessarily linear for all values of input.
    Instead, it follows an I/O relationship specified by you via the
    x\_array and y\_array coordinates. This is detailed below.  
    The x\_array and y\_array values represent vectors of coordinate
    points on the x and y axes, respectively. The x\_array values are
    progressively increasing input coordinate points, and the associated
    y\_array values represent the outputs at those points. There may be
    as few as two (x\_array\[n\], y\_array\[n\]) pairs specified, or as
    many as memory and simulation speed allow. This permits you to very
    finely approximate a non-linear function by capturing multiple
    input-output coordinate points.  
    Two aspects of the PWL Controlled Source warrant special attention.
    These are the handling of endpoints and the smoothing of the
    described transfer function near coordinate points.  
    In order to fully specify outputs for values of in outside of the
    bounds of the PWL function (i.e., less than x\_array\[0\] or greater
    than x\_array\[n\], where n is the largest user-specified coordinate
    index), the PWL Controlled Source model extends the slope found
    between the lowest two coordinate pairs and the highest two
    coordinate pairs. This has the effect of making the transfer
    function completely linear for in less than x\_array\[0\] and in
    greater than x\_array\[n\]. It also has the potentially subtle
    effect of unrealistically causing an output to reach a very large or
    small value for large inputs. You should thus keep in mind that the
    PWL Source does not inherently provide a limiting capability.  
    In order to diminish the potential for non-convergence of
    simulations when using the PWL block, a form of smoothing around the
    x\_array, y\_array coordinate points is necessary. This is due to
    the iterative nature of the simulator and its reliance on smooth
    first derivatives of transfer functions in order to arrive at a
    matrix solution. Consequently, the input\_domain and fraction
    parameters are included to allow you some control over the amount
    and nature of the smoothing performed.  
    Fraction is a switch that is either TRUE or FALSE. When TRUE (the
    default setting), the simulator assumes that the specified input
    domain value is to be interpreted as a fractional figure. Otherwise,
    it is interpreted as an absolute value. Thus, if fraction=TRUE and
    input\_domain=0.10, The simulator assumes that the smoothing radius
    about each coordinate point is to be set equal to 10% of the length
    of either the x\_array segment above each coordinate point, or the
    x\_array segment below each coordinate point. The specific segment
    length chosen will be the smallest of these two for each coordinate
    point.  
    On the other hand, if fraction=FALSE and input=0.10, then the
    simulator will begin smoothing the transfer function at 0.10 volts
    (or amperes) below each x\_array coordinate and will continue the
    smoothing process for another 0.10 volts (or amperes) above each
    x\_array coordinate point. Since the overlap of smoothing domains is
    not allowed, checking is done by the model to ensure that the
    specified input domain value is not excessive.  
    One subtle consequence of the use of the fraction=TRUE feature of
    the PWL Controlled Source is that, in certain cases, you may
    inadvertently create extreme smoothing of functions by choosing
    inappropriate coordinate value points. This can be demonstrated by
    considering a function described by three coordinate pairs, such as
    (-1,-1), (1,1), and (2,1). In this case, with a 10% input\_domain
    value specified (fraction=TRUE, input\_domain=0.10), you would
    expect to see rounding occur between in=0.9 and in=1.1, and nowhere
    else. On the other hand, if you were to specify the same function
    using the coordinate pairs (-100,-100), (1,1) and (201,1), you would
    find that rounding occurs between in=-19 and in=21. Clearly in the
    latter case the smoothing might cause an excessive divergence from
    the intended linearity above and below in=1.

Example SPICE Usage:

a7 2 4 xfer\_cntl1

.

.

.model xfer\_cntl1 pwl(x\_array=\[-2.0 -1.0 2.0 4.0 5.0\]

\+ y\_array=\[-0.2 -0.2 0.1 2.0 10.0\]

\+ input\_domain=0.05 fraction=TRUE)

