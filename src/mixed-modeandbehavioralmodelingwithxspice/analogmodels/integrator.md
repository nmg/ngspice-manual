# Integrator

NAME\_TABLE:

C\_Function\_Name: cm\_int

Spice\_Model\_Name: int

Description: "time-integration block"

PORT\_TABLE:

Port Name: in out

Description: "input" "output"

Direction: in out

Default\_Type: v v

Allowed\_Types: \[v,vd,i,id\] \[v,vd,i,id\]

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: no no

PARAMETER\_TABLE:

Parameter\_Name: in\_offset gain

Description: "input offset" "gain"

Data\_Type: real real

Default\_Value: 0.0 1.0

Limits: - -

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: yes yes

PARAMETER\_TABLE:

Parameter\_Name: out\_lower\_limit out\_upper\_limit

Description: "output lower limit" "output upper limit"

Data\_Type: real real

Default\_Value: - -

Limits: - -

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: yes yes

PARAMETER\_TABLE:

Parameter\_Name: limit\_range

Description: "upper & lower limit smoothing range"

Data\_Type: real

Default\_Value: 1.0e-6

Limits: -

Vector: no

Vector\_Bounds: -

Null\_Allowed: yes

PARAMETER\_TABLE:

Parameter\_Name: out\_ic

Description: "output initial condition"

Data\_Type: real

Default\_Value: 0.0

Limits: -

Vector: no

Vector\_Bounds: -

Null\_Allowed: yes

  - Description:  
    The Integrator block is a simple integration stage that approximates
    the integral with respect to time of an input signal. The block also
    includes gain and input offset parameters to allow for tailoring of
    the required signal, and output upper and lower limits to prevent
    convergence errors resulting from excessively large output values.
    Note that these limits specify integrator behavior similar to that
    found in an operational amplifier-based integration stage, in that
    once a limit is reached, additional storage does not occur. Thus,
    the input of a negative value to an integrator that is currently
    driving at the out upper limit level will immediately cause a drop
    in the output, regardless of how long the integrator was previously
    summing positive inputs. The incremental value of output below the
    output upper limit and above the output lower limit at which
    smoothing begins is specified via the limit range parameter. In AC
    analysis, the value returned is equal to the gain divided by the
    radian frequency of analysis.  
    Note that truncation error checking is included in the int block.
    This should provide for a more accurate simulation of the time
    integration function, since the model will inherently request
    smaller time increments between simulation points if truncation
    errors would otherwise be excessive.

Example SPICE Usage:

a13 7 12 time\_count

.

.

.model time\_count int(in\_offset=0.0 gain=1.0

\+ out\_lower\_limit=-1e12 out\_upper\_limit=1e12

\+ limit\_range=1e-9 out\_ic=0.0)

