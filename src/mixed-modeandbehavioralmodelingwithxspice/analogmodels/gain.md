# Gain

NAME\_TABLE:

C\_Function\_Name: cm\_gain

Spice\_Model\_Name: gain

Description: "A simple gain block"

PORT\_TABLE:

Port Name: in out

Description: "input" "output"

Direction: in out

Default\_Type: v v

Allowed\_Types: \[v,vd,i,id,vnam\] \[v,vd,i,id\]

Vector: no no

Vector.Bounds: - -

Null.Allowed: no no

PARAMETER\_TABLE:

Parameter\_Name: in\_offset gain out\_offset

Description: "input offset" "gain" "output offset"

Data\_Type: real real real

Default\_Value: 0.0 1.0 0.0

Limits: - - -

Vector: no no no

Vector\_Bounds: - - -

Null\_Allowed: yes yes yes

  - Description:  
    This function is a simple gain block with optional offsets on the
    input and the output. The input offset is added to the input, the
    sum is then multiplied by the gain, and the result is produced by
    adding the output offset. This model will operate in DC, AC, and
    Transient analysis modes.

Example:

``` listings
 
a1 1 2 amp 
.model amp gain(in_offset=0.1 gain=5.0 
+ out_offset=-0.01)
```

