# Controlled One-Shot

NAME\_TABLE:

C\_Function\_Name: cm\_oneshot

Spice\_Model\_Name: oneshot

Description: "controlled one-shot"

PORT\_TABLE:

Port Name: clk cntl\_in

Description: "clock input" "control input"

Direction: in in

Default\_Type: v v

Allowed\_Types: \[v,vd,i,id\] \[v,vd,i,id\]

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: no yes

PORT\_TABLE:

Port Name: clear out

Description: "clear signal" "output"

Direction: in out

Default\_Type: v v

Allowed\_Types: \[v,vd,i,id\] \[v,vd,i,id\]

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: yes no

PARAMETER\_TABLE:

Parameter\_Name: clk\_trig retrig

Description: "clock trigger value" "retrigger switch"

Data\_Type: real boolean

Default\_Value: 0.5 FALSE

Limits: - -

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: no yes

PARAMETER\_TABLE:

Parameter\_Name: pos\_edge\_trig

Description: "positive/negative edge trigger switch"

Data\_Type: boolean

Default\_Value: TRUE

Limits: -

Vector: no

Vector\_Bounds: -

Null\_Allowed: no

PARAMETER\_TABLE:

Parameter\_Name: cntl\_array pw\_array

Description: "control array" "pulse width array"

Data\_Type: real real

Default\_Value: 0.0 1.0e-6

Limits: - \[0.00 -\]

Vector: yes yes

Vector\_Bounds: - cntl\_array

Null\_Allowed: yes yes

PARAMETER\_TABLE:

Parameter\_Name: out\_low out\_high

Description: "output low value" "output high value"

Data\_Type: real real

Default\_Value: 0.0 1.0

Limits: - -

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: yes yes

PARAMETER\_TABLE:

Parameter\_Name: fall\_time rise\_time

Description: "output fall time" "output rise time"

Data\_Type: real real

Default\_Value: 1.0e-9 1.0e-9

Limits: - -

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: yes yes

PARAMETER\_TABLE:

Parameter\_Name: rise\_delay

Description: "output delay from trigger"

Data\_Type: real

Default\_Value: 1.0e-9

Limits: -

Vector: no

Vector\_Bounds: -

Null\_Allowed: yes

PARAMETER\_TABLE:

Parameter\_Name: fall\_delay

Description: "output delay from pw"

Data\_Type: real

Default\_Value: 1.0e-9

Limits: -

Vector: no

Vector\_Bounds: -

Null\_Allowed: yes

  - Description:  
    This function is a controlled oneshot with parametrizable values of
    low and high peak output, input trigger value level, delay, and
    output rise and fall times. It takes an input voltage or current
    value. This value is used as the independent variable in the
    piecewise linear curve described by the coordinate points of the
    cntl\_array and pw\_array pairs. From the curve, a pulse width value
    is determined. The one-shot will output a pulse of that width,
    triggered by the clock signal (rising or falling edge), delayed by
    the delay value, and with specified rise and fall times. A positive
    slope on the clear input will immediately terminate the pulse, which
    resets with its fall time.  
    From the above, it is easy to see that array sizes of 2 for both the
    cntl\_array and the pw\_array will yield a linear variation of the
    pulse width with respect to the control input. Any sizes greater
    than 2 will yield a piecewise linear transfer characteristic. For
    more detail, refer to the description of the piecewise linear
    controlled source, which uses a similar method to derive an output
    value given a control input.

Example SPICE Usage:

ain 1 2 3 4 pulse2

.model pulse2 oneshot(cntl\_array = \[-1 0 10 11\]

\+ pw\_array=\[1e-6 1e-6 1e-4 1e-4\]

\+ clk\_trig = 0.9 pos\_edge\_trig = FALSE

\+ out\_low = 0.0 out\_high = 4.5

\+ rise\_delay = 20.0-9 fall\_delay = 35.0e-9)

