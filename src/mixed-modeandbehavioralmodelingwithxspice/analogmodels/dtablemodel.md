# 2D table model

NAME\_TABLE:

C\_Function\_Name: cm\_table2D

Spice\_Model\_Name: table2D

Description: "2D table model"

PORT\_TABLE:

Port\_Name: inx iny out

Description: "inputx" "inputy" "output"

Direction: in in out

Default\_Type: v v i

Allowed\_Types: \[v,vd,i,id,vnam\] \[v,vd,i,id,vnam\] \[v,vd,i,id\]

Vector: no no no

Vector\_Bounds: - - -

Null\_Allowed: no no no

PARAMETER\_TABLE:

Parameter\_Name: order verbose

Description: "order" "verbose"

Data\_Type: int int

Default\_Value: 3 0

Limits: - -

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: yes yes

PARAMETER\_TABLE:

Parameter\_Name: offset gain

Description: "offset" "gain"

Data\_Type: real real

Default\_Value: 0.0 1.0

Limits: - -

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: yes yes

PARAMETER\_TABLE:

Parameter\_Name: file

Description: "file name"

Data\_Type: string

Default\_Value: "2D-table-model.txt"

Limits: -

Vector: no

Vector\_Bounds: -

Null\_Allowed: yes

  - Description:  
    The 2D table model reads a matrix from file "file name" (default
    2D-table-model.txt) which has x columns and y rows. Each x,y pair,
    addressed by inx and iny, yields an output value out. Linear
    interpolation is used for out, eno (essentially non oscillating)
    interpolation for its derivatives. Parameters offset (default 0) and
    gain (default 1) modify the output table values according to
    \(offset + gain out\). Parameter order (default 3) influences the
    calculation of the derivatives. Parameter verbose (default 0) yields
    test outputs, if set to 1 or 2. The table format is shown below. Be
    careful to include the data point inx = 0, iny = 0 into your table,
    because ngspice uses these during .OP computations. The x horizontal
    and y vertical address values have to increase monotonically.

Table Example:

\* table source

\* number of columns (x)

8

\* number of rows (y)

9

\* x horizontal (column) address values (real numbers)

\-1 0 1 2 3 4 5 6

\* y vertical (row) address values (real numbers)

\-0.6 0 0.6 1.2 1.8 2.4 3.0 3.6 4.2

\* table with output data (horizontally addressed by x, vertically by y)

1 0.9 0.8 0.7 0.6 0.5 0.4 0.3

1 1 1 1 1 1 1 1

1 1.2 1.4 1.6 1.8 2 2.2 2.4

1 1.5 2 2.5 3 3.5 4 4.5

1 2 3 4 5 6 7 8

1 2.5 4 5.5 7 8.5 10 11.5

1 3 5 7 9 11 13 15

1 3.5 6 8.5 11 13.5 16 18.5

1 4 7 10 13 16 19 22

  - Description:  
    The usage example consists of two input voltages referenced to
    ground and a current source output with two floating nodes.

Example SPICE Usage:

atab inx iny %id(out1 out2) tabmod

.model tabmod table2d (offset=0.0 gain=1 order=3
file="table-simple.txt")

# 3D table model

NAME\_TABLE:

C\_Function\_Name: cm\_table3D

Spice\_Model\_Name: table3D

Description: "3D table model"

PORT\_TABLE:

Port\_Name: inx iny inz

Description: "inputx" "inputy" "inputz"

Direction: in in in

Default\_Type: v v v

Allowed\_Types: \[v,vd,i,id,vnam\] \[v,vd,i,id,vnam\] \[v,vd,i,id,vnam\]

Vector: no no no

Vector\_Bounds: - - -

Null\_Allowed: no no no

PORT\_TABLE:

Port\_Name: out

Description: "output"

Direction: out

Default\_Type: i

Allowed\_Types: \[v,vd,i,id\]

Vector: no

Vector\_Bounds: -

Null\_Allowed: no

PARAMETER\_TABLE:

Parameter\_Name: order verbose

Description: "order" "verbose"

Data\_Type: int int

Default\_Value: 3 0

Limits: - -

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: yes yes

PARAMETER\_TABLE:

Parameter\_Name: offset gain

Description: "offset" "gain"

Data\_Type: real real

Default\_Value: 0.0 1.0

Limits: - -

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: yes yes

PARAMETER\_TABLE:

Parameter\_Name: file

Description: "file name"

Data\_Type: string

Default\_Value: "3D-table-model.txt"

Limits: -

Vector: no

Vector\_Bounds: -

Null\_Allowed: yes

  - Description:  
    The 3D table model reads a matrix from file "file name" (default
    3D-table-model.txt) which has x columns, y rows per table and z
    tables. Each x,y,z triple, addressed by inx, iny, and inz, yields an
    output value out. Linear interpolation is used for out, eno
    (essentially non oscillating) interpolation for its derivatives.
    Parameters offset (default 0) and gain (default 1) modify the output
    table values according to \(offset + gain out\). Parameter order
    (default 3) influences the calculation of the derivatives. Parameter
    verbose (default 0) yields test outputs, if set to 1 or 2. The table
    format is shown below. Be careful to include the data point inx = 0,
    iny = 0, inz = 0 into your table, because ngspice needs these to for
    the .OP calculation. The x horizontal, y vertical, and z table
    address values have to increase monotonically.

Table Example:

\* 3D table for nmos bsim 4, W=10um, L=0.13um

\*x

39

\*y

39

\*z

11

\*x (drain voltage)

\-0.1 -0.05 0 0.05 0.1 0.15 0.2 0.25 ...

\*y (gate voltage)

\-0.1 -0.05 0 0.05 0.1 0.15 0.2 0.25 ...

\*z (substrate voltage)

\-1.8 -1.6 -1.4 -1.2 -1 -0.8 -0.6 -0.4 -0.2 0 0.2

\*table -1.8

\-4.50688E-10 -4.50613E-10 -4.50601E-10 -4.50599E-10 ...

\-4.49622E-10 -4.49267E-10 -4.4921E-10 -4.49202E-10 ...

\-4.50672E-10 -4.49099E-10 -4.48838E-10 -4.48795E-10 ...

\-4.55575E-10 -4.4953E-10 -4.48435E-10 -4.48217E-10 ...

...

\*table -1.6

\-3.10015E-10 -3.09767E-10 -3.0973E-10 -3.09724E-10 ...

\-3.09748E-10 -3.08524E-10 -3.08339E-10 -3.08312E-10 ...

...

\*table -1.4

\-2.04848E-10 -2.04008E-10 -2.03882E-10 ...

\-2.07275E-10 -2.03117E-10 -2.02491E-10 ...

...

  - Description:  
    The usage example simulates a NMOS transistor with independent
    drain, gate and bulk nodes, referenced to source. Parameter *gain*
    may be used to emulate transistor width, with respect to the table
    transistor.

Example SPICE Usage:

amos1 %vd(d s) %vd(g s) %vd(b s) %id(d s) mostable1

.model mostable1 table3d (offset=0.0 gain=0.5 order=3

\+ verbose=1 file="table-3D-bsim4n.txt")

