# Slew Rate Block

NAME\_TABLE:

C\_Function\_Name: cm\_slew

Spice\_Model\_Name: slew

Description: "A simple slew rate follower block"

PORT\_TABLE:

Port Name: in out

Description: "input" "output"

Direction: in out

Default\_Type: v v

Allowed\_Types: \[v,vd,i,id\] \[v,vd,i,id\]

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: no no

PARAMETER\_TABLE:

Parameter\_Name: rise\_slope

Description: "maximum rising slope value"

Data\_Type: real

Default\_Value: 1.0e9

Limits: -

Vector: no

Vector\_Bounds: -

Null\_Allowed: yes

PARAMETER\_TABLE:

Parameter\_Name: fall\_slope

Description: "maximum falling slope value"

Data\_Type: real

Default\_Value: 1.0e9

Limits: -

Vector: no

Vector\_Bounds: -

Null\_Allowed: yes

PARAMETER\_TABLE:

Parameter\_Name: range

Description: "smoothing range"

Data\_Type: real

Default\_Value: 0.1

Limits: -

Vector: no

Vector\_Bounds: -

Null\_Allowed: yes

  - Description:  
    This function is a simple slew rate block that limits the absolute
    slope of the output with respect to time to some maximum or value.
    The actual slew rate effects of over-driving an amplifier circuit
    can thus be accurately modeled by cascading the amplifier with this
    model. The units used to describe the maximum rising and falling
    slope values are expressed in volts or amperes per second. Thus a
    desired slew rate of 0.5 V/\(\mu s\) will be expressed as 0.5e+6,
    etc.  
    The slew rate block will continue to raise or lower its output until
    the difference between the input and the output values is zero.
    Thereafter, it will resume following the input signal, unless the
    slope again exceeds its rise or fall slope limits. The range input
    specifies a smoothing region above or below the input value.
    Whenever the model is slewing and the output comes to within the
    input + or - the range value, the partial derivative of the output
    with respect to the input will begin to smoothly transition from 0.0
    to 1.0. When the model is no longer slewing (output = input),
    dout/din will equal 1.0.

Example SPICE Usage:

a15 1 2 slew1

.model slew1 slew(rise\_slope=0.5e6 fall\_slope=0.5e6)

