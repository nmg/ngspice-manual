# Limiter

NAME\_TABLE:

C\_Function\_Name: cm\_limit

Spice\_Model\_Name: limit

Description: "limit block"

PORT\_TABLE:

Port Name: in out

Description: "input" "output"

Direction: in out

Default\_Type: v v

Allowed\_Types: \[v,vd,i,id\] \[v,vd,i,id\]

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: no no

PARAMETER\_TABLE:

Parameter\_Name: in\_offset gain

Description: "input offset" "gain"

Data\_Type: real real

Default\_Value: 0.0 1.0

Limits: - -

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: yes yes

PARAMETER\_TABLE:

Parameter\_Name: out\_lower\_limit out\_upper\_limit

Description: "output lower limit" "output upper limit"

Data\_Type: real real

Default\_Value: 0.0 1.0

Limits: - -

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: yes yes

PARAMETER\_TABLE:

Parameter\_Name: limit\_range

Description: "upper & lower smoothing range"

Data\_Type: real

Default\_Value: 1.0e-6

Limits: -

Vector: no

Vector\_Bounds: -

Null\_Allowed: yes

PARAMETER\_TABLE:

Parameter\_Name: fraction

Description: "smoothing fraction/absolute value switch"

Data\_Type: boolean

Default\_Value: FALSE

Limits: -

Vector: no

Vector\_Bounds: -

Null\_Allowed: yes

  - Description:  
    The Limiter is a single input, single output function similar to the
    Gain Block. However, the output of the Limiter function is
    restricted to the range specified by the output lower and upper
    limits. This model will operate in DC, AC and Transient analysis
    modes. Note that the limit range is the value *below the upper limit
    and above the lower limit* at which smoothing of the output begins.
    For this model, then, the limit range represents the delta *with
    respect to the output level* at which smoothing occurs. Thus, for an
    input gain of 2.0 and output limits of 1.0 and -1.0 volts, the
    output will begin to smooth out at \(\pm\)0.9 volts, which occurs
    when the input value is at \(\pm\)0.4.

Example SPICE Usage:

a5 1 2 limit5

.model limit5 limit(in\_offset=0.1 gain=2.5 out\_lower\_limit=-5.0

\+ out\_upper\_limit=5.0 limit\_range=0.10 fraction=FALSE)

