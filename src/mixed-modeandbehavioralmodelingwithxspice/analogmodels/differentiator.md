# Differentiator

NAME\_TABLE:

C\_Function\_Name: cm\_d\_dt

Spice\_Model\_Name: d\_dt

Description: "time-derivative block"

PORT\_TABLE:

Port Name: in out

Description: "input" "output"

Direction: in out

Default\_Type: v v

Allowed\_Types: \[v,vd,i,id\] \[v,vd,i,id\]

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: no no

PARAMETER\_TABLE:

Parameter\_Name: gain out\_offset

Description: "gain" "output offset"

Data\_Type: real real

Default\_Value: 1.0 0.0

Limits: - -

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: yes yes

PARAMETER\_TABLE:

Parameter\_Name: out\_lower\_limit out\_upper\_limit

Description: "output lower limit" "output upper limit"

Data\_Type: real real

Default\_Value: - -

Limits: - -

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: yes yes

PARAMETER\_TABLE:

Parameter\_Name: limit\_range

Description: "upper & lower limit smoothing range"

Data\_Type: real

Default\_Value: 1.0e-6

Limits: -

Vector: no

Vector\_Bounds: -

Null\_Allowed: yes

  - Description:  
    The Differentiator block is a simple derivative stage that
    approximates the time derivative of an input signal by calculating
    the incremental slope of that signal since the previous time point.
    The block also includes gain and output offset parameters to allow
    for tailoring of the required signal, and output upper and lower
    limits to prevent convergence errors resulting from excessively
    large output values. The incremental value of output below the
    output upper limit and above the output lower limit at which
    smoothing begins is specified via the limit range parameter. In AC
    analysis, the value returned is equal to the radian frequency of
    analysis multiplied by the gain.  
    Note that since truncation error checking is not included in the
    d\_dt block, it is not recommended that the model be used to provide
    an integration function through the use of a feedback loop. Such an
    arrangement could produce erroneous results. Instead, you should
    make use of the "integrate" model, which does include truncation
    error checking for enhanced accuracy.

Example SPICE Usage:

a12 7 12 slope\_gen

.

.

.model slope\_gen d\_dt(out\_offset=0.0 gain=1.0

\+ out\_lower\_limit=1e-12 out\_upper\_limit=1e12

\+ limit\_range=1e-9)

