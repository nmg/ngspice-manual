# Magnetic Core

NAME\_TABLE:

C\_Function\_Name: cm\_core

Spice\_Model\_Name: core

Description: "magnetic core"

PORT\_TABLE:

Port\_Name: mc

Description: "magnetic core"

Direction: inout

Default\_Type: gd

Allowed\_Types: \[g,gd\]

Vector: no

Vector\_Bounds: -

Null\_Allowed: no

PARAMETER\_TABLE:

Parameter\_Name: H\_array B\_array

Description: "magnetic field array" "flux density array"

Data\_Type: real real

Default\_Value: - -

Limits: - -

Vector: yes yes

Vector\_Bounds: \[2 -\] \[2 -\]

Null\_Allowed: no no

PARAMETER\_TABLE:

Parameter\_Name: area length

Description: "cross-sectional area" "core length"

Data\_Type: real real

Default\_Value: - -

Limits: - -

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: no no

PARAMETER\_TABLE:

Parameter\_Name: input\_domain

Description: "input sm. domain"

Data\_Type: real

Default\_Value: 0.01

Limits: \[1e-12 0.5\]

Vector: no

Vector\_Bounds: -

Null\_Allowed: yes

PARAMETER\_TABLE:

Parameter\_Name: fraction

Description: "smoothing fraction/abs switch"

Data\_Type: boolean

Default\_Value: TRUE

Limits: -

Vector: no

Vector\_Bounds: -

Null\_Allowed: yes

PARAMETER\_TABLE:

Parameter\_Name: mode

Description: "mode switch (1 = pwl, 2 = hyst)"

Data\_Type: int

Default\_Value: 1

Limits: \[1 2\]

Vector: no

Vector\_Bounds: -

Null\_Allowed: yes

PARAMETER\_TABLE:

Parameter\_Name: in\_low in\_high

Description: "input low value" "input high value"

Data\_Type: real real

Default\_Value: 0.0 1.0

Limits: - -

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: yes yes

PARAMETER\_TABLE:

Parameter\_Name: hyst out\_lower\_limit

Description: "hysteresis" "output lower limit"

Data\_Type: real real

Default\_Value: 0.1 0.0

Limits: \[0 -\] -

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: yes yes

PARAMETER\_TABLE:

Parameter\_Name: out\_upper\_limit

Description: "output upper limit"

Data\_Type: real

Default\_Value: 1.0

Limits: -

Vector: no

Vector\_Bounds: -

Null\_Allowed: yes

  - Description:  
    This function is a conceptual model that is used as a building block
    to create a wide variety of inductive and magnetic circuit models.
    This function is almost always expected to be used in conjunction
    with the lcouple model to build up systems that mock the behavior of
    linear and nonlinear magnetic components. There are two fundamental
    modes of operation for the core model. These are the pwl mode (which
    is the default, and which is the most likely to be of use to you)
    and the hysteresis mode. These are detailed below.

PWL Mode (mode = 1)

The core model in PWL mode takes as input a voltage that it treats as a
magnetomotive force (mmf) value. This value is divided by the total
effective length of the core to produce a value for the Magnetic Field
Intensity, H. This value of H is then used to find the corresponding
Flux Density, B, using the piecewise linear relationship described by
you in the H array / B array coordinate pairs. B is then multiplied by
the cross-sectional area of the core to find the Flux value, which is
output as a current. The pertinent mathematical equations are listed
below:  

\[H = \frac{mmf}{L},{ where} L = Length\]

Here H, the Magnetic Field Intensity, is expressed in
ampere-turns/meter.  

\[B = f\left( H \right)\]

The B value is derived from a piecewise linear transfer function
described to the model via the (H\_array\[\],B\_array\[\]) parameter
coordinate pairs. This transfer function does not include hysteretic
effects; for that, you would need to substitute a HYST model for the
core.  

\[\varphi = BA,{ where} A = Area\]

The final current allowed to flow through the core is equal to
\(\varphi\). This value in turn is used by the "lcouple" code model to
obtain a value for the voltage reflected back across its terminals to
the driving electrical circuit.

The following example code shows the use of two lcouple models and one
core model to produce a simple primary/secondary transformer.

Example SPICE Usage:

a1 (2 0) (3 0) primary

.model primary lcouple (num\_turns = 155)

a2 (3 4) iron\_core

.model iron\_core core (H\_array = \[-1000 -500 -375 -250 -188 -125 -63
0

\+ 63 125 188 250 375 500 1000\]

\+ B\_array = \[-3.13e-3 -2.63e-3 -2.33e-3 -1.93e-3

\+ -1.5e-3 -6.25e-4 -2.5e-4 0 2.5e-4

\+ 6.25e-4 1.5e-3 1.93e-3 2.33e-3

\+ 2.63e-3 3.13e-3\]

\+ area = 0.01 length = 0.01)

a3 (5 0) (4 0) secondary

.model secondary lcouple (num\_turns = 310)

HYSTERESIS Mode (mode = 2)

The core model in HYSTERESIS mode takes as input a voltage that it
treats as a magnetomotive force (mmf) value. This value is used as input
to the equivalent of a hysteresis code model block. The parameters
defining the input low and high values, the output low and high values,
and the amount of hysteresis are as in that model. The output from this
mode, as in PWL mode, is a current value that is seen across the mc
port. An example of the core model used in this fashion is shown below:

Example SPICE Usage:

a1 (2 0) (3 0) primary

.model primary lcouple (num\_turns = 155)

a2 (3 4) iron\_core

.model iron\_core core (mode = 2 in\_low=-7.0 in\_high=7.0

\+ out\_lower\_limit=-2.5e-4 out\_upper\_limit=2.5e-4

\+ hyst = 2.3 )

a3 (5 0) (4 0) secondary

.model secondary lcouple (num\_turns = 310)

*One final note to be made about the two core model nodes is that
certain parameters are available in one mode, but not in the other*. In
particular, the in\_low, in\_high, out\_lower\_limit, out\_upper\_limit,
and hysteresis parameters are not available in PWL mode. Likewise, the
H\_array, B\_array, area, and length values are unavailable in
HYSTERESIS mode. The input domain and fraction parameters are common to
both modes (though their behavior is somewhat different; for explanation
of the input domain and fraction values for the HYSTERESIS mode, you
should refer to the hysteresis code model discussion).

