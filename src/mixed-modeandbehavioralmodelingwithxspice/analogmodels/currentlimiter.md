# Current Limiter

NAME\_TABLE:

C\_Function\_Name: cm\_ilimit

Spice\_Model\_Name: ilimit

Description: "current limiter block"

PORT\_TABLE:

Port Name: in pos\_pwr

Description: "input" "positive power supply"

Direction: in inout

Default\_Type: v g

Allowed\_Types: \[v,vd\] \[g,gd\]

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: no yes

PORT\_TABLE:

Port Name: neg\_pwr out

Description: "negative power supply" "output"

Direction: inout inout

Default\_Type: g g

Allowed\_Types: \[g,gd\] \[g,gd\]

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: yes no

PARAMETER\_TABLE:

Parameter\_Name: in\_offset gain

Description: "input offset" "gain"

Data\_Type: real real

Default\_Value: 0.0 1.0

Limits: - -

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: yes yes

PARAMETER\_TABLE:

Parameter\_Name: r\_out\_source r\_out\_sink

Description: "sourcing resistance" "sinking resistance"

Data\_Type: real real

Default\_Value: 1.0 1.0

Limits: \[1.0e-9 1.0e9\] \[1.0e-9 1.0e9\]

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: yes yes

PARAMETER\_TABLE:

Parameter\_Name: i\_limit\_source

Description: "current sourcing limit"

Data\_Type: real

Default\_Value: -

Limits: \[1.0e-12 -\]

Vector: no

Vector\_Bounds: -

Null\_Allowed: yes

PARAMETER\_TABLE:

Parameter\_Name: i\_limit\_sink

Description: "current sinking limit"

Data\_Type: real

Default\_Value: -

Limits: \[1.0e-12 -\]

Vector: no

Vector\_Bounds: -

Null\_Allowed: yes

PARAMETER\_TABLE:

Parameter\_Name: v\_pwr\_range i\_source\_range

Description: "upper & lower power "sourcing current

supply smoothing range" smoothing range"

Data\_Type: real real

Default\_Value: 1.0e-6 1.0e-9

Limits: \[1.0e-15 -\] \[1.0e-15 -\]

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: yes yes

PARAMETER\_TABLE:

Parameter\_Name: i\_sink\_range

Description: "sinking current smoothing range"

Data\_Type: real

Default\_Value: 1.0e-9

Limits: \[1.0e-15 -\]

Vector: no

Vector\_Bounds: -

Null\_Allowed: yes

PARAMETER\_TABLE:

Parameter\_Name: r\_out\_domain

Description: "internal/external voltage delta smoothing range"

Data\_Type: real

Default\_Value: 1.0e-9

Limits: \[1.0e-15 -\]

Vector: no

Vector\_Bounds: -

Null\_Allowed: yes

  - Description:  
    The Current Limiter models the behavior of an operational amplifier
    or comparator device at a high level of abstraction. All of its pins
    act as inputs; three of the four also act as outputs. The model
    takes as input a voltage value from the in connector. It then
    applies an offset and a gain, and derives from it an equivalent
    internal voltage (*veq*), which it limits to fall between pos\_pwr
    and neg\_pwr. If *veq* is greater than the output voltage seen on
    the out connector, a sourcing current will flow from the output pin.
    Conversely, if the voltage is less than *vout*, a sinking current
    will flow into the output pin.  
    Depending on the polarity of the current flow, either a sourcing or
    a sinking resistance value (r\_out\_source, r\_out\_sink) is applied
    to govern the vout/i\_out relationship. The chosen resistance will
    continue to control the output current until it reaches a maximum
    value specified by either i\_limit\_source or i\_limit\_sink. The
    latter mimics the current limiting behavior of many operational
    amplifier output stages.  
    During all operation, the output current is reflected either in the
    pos\_pwr connector current or the neg\_pwr current, depending on the
    polarity of i\_out. Thus, realistic power consumption as seen in the
    supply rails is included in the model.  
    The user-specified smoothing parameters relate to model operation as
    follows: v\_pwr\_range controls the voltage below vpos\_pwr and
    above vneg\_pwr inputs beyond which
    \(veq = gain\left( {vin + v_{offset}} \right)\) is smoothed;
    i\_source\_range specifies the current below i\_limit\_source at
    which smoothing begins, as well as specifying the current increment
    above i\_out=0.0 at which i\_pos\_pwr begins to transition to zero;
    i\_sink\_range serves the same purpose with respect to
    i\_limit\_sink and i\_neg\_pwr that i\_source\_range serves for
    i\_limit\_source and i\_pos\_pwr; r\_out\_domain specifies the
    incremental value above and below (veq-vout)=0.0 at which r\_out
    will be set to r\_out\_source and r\_out\_sink, respectively. For
    values of (veq-vout) less than r\_out\_domain and greater than
    -r\_out\_domain, r\_out is interpolated smoothly between
    r\_out\_source and r\_out\_sink.

Example SPICE Usage:

a10 3 10 20 4 amp3

.

.

.model amp3 ilimit(in\_offset=0.0 gain=16.0 r\_out\_source=1.0

\+ r\_out\_sink=1.0 i\_limit\_source=1e-3

\+ i\_limit\_sink=10e-3 v\_pwr\_range=0.2

\+ i\_source\_range=1e-6 i\_sink\_range=1e-6

\+ r\_out\_domain=1e-6)

