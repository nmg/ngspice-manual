# Hysteresis Block

NAME\_TABLE:

C\_Function\_Name: cm\_hyst

Spice\_Model\_Name: hyst

Description: "hysteresis block"

PORT\_TABLE:

Port Name: in out

Description: "input" "output"

Direction: in out

Default\_Type: v v

Allowed\_Types: \[v,vd,i,id\] \[v,vd,i,id\]

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: no no

PARAMETER\_TABLE:

Parameter\_Name: in\_low in\_high

Description: "input low value" "input high value"

Data\_Type: real real

Default\_Value: 0.0 1.0

Limits: - -

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: yes yes

PARAMETER\_TABLE:

Parameter\_Name: hyst out\_lower\_limit

Description: "hysteresis" "output lower limit"

Data\_Type: real real

Default\_Value: 0.1 0.0

Limits: \[0.0 -\] -

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: yes yes

PARAMETER\_TABLE:

Parameter\_Name: out\_upper\_limit input\_domain

Description: "output upper limit" "input smoothing domain"

Data\_Type: real real

Default\_Value: 1.0 0.01

Limits: - -

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: yes yes

PARAMETER\_TABLE:

Parameter\_Name: fraction

Description: "smoothing fraction/absolute value switch"

Data\_Type: boolean

Default\_Value: TRUE

Limits: -

Vector: no

Vector\_Bounds: -

Null\_Allowed: yes

  - Description:  
    The Hysteresis block is a simple buffer stage that provides
    hysteresis of the output with respect to the input. The in low and
    in high parameter values specify the center voltage or current
    inputs about which the hysteresis effect operates. The output values
    are limited to out lower limit and out upper limit. The value of
    hyst is added to the in low and in high points in order to specify
    the points at which the slope of the hysteresis function would
    normally change abruptly as the input transitions from a low to a
    high value. Likewise, the value of hyst is subtracted from the in
    high and in low values in order to specify the points at which the
    slope of the hysteresis function would normally change abruptly as
    the input transitions from a high to a low value. In fact, the slope
    of the hysteresis function is never allowed to change abruptly but
    is smoothly varied whenever the input domain smoothing parameter is
    set greater than zero.

Example SPICE Usage:

a11 1 2 schmitt1

.

.

.model schmitt1 hyst(in\_low=0.7 in\_high=2.4 hyst=0.5

\+ out\_lower\_limit=0.5 out\_upper\_limit=3.0

\+ input\_domain=0.01 fraction=TRUE)

