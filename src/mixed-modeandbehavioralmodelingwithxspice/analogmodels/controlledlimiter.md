# Controlled Limiter

NAME\_TABLE:

C\_Function\_Name: cm\_climit

Spice\_Model\_Name: climit

Description: "controlled limiter block"

PORT\_TABLE:

Port\_Name: in cntl\_upper

Description: "input" "upper lim. control input"

Direction: in in

Default\_Type: v v

Allowed\_Types: \[v,vd,i,id,vnam\] \[v,vd,i,id,vnam\]

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: no no

PORT\_TABLE:

Port\_Name: cntl\_lower out

Description: "lower limit control input" "output"

Direction: in out

Default\_Type: v v

Allowed\_Types: \[v,vd,i,id,vnam\] \[v,vd,i,id\]

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: no no

PARAMETER\_TABLE:

Parameter\_Name: in\_offset gain

Description: "input offset" "gain"

Data\_Type: real real

Default\_Value: 0.0 1.0

Limits: - -

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: yes yes

PARAMETER\_TABLE:

Parameter\_Name: upper\_delta lower\_delta

Description: "output upper delta" "output lower delta"

Data\_Type: real real

Default\_Value: 0.0 0.0

Limits: - -

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: yes yes

PARAMETER\_TABLE:

Parameter\_Name: limit\_range fraction

Description: "upper & lower sm. range" "smoothing %/abs switch"

Data\_Type: real boolean

Default\_Value: 1.0e-6 FALSE

Limits: - -

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: yes yes

  - Description:  
    The Controlled Limiter is a single input, single output function
    similar to the Gain Block. However, the output of the Limiter
    function is restricted to the range specified by the output lower
    and upper limits. This model will operate in DC, AC, and Transient
    analysis modes. Note that the limit range is the value *below the
    \(cntlupper\) limit and above the \(cntllower\) limit* at which
    smoothing of the output begins (minimum positive value of voltage
    must exist between the *\(cntlupper\)* input and the *\(cntllower\)*
    input at all times). For this model, then, the limit range
    represents the delta *with respect to the output level* at which
    smoothing occurs. Thus, for an input gain of 2.0 and output limits
    of 1.0 and -1.0 volts, the output will begin to smooth out at
    \(\pm\)0.9 volts, which occurs when the input value is at
    \(\pm\)0.4. Note also that the Controlled Limiter code tests the
    input values of \(cntlupper\) and \(cntllower\) to make sure that
    they are spaced far enough apart to guarantee the existence of a
    linear range between them. The range is calculated as the difference
    between (\(cntlupper - upperdelta - limitrange\)) and
    (\(cntllower + lowerdelta + limitrange\)) and must be greater than
    or equal to zero. Note that when the limit range is specified as a
    fractional value, the limit range used in the above is taken as the
    calculated fraction of the difference between \(cntlupper\) and
    \(cntllower\). Still, the potential exists for too great a limit
    range value to be specified for proper operation, in which case the
    model will return an error message.

Example SPICE Usage:

a6 3 6 8 4 varlimit

.

.

.model varlimit climit(in\_offset=0.1 gain=2.5 upper\_delta=0.0

\+ lower\_delta=0.0 limit\_range=0.10 fraction=FALSE)

