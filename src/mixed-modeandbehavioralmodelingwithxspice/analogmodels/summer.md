# Summer

NAME\_TABLE:

C\_Function\_Name: cm\_summer

Spice\_Model\_Name: summer

Description: "A summer block"

PORT\_TABLE:

Port Name: in out

Description: "input vector" "output"

Direction: in out

Default\_Type: v v

Allowed\_Types: \[v,vd,i,id,vnam\] \[v,vd,i,id\]

Vector: yes no

Vector\_Bounds: - -

Null\_Allowed: no no

PARAMETER\_TABLE:

Parameter\_Name: in\_offset in\_gain

Description: "input offset vector" "input gain vector"

Data\_Type: real real

Default\_Value: 0.0 1.0

Limits: - -

Vector: yes yes

Vector\_Bounds: in in

Null\_Allowed: yes yes

PARAMETER\_TABLE:

Parameter\_Name: out\_gain out\_offset

Description: "output gain" "output offset"

Data\_Type: real real

Default\_Value: 1.0 0.0

Limits: - -

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: yes yes

  - Description:  
    This function is a summer block with 2-to-N input ports. Individual
    gains and offsets can be applied to each input and to the output.
    Each input is added to its respective offset and then multiplied by
    its gain. The results are then summed, multiplied by the output gain
    and added to the output offset. This model will operate in DC, AC,
    and Transient analysis modes.

Example usage:

``` listings
a2 [1 2] 3 sum1 
.model sum1 summer(in_offset=[0.1 -0.2] in_gain=[2.0 1.0] 
+ out_gain=5.0 out_offset=-0.01)
```

