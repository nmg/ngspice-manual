# Zener Diode

NAME\_TABLE:

C\_Function\_Name: cm\_zener

Spice\_Model\_Name: zener

Description: "zener diode"

PORT\_TABLE:

Port Name: z

Description: "zener"

Direction: inout

Default\_Type: gd

Allowed\_Types: \[gd\]

Vector: no

Vector\_Bounds: -

Null\_Allowed: no

PARAMETER\_TABLE:

Parameter\_Name: v\_breakdown i\_breakdown

Description: "breakdown voltage" "breakdown current"

Data\_Type: real real

Default\_Value: - 2.0e-2

Limits: \[1.0e-6 1.0e6\] \[1.0e-9 -\]

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: no yes

PARAMETER\_TABLE:

Parameter\_Name: i\_sat n\_forward

Description: "saturation current" "forward emission coefficient"

Data\_Type: real real

Default\_Value: 1.0e-12 1.0

Limits: \[1.0e-15 -\] \[0.1 10\]

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: yes yes

PARAMETER\_TABLE:

Parameter\_Name: limit\_switch

Description: "switch for on-board limiting (convergence aid)"

Data\_Type: boolean

Default\_Value: FALSE

Limits: -

Vector: no

Vector\_Bounds: -

Null\_Allowed: yes

STATIC\_VAR\_TABLE:

Static\_Var\_Name: previous\_voltage

Data\_Type: pointer

Description: "iteration holding variable for limiting"

  - Description:  
    The Zener Diode models the DC characteristics of most zeners. This
    model differs from the Diode/Rectifier by providing a user-defined
    dynamic resistance in the reverse breakdown region. The forward
    characteristic is defined by only a single point, since most data
    sheets for zener diodes do not give detailed characteristics in the
    forward region.  
    The first three parameters define the DC characteristics of the
    zener in the breakdown region and are usually explicitly given on
    the data sheet.  
    The saturation current refers to the relatively constant reverse
    current that is produced when the voltage across the zener is
    negative, but breakdown has not been reached. The reverse leakage
    current determines the slight increase in reverse current as the
    voltage across the zener becomes more negative. It is modeled as a
    resistance parallel to the zener with value v breakdown / i rev.  
    Note that the limit switch parameter engages an internal limiting
    function for the zener. This can, in some cases, prevent the
    simulator from converging to an unrealistic solution if the voltage
    across or current into the device is excessive. If use of this
    feature fails to yield acceptable results, the convlimit option
    should be tried (add the following statement to the SPICE input
    deck: .options convlimit)

Example SPICE Usage:

a9 3 4 vref10

.

.

.model vref10 zener(v\_breakdown=10.0 i\_breakdown=0.02

\+ r\_breakdown=1.0 i\_rev=1e-6 i\_sat=1e-12)

