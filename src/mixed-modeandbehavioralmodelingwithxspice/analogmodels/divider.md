# Divider

NAME\_TABLE:

C\_Function\_Name: cm\_divide

Spice\_Model\_Name: divide

Description: "divider block"

PORT\_TABLE:

Port\_Name: num den out

Description: "numerator" "denominator" "output"

Direction: in in out

Default\_Type: v v v

Allowed\_Types: \[v,vd,i,id,vnam\] \[v,vd,i,id,vnam\] \[v,vd,i,id\]

Vector: no no no

Vector\_Bounds: - - -

Null\_Allowed: no no no

PARAMETER\_TABLE:

Parameter\_Name: num\_offset num\_gain

Description: "numerator offset" "numerator gain"

Data\_Type: real real

Default\_Value: 0.0 1.0

Limits: - -

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: yes yes

PARAMETER\_TABLE:

Parameter\_Name: den\_offset den\_gain

Description: "denominator offset" "denominator gain"

Data\_Type: real real

Default\_Value: 0.0 1.0

Limits: - -

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: yes yes

PARAMETER\_TABLE:

Parameter\_Name: den\_lower\_limit

Description: "denominator lower limit"

Data\_Type: real

Default\_Value: 1.0e-10

Limits: -

Vector: no

Vector\_Bounds: -

Null\_Allowed: yes

PARAMETER\_TABLE:

Parameter\_Name: den\_domain

Description: "denominator smoothing domain"

Data\_Type: real

Default\_Value: 1.0e-10

Limits: -

Vector: no

Vector\_Bounds: -

Null\_Allowed: yes

PARAMETER\_TABLE:

Parameter\_Name: fraction

Description: "smoothing fraction/absolute value switch"

Data\_Type: boolean

Default\_Value: false

Limits: -

Vector: no

Vector\_Bounds: -

Null\_Allowed: yes

PARAMETER\_TABLE:

Parameter\_Name: out\_gain out\_offset

Description: "output gain" "output offset"

Data\_Type: real real

Default\_Value: 1.0 0.0

Limits: - -

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: yes yes

  - Description:  
    This function is a two-quadrant divider. It takes two inputs; num
    (numerator) and den (denominator). Divide offsets its inputs,
    multiplies them by their respective gains, divides the results,
    multiplies the quotient by the output gain, and offsets the result.
    The denominator is limited to a value above zero via a user
    specified lower limit. This limit is approached through a quadratic
    smoothing function, the domain of which may be specified as a
    fraction of the lower limit value (default), or as an absolute
    value. This model will operate in DC, AC and Transient analysis
    modes. However, in ac analysis it is important to remember that
    results are invalid unless only *one* input of the divider is
    connected to a node that is connected to an ac signal (this is
    exemplified by the use of the divider to perform a potentiometer
    function: one input is dc, the other carries the ac signal).

Example SPICE Usage:

a4 1 2 4 divider

.model divider divide(num\_offset=0.1 num\_gain=2.5 den\_offset=-0.1

\+ den\_gain=5.0 den\_lower\_limit=1e-5 den\_domain=1e-6

\+ fraction=FALSE out\_gain=1.0 out\_offset=0.0)

