# Memristor

NAME\_TABLE:

C\_Function\_Name: cm\_memristor

Spice\_Model\_Name: memristor

Description: "Memristor Interface"

PORT\_TABLE:

Port\_Name: memris

Description: "memristor terminals"

Direction: inout

Default\_Type: gd

Allowed\_Types: \[gd\]

Vector: no

Vector\_Bounds: -

Null\_Allowed: no

PARAMETER\_TABLE:

Parameter\_Name: rmin rmax

Description: "minimum resistance" "maximum resistance"

Data\_Type: real real

Default\_Value: 10.0 10000.0

Limits: - -

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: no no

PARAMETER\_TABLE:

Parameter\_Name: rinit vt

Description: "initial resistance" "threshold"

Data\_Type: real real

Default\_Value: 7000.0 0.0

Limits: - -

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: no no

PARAMETER\_TABLE:

Parameter\_Name: alpha beta

Description: "model parameter 1" "model parameter 2"

Data\_Type: real real

Default\_Value: 0.0 1.0

Limits: - -

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: no no

  - Description:  
    The memristor is a two-terminal resistor with memory, whose
    resistance depends on the time integral of the voltage across its
    terminals. rmin and rmax provide the lower and upper limits of the
    resistance, rinit is its starting value (no voltage applied so far).
    The voltage has to be above a threshold vt to become effective in
    changing the resistance. alpha and beta are two model parameters.
    The memristor code model is derived from a SPICE subcircuit
    published in \[[23](#LyXCite-key_23)\].

Example SPICE Usage:

amen 1 2 memr

.model memr memristor (rmin=1k rmax=10k rinit=7k

\+ alpha=0 beta=2e13 vt=1.6)

