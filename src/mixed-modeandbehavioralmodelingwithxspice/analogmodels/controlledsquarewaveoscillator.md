# Controlled Square Wave Oscillator

NAME\_TABLE:

C\_Function\_Name: cm\_square

Spice\_Model\_Name: square

Description: "controlled square wave oscillator"

PORT\_TABLE:

Port Name: cntl\_in out

Description: "control input" "output"

Direction: in out

Default\_Type: v v

Allowed\_Types: \[v,vd,i,id\] \[v,vd,i,id\]

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: no no

PARAMETER\_TABLE:

Parameter\_Name: cntl\_array freq\_array

Description: "control array" "frequency array"

Data\_Type: real real

Default\_Value: 0.0 1.0e3

Limits: - \[0 -\]

Vector: yes yes

Vector\_Bounds: \[2 -\] cntl\_array

Null\_Allowed: no no

PARAMETER\_TABLE:

Parameter\_Name: out\_low out\_high

Description: "output peak low value" "output peak high value"

Data\_Type: real real

Default\_Value: -1.0 1.0

Limits: - -

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: yes yes

PARAMETER.TABLE:

Parameter\_Name: duty\_cycle rise\_time

Description: "duty cycle" "output rise time"

Data\_Type: real real

Default\_Value: 0.5 1.0e-9

Limits: \[1e-6 0.999999\] -

Vector: no

Vector\_Bounds: - -

Null\_Allowed: yes yes

PARAMETER\_TABLE:

Parameter\_Name: fall\_time

Description: "output fall time"

Data\_Type: real

Default\_Value: 1.0e-9

Limits: -

Vector: no

Vector\_Bounds: -

Null\_Allowed: yes

  - Description:  
    This function is a controlled square wave oscillator with
    parametrizable values of low and high peak output, duty cycle, rise
    time, and fall time. It takes an input voltage or current value.
    This value is used as the independent variable in the piecewise
    linear curve described by the coordinate points of the cntl\_array
    and freq\_array pairs. From the curve, a frequency value is
    determined, and the oscillator will output a square wave at that
    frequency.  
    From the above, it is easy to see that array sizes of 2 for both the
    cntl\_array and the freq\_array will yield a linear variation of the
    frequency with respect to the control input. Any sizes greater than
    2 will yield a piecewise linear transfer characteristic. For more
    detail, refer to the description of the piecewise linear controlled
    source, which uses a similar method to derive an output value given
    a control input.

Example SPICE Usage:

ain 1 2 pulse1

.model pulse1 square(cntl\_array = \[-1 0 5 6\]

\+ freq\_array=\[10 10 1000 1000\] out\_low = 0.0

\+ out\_high = 4.5 duty\_cycle = 0.2

\+ rise\_time = 1e-6 fall\_time = 2e-6)

