# Simple Diode Model

NAME\_TABLE:

C\_Function\_Name: cm\_sidiode

Spice\_Model\_Name: sidiode

Description: "simple diode"

PORT\_TABLE:

Port\_Name: ds

Description: "diode port"

Direction: inout

Default\_Type: gd

Allowed\_Types: \[gd\]

Vector: no

Vector\_Bounds: -

Null\_Allowed: no

PARAMETER\_TABLE:

Parameter\_Name: ron roff

Description: "resistance on-state" "resistance off-state"

Data\_Type: real real

Default\_Value: 1 1

1

If roff is not given, ron is the default

Limits: \[1e-6 - \] \[1e-12 -\]

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: no no

PARAMETER\_TABLE:

Parameter\_Name: vfwd vrev

Description: "forward voltage" "reverse breakdown voltage"

Data\_Type: real real

Default\_Value: 0. 1e30

Limits: \[0. -\] \[0. -\]

Vector: no no

Vector Bounds: - -

Null\_Allowed: yes yes

PARAMETER\_TABLE:

Parameter\_Name: ilimit revilimit

Description: "limit of on-current" "limit of breakdown current"

Data\_Type: real real

Default\_Value: 1e30 1e30

Limits: \[1e-15 -\] \[1e-15 -\]

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: yes yes

PARAMETER\_TABLE:

Parameter\_Name: epsilon revepsilon

Description: "width quadrat. reg. 1" "width quadratic region 2"

Data\_Type: real real

Default\_Value: 0. 0.

Limits: \[0. -\] \[0. -\]

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: yes yes

PARAMETER\_TABLE:

Parameter\_Name: rrev

Description: "resistance in breakdown"

Data\_Type: real

Default\_Value: 0.

Limits: -

Vector: no

Vector\_Bounds: -

Null\_Allowed: yes

STATIC\_VAR\_TABLE:

Static\_Var\_Name: locdata

Data\_Type: pointer

Description: "table with constants"

This is a model for a simple diode. Three regions are modelled as linear
I(V) curves: Reverse (breakdown) current with Rrev starting at Vrev into
the negative direction, Off current with Roff between Vrev and Vfwd and
an On region with Ron, staring at Vfwd. The interface between the
regions is described by a quadratic function, the width of the interface
region is determined by Revepsilon and Epsilon. Current limits in the
reverse breakdown (Revilimit) and in the forward (on) state (Ilimit) may
be set. The interface is a tanh function. Thus the first derivative of
the I(V) curve is continuous. All parameter values are entered as
positive numbers. A diode capacitance is not modelled.

Example SPICE Usage:

a1 a k ds1

.model ds1 sidiode(Roff=1000 Ron=0.7 Rrev=0.2 Vfwd=1

\+ Vrev=10 Revepsilon=0.2 Epsilon=0.2 Ilimit=7 Revilimit=7)

