# Multiplier

NAME\_TABLE:

C\_Function\_Name: cm\_mult

Spice\_Model\_Name: mult

Description: "multiplier block"

PORT\_TABLE:

Port\_Name: in out

Description: "input vector" "output"

Direction: in out

Default\_Type: v v

Allowed\_Types: \[v,vd,i,id,vnam\] \[v,vd,i,id\]

Vector: yes no

Vector\_Bounds: \[2 -\] -

Null\_Allowed: no no

PARAMETER\_TABLE:

Parameter\_Name: in\_offset in\_gain

Description: "input offset vector" "input gain vector"

Data\_Type: real real

Default\_Value: 0.0 1.0

Limits: - -

Vector: yes yes

Vector\_Bounds: in in

Null\_Allowed: yes yes

PARAMETER\_TABLE:

Parameter\_Name: out\_gain out\_offset

Description: "output gain" "output offset"

Data\_Type: real real

Default\_Value: 1.0 0.0

Limits: - -

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: yes yes

  - Description:  
    This function is a multiplier block with 2-to-N input ports.
    Individual gains and offsets can be applied to each input and to the
    output. Each input is added to its respective offset and then
    multiplied by its gain. The results are multiplied along with the
    output gain and are added to the output offset. This model will
    operate in DC, AC, and Transient analysis modes. However, in ac
    analysis it is important to remember that results are invalid unless
    only *one* input of the multiplier is connected to a node that i
    connected to an AC signal (this is exemplified by the use of a
    multiplier to perform a potentiometer function: one input is DC, the
    other carries the AC signal).

Example SPICE Usage:

``` listings
a3 [1 2 3] 4 sigmult 
.model sigmult mult(in_offset=[0.1 0.1 -0.1] 
+ in_gain=[10.0 10.0 10.0] out_gain=5.0 out_offset=0.05)
```

