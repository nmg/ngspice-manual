# S-Domain Transfer Function

NAME\_TABLE:

C\_Function\_Name: cm\_s\_xfer

Spice\_Model\_Name: s\_xfer

Description: "s-domain transfer function"

PORT\_TABLE:

Port Name: in out

Description: "input" "output"

Direction: in out

Default\_Type: v v

Allowed\_Types: \[v,vd,i,id\] \[v,vd,i,id\]

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: no no

PARAMETER\_TABLE:

Parameter\_Name: in\_offset gain

Description: "input offset" "gain"

Data\_Type: real real

Default\_Value: 0.0 1.0

Limits: - -

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: yes yes

PARAMETER\_TABLE:

Parameter\_Name: num\_coeff

Description: "numerator polynomial coefficients"

Data\_Type: real

Default\_Value: -

Limits: -

Vector: yes

Vector\_Bounds: \[1 -\]

Null\_Allowed: no

PARAMETER\_TABLE:

Parameter\_Name: den\_coeff

Description: "denominator polynomial coefficients"

Data\_Type: real

Default\_Value: -

Limits: -

Vector: yes

Vector\_Bounds: \[1 -\]

Null\_Allowed: no

PARAMETER\_TABLE:

Parameter\_Name: int\_ic

Description: "integrator stage initial conditions"

Data\_Type: real

Default\_Value: 0.0

Limits: -

Vector: yes

Vector\_Bounds: den\_coeff

Null\_Allowed: yes

PARAMETER\_TABLE:

Parameter\_Name: denormalized\_freq

Description: "denorm. corner freq.(radians) for 1 rad/s coeffs"

Data\_Type: real

Default\_Value: 1.0

Limits: -

Vector: no

Vector\_Bounds: -

Null\_Allowed: yes

  - Description:  
    The s-domain transfer function is a single input, single output
    transfer function in the Laplace transform variable \`s' that allows
    for flexible modulation of the frequency domain characteristics of a
    signal. Ac and transient simulations are supported. The code model
    may be configured to produce an arbitrary s-domain transfer function
    with the following restrictions:

1\. The degree of the numerator polynomial cannot exceed that

of the denominator polynomial in the variable "s".

2\. The coefficients for a polynomial must be stated

explicitly. That is, if a coefficient is zero, it must be

included as an input to the num coeff or den coeff vector.

The order of the coefficient parameters is from that associated with the
highest-powered term decreasing to that of the lowest. Thus, for the
coefficient parameters specified below, the equation in \`s' is shown:

.model filter s\_xfer(gain=0.139713

\+ num\_coeff=\[1.0 0.0 0.7464102\]

\+ den\_coeff=\[1.0 0.998942 0.001170077\]

\+ int\_ic=\[0 0\])

It specifies a transfer function of the
form

\(N\left( s \right) = 0.139713 \cdot \frac{s^{2} + 0.7464102}{s^{2} + 0.998942s + 0.00117077}\)

The s-domain transfer function includes **gain** and **in\_offset**
(input offset) parameters to allow for tailoring of the required signal.
There are no limits on the internal signal values or on the output value
of the s-domain transfer function, so you are cautioned to specify gain
and coefficient values that will not cause the model to produce
excessively large values. In AC analysis, the value returned is equal to
the real and imaginary components of the total s-domain transfer
function at each frequency of interest.

The **denormalized\_freq** term allows you to specify coefficients for a
normalized filter (i.e. one in which the frequency of interest is 1
rad/s). Once these coefficients are included, specifying the
denormalized frequency value \`shifts' the corner frequency to the
actual one of interest. As an example, the following transfer function
describes a Chebyshev low-pass filter with a corner (pass-band)
frequency of 1
rad/s:  

\(N\left( s \right) = 0.139713 \cdot \frac{1.0}{s^{2} + 1.09773s + 1.10251}\)

In order to define an s\_xfer model for the above, but with the corner
frequency equal to 1500 rad/s (9425 Hz), the following instance and
model lines would be needed:

a12 node1 node2 cheby1

.model cheby1 s\_xfer(num\_coeff=\[1\] den\_coeff=\[1 1.09773 1.10251\]

\+ int\_ic=\[0 0\] denormalized\_freq=1500)

In the above, you add the normalized coefficients and scale the filter
through the use of the denormalized freq parameter. Similar results
could have been achieved by performing the denormalization prior to
specification of the coefficients, and setting denormalized freq to the
value 1.0 (or not specifying the frequency, as the default is 1.0 rad/s)
Note in the above that frequencies are *always specified as
radians/second*.

Truncation error checking is included in the s-domain transfer block.
This should provide for more accurate simulations, since the model will
inherently request smaller time increments between simulation points if
truncation errors would otherwise be excessive.

The **int\_ic** parameter is an array that must be of size one less as
the array of values specified for the **den\_coeff** parameter. Even if
a 0 start value is required, you have to add the specific int\_ic vector
to the set of coefficients (see the examples above and below).

Example SPICE Usage:

a14 9 22 cheby\_LP\_3kHz

.

.

.model cheby\_LP\_3kHz s\_xfer(in\_offset=0.0 gain=1.0 int\_ic=\[0 0\]

\+ num\_coeff=\[1.0\]

\+ den\_coeff=\[1.0 1.42562 1.51620\])

