# Analog Switch

NAME\_TABLE:

C\_Function\_Name: cm\_aswitch

Spice\_Model\_Name: aswitch

Description: "analog switch"

PORT\_TABLE:

Port Name: cntl\_in out

Description: "input" "resistive output"

Direction: in out

Default\_Type: v gd

Allowed\_Types: \[v,vd,i,id\] \[gd\]

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: no no

PARAMETER\_TABLE:

Parameter\_Name: cntl\_off cntl\_on

Description: "control \`off' value" "control \`on' value"

Data\_Type: real real

Default\_Value: 0.0 1.0

Limits: - -

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: yes yes

PARAMETER\_TABLE:

Parameter\_Name: r\_off log

Description: "off resistance" "log/linear switch"

Data\_Type: real boolean

Default\_Value: 1.0e12 TRUE

Limits: - -

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: yes yes

PARAMETER\_TABLE:

Parameter\_Name: r\_on

Description: "on resistance"

Data\_Type: real

Default\_Value: 1.0

Limits: -

Vector: no

Vector\_Bounds: -

Null\_Allowed: yes

  - Description:  
    The Analog Switch is a resistor that varies either logarithmically
    or linearly between specified values of a controlling input voltage
    or current. Note that the input is not internally limited.
    Therefore, if the controlling signal exceeds the specified OFF state
    or ON state value, the resistance may become excessively large or
    excessively small (in the case of logarithmic dependence), or may
    become negative (in the case of linear dependence). For the
    experienced user, these excursions may prove valuable for modeling
    certain devices, but in most cases you are advised to add limiting
    of the controlling input if the possibility of excessive control
    value variation exists.

Example SPICE Usage:

a8 3 %gd(6 7) switch3

.

.

.model switch3 aswitch(cntl\_off=0.0 cntl\_on=5.0 r\_off=1e6

\+ r\_on=10.0 log=TRUE)

