# Search path for file input

Several code models (filesource [12.2.8](#subsec_Filesource), d\_source
[12.4.21](#subsec_Digital_Source), d\_state
[12.4.18](#subsec_State_Machine)) call additional files for supply of
input data. A call to file="path/filename" (or input\_file=,
state\_file=) in the .model card will start a search sequence for
finding the file. path may be an absolute path. If path is omitted or is
a relative path, filename is looked for according to the following
search list:

1.  Infile\_Path/\<path/filename\> (Infile\_Path is the path of the
    input file \*.sp containing the netlist)
2.  NGSPICE\_INPUT\_DIR/\<path/filename\>  (where an additional path is
    set by the environmental variable)
3.  \<path/filename\>  (where the search is relative to the current
    directory (OS dependent))

