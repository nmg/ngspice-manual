# Examples

The following is a list of instance card and associated .MODEL card
examples showing use of predefined models within an XSPICE deck:

a1 1 2 amp

.model amp gain(in\_offset=0.1 gain=5.0 out\_offset=-0.01)  
  
a2 %i\[1 2\] 3 sum1

.model sum1 summer(in\_offset=\[0.1 -0.2\] in\_gain=\[2.0 1.0\]

\+ out\_gain=5.0 out\_offset=-0.01)  
  

a21 %i\[1 %vd(2 5) 7 10\] 3 sum2

.model sum2 summer(out\_gain=10.0)  
  
a5 1 2 limit5

.model limit5 limit(in\_offset=0.1 gain=2.5

\+ out\_lower\_limit=-5.0 out\_upper\_limit=5.0 limit\_range=0.10

\+ fraction=FALSE)  
  
a7 2 %id(4 7) xfer\_cntl1

.model xfer\_cntl1 pwl(x\_array=\[-2.0 -1.0 2.0 4.0 5.0\]

\+ y\_array=\[-0.2 -0.2 0.1 2.0 10.0\]

\+ input\_domain=0.05 fraction=TRUE)  
  
a8 3 %gd(6 7) switch3

.model switch3 aswitch(cntl\_off=0.0 cntl\_on=5.0 r\_off=1e6

\+ r\_on=10.0 log=TRUE)

