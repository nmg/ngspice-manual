# Analog Models

The following analog models are supplied with XSPICE. The descriptions
included consist of the model Interface Specification File and a
description of the model's operation. This is followed by an example of
a simulator-deck placement of the model, including the .MODEL card and
the specification of all available parameters.

