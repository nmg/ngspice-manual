# Digital Models

The following digital models are supplied with XSPICE. The descriptions
included below consist of an example model Interface Specification File
and a description of the model's operation. This is followed by an
example of a simulator-deck placement of the model, including the .MODEL
card and the specification of all available parameters. Note that these
models have not been finalized at this time.

Some information common to all digital models and/or digital nodes is
included here. The following are general rules that should make working
with digital nodes and models more straightforward:

1.  All digital nodes are initialized to ZERO at the start of a
    simulation (i.e., when INIT=TRUE). This means that a model need not
    post an explicit value to an output node upon initialization if its
    output would normally be a ZERO (although posting such would
    certainly cause no harm).
2.  Digital nodes may have one out of twelve possible node values. See
    [12.5.1](#subsec_Digital_Node_Type) for details.
3.  Digital models typically have defined their rise and fall delays for
    their output signals. A capacitive input load value may be defined
    as well to determine a load-dependent delay, but is currently not
    used in any code model (see [28.7.1.4](#TOTAL_LOAD_a_)).
4.  Several commands are available for outputting data, e.g. eprint,
    edisplay, and eprvcd. Digital inputs may be read from files. Please
    see Chapt. [12.5.4](#subsec__Digital__Input_Output) for more
    details.
5.  Hybrid models (see Chapt. [12.3](#sec_XSPICE_Hybrid_Models)) provide
    an interface between the digital event driven world and the analog
    world of ngspice to enable true mixed mode simulation.

