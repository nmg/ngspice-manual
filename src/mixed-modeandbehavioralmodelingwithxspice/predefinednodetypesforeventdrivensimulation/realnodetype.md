# Real Node Type

The \`real' node type provides for event-driven simulation with
double-precision floating point data. This type is useful for evaluating
sampled-data filters and systems. The type implements all optional
functions for User-Defined Nodes, including inversion and node
resolution. For inversion, the sign of the value is reversed. For node
resolution, the resultant value at a node is the sum of all values
output to that node. The node is implemented as a user defined node in
ngspice/src/xspice/icm/xtraevt/real.

