# Int Node Type

The \`int' node type provides for event-driven simulation with integer
data. This type is useful for evaluating round-off error effects in
sampled-data systems. The type implements all optional functions for
User-Defined Nodes, including inversion and node resolution. For
inversion, the sign of the integer value is reversed. For node
resolution, the resultant value at a node is the sum of all values
output to that node. The node is implemented as a user defined node in
ngspice/src/xspice/icm/xtraevt/int.

