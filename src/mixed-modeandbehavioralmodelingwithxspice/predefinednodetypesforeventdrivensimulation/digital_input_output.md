# \(Digital\) Input/Output

The analog code models use the standard (analog) nodes provided by
ngspice and thus are using all the commands for sourcing, storing,
printing, and plotting data.

I/O for event nodes (digital, real, int, and UDNs) is offered by the
following tools: For output you may use the plot
([17.5.48](#subsec_Plot__Plot_values)) or eprint
([17.5.25](#subsec_Eprint___Print_an)) commands, as well as edisplay
([17.5.24](#subsec_Edisplay_)) and eprvcd
([17.5.26](#subsec_Edisplay__1)). The latter writes all node data to a
[VCD](http://en.wikipedia.org/wiki/Value_change_dump) file (a digital
standard interface) that may be analysed by viewers like
[gtkwave](http://gtkwave.sourceforge.net/). For input, you may create a
test bench with existing code models (oscillator
([12.3.3](#subsec_Controlled_Digital_Oscillator)), frequency divider
([12.4.19](#subsec_Frequency_Divider)), state machine
([12.4.18](#subsec_State_Machine)) etc.). Reading data from a file is
offered by d\_source ([12.4.21](#subsec_Digital_Source)). Some [comments
and
hints](https://sourceforge.net/p/ngspice/discussion/ngspice-tips/thread/3e193172/)
have been provided by Sdaau. You may also use the analog input from
file, (filesource [12.2.8](#subsec_Filesource)) and convert its analog
input to the digital type by the adc\_bridge
([12.3.2](#subsec_Analog_to_Digital_Node_Bridge)). If you want reading
data from a [VCD](http://en.wikipedia.org/wiki/Value_change_dump) file,
please have a look at [ngspice tips and examples
forum](https://sourceforge.net/p/ngspice/discussion/ngspice-tips/thread/635bb14a/)
and apply a python script provided by Sdaau to translate the VCD data to
d\_source or filesource input.

