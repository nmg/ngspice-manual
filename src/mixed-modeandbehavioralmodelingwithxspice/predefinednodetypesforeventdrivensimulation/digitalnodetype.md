# Digital Node Type

The \`digital' node type is directly built into the simulator. 12
digital node values are available. They are described by a two character
string (the state/strength token). The first character (0, 1, or U)
gives the state of the node (logic zero, logic one, or unknown logic
state). The second character (s, r, z, u) gives the "strength" of the
logic state (strong, resistive, hi-impedance, or undetermined). So these
are the values we have: 0s, 1s, Us, 0r, 1r, Ur, 0z, 1z, Uz, 0u, 1u, Uu.

