# Pullup

NAME\_TABLE:

C\_Function\_Name: cm\_d\_pullup

Spice\_Model\_Name: d\_pullup

Description: "digital pullup resistor"

PORT\_TABLE:

Port Name: out

Description: "output"

Direction: out

Default\_Type: d

Allowed\_Types: \[d\]

Vector: no

Vector\_Bounds: -

Null\_Allowed: no

PARAMETER\_TABLE:

Parameter\_Name: load

Description: "load value (F)"

Data\_Type: real

Default\_Value: 1.0e-12

Limits: -

Vector: no

Vector\_Bounds: -

Null\_Allowed: yes

  - Description:  
    The digital pullup resistor is a device that emulates the behavior
    of an analog resistance value tied to a high voltage level. The
    pullup may be used in conjunction with tristate buffers to provide
    open-collector wired or constructs, or any other logical constructs
    that rely on a resistive pullup common to many tristated output
    devices. The model posts an input load value (in farads) based on
    the parameter load.

Example SPICE Usage:

a2 9 pullup1

.model pullup1 d\_pullup(load = 20.0e-12)

