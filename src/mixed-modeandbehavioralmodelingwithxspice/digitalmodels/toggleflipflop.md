# Toggle Flip Flop

NAME\_TABLE:

C\_Function\_Name: cm\_d\_tff

Spice\_Model\_Name: d\_tff

Description: "digital toggle flip flop"

PORT\_TABLE:

Port Name: t clk

Description: "toggle input" "clock"

Direction: in in

Default\_Type: d d

Allowed\_Types: \[d\] \[d\]

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: no no

PORT\_TABLE:

Port Name: set reset

Description: "set" "reset"

Direction: in in

Default\_Type: d d

Allowed\_Types: \[d\] \[d\]

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: yes yes

PORT.TABLE:

Port Name: out Nout

Description: "data output" "inverted data output"

Direction: out out

Default\_Type: d d

Allowed\_Types: \[d\] \[d\]

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: yes yes

PARAMETER\_TABLE:

Parameter\_Name: clk\_delay set\_delay

Description: "delay from clk" "delay from set"

Data\_Type: real real

Default\_Value: 1.0e-9 1.0e-9

Limits: \[1.0e-12 -\] \[1.0e-12 -\]

Vector: no no

Vector\_Bounds - -

Null\_Allowed: yes yes

PARAMETER\_TABLE:

Parameter\_Name: reset\_delay ic

Description: "delay from reset" "output initial state"

Data\_Type: real int

Default\_Value: 1.0e-9 0

Limits: \[1.0e-12 -\] \[0 2\]

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: yes yes

PARAMETER\_TABLE:

Parameter\_Name: t\_load clk\_load

Description: "toggle load value (F)" "clk load value (F)"

Data\_Type: real real

Default\_Value: 1.0e-12 1.0e-12

Limits: - -

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: yes yes

PARAMETER\_TABLE:

Parameter\_Name: set\_load reset\_load

Description: "set load value (F)" "reset load (F)"

Data\_Type: real real

Default.Value: 1.0e-12 1.0e-12

Limits: - -

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: yes yes

PARAMETER\_TABLE:

Parameter\_Name: rise\_delay fall\_delay

Description: "rise delay" "fall delay"

Data\_Type: real real

Default\_Value: 1.0e-9 1.0e-9

Limits: \[1.0e-12 -\] \[1.0e-12 -\]

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: yes yes

  - Description:  
    The digital toggle-type flip flop is a one-bit, edge-triggered
    storage element that will toggle its current state whenever the clk
    input line transitions from low to high (ZERO to ONE). In addition,
    asynchronous set and reset signals exist, and each of the three
    methods of changing the stored output of the d\_tff have separate
    load values and delays associated with them. Additionally, you may
    specify separate rise and fall delay values that are added to those
    specified for the input lines; these allow for more faithful
    reproduction of the output characteristics of different IC
    fabrication technologies.  
    Note that any UNKNOWN inputs other than t immediately cause the
    output to go UNKNOWN.

Example SPICE Usage:

a8 2 12 4 5 6 3 flop3

.model flop3 d\_tff(clk\_delay = 13.0e-9 set\_delay = 25.0e-9

\+ reset\_delay = 27.0e-9 ic = 2 rise\_delay = 10.0e-9

\+ fall\_delay = 3e-9 t\_load = 0.2e-12)

