# Frequency Divider

NAME\_TABLE:

C\_Function\_Name: cm\_d\_fdiv

Spice\_Model\_Name: d\_fdiv

Description: "digital frequency divider"

PORT\_TABLE:

Port Name: freq\_in freq\_out

Description: "frequency input" "frequency output"

Direction: in out

Default\_Type: d d

Allowed\_Types: \[d\] \[d\]

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: no no

PARAMETER\_TABLE:

Parameter\_Name: div\_factor high\_cycles

Description: "divide factor" "\# of cycles for high out"

Data\_Type: int int

Default\_Value: 2 1

Limits: \[1 -\] \[1 div\_factor-1\]

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: yes yes

PARAMETER\_TABLE:

Parameter\_Name: i\_count

Description: "divider initial count value"

Data\_Type: int

Default\_Value: 0

Limits: -

Vector: no

Vector\_Bounds: -

Null\_Allowed: yes

PARAMETER\_TABLE:

Parameter\_Name: rise\_delay fall\_delay

Description: "rise delay" "fall delay"

Data\_Type: real real

Default\_Value: 1.0e-9 1.0e-9

Limits: \[1.0e-12 -\] \[1.0e-12 -\]

Vector: yes yes

Vector\_Bounds: in in

Null\_Allowed: yes yes

PARAMETER\_TABLE:

Parameter\_Name: freq\_in\_load

Description: "freq\_in load value (F)"

Data\_Type: real

Default\_Value: 1.0e-12

Limits: -

Vector: no

Vector\_Bounds: -

Null\_Allowed: yes

  - Description:  
    The digital frequency divider is a programmable step-down divider
    that accepts an arbitrary divisor (div\_factor), a duty-cycle term
    (high\_cycles), and an initial count value (i\_count). The generated
    output is synchronized to the rising edges of the input signal. Rise
    delay and fall delay on the outputs may also be specified
    independently.

Example SPICE Usage:

a4 3 7 divider

.model divider d\_fdiv(div\_factor = 5 high\_cycles = 3

\+ i\_count = 4 rise\_delay = 23e-9

\+ fall\_delay = 9e-9)

