# LUT

NAME\_TABLE:

C\_Function\_Name: cm\_d\_lut

Spice\_Model\_Name: d\_lut

Description: "digital n-input look-up table gate"

PORT\_TABLE:

Port\_Name: in out

Description: "input" "output"

Direction: in out

Default\_Type: d d

Allowed\_Types: \[d\] \[d\]

Vector: yes no

Vector\_Bounds: \[1 -\] -

Null\_Allowed: no no

PARAMETER\_TABLE:

Parameter\_Name: rise\_delay fall\_delay

Description: "rise delay" "fall delay"

Data\_Type: real real

Default\_Value: 1.0e-9 1.0e-9

Limits: \[1.0e-12 -\] \[1.0e-12 -\]

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: yes yes

PARAMETER\_TABLE:

Parameter\_Name: input\_load

Description: "input load value (F)"

Data\_Type: real

Default\_Value: 1.0e-12

Limits: -

Vector: no

Vector\_Bounds: -

Null\_Allowed: yes

PARAMETER\_TABLE:

Parameter\_Name: table\_values

Description: "lookup table values"

Data\_Type: string

Default\_Value: "0"

Limits: -

Vector: no

Vector\_Bounds: -

Null\_Allowed: no

  - Description:  
    The lookup table provides a way to map any arbitrary n-input,
    1-output combinational logic block to XSPICE. The inputs are mapped
    to the output using a string of length 2^n. The string may contain
    values "0", "1" or "X", corresponding to an output of low, high, or
    unknown, respectively. The outputs are only mapped for inputs which
    are valid logic levels. Any unknown bit in the input vector will
    always produce an unknown output. The first character of the string
    table\_values corresponds to all inputs value zero, and the last
    (2^n) character corresponds to all inputs value one, with the first
    signal in the input vector being the least significant bit. For
    example, a 2-input lookup table representing the function (A \* B)
    (that is, A AND B), with input vector \[A B\] can be constructed
    with a table\_values string of "0001"; function (~A \* B) with input
    vector \[A B\] can be constructed with a table\_values string of
    "0010". The delays associated with an output rise and those
    associated with an output fall may be specified independently. The
    model also posts an input load value (in farads) based on the
    parameter input\_load. The output of this model does not respond to
    the total loading it sees on the output; it will always drive the
    output strongly with the specified delays.

Example SPICE Usage:

\* LUT encoding 3-bit parity function

a4 \[1 2 3\] 5 lut\_pty3\_1

.model lut\_pty3\_1 d\_lut(table\_values = "01101001"

\+ input\_load 2.0e-12)

