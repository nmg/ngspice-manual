# And

NAME\_TABLE:

C\_Function\_Name: cm\_d\_and

Spice\_Model\_Name: d\_and

Description: "digital \`and' gate"

PORT\_TABLE:

Port Name: in out

Description: "input" "output"

Direction: in out

Default\_Type: d d

Allowed\_Types: \[d\] \[d\]

Vector: yes no

Vector\_Bounds: \[2 -\] -

Null\_Allowed: no no

PARAMETER\_TABLE:

Parameter\_Name: rise\_delay fall\_delay

Description: "rise delay" "fall delay"

Data\_Type: real real

Default\_Value: 1.0e-9 1.0e-9

Limits: \[1.0e-12 -\] \[1.0e-12 -\]

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: yes yes

PARAMETER\_TABLE:

Parameter\_Name: input\_load

Description: "input load value (F)"

Data\_Type: real

Default\_Value: 1.0e-12

Limits: -

Vector: no

Vector\_Bounds: -

Null\_Allowed: yes

  - Description:  
    The digital and gate is an n-input, single-output and gate that
    produces an active \`1' value if, and only if, all of its inputs are
    also \`1' values. If ANY of the inputs is a \`0', the output will
    also be a \`0'; if neither of these conditions holds, the output
    will be unknown. The delays associated with an output rise and those
    associated with an output fall may be specified independently. The
    model also posts an input load value (in farads) based on the
    parameter input load. The output of this model does *not*, however,
    respond to the total loading it sees on its output; it will always
    drive the output strongly with the specified delays.

Example SPICE Usage:

a6 \[1 2\] 8 and1

.model and1 d\_and(rise\_delay = 0.5e-9 fall\_delay = 0.3e-9

\+ input\_load = 0.5e-12)

