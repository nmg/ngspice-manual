# D Latch

NAME\_TABLE:

C\_Function\_Name: cm\_d\_dlatch

Spice\_Model\_Name: d\_dlatch

Description: "digital d-type latch"

PORT\_TABLE:

Port Name: data enable

Description: "input data" "enable input"

Direction: in in

Default\_Type: d d

Allowed\_Types: \[d\] \[d\]

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: no no

PORT\_TABLE:

Port Name: set reset

Description: "set" "reset"

Direction: in in

Default\_Type: d d

Allowed\_Types: \[d\] \[d\]

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: yes yes

PORT\_TABLE:

Port Name: out Nout

Description: "data output" "inverter data output"

Direction: out out

Default\_Type: d d

Allowed\_Types: \[d\] \[d\]

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: no no

PARAMETER\_TABLE:

Parameter\_Name: data\_delay

Description: "delay from data"

Data\_Type: real

Default\_Value: 1.0e-9

Limits: \[1.0e-12 -\]

Vector: no

Vector\_Bounds: -

Null\_Allowed: yes

PARAMETER\_TABLE:

Parameter\_Name: enable\_delay set\_delay

Description: "delay from enable" "delay from SET"

Data\_Type: real real

Default\_Value: 1.0e-9 1.0e-9

Limits: \[1.0e-12 -\] \[1.0e-12 -\]

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: yes yes

PARAMETER\_TABLE:

Parameter\_Name: reset\_delay ic

Description: "delay from RESET" "output initial state"

Data\_Type: real boolean

Default\_Value: 1.0e-9 0

Limits: \[1.0e-12 -\] -

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: yes yes

PARAMETER\_TABLE:

Parameter\_Name: data\_load enable\_load

Description: "data load (F)" "enable load value (F)"

Data\_Type: real real

Default\_Value: 1.0e-12 1.0e-12

Limits: - -

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: yes yes

PARAMETER\_TABLE:

Parameter\_Name: set\_load reset\_load

Description: "set load value (F)" "reset load (F)"

Data\_Type: real real

Default\_Value: 1.0e-12 1.0e-12

Limits: - -

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: yes yes

PARAMETER\_TABLE:

Parameter\_Name: rise\_delay fall\_delay

Description: "rise delay" "fall delay"

Data\_Type: real real

Default\_Value: 1.0e-9 1.0e-9

Limits: \[1.0e-12 -\] \[1.0e-12 -\]

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: yes yes

  - Description:  
    The digital d-type latch is a one-bit, level-sensitive storage
    element that will output the value on the data line whenever the
    enable input line is high (ONE). The value on the data line is
    stored (i.e., held on the out line) whenever the enable line is low
    (ZERO).  
    In addition, asynchronous set and reset signals exist, and each of
    the four methods of changing the stored output of the d\_dlatch
    (i.e., data changing with enable=ONE, enable changing to ONE from
    ZERO with a new value on data, raising set and raising reset) have
    separate delays associated with them. You may also specify separate
    rise and fall delay values that are added to those specified for the
    input lines; these allow for more faithful reproduction of the
    output characteristics of different IC fabrication technologies.  
    Note that any UNKNOWN inputs other than on the data line when
    enable=ZERO immediately cause the output to go UNKNOWN.

Example SPICE Usage:

a4 12 4 5 6 3 14 latch1

.model latch1 d\_dlatch(data\_delay = 13.0e-9 enable\_delay = 22.0e-9

\+ set\_delay = 25.0e-9

\+ reset\_delay = 27.0e-9 ic = 2

\+ rise\_delay = 10.0e-9 fall\_delay = 3e-9)

