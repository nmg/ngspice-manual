# Buffer

NAME\_TABLE:

C\_Function\_Name: cm\_d\_buffer

Spice\_Model\_Name: d\_buffer

Description: "digital one-bit-wide buffer"

PORT\_TABLE:

Port Name: in out

Description: "input" "output"

Direction: in out

Default\_Type: d d

Allowed\_Types: \[d\] \[d\]

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: no no

PARAMETER\_TABLE:

Parameter\_Name: rise\_delay fall\_delay

Description: "rise delay" "fall delay"

Data\_Type: real real

Default\_Value: 1.0e-9 1.0e-9

Limits: \[1.0e-12 -\] \[1.0e-12 -\]

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: yes yes

PARAMETER\_TABLE:

Parameter\_Name: input\_load

Description: "input load value (F)"

Data\_Type: real

Default\_Value: 1.0e-12

Limits: -

Vector: no

Vector\_Bounds: -

Null\_Allowed: yes

  - Description:  
    The buffer is a single-input, single-output digital buffer that
    produces as output a time-delayed copy of its input. The delays
    associated with an output rise and those associated with an output
    fall may be different. The model also posts an input load value (in
    farads) based on the parameter input load. The output of this model
    does *not*, however, respond to the total loading it sees on its
    output; it will always drive the output strongly with the specified
    delays.

Example SPICE Usage:

a6 1 8 buff1

.model buff1 d\_buffer(rise\_delay = 0.5e-9 fall\_delay = 0.3e-9

\+ input\_load = 0.5e-12)

