# General LUT

NAME\_TABLE:

C\_Function\_Name: cm\_d\_genlut

Spice\_Model\_Name: d\_genlut

Description: "digital n-input x m-output look-up table gate"

PORT\_TABLE:

Port\_Name: in out

Description: "input" "output"

Direction: in out

Default\_Type: d d

Allowed\_Types: \[d\] \[d\]

Vector: yes yes

Vector\_Bounds: - -

Null\_Allowed: no no

PARAMETER\_TABLE:

Parameter\_Name: rise\_delay fall\_delay

Description: "rise delay" "fall delay"

Data\_Type: real real

Default\_Value: 1.0e-9 1.0e-9

Limits: \[1.0e-12 -\] \[1.0e-12 -\]

Vector: yes yes

Vector\_Bounds: - -

Null\_Allowed: yes yes

PARAMETER\_TABLE:

Parameter\_Name: input\_load input\_delay

Description: "input load value (F)" "input delay"

Data\_Type: real real

Default\_Value: 1.0e-12 0.0

Limits: - -

Vector: yes yes

Vector\_Bounds: - -

Null\_Allowed: yes yes

PARAMETER\_TABLE:

Parameter\_Name: table\_values

Description: "lookup table values"

Data\_Type: string

Default\_Value: "0"

Limits: -

Vector: no

Vector\_Bounds: -

Null\_Allowed: no

  - Description:  
    The lookup table provides a way to map any arbitrary n-input,
    m-output combinational logic block to XSPICE. The inputs are mapped
    to the output using a string of length m \* (2^n). The string may
    contain values "0", "1", "X", or "Z", corresponding to an output of
    low, high, unknown, or high-impedance, respectively. The outputs are
    only mapped for inputs which are valid logic levels. Any unknown bit
    in the input vector will always produce an unknown output. The
    character string is in groups of (2^n) characters, one group
    corresponding to each output pin, in order. The first character of a
    group in the string table\_values corresponds to all inputs value
    zero, and the last (2^n) character in the group corresponds to all
    inputs value one, with the first signal in the input vector being
    the least significant bit. For example, a 2-input lookup table
    representing the function (A \* B) (that is, A AND B), with input
    vector \[A B\] can be constructed with a table\_values string of
    "0001"; function (~A \* B) with input vector \[A B\] can be
    constructed with a "table\_values" string of "0010". The delays
    associated with each output pin's rise and those associated with
    each output pin's fall may be specified independently. The model
    also posts independent input load values per input pin (in farads)
    based on the parameter input\_load. The parameter input\_delay
    provides a way to specify additional delay between each input pin
    and the output. This delay is added to the rise- or fall-time of the
    output. The output of this model does not respond to the total
    loading it sees on the output; it will always drive the output
    strongly with the specified delays.

Example SPICE Usage:

\* LUT encoding 3-bit parity function

a4 \[1 2 3\] \[5\] lut\_pty3\_1

.model lut\_pty3\_1 d\_genlut(table\_values = "01101001"

\+ input\_load \[2.0e-12\])

\* LUT encoding a tristate inverter function (en in out)

a2 \[1 2\] \[3\] lut\_triinv\_1

.model lut\_triinv\_1 d\_genlut(table\_values = "Z1Z0")

\* LUT encoding a half-adder function (A B Carry Sum)

a8 \[1 2\] \[3 4\] lut\_halfadd\_1

.model lut\_halfadd\_1 d\_genlut(table\_values = "00010110"

\+ rise\_delay \[ 1.5e-9 1.0e-9 \] fall\_delay \[ 1.5e-9 1.0e-9 \])

