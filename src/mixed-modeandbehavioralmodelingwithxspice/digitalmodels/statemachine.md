# State Machine

NAME\_TABLE:

C\_Function\_Name: cm\_d\_state

Spice\_Model\_Name: d\_state

Description: "digital state machine"

PORT\_TABLE:

Port Name: in clk

Description: "input" "clock"

Direction: in in

Default\_Type: d d

Allowed\_Types: \[d\] \[d\]

Vector: yes no

Vector\_Bounds: \[1 -\] -

Null\_Allowed: yes no

PORT\_TABLE:

Port Name: reset out

Description: "reset" "output"

Direction: in out

Default\_Type: d d

Allowed\_Types: \[d\] \[d\]

Vector: no yes

Vector\_Bounds: - \[1 -\]

Null\_Allowed: yes no

PARAMETER\_TABLE:

Parameter\_Name: clk\_delay reset\_delay

Description: "delay from CLK" "delay from RESET"

Data\_Type: real real

Default\_Value: 1.0e-9 1.0e-9

Limits: \[1.0e-12 -\] \[1.0e-12 -\]

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: yes yes

PARAMETER\_TABLE: Parameter\_Name: state\_file

Description: "state transition specification file name"

Data\_Type: string

Default\_Value: "state.txt"

Limits: -

Vector: no

Vector\_Bounds: -

Null\_Allowed: no

PARAMETER\_TABLE:

Parameter\_Name: reset\_state

Description: "default state on RESET & at DC"

Data\_Type: int

Default\_Value: 0

Limits: -

Vector: no

Vector\_Bounds: -

Null\_Allowed: no

PARAMETER\_TABLE:

Parameter\_Name: input\_load

Description: "input loading capacitance (F)"

Data\_Type: real

Default\_Value: 1.0e-12

Limits: -

Vector: no

Vector\_Bounds: -

Null\_Allowed: yes

PARAMETER\_TABLE:

Parameter\_Name: clk\_load

Description: "clock loading capacitance (F)"

Data\_Type: real

Default\_Value: 1.0e-12

Limits: -

Vector: no

Vector\_Bounds: -

Null\_Allowed: yes

PARAMETER\_TABLE:

Parameter\_Name: reset\_load

Description: "reset loading capacitance (F)"

Data\_Type: real

Default\_Value: 1.0e-12

Limits: -

Vector: no

Vector\_Bounds: -

Null\_Allowed: yes

  - Description:  
    The digital state machine provides for straightforward descriptions
    of clocked combinational logic blocks with a variable number of
    inputs and outputs and with an unlimited number of possible states.
    The model can be configured to behave as virtually any type of
    counter or clocked combinational logic block and can be used to
    replace very large digital circuit schematics with an identically
    functional but faster representation.  
    The d state model is configured through the use of a state
    definition file (state.in) that resides in a directory of your
    choosing. The file defines all states to be understood by the model,
    plus input bit combinations that trigger changes in state. An
    example state.in file is shown below:

\----------- begin file -------------

\* This is an example state.in file. This file

\* defines a simple 2-bit counter with one input. The

\* value of this input determines whether the counter counts

\* up (in = 1) or down (in = 0).

0 0s 0s 0 -\> 3

1 -\> 1

1 0s 1z 0 -\> 0

1 -\> 2

2 1z 0s 0 -\> 1

1 -\> 3

3 1z 1z 0 -\> 2

3 1z 1z 1 -\> 0

\------------------ end file ---------------

Several attributes of the above file structure should be noted. First,
*all lines in the file must be one of four types*. These are:

1.  A comment, beginning with a \`\*' in the first column.
2.  A header line, which is a complete description of the current state,
    the outputs corresponding to that state, an input value, and the
    state that the model will assume should that input be encountered.
    The first line of a state definition must *always* be a header line.
3.  A continuation line, which is a partial description of a state,
    consisting of an input value and the state that the model will
    assume should that input be encountered. Note that continuation
    lines may only be used after the initial header line definition for
    a state.
4.  A line containing nothing but white-spaces (space, form-feed,
    newline, carriage return, tab, vertical tab).

A line that is not one of the above will cause a file-loading error.
Note that in the example shown, whitespace (any combination of blanks,
tabs, commas) is used to separate values, and that the character -\> is
used to underline the state transition implied by the input preceding
it. This particular character is not critical in of itself, and can be
replaced with any other character or non-broken combination of
characters that you prefer (e.g. ==\>, \>\>, ':', resolves\_to, etc.)

The order of the output and input bits in the file is important; the
first column is always interpreted to refer to the 'zeroth' bit of input
and output. Thus, in the file above, the output from state 1 sets
out\[0\] to 0s, and out\[1\] to 1z.

The state numbers need not be in any particular order, but a state
definition (which consists of the sum total of all lines that define the
state, its outputs, and all methods by which a state can be exited) must
be made on contiguous line numbers; a state definition cannot be broken
into sub-blocks and distributed randomly throughout the file. On the
other hand, the state definition can be broken up by as many comment
lines as you desire.

Header files may be used throughout the state.in file, and continuation
lines can be discarded completely if you so choose: continuation lines
are primarily provided as a convenience.

Example SPICE Usage:

a4 \[2 3 4 5\] 1 12 \[22 23 24 25 26 27 28 29\] state1

.model state1 d\_state(clk\_delay = 13.0e-9 reset\_delay = 27.0e-9

\+ state\_file = "newstate.txt" reset\_state = 2)

  - Note:  
    The file named by the parameter filename in state\_file="filename"
    is sought after according to a search list described
    in[12.1.3](#subsec_Search_path_for).

