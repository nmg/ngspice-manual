# RAM

NAME\_TABLE:

C\_Function\_Name: cm\_d\_ram

Spice\_Model\_Name: d\_ram

Description: "digital random-access memory"

PORT\_TABLE:

Port Name: data\_in data\_out

Description: "data input line(s)" "data output line(s)"

Direction: in out

Default\_Type: d d

Allowed\_Types: \[d\] \[d\]

Vector: yes yes

Vector\_Bounds: \[1 -\] data\_in

Null\_Allowed: no no

PORT\_TABLE:

Port Name: address write\_en

Description: "address input line(s)" "write enable line"

Direction: in in

Default\_Type: d d

Allowed\_Types: \[d\] \[d\]

Vector: yes no

Vector\_Bounds: \[1 -\] -

Null\_Allowed: no no

PORT\_TABLE:

Port Name: select

Description: "chip select line(s)"

Direction: in

Default\_Type: d

Allowed\_Types: \[d\]

Vector: yes

Vector\_Bounds: \[1 16\]

Null\_Allowed: no

PARAMETER\_TABLE:

Parameter\_Name: select\_value

Description: "decimal active value for select line comparison"

Data\_Type: int

Default\_Value: 1

Limits: \[0 32767\]

Vector: no

Vector\_Bounds: -

Null\_Allowed: yes

PARAMETER\_TABLE:

Parameter\_Name: ic

Description: "initial bit state @ dc"

Data\_Type: int

Default\_Value: 2

Limits: \[0 2\]

Vector: no

Vector\_Bounds: -

Null\_Allowed: yes

PARAMETER\_TABLE:

Parameter\_Name: read\_delay

Description: "read delay from address/select/write.en active"

Data\_Type: real

Default\_Value: 100.0e-9

Limits: \[1.0e-12 -\]

Vector: no

Vector\_Bounds: -

Null\_Allowed: yes

PARAMETER\_TABLE:

Parameter\_Name: data\_load address\_load

Description: "data\_in load value (F)" "addr. load value (F)"

Data\_Type: real real

Default\_Value: 1.0e-12 1.0e-12

Limits: - -

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: yes yes

PARAMETER\_TABLE:

Parameter\_Name: select\_load

Description: "select load value (F)"

Data\_Type: real

Default\_Value: 1.0e-12

Limits: -

Vector: no

Vector\_Bounds: -

Null\_Allowed: yes

PARAMETER\_TABLE:

Parameter\_Name: enable\_load

Description: "enable line load value (F)"

Data\_Type: real

Default\_Value: 1.0e-12

Limits: -

Vector: no

Vector\_Bounds: -

Null\_Allowed: yes

  - Description:  
    The digital RAM is an M-wide, N-deep random access memory element
    with programmable select lines, tristated data out lines, and a
    single write/~read line. The width of the RAM words (M) is set
    through the use of the word width parameter. The depth of the RAM
    (N) is set by the number of address lines input to the device. The
    value of N is related to the number of address input lines (P) by
    the following equation: \[2^{P} = N\]There is no reset line into the
    device. However, an initial value for all bits may be specified by
    setting the ic parameter to either 0 or 1. In reading a word from
    the ram, the read delay value is invoked, and output will not appear
    until that delay has been satisfied. Separate rise and fall delays
    are not supported for this device.  
    Note that UNKNOWN inputs on the address lines are not allowed during
    a write. In the event that an address line does indeed go unknown
    during a write, *the entire contents of the ram will be set to
    unknown*. This is in contrast to the data in lines being set to
    unknown during a write; in that case, only the selected word will be
    corrupted, and this is corrected once the data lines settle back to
    a known value. Note that protection is added to the write en line
    such that extended UNKNOWN values on that line are interpreted as
    ZERO values. This is the equivalent of a read operation and will not
    corrupt the contents of the RAM. A similar mechanism exists for the
    select lines. If they are unknown, then it is assumed that the chip
    is not selected.  
    Detailed timing-checking routines are not provided in this model,
    other than for the enable delay and select delay restrictions on
    read operations. You are advised, therefore, to carefully check the
    timing into and out of the RAM for correct read and write cycle
    times, setup and hold times, etc. for the particular device they are
    attempting to model.

Example SPICE Usage:

a4 \[3 4 5 6\] \[3 4 5 6\] \[12 13 14 15 16 17 18 19\] 30 \[22 23 24\]
ram2

.model ram2 d\_ram(select\_value = 2 ic = 2 read\_delay = 80e-9)

