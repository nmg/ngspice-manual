# Pulldown

NAME\_TABLE:

C\_Function\_Name: cm\_d\_pulldown

Spice\_Model\_Name: d\_pulldown

Description: "digital pulldown resistor"

PORT\_TABLE:

Port Name: out

Description: "output"

Direction: out

Default\_Type: d

Allowed\_Types: \[d\]

Vector: no

Vector\_Bounds: -

Null\_Allowed: no

PARAMETER\_TABLE:

Parameter\_Name: load

Description: "load value (F)"

Data\_Type: real

Default\_Value: 1.0e-12

Limits: -

Vector: no

Vector\_Bounds: -

Null\_Allowed: yes

  - Description:  
    The digital pulldown resistor is a device that emulates the behavior
    of an analog resistance value tied to a low voltage level. The
    pulldown may be used in conjunction with tristate buffers to provide
    open-collector wired or constructs, or any other logical constructs
    that rely on a resistive pulldown common to many tristated output
    devices. The model posts an input load value (in farads) based on
    the parameter load.

Example SPICE Usage:

a4 9 pulldown1

.model pulldown1 d\_pulldown(load = 20.0e-12)

