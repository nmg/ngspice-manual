# Nor

NAME\_TABLE:

C\_Function\_Name: cm\_d\_nor

Spice\_Model\_Name: d\_nor

Description: "digital \`nor' gate"

PORT\_TABLE:

Port Name: in out

Description: "input" "output"

Direction: in out

Default\_Type: d d

Allowed\_Types: \[d\] \[d\]

Vector: yes no

Vector\_Bounds: \[2 -\] -

Null\_Allowed: no no

PARAMETER\_TABLE:

Parameter\_Name: rise\_delay fall\_delay

Description: "rise delay" "fall delay"

Data\_Type: real real

Default\_Value: 1.0e-9 1.0e-9

Limits: \[1.0e-12 -\] \[1.0e-12 -\]

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: yes yes

PARAMETER\_TABLE:

Parameter\_Name: input\_load

Description: "input load value (F)"

Data\_Type: real

Default\_Value: 1.0e-12

Limits: -

Vector: no

Vector\_Bounds: -

Null\_Allowed: yes

  - Description:  
    The digital nor gate is an n-input, single-output nor gate that
    produces an active \`0' value if at least one of its inputs is a
    \`1' value. The gate produces a \`0' value if all inputs are \`0';
    if neither of these two conditions holds, the output is unknown. The
    delays associated with an output rise and those associated with an
    output fall may be specified independently. The model also posts an
    input load value (in farads) based on the parameter input load. The
    output of this model does *not*, however, respond to the total
    loading it sees on its output; it will always drive the output
    strongly with the specified delays.

Example SPICE Usage:

anor12 \[1 2 3 4\] 8 nor12

.model nor12 d\_nor(rise\_delay = 0.5e-9 fall\_delay = 0.3e-9

\+ input\_load = 0.5e-12)

