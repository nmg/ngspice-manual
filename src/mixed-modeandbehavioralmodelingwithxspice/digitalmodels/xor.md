# Xor

NAME\_TABLE:

C\_Function\_Name: cm\_d\_xor

Spice\_Model\_Name: d\_xor

Description: "digital exclusive-or gate"

PORT\_TABLE:

Port Name: in out

Description: "input" "output"

Direction: in out

Default\_Type: d d

Allowed\_Types: \[d\] \[d\]

Vector: yes no

Vector\_Bounds: \[2 -\] -

Null\_Allowed: no no

PARAMETER\_TABLE:

Parameter\_Name: rise\_delay fall\_delay

Description: "rise delay" "fall delay"

Data\_Type: real real

Default\_Value: 1.0e-9 1.0e-9

Limits: \[1.0e-12 -\] \[1.0e-12 -\]

Vector: no no

Vector\_Bounds: - -

Null\_Allowed: yes yes

PARAMETER\_TABLE:

Parameter\_Name: input\_load

Description: "input load value (F)"

Data\_Type: real

Default\_Value: 1.0e-12

Limits: -

Vector: no

Vector\_Bounds: -

Null\_Allowed: yes

  - Description:  
    The digital xor gate is an n-input, single-output xor gate that
    produces an active \`1' value if an odd number of its inputs are
    also \`1' values. The delays associated with an output rise and
    those associated with an output fall may be specified
    independently.  
    The model also posts an input load value (in farads) based on the
    parameter input load. The output of this model does *not*, however,
    respond to the total loading it sees on its output; it will always
    drive the output strongly with the specified delays. Note also that
    to maintain the technology-independence of the model, any UNKNOWN
    input, or any floating input causes the output to also go UNKNOWN.

Example SPICE Usage:

a9 \[1 2\] 8 xor3

.model xor3 d\_xor(rise\_delay = 0.5e-9 fall\_delay = 0.3e-9

\+ input\_load = 0.5e-12)

