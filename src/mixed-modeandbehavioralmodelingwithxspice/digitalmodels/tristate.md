# Tristate

NAME\_TABLE:

C\_Function\_Name: cm\_d\_tristate

Spice\_Model\_Name: d\_tristate

Description: "digital tristate buffer"

PORT\_TABLE:

Port Name: in enable out

Description: "input" "enable" "output"

Direction: in in out

Default\_Type: d d d

Allowed\_Types: \[d\] \[d\] \[d\]

Vector: no no no

Vector\_Bounds: - - -

Null\_Allowed: no no no

PARAMETER\_TABLE:

Parameter\_Name: delay

Description: "delay"

Data\_Type: real

Default\_Value: 1.0e-9

Limits: \[1.0e-12 -\]

Vector: no

Vector\_Bounds: -

Null\_Allowed: yes

PARAMETER\_TABLE:

Parameter\_Name: input\_load

Description: "input load value (F)"

Data\_Type: real

Default\_Value: 1.0e-12

Limits: -

Vector: no

Vector\_Bounds: -

Null\_Allowed: yes

PARAMETER\_TABLE:

Parameter\_Name: enable\_load

Description: "enable load value (F)"

Data\_Type: real

Default\_Value: 1.0e-12

Limits: -

Vector: no

Vector\_Bounds: -

Null\_Allowed: yes

  - Description:  
    The digital tristate is a simple tristate gate that can be
    configured to allow for open-collector behavior, as well as standard
    tristate behavior. The state seen on the input line is reflected in
    the output. The state seen on the enable line determines the
    strength of the output. Thus, a ONE forces the output to its state
    with a STRONG strength. A ZERO forces the output to go to a
    HI\_IMPEDANCE strength. The delays associated with an output state
    or strength change cannot be specified independently, nor may they
    be specified independently for rise or fall conditions; other gate
    models may be used to provide such delays if needed. The model posts
    input and enable load values (in farads) based on the parameters
    input load and enable. The output of this model does *not*, however,
    respond to the total loading it sees on its output; it will always
    drive the output with the specified delay. Note also that to
    maintain the technology-independence of the model, any UNKNOWN
    input, or any floating input causes the output to also go UNKNOWN.
    Likewise, any UNKNOWN input on the enable line causes the output to
    go to an UNDETERMINED strength value.

Example SPICE Usage:

a9 1 2 8 tri7

.model tri7 d\_tristate(delay = 0.5e-9 input\_load = 0.5e-12

\+ enable\_load = 0.5e-12)

