# Hybrid Models

The following hybrid models are supplied with XSPICE. The descriptions
included below consist of the model Interface Specification File and a
description of the model's operation. This is followed by an example of
a simulator-deck placement of the model, including the .MODEL card and
the specification of all available parameters.

A note should be made with respect to the use of hybrid models for other
than simple digital-to-analog and analog-to-digital translations. The
hybrid models represented in this section address that specific need,
but in the development of user-defined nodes you may find a need to
translate not only between digital and analog nodes, but also between
real and digital, real and int, etc. In most cases such translations
will not need to be as involved or as detailed as shown in the
following.

