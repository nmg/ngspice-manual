#  Example Circuits

This section starts with an ngspice example to walk you through the
basic features of ngspice using its command line user interface. The
operation of ngspice will be illustrated through several examples
(Chapt. 20.1 to 20.7).

The first example uses the simple one-transistor amplifier circuit
illustrated in Fig. [21.1](#cap_Transistor_Amplifier_Simulation). This
circuit is constructed entirely with ngspice compatible devices and is
used to introduce basic concepts, including:

![image:
3\_home\_nmg\_Documents\_gitroot\_ngspice-ngspice-manuals\_Images\_Example\_Circuit\_C1.png](3_home_nmg_Documents_gitroot_ngspice-ngspice-manuals_Images_Example_Circuit_C1.png)

Figure 21.1:  Transistor Amplifier Simulation Example

  - Invoking the simulator:
  - Running simulations in different analysis modes
  - Printing and plotting analog results
  - Examining status, including execution time and memory usage
  - Exiting the simulator

The remainder of the section (from Chapt. [21.2](#sec_Differential_Pair)
onward) lists several circuits, which have been accompanying any ngspice
distribution, and may be regarded as the \`classical' SPICE circuits.

