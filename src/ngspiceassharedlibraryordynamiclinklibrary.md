#  ngspice as shared library or dynamic link library

ngspice may be compiled as a shared library. This allows adding ngspice
to an application that then gains control over the simulator. The shared
module offers an interface that exports functions controlling the
simulator and callback functions for feedback.

So you may send an input \`file' with a netlist to ngspice, start the
simulation in a separate thread, read back simulation data at each time
point, stop the simulator depending on some condition, alter device or
model parameters and then resume the simulation.

Shared ngspice does not have any user interface. The calling process is
responsible for this. It may offer a graphical user interface, add
plotting capability or any other interactive element. You may develop
and optimize these user interface elements without a need to alter the
ngspice source code itself, using a console application or GUIs like
gtk, Delphi, Qt or others.

