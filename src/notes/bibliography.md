# Bibliography

1A. Vladimirescu and S. Liu, *\`The Simulation of MOS Integrated
Circuits Using SPICE2'* ERL Memo No. ERL M80/7, Electronics Research
Laboratory University of California, Berkeley, October 1980

2 T. Sakurai and A. R. Newton, *\`A Simple MOSFET Model for Circuit
Analysis and its application to CMOS gate delay analysis and
series-connected MOSFET Structure*' [ERL Memo No. ERL
M90/19](http://www.eecs.berkeley.edu/Pubs/TechRpts/1990/1429.html),
Electronics Research Laboratory, University of California, Berkeley,
March 1990

3B. J. Sheu, D. L. Scharfetter, and P. K. Ko, \`*SPICE2 Implementation
of BSIM'* ERL Memo No. ERL M85/42, Electronics Research Laboratory
University of California, Berkeley, May 1985

4J. R. Pierret, *\`A MOS Parameter Extraction Program for the BSIM
Model'* ERL Memo Nos. ERL M84/99 and M84/100, Electronics Research
Laboratory University of California, Berkeley, November 1984

5Min-Chie Jeng, \`*Design and Modeling of Deep Submicrometer MOSFETSs'*
[ERL Memo Nos. ERL
M90/90](http://www.eecs.berkeley.edu/Pubs/TechRpts/1990/1601.html),
Electronics Research Laboratory, University of California, Berkeley,
October 1990

6Soyeon Park, \`*Analysis and SPICE implementation of High Temperature
Effects on MOSFET*', Master's thesis, University of California,
Berkeley, December 1986.

7Clement Szeto, \`*Simulation of Temperature Effects in MOSFETs
(STEIM)*', Master's thesis, University of California, Berkeley, May
1988.

8J.S. Roychowdhury and D.O. Pederson, *\`Efficient Transient Simulation
of Lossy Interconnect*', Proc. of the 28th ACM/IEEE Design Automation
Conference, June 17-21 1991, San Francisco

9A. E. Parker and D. J. Skellern, \`*An Improved FET Model for Computer
Simulators*', IEEE Trans CAD, vol. 9, no. 5, pp. 551-553, May 1990.

10R. Saleh and A. Yang, Editors, \`*Simulation and Modeling*', IEEE
Circuits and Devices, vol. 8, no. 3, pp. 7-8 and 49, May 1992.

11H.Statz et al., \`*GaAs FET Device and Circuit Simulation in SPICE*',
IEEE Transactions on Electron Devices, V34, Number 2, February 1987,
pp160-169.

12Weidong Liu et al.: \`*BSIM3v3.2.2 MOSFET Model User's Manual*',
[BSIM3v3.2.2](http://ngspice.sourceforge.net/external-documents/models/bsim322_manual.pdf)

13Weidong Lui et al.: \`*BSIM3.v3.3.0 MOSFET Model User's Manual*',
[BSIM3v3.3.0](http://ngspice.sourceforge.net/external-documents/models/bsim330_manual.pdf)

14\`SPICE3.C1 *Nutmeg Programmer's Manual*', Department of Electrical
Engineering and Computer Sciences, University of California, Berkeley,
California, April, 1987.

15Thomas L. Quarles: [SPICE3 Version 3C1 User's
Guide](www.eecs.berkeley.edu/Pubs/TechRpts/1989/ERL-89-46.pdf),
Department of Electrical Engineering and Computer Sciences, University
of California, Berkeley, California, April, 1989.

16Brian Kernighan and Dennis Ritchie: \`*The C Programming Language*',
Second Edition, Prentice-Hall, Englewood Cliffs, New Jersey, 1988.

17\`*Code-Level Modeling in XSPICE*', F.L. Cox, W.B. Kuhn, J.P. Murray,
and S.D. Tynor, published in the Proceedings of the 1992 International
Symposium on Circuits and Systems, San Diego, CA, May 1992, vol 2, pp.
871-874.

18\`*A Physically Based Compact Model of Partially Depleted SOI MOSFETs
for Analog Circuit Simulation*', Mike S. L. Lee, Bernard M. Tenbroek,
William Redman-White, James Benson, and Michael J. Uren, IEEE JOURNAL OF
SOLID-STATE CIRCUITS, VOL. 36, NO. 1, JANUARY 2001, pp. 110-121

19\`*A Realistic Large-signal MESFET Model for SPICE*', A. E. Parker,
and D. J. Skellern, IEEE Transactions on Microwave Theory and
Techniques, vol. 45, no. 9, Sept. 1997, pp. 1563-1571.

20 \`*Integrating RTS Noise into Circuit Analysis*', T. B. Tang and A.
F. Murray, IEEE ISCAS, 2009, Proc. of IEEE ISCAS, Taipei, Taiwan, May
2009, pp 585-588

21R. Storn, and K. Price: \`*Differential Evolution*', technical report
TR-95-012, ICSI, March 1995, see [report
download](http://www.icsi.berkeley.edu/~storn/TR-95-012.pdf), or the [DE
web page](http://www.icsi.berkeley.edu/~storn/code.html)

22M. J. M. Pelgrom e.a.: \`*Matching Properties of MOS Transistors*',
IEEE J. Sol. State Circ, vol. 24, no. 5, Oct. 1989, pp. 1433-1440

23 Y. V. Pershin, M. Di Ventra: \`SPICE *model of memristive devices
with threshold*', arXiv:1204.2600v1 \[physics.comp-ph\] 12 Apr 2012,
<http://arxiv.org/pdf/1204.2600.pdf>

  

# Part II  XSPICE Software User's Manual

