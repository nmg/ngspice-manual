# User defined configuration file .spiceinit

In addition to spinit you may define a (personal) file .spiceinit and
put it into the current directory or in your home directory. The typical
search sequence for .spiceinit is: current directory, HOME (Linux) and
then USERPROFILE (Windows). USERPROFILE is typically C:\\Users\\\<User
name\>. This file will be read in and executed after spinit, but before
any other input file is read. It may contain further scripts, set
variables, or issue commands from Chapt.[17.5](#sec_Commands) to
override commands given in spinit. For example set filetype=ascii will
yield ASCII output in the output data file (rawfile), instead of the
compact binary format that is used by default. set ngdebug will yield a
lot of additional debug output. Any other contents of the script, e.g.
plotting preferences, may be included here also. If the command line
option -n is used upon ngspice start up, this file will be ignored.

.spiceinit may contain:

``` listings
* User defined ngspice init file
set filetype=ascii
*set ngdebug
set numthreads = 8
*set outputpath=C:\Spice64\out
```

