# Ngspice control via input, output fifos

The following bash script (under Linux)

\- launches ngspice in another thread.

\- writes some commands in ngspice input

\- reads the output and prints them on the console.

Example:

``` listings
#!/usr/bin/env bash

NGSPICE_COMMAND="ngspice"

rm input.fifo
rm output.fifo

mkfifo input.fifo
mkfifo output.fifo

$NGSPICE_COMMAND  -p -i <input.fifo >output.fifo &

exec 3>input.fifo
echo "I can write to input.fifo"

echo "Start processing..."
echo ""

echo "source circuit.cir" >&3
echo "unset askquit" >&3
echo "set nobreak" >&3
echo "tran 0.01ms 0.1ms">&3
echo "print n0" >&3
echo "quit" >&3

echo "Try to open output.fifo ..."
exec 4<output.fifo
echo "I can read from output.fifo"

echo "Ready to read..."
while read output
do
      echo $output
done <&4

exec 3>&-
exec 4>&-

echo "End processing"
```

The input file for SPICE is:

Circuit.cir:

``` listings
* Circuit.cir
V1 n0 0 SIN(0 10 1kHz)
C1 n1 n0 3.3nF
R1 0 n1 1k
.end
```

