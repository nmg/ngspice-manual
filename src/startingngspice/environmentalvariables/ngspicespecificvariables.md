# Ngspice specific variables

  - SPICE\_LIB\_DIR  
    default: /usr/local/share/ngspice (Linux, CYGWIN),
    C:\\Spice\\share\\ngspice (Windows)
  - SPICE\_EXEC\_DIR  
    default: /usr/local/bin (Linux, CYGWIN), C:\\Spice\\bin (Windows)
  - SPICE\_BUGADDR  
    default: http://ngspice.sourceforge.net/bugrep.html  
    Where to send bug reports on ngspice.
  - SPICE\_EDITOR  
    default: vi (Linux, CYGWIN), notepad.exe (MINGW, Visual Studio)  
    Set the editor called in the **edit** command. Always overrides the
    EDITOR env. variable.
  - SPICE\_ASCIIRAWFILE  
    default: 0  
    Format of the rawfile. 0 for binary, and 1 for ascii.
  - SPICE\_NEWS  
    default: $SPICE\_LIB\_DIR/news  
    A file that is copied verbatim to stdout when ngspice starts in
    interactive mode.
  - SPICE\_HELP\_DIR  
        default: $SPICE\_LIB\_DIR/helpdir  
    Help directory, not used in Windows mode
  - SPICE\_HOST  
    default: empty string  
    Used in the **rspice** command (probably obsolete, to be documented)
  - SPICE\_SCRIPTS  
    default: $SPICE\_LIB\_DIR/scripts  
    In this directory the spinit file will be searched.
  - SPICE\_PATH  
    default: $SPICE\_EXEC\_DIR/ngspice  
    Used in the **aspice** command (probably obsolete, to be documented)
  - NGSPICE\_MEAS\_PRECISION  
    default: 5  
    Sets the number of digits if output values are printed by the
    **meas(ure)** command.
  - SPICE\_NO\_DATASEG\_CHECK  
    default: undefined  
    If defined, will suppress memory resource info (probably obsolete,
    not used on Windows or where the /proc information system is
    available.)
  - NGSPICE\_INPUT\_DIR  
    default: undefined  
    If defined, using a valid directory name, will add the given
    directory to the search path when looking for input files (\*.cir,
    \*.inc, \*.lib).

