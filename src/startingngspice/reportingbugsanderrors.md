# Reporting bugs and errors

Ngspice is a complex piece of software. The source code contains over
1500 files. Various models and simulation procedures are provided, some
of them not used and tested intensively. Therefore errors may be found,
some still evolving from the original spice3f5 code, others introduced
during the ongoing code enhancements.

If you happen to experience an error during the usage of ngspice, please
send a report to the development team. Ngspice is hosted on sourceforge,
the preferred place to post a bug report is the [ngspice bug
tracker](http://sourceforge.net/tracker/?group_id=38962&atid=423915). We
would prefer to have your bug tested against the actual source code
available at Git, but of course a report using the most recent ngspice
release is welcome\! Please provide the following information with your
report:

Ngspice version

Operating system

Small input file to reproduce the bug

Actual output versus the expected output

