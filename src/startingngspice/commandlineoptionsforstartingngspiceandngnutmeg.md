# Command line options for starting ngspice and ngnutmeg

Command Synopsis:

``` listings
ngspice [ -o logfile] [ -r rawfile] [ -b ] [ -i ] [ input files ]
ngnutmeg [ - ] [ datafile ... ]
```

Options are:

<table>
<colgroup>
<col width="33%" />
<col width="33%" />
<col width="33%" />
</colgroup>
<tbody>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-20528"></span>Option
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-20531"></span>Long option
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-20534"></span>Meaning
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-20537"></span><span style="font-family:monospace;">-</span>
</div></td>
<td></td>
<td><div class="plain_layout" style="text-align: left;">
<span id="magicparlabel-20543"></span>Don't try to load the default data file (&quot;rawspice.raw&quot;) if no other files are given (ngnutmeg only).
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-20546"></span><span style="font-family:monospace;">-n</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-20549"></span><span style="font-family:monospace;">--no-spiceinit </span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-20552"></span>Don't try to source the file <span style="font-family:sans-serif;">.spiceinit</span> upon start-up. Normally ngspice and ngnutmeg try to find the file in the current directory, and if it is not found then in the user's home directory (obsolete).
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-20555"></span><span style="font-family:monospace;">-t TERM</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-20558"></span><span style="font-family:monospace;">--terminal=TERM </span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-20561"></span>The program is being run on a terminal with mfb name term (obsolete).
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-20564"></span><span style="font-family:monospace;">-b</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-20567"></span><span style="font-family:monospace;">--batch </span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-20570"></span>Run in batch mode. Ngspice reads the default input source (e.g. keyboard) or reads the given input file and performs the analyses specified; output is either Spice2-like line-printer plots (&quot;ascii plots&quot;) or a ngspice rawfile. See the following section for details. Note that if the input source is not a terminal (e.g. using the IO redirection notation of &quot;&lt;&quot;) ngspice defaults to batch mode (-i overrides). This option is valid for ngspice only.
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-20573"></span><span style="font-family:monospace;">-s</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-20576"></span><span style="font-family:monospace;">--server </span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-20579"></span>Run in server mode. This is like batch mode, except that a temporary rawfile is used and then written to the standard output, preceded by a line with a single &quot;@&quot;, after the simulation is done. This mode is used by the ngspice daemon. This option is valid for ngspice only.
</div>
<div class="plain_layout">
<span id="magicparlabel-20580"></span>Example for using pipes from the console window:
</div>
<div class="plain_layout">
<span id="magicparlabel-20581"></span><span style="font-family:monospace;">cat adder.cir|ngspice -s|more</span>
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-20584"></span><span style="font-family:monospace;">-i</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-20587"></span><span style="font-family:monospace;">--interactive </span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-20590"></span>Run in interactive mode. This is useful if the standard input is not a terminal but interactive mode is desired. Command completion is not available unless the standard input is a terminal, however. This option is valid for ngspice only.
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-20593"></span><span style="font-family:monospace;">-r FILE</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-20596"></span><span style="font-family:monospace;">--rawfile=FILE</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-20599"></span>Use rawfile as the default file into which the results of the simulation are saved. This option is valid for ngspice only.
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-20602"></span><span style="font-family:monospace;">-p</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-20605"></span><span style="font-family:monospace;">--pipe </span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-20608"></span>Allow a program (e.g., xcircuit) to act as a GUI frontend for ngspice through a pipe. Thus ngspice will assume that the input pipe is a tty and allows to run in interactive mode.
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-20611"></span><span style="font-family:monospace;">-o FILE</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-20614"></span><span style="font-family:monospace;">--output=FILE</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-20617"></span>All logs generated during a batch run (-b) will be saved in outfile.
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-20620"></span><span style="font-family:monospace;">-h</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-20623"></span><span style="font-family:monospace;">--help </span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-20626"></span>A short help statement of the command line syntax.
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-20629"></span><span style="font-family:monospace;">-v</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-20632"></span><span style="font-family:monospace;">--version </span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-20635"></span>Prints a version information.
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-20638"></span><span style="font-family:monospace;">-a</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-20641"></span><span style="font-family:monospace;">--autorun</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-20644"></span>Start simulation immediately, as if a control section
</div>
<div class="plain_layout">
<span id="magicparlabel-20645"></span><span style="font-family:monospace;">.control</span>
</div>
<div class="plain_layout">
<span id="magicparlabel-20646"></span><span style="font-family:monospace;">run</span>
</div>
<div class="plain_layout">
<span id="magicparlabel-20647"></span><span style="font-family:monospace;">.endc</span>
</div>
<div class="plain_layout">
<span id="magicparlabel-20648"></span>had been added to the input file.
</div></td>
</tr>
<tr class="even">
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-20654"></span><span style="font-family:monospace;">--soa-log=FILE</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-20657"></span>output from Safe Operating Area (SOA) check
</div></td>
</tr>
</tbody>
</table>

Further arguments to ngspice are taken to be ngspice input files, which
are read and saved (if running in batch mode then they are run
immediately). Ngspice accepts Spice3 (and also most Spice2) input files,
and outputs ASCII plots, Fourier analyses, and node printouts as
specified in .plot, .four, and .print cards. If an out parameter is
given on a .width card ([15.6.7](#subsec__width)), the effect is the
same as set width = .... Since ngspice ASCII plots do not use multiple
ranges, however, if vectors together on a .plot card have different
ranges they do not provide as much information as they do in a scalable
graphics plot.

For ngnutmeg, further arguments are taken to be data files in binary or
ASCII raw file format (generated with -r in batch mode or the **write**
(see [17.5.93](#subsec_Write__Write_data)) command) that are loaded into
ngnutmeg. If the file is in binary format, it may be only partially
completed (useful for examining output before the simulation is
finished). One file may contain any number of data sets from different
analyses.

