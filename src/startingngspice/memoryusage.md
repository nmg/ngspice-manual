# Memory usage

Ngspice started with batch option (-b) and rawfile output (-r rawfile)
will store all simulation data immediately into the rawfile without
keeping them in memory. Thus very large circuits may be simulated, the
memory requested upon ngspice start up will depend on the circuit size,
but will not increase during simulation.

If you start ngspice in interactive mode or interactively with control
section, all data will be kept in memory, to be available for later
evaluation. A large circuit may outgrow even Gigabytes of memory. The
same may happen after a very long simulation run with many vectors and
many time steps to be stored. Issuing the save \<nodes\> command will
help to reduce memory requirements by saving only the data defined by
the command. You may alos choose option INTERP
([15.1.4](#subsec_Transient_Analysis_Options)) to reduce memory usage.

