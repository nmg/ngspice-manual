# PSPICE Compatibility mode

If the variable ([17.7](#sec_Variables)) ngbehavior is set to 'ps' or
'psa' with the commands

set ngbehavior=ps

or

set ngbehavior=psa

in spinit or .spiceinit, ngspice will translate all files that have been
read into ngspice netlist by the .include command (ps) or the complete
netlist (psa) from PSPICE syntax to ngspice. This feature allows reading
of PSPICE (or TINA) compatible device libraries (ps) that are often
supplied by the semiconductor device manufacturers. Or you may choose to
use complete PSPICE simulation decks (psa). Some ngspice input files may
fail, however. For example ngspice\\examples\\memristor\\memristor.sp
will not do, because it uses the parameter vt, and vt is a reserved word
in PSPICE.

PSPICE to ngspice translation details:

  - .model replacement in ako (a kind of) model descriptions
  - replace the E source TABLE function by a B source pwl
  - add predefined params TEMP, VT, GMIN to beginning of deck
  - add predefined params TEMP, VT to beginning of each .subckt call
  - add .functions limit, pwr, pwrs, stp, if, int
  - replace  
    S1 D S DG GND SWN  
    .MODEL SWN VSWITCH(VON=0.55 VOFF=0.49  
    \+ RON={1/(2\*M\*(W/LE)\*(KPN/2)\*10)} ROFF=1G)  
    by  
    as1 %vd(DG GND) % gd(D S) aswn  
    .model aswn aswitch(cntl\_off=0.49 cntl\_on=0.55  
    \+ r\_off=1G r\_on={1/(2\*M\*(W/LE)\*(KPN/2)\*10)} log=TRUE)
  - replace & by &&
  - replace | by ||
  - replace T\_ABS by temp and T\_REL\_GLOBAL by dtemp
  - get the area factor for diodes and bipolar devices  
    d1 n1 n2 dmod 7 –\> d1 n1 n2 dmod area=7  
    q2 n1 n2 n3 \[n4\] bjtmod 1.35 –\> q2 n1 n2 n3 n4 bjtmod area=1.35  
    q3 1 2 3 4 bjtmod 1.45 –\> q2 1 2 3 4 bjtmod area=1.45

In ps or psa mode ngspice will treat all .lib entries like .include.
There is no hierarchically library handling. So for reading HSPICE
compatible libraries, you definitely have to unset the ps mode, e.g. by
not adding set ngbehavior=ps or disabling it by

unset ngbehavior=ps

