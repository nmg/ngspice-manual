# Missing functions

You may add one or more function definitions to your input file, as
listed below.

.func LIMIT(x,a,b) {min(max(x, a), b)}

.func PWR(x,a) {abs(x) \*\* a}

.func PWRS(x,a) {sgn(x) \* PWR(x,a)}

.func stp(x) {u(x)}

