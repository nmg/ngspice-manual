# LTSPICE Compatibility mode

If the variable ([17.7](#sec_Variables)) ngbehavior is set to 'lt' or
'lta' with the commands

set ngbehavior=lt

or

set ngbehavior=lta

in spinit or .spiceinit, ngspice will translate all files that have been
read into ngspice netlist by the .include command (lt) or the complete
netlist (lta) from LTSPICE syntax to ngspice. This feature allows
reading of LTSPICE compatible device libraries or complete netlists.

Currently we offer only a subset of the documented or undocumented
functions (uplim, dnlim, uplim\_tanh, dnlim\_tanh). More user input is
definitely required here\!

This compatibility mode also adds a simple diode using the sidiode code
model (chapt. [12.2.29](#subsec_Simple_Diode_Model)). The diode model

d1 a k ds1

.model ds1 d(Roff=1000 Ron=0.7 Rrev=0.2 Vfwd=1

\+ Vrev=10 Revepsilon=0.2 Epsilon=0.2 Ilimit=7 Revilimit=15)

is translated automatically to the equivalent code model diode

ad1 a k ads1

.model ads1 sidiode(Roff=1000 Ron=0.7 Rrev=0.2 Vfwd=1

\+ Vrev=10 Revepsilon=0.2 Epsilon=0.2 Ilimit=7 Revilimit=15)

