# LTSPICE/PSPICE Compatibility mode

If the variable ([17.7](#sec_Variables)) ngbehavior is set to 'ltps' or
'ltpsa' with the commands

set ngbehavior=ltps

or

set ngbehavior=ltpsa

in spinit or .spiceinit, ngspice will translate all files that have been
read into ngspice netlist by the .include command (ltps) or the complete
netlist (ltpsa) [16.13.6](#subsec_Compatibility_mode_1_1),
[16.13.5](#subsec_Compatibility_mode_1) from LTSPICE and PSPICE syntax
to ngspice. This feature allows reading of LTSPICE and PSPICE compatible
device libraries or complete netlists.

