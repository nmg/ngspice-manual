# VSwitch

The VSwitch

S1 2 3 11 0 SW

.MODEL SW VSWITCH(VON=5V VOFF=0V RON=0.1 ROFF=100K)

may become

a1 %v(11) %gd(2 3) sw

.MODEL SW aswitch(cntl\_off=0.0 cntl\_on=5.0 r\_off=1e5

\+ r\_on=0.1 log=TRUE)

The XSPICE option has to be enabled.

