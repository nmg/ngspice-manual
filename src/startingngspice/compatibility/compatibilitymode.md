# Compatibility mode

The variable ([17.7](#sec_Variables)) ngbehavior sets the compatibility
mode. 'all' is set as the default value. 'spice3' as invoked by the
command

set ngbehavior=spice3

in spinit or .spiceinit will disable some of the advanced ngspice
features. 'ps' will enable including a library by a simple .lib
\<lib\_filename\> statement that is not compatible to the more
comfortable library handling described in Chapt. [2.7](#sec__LIB).

