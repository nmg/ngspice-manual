# .lib

The ngspice .lib command (see [2.7](#sec__LIB)) requires two parameters,
a file name followed by a library name. If no library name is given, the
line

.lib filename

should be replaced by

.inc filename

Alternatively, the compatibility mode
([16.13.1](#subsec_Compatibility_mode)) may be set to 'ps'.

