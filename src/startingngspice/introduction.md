# Introduction

Ngspice consists of the simulator and a front-end for data analysis and
plotting. Input to the simulator is a netlist file, including commands
for circuit analysis and output control. Interactive ngspice can plot
data from a simulation on a PC or a workstation display.

Ngspice on Linux (and OSs like Cygwin, BCD, Solaris ...) uses the X
Window System for plotting (see Chapt. [18.3](#sec_LINUX)) if the
environment variable DISPLAY is available. Otherwise, a console mode
(non-graphical) interface is used. If you are using X on a workstation,
the DISPLAY variable should already be set; if you want to display
graphics on a system different from the one you are running ngspice or
ngutmeg on, DISPLAY should be of the form machine:0.0. See the
appropriate documentation on the X Window System for more details.

The MS Windows versions of ngspice and ngnutmeg will have a native
graphics interface (see Chapt. [18.1](#sec_MS_Windows)).

The front-end may be run as a separate \`stand-alone' program under the
name ngnutmeg. ngnutmeg is a subset of ngspice dedicated to data
evaluation, still made available for historical reasons. Ngnutmeg will
read in the \`raw' data output file created by ngspice -r or by the
write command during an interactive ngspice session.

