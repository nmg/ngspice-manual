# Simulation time

Simulating large circuits may take an considerable amount of CPU time.
If this is of importance, you should compile ngspice with the flags for
optimum speed, set during configuring ngspice compilation. Under Linux,
MINGW, and CYGWIN you should select the following option to disable the
debug mode, which slows down ngspice:

./configure --disable-debug

Adding --disable-debug will set the -O2 optimization flag for compiling
and linking.

Under MS Visual Studio, you will have to select the **release** version,
which includes optimization for speed.

If you have selected XSPICE (see Chapt. [12](#cha_Behavioral_Modeling)
and [II](#par_XSPICE_Software_User_s)) as part of your compilation
configuration (by adding the option --enable-xspice to your ./configure
command), the value of **trtol** (see [15.1.4](#des_TRTOL)) is set
internally to 1 (instead of default 7) for higher precision if XSPICE
code model 'A' devices included in the circuit. This may double or even
triple the CPU time needed for any transient simulation, because the
amount of time steps and thus iteration steps is more than doubled. For
MS Visual Studio compilation there is currently no simple way to exclude
XSPICE during compilation.

You may enforce higher speed during XSPICE usage by setting the variable
xtrtol in your .spiceinit initialization file or in the .control section
in front of the tran command (via set xtrtol=2 using the set command
[17.5.63](#subsec_Set__Set_the)) and override the above trtol reduction.
Beware however of precision or convergence issues if you use XSPICE 'A'
devices, especially if xtrtol is set to values larger than 2.

If your circuit comprises mostly of MOS transistors, and you have a
multi-core processor at hand, you may benefit from OpenMP parallel
processing, as described next ([16.10](#sec_Ngspice_on_multi_core)).

