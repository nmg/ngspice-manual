# Some results

Some results on an inverter chain with 627 CMOS inverters, running for
200ns, compiled with Visual Studio professional 2008 on Windows 7 (full
optimization) or gcc 4.4, SUSE Linux 11.2, -O2, on a i7 860 machine with
four real cores (and 4 virtuals using hyperthreading) are shown in table
[16.1](#tab_OpenMP_performance).

Table 16.1:  OpenMP performance

<table>
<colgroup>
<col width="33%" />
<col width="33%" />
<col width="33%" />
</colgroup>
<tbody>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-20884"></span><span style="font-family:monospace;">Threads </span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-20887"></span><span style="font-family:monospace;">CPU time [s]</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-20890"></span><span style="font-family:monospace;">CPU time [s]</span>
</div></td>
</tr>
<tr class="even">
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-20896"></span><span style="font-family:monospace;">Windows</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-20899"></span><span style="font-family:monospace;">Linux</span>
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-20902"></span><span style="font-family:monospace;">1 (standard)</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-20905"></span><span style="font-family:monospace;">167</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-20908"></span><span style="font-family:monospace;">165</span>
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-20911"></span><span style="font-family:monospace;">1 (OpenMP)</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-20914"></span><span style="font-family:monospace;">174</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-20917"></span><span style="font-family:monospace;">167</span>
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-20920"></span><span style="font-family:monospace;">2</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-20923"></span><span style="font-family:monospace;">110</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-20926"></span><span style="font-family:monospace;">110</span>
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-20929"></span><span style="font-family:monospace;">3</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-20932"></span><span style="font-family:monospace;">95</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-20935"></span><span style="font-family:monospace;">94-120</span>
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-20938"></span><span style="font-family:monospace;">4</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-20941"></span><span style="font-family:monospace;">83</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-20944"></span><span style="font-family:monospace;">107</span>
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-20947"></span><span style="font-family:monospace;">6</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-20950"></span><span style="font-family:monospace;">94</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-20953"></span><span style="font-family:monospace;">90</span>
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-20956"></span><span style="font-family:monospace;">8</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-20959"></span><span style="font-family:monospace;">93</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-20962"></span><span style="font-family:monospace;">91</span>
</div></td>
</tr>
</tbody>
</table>

So we see a ngspice speed up of nearly a factor of two\! Even on an
older notebook with a dual core processor, more than 1.5x improvement
using two threads was attained. Similar results are to be expected from
BSIM4.

