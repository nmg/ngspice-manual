# Literature

\[1\] R.K. Perng, T.-H. Weng, and K.-C. Li: "On Performance Enhancement
of Circuit Simulation Using Multithreaded Techniques", IEEE
International Conference on Computational Science and Engineering, 2009,
pp. 158-165

