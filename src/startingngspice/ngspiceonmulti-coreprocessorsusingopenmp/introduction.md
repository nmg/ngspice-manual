# Introduction

Today's computers typically come with CPUs having more than one core. It
will thus be useful to enhance ngspice to make use of such multi-core
processors.

Using circuits comprising mostly of transistors and e.g. the BSIM3
model, around 2/3 of the CPU time is spent in evaluating the model
equations (e.g. in the BSIM3Load() function). The same happens with
other advanced transistor models. Thus this function should be
paralleled, if possible. Resulting from that the parallel processing has
to be within a dedicated device model. Solving the matrix takes about
10% to 50% of the CPU time, so paralleling the matrix solver is
sometimes of secondary interest only\! And it is difficult to achive
with our Sparse Matrix und KLU solvers.

Another alternative is using CUSPICE, that is ngspice (current version
27) designed for running massively parallel on NVIDIA GPUs.
[CUDA](https://developer.nvidia.com/cuda-toolkit) enhancements to C code
are applied. For LINUX, please see the [user
guide](http://ngspice.sourceforge.net/cuspice/CUSPICE_User_Guide.pdf).
For MS Windows, an executable is available at the [ngspice download
pages](http://ngspice.sourceforge.net/download.html#exp1).

