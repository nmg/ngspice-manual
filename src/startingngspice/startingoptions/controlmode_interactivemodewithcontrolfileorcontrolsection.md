# Control mode \(Interactive mode with control file or control section\)

If you add the following control section to your input file
adder-mos.cir, you may call

ngspice adder-mos.cir

from the command line and see ngspice starting, simulating and then
plotting immediately.

Control section:

``` listings
*  ADDER - 4 BIT ALL-NAND-GATE BINARY ADDER 
.control 
unset askquit 
save vcc#branch  
run 
plot vcc#branch  
rusage all 
.endc
```

Any suitable command listed in Chapt. [17.5](#sec_Commands) may be added
to the control section, as well as control structures described in
Chapt. [17.6](#sec_Control_Structures). Batch-like behavior may be
obtained by changing the control section to

Control section with batch-like behavior:

``` listings
*  ADDER - 4 BIT ALL-NAND-GATE BINARY ADDER 
.control 
unset askquit 
save vcc#branch  
run 
write adder.raw vcc#branch  
quit 
.endc
```

If you put this control section into a file, say adder-start.sp, you may
just add the line

.include adder-start.sp 

to your input file adder-mos.cir to obtain the batch-like behavior. In
the following example the line .tran ... from the input file is
overridden by the **tran** command given in the control section.

Control section overriding the .tran command:

``` listings
*  ADDER - 4 BIT ALL-NAND-GATE BINARY ADDER 
.control 
unset askquit 
save vcc#branch  
tran 1n 500n 
plot vcc#branch  
rusage all 
.endc
```

The commands within the .control section are executed in the order they
are listed and only **after** the circuit has been read in and parsed.
If you want to have a command being executed **before** circuit parsing,
you may use the prefix pre\_
([17.5.49](#subsec_Pre__command__execute_commands)) to the command.

A warning is due however: If your circuit file contains such a control
section (.control ... .endc), you should *not* start ngspice in batch
mode (with -b as parameter). The outcome may be unpredictable\!

