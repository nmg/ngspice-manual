# Batch mode

Let's take as an example the Four-Bit binary adder MOS circuit shown in
Chapt. [21.6](#sec_MOS_Four_Bit), stored in a file adder-mos.cir. You
may start the simulation immediately by calling

ngspice -b -r adder.raw -o adder.log adder-mos.cir

ngspice will start, simulate according to the .tran command and store
the output data in a rawfile adder.raw. Comments, warnings and infos go
to log file adder.log. Commands for batch mode operation are described
in Chapt. [15](#chap_Analyses_and_Output).

