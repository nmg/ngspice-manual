# Interactive mode

If you call

ngspice 

ngspice will start, load spinit
([16.5](#sec_Standard_configuration_file)) and .spiceinit
([16.6](#sec_User_defined_configuration), if available), and then waits
for your manual input. Any of the commands described in
[17.5](#sec_Commands) may be chosen, but many of them are useful only
after a circuit has been loaded by

ngspice 1 -\> source adder-mos.cir

others require the simulation being done already (e.g. plot):

ngspice 2 -\>run  
ngspice 3 -\>plot allv

If you call ngspice from the command line with a circuit file as
parameter:

ngspice adder-mos.cir

ngspice will start, load the circuit file, parse the circuit (same
circuit file as above, containing only dot commands (see Chapt.
[15](#chap_Analyses_and_Output)) for analysis and output control).
ngspice then just waits for your input. You may start the simulation by
issuing the run command. Following completion of the simulation you may
analyze the data by any of the commands given in Chapt.
[17.5](#sec_Commands).

