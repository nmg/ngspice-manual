# Tests

The ngspice distribution is accompanied by a suite of test input and
output files, located in the directory ngspice/tests. Originally this
suite was meant to see if ngspice with all models was made and installed
properly. It is started by

$ make check

from within your compilation and development shell. A sequence of
simulations is thus started, its outputs compared to given output files
by comparisons string by string. This feature is momentarily used only
to check for the BSIM3 model ([11.2.10](#subsec_BSIM3_model)) and the
XSPICE extension ([12](#cha_Behavioral_Modeling)). Several other input
files located in directory ngspice/tests may serve as light-weight
examples for invoking devices and simple circuits.

Today's very complex device models (BSIM4 (see
[11.2.11](#subsec_BSIM4_model)), HiSIM (see
[11.2.15](#subsec_HiSIM_models_of)) and others) require a different
strategy for verification. Under development for ngspice is the CMC
Regression test by Colin McAndrew, which accompanies every new model.
These tests cover a large range of different DC, AC and noise
simulations with different geometry ranges and operating conditions and
are more meaningful the transient simulations with their step size
dependencies. A major advantage is the scalability of the diff
comparisons, which check for equality within a given tolerance. A set of
Perl modules cares for input, output and comparisons of the models.
Currently BSIM3, BSIM4, BSIMSOI4, HiSIM, and HiSIM\_HV models implement
the new test. You may invoke it by running the command given above or by

$ make -i check 2\>&1 | tee results

\-i will make make to ignore any errors, tee will provide console output
as well as printing to file 'results'. Be aware that under MS Windows
you will need the console binary (see
[32.2.5](#subsec_ngspice_mingw_or)) to run the CMC tests, and you have
to have Perl installed\!

