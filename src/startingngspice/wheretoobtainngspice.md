# Where to obtain ngspice

The actual distribution of ngspice may be downloaded from the [ngspice
download web page](http://sourceforge.net/projects/ngspice/files/). The
installation for Linux or MS Windows is described in the file
**INSTALL** to be found in the top level directory. You may also have a
look at Chapt. [32](#cha_Compilation_notes) of this manual for compiling
instructions.

If you want to check out the source code that is actually under
development, you may have a look at the ngspice source code repository,
which is stored using the Git Source Code Management (SCM) tool. The Git
repository may be browsed on the [Git web
page](http://sourceforge.net/scm/?type=git&group_id=38962), also useful
for downloading individual files. You may however download (or clone)
the complete repository including all source code trees from the console
window (Linux, CYGWIN or MSYS/MINGW) by issuing the command (in a single
line)

git clone git://git.code.sf.net/p/ngspice/ngspice

You need to have Git installed, which is available for all three OSs.
The whole source tree is then available in \<current
directory\>/ngspice. Compilation and local installation is again
described in **INSTALL** (or Chapt. [32](#cha_Compilation_notes)). If
you later want to update your files and download the recent changes from
sourceforge into your local repository, cd into the ngspice directory
and just type

git pull

git pull will deny to overwrite modified files in your working
directory. To drop your local changes first, you can run

git reset --hard

To learn more about git, which can be both powerful and difficult to
master, please consult <http://git-scm.com/>, especially:
<http://git-scm.com/documentation>, which has pointers to documentation
and tutorials.

