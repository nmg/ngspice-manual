# Compatibility

ngspice is a direct derivative of spice3f5 from UC Berkeley and thus
inherits all of the commands available in its predecessor. Thanks to the
open source policy of UCB (original spice3 from 1994 is still available
[here](http://embedded.eecs.berkeley.edu/pubs/downloads/spice/index.htm)),
several commercial variants have sprung off, either being more dedicated
to IC design or more concentrating on simulating discrete and board
level electronics. None of the commercial and almost none of the freely
downloadable SPICE providers publishes the source code. All of them have
proceeded with the development, by adding functionality, or by adding a
more dedicated user interface. Some have kept the original SPICE syntax
for their netlist description, others have quickly changed some if not
many of the commands, functions and procedures. Thus it is difficult, if
not impossible, to offer a simulator that acknowledges all of these
netlist dialects. ngspice includes some features that enhance
compatibility that are included automatically. This selection may be
controlled to some extend by setting the compatibility mode. Others may
be invoked by the user by small additions to the netlist input file.
Some of them are listed in this chapter, some will be integrated into
ngspice at a later stage, others will be added if they are reported by
users.

