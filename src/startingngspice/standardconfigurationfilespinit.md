# Standard configuration file spinit

At startup ngspice reads its configuration file spinit. spinit may be
found in a path relative to the location of the ngspice executable  
..\\share\\ngspice\\scripts. The path may be overridden by setting the
environmental variable SPICE\_SCRIPTS to a path where spinit is located.
Ngspice for Windows will additionally search for spinit in the directory
where ngspice.exe resides. If spinit is not found a warning message is
issued, but ngspice continues.

Standard spinit contents:

``` listings
* Standard ngspice init file
alias exit quit
alias acct rusage all
** set the number of threads in openmp 
** default (if compiled with --enable-openmp) is: 2 
set num_threads=4

if $?sharedmode
  unset interactive
  unset moremode
else
  set interactive
  set x11lineararcs
end

strcmp __flag $program "ngspice"
if $__flag = 0

 codemodel ../lib/spice/spice2poly.cm
 codemodel ../lib/spice/analog.cm
 codemodel ../lib/spice/digital.cm
 codemodel ../lib/spice/xtradev.cm
 codemodel ../lib/spice/xtraevt.cm
 codemodel ../lib/spice/table.cm

end
unset __flag
```

spinit contains a script, made of commands from Chapt.
[17.5](#sec_Commands), that is run upon start up of ngspice. Aliases
(name equivalences) can be set. The asterisk \`\*' comments out a line.
If used by ngspice, spinit will then load the XSPICE code models from a
path relative to the current directory where the ngspice executable
resides. You may also define absolute paths.

If the standard path for the libraries (see standard spinit above or
/usr/local/lib/spice under CYGWIN and Linux) is not adequate, you can
add the ./configure options --prefix=/usr --libdir=/usr/lib64 to set the
codemodel search path to /usr/lib64/spice. Besides the standard lib only
lib64 is acknowledged.

Special care has to be taken when using the ngspice shared library. If
you apply ngspice.dll under Windows OS, the standard is to use relative
paths for the code models as shown above. However, the path is relative
to the calling program, not to the dll. This is fine when ngspice.dll
and the calling program reside in the same directory. If ngspice.dll is
placed in a different directory, please check Chapt.
[32.2](#sec_Ngspice_Compilation_under).

The Linux shared library ... t.b.d.

