#  TCLspice

Spice historically comes as a simulation engine with a Command Line
Interface. The Spice engine can also be used with a Graphical User
Interface. Tclspice represents a third approach to interfacing ngspice
simulation functionality. Tclspice is nothing more than a new way of
compiling and using SPICE source code. Spice is no longer considered as
a standalone program but as a library invoked by a TCL interpreter. It
either permits direct simulation in a TCL shell (this is quite analogous
to the command line interface of ngspice), or it permits the elaboration
of more complex, more specific, or more user friendly simulation
programs, by writing TCL scripts.

