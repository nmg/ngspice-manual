# Linear Dependent Sources

Ngspice allows circuits to contain linear dependent sources
characterized by any of the four equations

<table>
<colgroup>
<col width="25%" />
<col width="25%" />
<col width="25%" />
<col width="25%" />
</colgroup>
<tbody>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-5141"></span><span class="math inline"><em>i</em> = <em>g</em><em>v</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-5144"></span><span class="math inline"><em>v</em> = <em>e</em><em>v</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-5147"></span><span class="math inline"><em>i</em> = <em>f</em><em>i</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-5150"></span><span class="math inline"><em>v</em> = <em>h</em><em>i</em></span>
</div></td>
</tr>
</tbody>
</table>

where \(g\), \(e\), \(f\), and \(h\) are constants representing
transconductance, voltage gain, current gain, and transresistance,
respectively. Non-linear dependent sources for voltages or currents (B,
E, G) are described in Chapt. [5](#sec_Non_linear_Dependent_Sources).

