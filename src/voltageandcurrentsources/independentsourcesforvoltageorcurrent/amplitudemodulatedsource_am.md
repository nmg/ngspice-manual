# Amplitude modulated source \(AM\)

General Form (the PHASE parameter is only possible when XSPICE is
enabled):

``` listings
AM(VA VO MF FC TD PHASES)
```

Examples:

``` listings
V1 12 0 AM(0.5 1 20K 5MEG 1m)
```

<table>
<colgroup>
<col width="25%" />
<col width="25%" />
<col width="25%" />
<col width="25%" />
</colgroup>
<tbody>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-4656"></span>Name
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4659"></span>Parameter
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4662"></span>Default value
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4665"></span>Units
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-4668"></span>VA
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4671"></span>Amplitude
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4674"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4677"></span><span class="math inline"><em>V</em></span>, <span class="math inline"><em>A</em></span>
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-4680"></span>VO
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4683"></span>Offset
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4686"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4689"></span><span class="math inline"><em>V</em></span>, <span class="math inline"><em>A</em></span>
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-4692"></span>MF
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4695"></span>Modulating frequency
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4698"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4701"></span><span class="math inline"><em>H</em><em>z</em></span>
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-4704"></span>FC
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4707"></span>Carrier frequency
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4710"></span><span class="math inline">$\frac{1}{TSTOP}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4713"></span><span class="math inline"><em>H</em><em>z</em></span>
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-4716"></span>TD
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4719"></span>Signal delay
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4722"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4725"></span><span class="math inline"><em>s</em></span>
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-4728"></span>PHASES
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4731"></span>Phase
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4734"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4737"></span>degrees
</div></td>
</tr>
</tbody>
</table>

The shape of the waveform is described by the following equation:

\[\begin{array}{ll}
{V\left( t \right) = V_{A}\left( {VO + \sin\left( {2\pi \cdot MF \cdot t} \right) + PHASES} \right)\sin\left( {2\pi \cdot FC \cdot t + PHASES} \right)} & \\
\end{array}\]

