# Transient noise source

General Form:

``` listings
TRNOISE(NA NT NALPHA NAMP RTSAM RTSCAPT RTSEMT)
```

Examples:

``` listings
VNoiw 1 0 DC 0 TRNOISE(20n 0.5n 0 0)      $ white 
VNoi1of 1 0 DC 0 TRNOISE(0 10p 1.1 12p)   $ 1/f
VNoiw1of 1 0 DC 0 TRNOISE(20 10p 1.1 12p) $ white and 1/f
IALL 10 0 DC 0 trnoise(1m 1u 1.0 0.1m 15m 22u 50u)
                                          $ white, 1/f, RTS
```

Transient noise is an experimental feature allowing (low frequency)
transient noise injection and analysis. See Chapt.
[15.3.10](#subsec_Transient_noise_analysis) for a detailed description.
NA is the Gaussian noise rms voltage amplitude, NT is the time between
sample values (breakpoints will be enforced on multiples of this value).
NALPHA (exponent to the frequency dependency), NAMP (rms voltage or
current amplitude) are the parameters for 1/f noise, RTSAM the random
telegraph signal amplitude, RTSCAPT the mean of the exponential
distribution of the trap capture time, and RTSEMT its emission time
mean. White Gaussian, 1/f, and RTS noise may be combined into a single
statement.

<table>
<colgroup>
<col width="25%" />
<col width="25%" />
<col width="25%" />
<col width="25%" />
</colgroup>
<tbody>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-4805"></span>Name
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4808"></span>Parameter
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4811"></span>Default value
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4814"></span>Units
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-4817"></span>NA
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4820"></span>Rms noise amplitude (Gaussian)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4823"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4826"></span><span class="math inline"><em>V</em></span>, <span class="math inline"><em>A</em></span>
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-4829"></span>NT
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4832"></span>Time step
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4835"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4838"></span><span class="math inline"><em>s</em><em>e</em><em>c</em></span>
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-4841"></span>NALPHA
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4844"></span>1/f exponent
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4847"></span><span class="math inline">0 &lt; <em>α</em> &lt; 2</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4850"></span>-
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-4853"></span>NAMP
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4856"></span>Amplitude (1/f)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4859"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4862"></span><span class="math inline"><em>V</em></span>, <span class="math inline"><em>A</em></span>
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-4865"></span>RTSAM
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4868"></span>Amplitude
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4871"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4874"></span><span class="math inline"><em>V</em></span>, <span class="math inline"><em>A</em></span>
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-4877"></span>RTSCAPT
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4880"></span>Trap capture time
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4883"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4886"></span><span class="math inline"><em>s</em><em>e</em><em>c</em></span>
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-4889"></span>RTSEMT
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4892"></span>Trap emission time
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4895"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4898"></span><span class="math inline"><em>s</em><em>e</em><em>c</em></span>
</div></td>
</tr>
</tbody>
</table>

If you set NT and RTSAM to 0, the noise option TRNOISE ... is ignored.
Thus you may switch off the noise contribution of an individual voltage
source VNOI by the command

alter @vnoi\[trnoise\] = \[ 0 0 0 0 \] $ no noise

alter @vrts\[trnoise\] = \[ 0 0 0 0 0 0 0\] $ no noise

See Chapt. [17.5.3](#subsec_Alter___Change_a) for the alter command.

You may switch off all TRNOISE noise sources by setting

set notrnoise

to your .spiceinit file (for all your simulations) or into your control
section in front of the next run or tran command (for this specific and
all following simulations). The command

unset notrnoise

will reinstate all noise sources.

The noise generators are implemented into the independent **voltage**
(vsrc) and **current** (isrc) sources.

