# Piece-Wise Linear

General Form:

``` listings
PWL(T1 V1 <T2 V2 T3 V3 T4 V4 ...>) <r=value> <td=value>
```

Examples:

``` listings
VCLOCK 7 5 PWL(0 -7 10NS -7 11NS -3 17NS -3 18NS -7 50NS -7)
+ r=0 td=15NS
```

Each pair of values (\(T_{i}\), \(V_{i}\)) specifies that the value of
the source is \(V_{i}\) (in Volts or Amps) at time = \(T_{i}\). The
value of the source at intermediate values of time is determined by
using linear interpolation on the input values. The parameter *r*
determines a repeat time point. If *r* is not given, the whole sequence
of values (\(T_{i}\), \(V_{i}\)) is issued once, then the output stays
at its final value. If *r = 0*, the whole sequence from time 0 to time
*Tn* is repeated forever. If *r = 10ns*, the sequence between 10ns and
50ns is repeated forever. the *r* value has to be one of the time points
T1 to Tn of the PWL sequence. If *td* is given, the whole PWL sequence
is delayed by the value of *td*. Please note that for now *r* and *td*
are available only with the voltage source, not with the current source.

