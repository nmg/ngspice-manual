# Sinusoidal

General form (the PHASE parameter is only possible when XSPICE is
enabled):

``` listings
SIN(VO VA FREQ TD THETA PHASE)
```

Examples:

``` listings
VIN 3 0 SIN(0 1 100MEG 1NS 1E10)
```

<table>
<colgroup>
<col width="25%" />
<col width="25%" />
<col width="25%" />
<col width="25%" />
</colgroup>
<tbody>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-4200"></span>Name
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4203"></span>Parameter
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4206"></span>Default Value
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4209"></span>Units
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-4212"></span>VO
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4215"></span>Offset
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4218"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4221"></span><span class="math inline"><em>V</em></span>, <span class="math inline"><em>A</em></span>
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-4224"></span>VA
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4227"></span>Amplitude
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4230"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4233"></span><span class="math inline"><em>V</em></span>, <span class="math inline"><em>A</em></span>
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-4236"></span>FREQ
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4239"></span>Frequency
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4242"></span><span class="math inline">$\frac{1}{TSTOP}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4245"></span><span class="math inline"><em>H</em><em>z</em></span>
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-4248"></span>TD
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4251"></span>Delay
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4254"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4257"></span>sec
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-4260"></span>THETA
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4263"></span>Damping factor
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4266"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4269"></span><span class="math inline">$\frac{1}{sec}$</span>
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-4272"></span>PHASE
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4275"></span>Phase
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4278"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4281"></span>degrees
</div></td>
</tr>
</tbody>
</table>

The shape of the waveform is described by the following formula:

\[\begin{array}{ll}
{V\left( t \right) = \begin{cases}
{V0} & {{if 0 \leq t} < TD} \\
{V0 + VA e^{- ({t - TD})THETA}\sin\left( {2\pi \cdot FREQ \cdot \left( {t - TD} \right) + PHASE} \right)} & {{if} TD \leq t < TSTOP.} \\
\end{cases}} & \\
\end{array}\]

