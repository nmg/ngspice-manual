# External voltage or current input

General Form:

``` listings
EXTERNAL
```

Examples:

``` listings
Vex 1  0 dc 0 external
Iex i1 i2 dc 0 external <m = xx>
```

Voltages or currents may be set from the calling process, if ngspice is
compiled as a shared library and loaded by the process. See Chapt.
[19.6.3](#subsec_Callback_functions_1) for an explanation.

