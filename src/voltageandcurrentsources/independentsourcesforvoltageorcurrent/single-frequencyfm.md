# Single-Frequency FM

General Form (the PHASE parameters are only possible when XSPICE is
enabled):

``` listings
SFFM(VO VA FC MDI FS PHASEC PHASES)
```

Examples:

``` listings
V1 12 0 SFFM(0 1M 20K 5 1K)
```

<table>
<colgroup>
<col width="25%" />
<col width="25%" />
<col width="25%" />
<col width="25%" />
</colgroup>
<tbody>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-4504"></span>Name
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4507"></span>Parameter
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4510"></span>Default value
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4513"></span>Units
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-4516"></span>VO
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4519"></span>Offset
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4522"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4525"></span><span class="math inline"><em>V</em></span>, <span class="math inline"><em>A</em></span>
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-4528"></span>VA
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4531"></span>Amplitude
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4534"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4537"></span><span class="math inline"><em>V</em></span>, <span class="math inline"><em>A</em></span>
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-4540"></span>FC
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4543"></span>Carrier frequency
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4546"></span><span class="math inline">$\frac{1}{TSTOP}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4549"></span><span class="math inline"><em>H</em><em>z</em></span>
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-4552"></span>MDI
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4555"></span>Modulation index
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4558"></span>-
</div></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-4564"></span>FS
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4567"></span>Signal frequency
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4570"></span><span class="math inline">$\frac{1}{TSTOP}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4573"></span><span class="math inline"><em>H</em><em>z</em></span>
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-4576"></span>PHASEC
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4579"></span>carrier phase
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4582"></span>0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4585"></span>degrees
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-4588"></span>PHASES
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4591"></span>signal phase
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4594"></span>0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4597"></span>degrees
</div></td>
</tr>
</tbody>
</table>

The shape of the waveform is described by the following equation:

\[\begin{array}{ll}
{V\left( t \right) = V_{O} + V_{A}\sin\left( {2\pi \cdot FC \cdot t + MDI\sin\left( {2\pi \cdot FS \cdot t + PHASES} \right) + PHASEC} \right)} & \\
\end{array}\]

