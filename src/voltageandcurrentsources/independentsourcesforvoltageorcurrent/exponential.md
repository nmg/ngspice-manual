# Exponential

General Form:

``` listings
EXP(V1 V2 TD1 TAU1 TD2 TAU2)
```

Examples:

``` listings
VIN 3 0 EXP(-4 -1 2NS 30NS 60NS 40NS)
```

<table>
<colgroup>
<col width="25%" />
<col width="25%" />
<col width="25%" />
<col width="25%" />
</colgroup>
<tbody>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-4340"></span>Name
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4343"></span>Parameter
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4346"></span>Default Value
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4349"></span>Units
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-4352"></span>V1
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4355"></span>Initial value
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4358"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4361"></span><span class="math inline"><em>V</em></span>, <span class="math inline"><em>A</em></span>
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-4364"></span>V2
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4367"></span>pulsed value
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4370"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4373"></span><span class="math inline"><em>V</em></span>, <span class="math inline"><em>A</em></span>
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-4376"></span>TD1
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4379"></span>rise delay time
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4382"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4385"></span>sec
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-4388"></span>TAU1
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4391"></span>rise time constant
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4394"></span>TSTEP
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4397"></span>sec
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-4400"></span>TD2
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4403"></span>fall delay time
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4406"></span>TD1+TSTEP
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4409"></span>sec
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-4412"></span>TAU2
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4415"></span>fall time constant
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4418"></span>TSTEP
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4421"></span>sec
</div></td>
</tr>
</tbody>
</table>

The shape of the waveform is described by the following formula:

Let \(V21 = V2 - V1, V12 = V1 - V2\):

\[\begin{array}{ll}
{V\left( t \right) = \begin{cases}
{V1} & {{if} 0 \leq t < TD1,} \\
{V1 + V21\left( {1 - e^{- \frac{({t - TD1})}{TAU1}}} \right)} & {{if} TD1 \leq t < TD2,} \\
{V1 + V21\left( {1 - e^{- \frac{({t - TD1})}{TAU1}}} \right) + V12\left( {1 - e^{- \frac{({t - TD2})}{TAU2}}} \right)} & {{if} TD2 \leq t < TSTOP.} \\
\end{cases}} & \\
\end{array}\]

