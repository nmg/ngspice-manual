# Arbitrary Phase Sources

The XSPICE option supports arbitrary phase independent sources that
output at TIME=0.0 a value corresponding to some specified phase shift.
Other versions of SPICE use the TD (delay time) parameter to set
phase-shifted sources to their time-zero value until the delay time has
elapsed. The XSPICE phase parameter is specified in degrees and is
included after the SPICE3 parameters normally used to specify an
independent source. Partial XSPICE deck examples of usage for pulse and
sine waveforms are shown below:

\* Phase shift is specified after Berkeley defined parameters

\* on the independent source cards. Phase shift for both of the

\* following is specified as +45 degrees

\*

v1 1 0 0.0 sin(0 1 1k 0 0 45.0)

r1 1 0 1k

\*

v2 2 0 0.0 pulse(-1 1 0 1e-5 1e-5 5e-4 1e-3 45.0)

r2 2 0 1k

\*

