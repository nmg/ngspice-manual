# Pulse

General form (the PHASE parameter is only possible when XSPICE is
enabled):

``` listings
PULSE(V1 V2 TD TR TF PW PER PHASE)
```

Examples:

``` listings
VIN 3 0 PULSE(-1 1 2NS 2NS 2NS 50NS 100NS)
```

<table>
<colgroup>
<col width="25%" />
<col width="25%" />
<col width="25%" />
<col width="25%" />
</colgroup>
<tbody>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-3973"></span>Name
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3976"></span>Parameter
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3979"></span>Default Value
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3982"></span>Units
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-3985"></span>V1
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3988"></span>Initial value
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3991"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3994"></span><span class="math inline"><em>V</em></span>, <span class="math inline"><em>A</em></span>
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-3997"></span>V2
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4000"></span>Pulsed value
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4003"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4006"></span><span class="math inline"><em>V</em></span>, <span class="math inline"><em>A</em></span>
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-4009"></span>TD
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4012"></span>Delay time
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4015"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4018"></span>sec
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-4021"></span>TR
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4024"></span>Rise time
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4027"></span>TSTEP
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4030"></span>sec
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-4033"></span>TF
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4036"></span>Fall time
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4039"></span>TSTEP
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4042"></span>sec
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-4045"></span>PW
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4048"></span>Pulse width
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4051"></span>TSTOP
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4054"></span>sec
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-4057"></span>PER
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4060"></span>Period
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4063"></span>TSTOP
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4066"></span>sec
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-4069"></span>PHASE
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4072"></span>Phase
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4075"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4078"></span>degrees
</div></td>
</tr>
</tbody>
</table>

A single pulse, without phase offset, is described by the following
table:

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-4103"></span>Time
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4106"></span>Value
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-4109"></span>0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4112"></span>V1
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-4115"></span>TD
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4118"></span>V1
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-4121"></span>TD+TR
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4124"></span>V2
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-4127"></span>TD+TR+PW
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4130"></span>V2
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-4133"></span>TD+TR+PW+TF
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4136"></span>V1
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-4139"></span>TSTOP
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4142"></span>V1
</div></td>
</tr>
</tbody>
</table>

Intermediate points are determined by linear interpolation.

