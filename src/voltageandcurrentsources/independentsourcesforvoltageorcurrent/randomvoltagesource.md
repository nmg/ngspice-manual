# Random voltage source

The TRRANDOM option yields statistically distributed voltage values,
derived from the ngspice random number generator. These values may be
used in the transient simulation directly within a circuit, e.g. for
generating a specific noise voltage, but especially they may be used in
the control of behavioral sources (B, E, G sources
[5](#sec_Non_linear_Dependent_Sources), voltage controllable A sources
[12](#cha_Behavioral_Modeling), capacitors
[3.2.8](#subsec_Capacitors__dependent_on), inductors
[3.2.12](#subsec_Inductors__dependent_on), or resistors
[3.2.4](#subsec_Resistors__dependent_on)) to simulate the circuit
dependence on statistically varying device parameters. A Monte-Carlo
simulation may thus be handled in a single simulation run.

General Form:

``` listings
TRRANDOM(TYPE TS <TD <PARAM1 <PARAM2>>>)
```

Examples:

``` listings
VR1 r1  0 dc 0 trrandom (2 10m 0 1) $ Gaussian
```

TYPE determines the random variates generated: 1 is uniformly
distributed, 2 Gaussian, 3 exponential, 4 Poisson. TS is the duration of
an individual voltage value. TD is a time delay with 0 V output before
the random voltage values start up. PARAM1 and PARAM2 depend on the type
selected.

<table>
<colgroup>
<col width="12%" />
<col width="12%" />
<col width="12%" />
<col width="12%" />
<col width="12%" />
<col width="12%" />
<col width="12%" />
<col width="12%" />
</colgroup>
<tbody>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-4979"></span>TYPE
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4982"></span>description
</div></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-4988"></span>PARAM1
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-4991"></span>default
</div></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-4997"></span>PARAM2
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-5000"></span>default
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-5003"></span>1
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-5006"></span>Uniform
</div></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-5012"></span>Range
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-5015"></span>1
</div></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-5021"></span>Offset
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-5024"></span>0
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-5027"></span>2
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-5030"></span>Gaussian
</div></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-5036"></span>Standard Dev.
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-5039"></span>1
</div></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-5045"></span>Mean
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-5048"></span>0
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-5051"></span>3
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-5054"></span>Exponential
</div></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-5060"></span>Mean
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-5063"></span>1
</div></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-5069"></span>Offset
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-5072"></span>0
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-5075"></span>4
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-5078"></span>Poisson
</div></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-5084"></span>Lambda
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-5087"></span>1
</div></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-5093"></span>Offset
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-5096"></span>0
</div></td>
</tr>
</tbody>
</table>

