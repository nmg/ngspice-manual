# Polynomial Source Compatibility

Dependent polynomial sources available in SPICE2G6 are fully supported
in ngspice using the XSPICE extension ([25.1](#sec_ngspice_with_the)).
The form used to specify these sources is shown in Table
[4.1](#cap_Dependent_Polynomial_Sources). For details on its usage
please see Chapt. [5.5](#subsec_POLY).

Dependent Polynomial Sources

Source Type

Instance Card

POLYNOMIAL VCVS

EXXXXXXX N+ N- POLY(ND) NC1+ NC1- P0 (P1...)

POLYNOMIAL VCCS

GXXXXXXX N+ N- POLY(ND) NC1+ NC1- P0 (P1...)

POLYNOMIAL CCCS

FXXXXXXX N+ N- POLY(ND) VNAM1 \!VNAM2...? P0 (P1...)

POLYNOMIAL CCVS

HXXXXXXX N+ N- POLY(ND) VNAM1 \!VNAM2...? P0 (P1...)

Table 4.1:  Dependent Polynomial Sources

