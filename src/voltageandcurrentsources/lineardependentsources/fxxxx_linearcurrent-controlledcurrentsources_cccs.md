# Fxxxx: Linear Current-Controlled Current Sources \(CCCS\)

General form:

``` listings
FXXXXXXX N+ N- VNAM VALUE <m=val>
```

Examples:

``` listings
F1 13 5 VSENS 5 m=2
```

**n+** and **n-** are the positive and negative nodes, respectively.
Current flow is from the positive node, through the source, to the
negative node. **vnam** is the name of a voltage source through which
the controlling current flows. The direction of positive controlling
current flow is from the positive node, through the source, to the
negative node of **vnam**. **value** is the current gain. m is an
optional multiplier to the output current.

