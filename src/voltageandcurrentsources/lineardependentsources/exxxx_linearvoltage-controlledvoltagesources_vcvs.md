# Exxxx: Linear Voltage-Controlled Voltage Sources \(VCVS\)

General form:

``` listings
EXXXXXXX N+ N- NC+ NC- VALUE
```

Examples:

``` listings
E1 2 3 14 1 2.0
```

**n+** is the positive node, and **n-** is the negative node. **nc+**
and **nc-** are the positive and negative controlling nodes,
respectively. **value** is the voltage gain.

