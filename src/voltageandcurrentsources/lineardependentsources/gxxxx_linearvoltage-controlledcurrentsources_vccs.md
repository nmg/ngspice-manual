# Gxxxx: Linear Voltage-Controlled Current Sources \(VCCS\)

General form:

``` listings
GXXXXXXX N+ N- NC+ NC- VALUE <m=val>
```

Examples:

``` listings
G1 2 0 5 0 0.1
```

**n+** and **n-** are the positive and negative nodes, respectively.
Current flow is from the positive node, through the source, to the
negative

node. **nc+** and **nc-** are the positive and negative controlling
nodes, respectively. **value** is the transconductance (in mhos). m is
an optional multiplier to the output current. val may be a numerical
value or an expression according to
[2.8.5](#subsec_Syntax_of_expressions) containing references to other
parameters.

