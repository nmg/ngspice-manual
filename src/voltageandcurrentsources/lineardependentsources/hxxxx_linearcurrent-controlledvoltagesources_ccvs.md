# Hxxxx: Linear Current-Controlled Voltage Sources \(CCVS\)

General form:

``` listings
HXXXXXXX n+ n- vnam value
```

Examples:

``` listings
HX 5 17 VZ 0.5K
```

**n+** and **n-** are the positive and negative nodes, respectively.
**vnam** is the name of a voltage source through which the controlling
current flows. The direction of positive controlling current flow is
from the positive node, through the source, to the negative node of
**vnam**. **value** is the transresistance (in ohms).

