# Independent Sources for Voltage or Current

General form:

``` listings
VXXXXXXX N+ N- <<DC> DC/TRAN VALUE> <AC <ACMAG <ACPHASE>>>
+ <DISTOF1 <F1MAG <F1PHASE>>> <DISTOF2 <F2MAG <F2PHASE>>>
IYYYYYYY N+ N- <<DC> DC/TRAN VALUE> <AC <ACMAG <ACPHASE>>>
+ <DISTOF1 <F1MAG <F1PHASE>>> <DISTOF2 <F2MAG <F2PHASE>>>
```

Examples:

``` listings
VCC 10 0 DC 6
VIN 13 2 0.001 AC 1 SIN(0 1 1MEG)
ISRC 23 21 AC 0.333 45.0 SFFM(0 1 10K 5 1K)
VMEAS 12 9
VCARRIER 1 0 DISTOF1 0.1 -90.0
VMODULATOR 2 0 DISTOF2 0.01
IIN1 1 5 AC 1 DISTOF1 DISTOF2 0.001
```

**n+** and **n-** are the positive and negative nodes, respectively.
Note that voltage sources need not be grounded. Positive current is
assumed to flow from the positive node, through the source, to the
negative node. A current source of positive value forces current to flow
out of the **n+** node, through the source, and into the **n-** node.
Voltage sources, in addition to being used for circuit excitation, are
the \`ammeters' for ngspice, that is, zero valued voltage sources may be
inserted into the circuit for the purpose of measuring current. They of
course have no effect on circuit operation since they represent
short-circuits.

**DC**/**TRAN** is the dc and transient analysis value of the source. If
the source value is zero both for dc and transient analyses, this value
may be omitted. If the source value is time-invariant (e.g., a power
supply), then the value may optionally be preceded by the letters DC.

**ACMAG** is the ac magnitude and **ACPHASE** is the ac phase. The
source is set to this value in the ac analysis. If **ACMAG** is omitted
following the keyword **AC**, a value of unity is assumed. If
**ACPHASE** is omitted, a value of zero is assumed. If the source is not
an ac small-signal input, the keyword **AC** and the ac values are
omitted.

**DISTOF1** and **DISTOF2** are the keywords that specify that the
independent source has distortion inputs at the frequencies **F1** and
**F2** respectively (see the description of the .DISTO control line).
The keywords may be followed by an optional magnitude and phase. The
default values of the magnitude and phase are 1.0 and 0.0 respectively.

Any independent source can be assigned a time-dependent value for
transient analysis. If a source is assigned a time-dependent value, the
time-zero value is used for dc analysis. There are nine independent
source functions:

  - pulse,
  - exponential,
  - sinusoidal,
  - piece-wise linear,
  - single-frequency FM
  - AM
  - transient noise
  - random voltages or currents
  - and external data (only with ngspice shared library).

If parameters other than source values are omitted or set to zero, the
default values shown are assumed. **TSTEP** is the printing increment
and **TSTOP** is the final time – see the .TRAN control line for an
explanation.

