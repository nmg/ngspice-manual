# Single Lossy Transmission Line \(TXL\)

General form:

``` listings
YXXXXXXX N1 0 N2 0 mname <LEN=LENGTH>
```

Example:

``` listings
Y1 1 0 2 0 ymod LEN=2
.MODEL ymod txl R=12.45 L=8.972e-9 G=0 C=0.468e-12 length=16 
```

**n1** and **n2** are the nodes of the two ports. The optional instance
parameter **len** is the length of the line and may be expressed in
multiples of \[\(unit\)\]. Typically \(unit\) is given in meters.
**len** will override the model parameter **length **for the specific
instance only.

The TXL model takes a number of parameters:

<table>
<colgroup>
<col width="20%" />
<col width="20%" />
<col width="20%" />
<col width="20%" />
<col width="20%" />
</colgroup>
<tbody>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-6500"></span>Name
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6503"></span>Parameter
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6506"></span>Units/Type
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6509"></span>Default
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6512"></span>Example
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-6515"></span>R
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6518"></span>resistance/length
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6521"></span><span class="math inline">$\frac{\Omega}{unit}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6524"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6527"></span>0.2
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-6530"></span>L
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6533"></span>inductance/length
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6536"></span><span class="math inline">$\frac{H}{unit}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6539"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6542"></span>9.13e-9
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-6545"></span>G
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6548"></span>conductance/length
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6551"></span><span class="math inline">$\frac{mhos}{unit}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6554"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6557"></span>0.0
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-6560"></span>C
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6563"></span>capacitance/length
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6566"></span><span class="math inline">$\frac{F}{unit}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6569"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6572"></span>3.65e-12
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-6575"></span>LENGTH
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6578"></span>length of line
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6581"></span><span class="math inline"><em>u</em><em>n</em><em>i</em><em>t</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6584"></span>no default
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6587"></span>1.0
</div></td>
</tr>
</tbody>
</table>

Model parameter **length** must be specified as a multiple of \(unit\).
Typically \(unit\) is given in \[m\]. For transient simulation only.

