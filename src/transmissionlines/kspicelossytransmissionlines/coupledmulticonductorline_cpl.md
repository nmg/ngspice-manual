# Coupled Multiconductor Line \(CPL\)

The CPL multiconductor line model is in theory similar to the RLGC
model, but without frequency dependent loss (neither skin effect nor
frequency-dependent dielectric loss). Up to 8 coupled lines are
supported in NGSPICE.

General form:

``` listings
PXXXXXXX NI1 NI2...NIX GND1 NO1 NO2...NOX GND2 mname <LEN=LENGTH>
```

Example:

``` listings
P1 in1 in2 0 b1 b2 0 PLINE  
.model PLINE CPL length={Len} 
+R=1 0 1 
+L={L11} {L12} {L22} 
+G=0 0 0 
+C={C11} {C12} {C22}
.param Len=1 Rs=0 
+ C11=9.143579E-11 C12=-9.78265E-12 C22=9.143578E-11 
+ L11=3.83572E-7 L12=8.26253E-8 L22=3.83572E-7 
```

**ni1** ... **nix** are the nodes at port 1 with gnd1; **no1** ...
**nox** are the nodes at port 2 with gnd2. The optional instance
parameter **len** is the length of the line and may be expressed in
multiples of \[\(unit\)\]. Typically \(unit\) is given in meters.
**len** will override the model parameter **length **for the specific
instance only.

The CPL model takes a number of parameters:

<table>
<colgroup>
<col width="20%" />
<col width="20%" />
<col width="20%" />
<col width="20%" />
<col width="20%" />
</colgroup>
<tbody>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-6659"></span>Name
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6662"></span>Parameter
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6665"></span>Units/Type
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6668"></span>Default
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6671"></span>Example
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-6674"></span>R
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6677"></span>resistance/length
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6680"></span><span class="math inline">$\frac{\Omega}{unit}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6683"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6686"></span>0.2
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-6689"></span>L
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6692"></span>inductance/length
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6695"></span><span class="math inline">$\frac{H}{unit}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6698"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6701"></span>9.13e-9
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-6704"></span>G
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6707"></span>conductance/length
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6710"></span><span class="math inline">$\frac{mhos}{unit}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6713"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6716"></span>0.0
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-6719"></span>C
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6722"></span>capacitance/length
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6725"></span><span class="math inline">$\frac{F}{unit}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6728"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6731"></span>3.65e-12
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-6734"></span>LENGTH
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6737"></span>length of line
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6740"></span><span class="math inline"><em>u</em><em>n</em><em>i</em><em>t</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6743"></span>no default
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6746"></span>1.0
</div></td>
</tr>
</tbody>
</table>

All RLGC parameters are given in Maxwell matrix form. For the R and G
matrices the diagonal elements must be specified, for L and C matrices
the lower or upper triangular elements must specified. The parameter
LENGTH is a scalar and is mandatory. For transient simulation only.

