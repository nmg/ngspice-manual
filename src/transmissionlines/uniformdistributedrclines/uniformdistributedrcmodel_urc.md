# Uniform Distributed RC Model \(URC\)

The URC model is derived from a model proposed by L. Gertzberg in 1974.
The model is accomplished by a subcircuit type expansion of the URC line
into a network of lumped RC segments with internally generated nodes.
The RC segments are in a geometric progression, increasing toward the
middle of the URC line, with \(K\) as a proportionality constant. The
number of lumped segments used, if not specified for the URC line
device, is determined by the following formula:

\[\begin{array}{ll}
{N = \frac{\log\left| {F_{\lbrack font\ rm\ \lbrack char\ m\ mathalpha\rbrack\lbrack char\ a\ mathalpha\rbrack\lbrack char\ x\ mathalpha\rbrack\rbrack}\frac{R}{L}\frac{C}{L}2\pi L^{2}\left| \frac{\left( {K - 1} \right)}{K} \right|^{2}} \right|}{\log K}} & \\
\end{array}\]The URC line is made up strictly of resistor and capacitor
segments unless the **ISPERL** parameter is given a nonzero value, in
which case the capacitors are replaced with reverse biased diodes with a
zero-bias junction capacitance equivalent to the capacitance replaced,
and with a saturation current of **ISPERL** amps per meter of
transmission line and an optional series resistance equivalent to
**RSPERL** ohms per meter.

<table style="width:100%;">
<colgroup>
<col width="16%" />
<col width="16%" />
<col width="16%" />
<col width="16%" />
<col width="16%" />
<col width="16%" />
</colgroup>
<tbody>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-6308"></span>Name
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6311"></span>Parameter
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6314"></span>Units
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6317"></span>Default
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6320"></span>Example
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6323"></span>Area
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-6326"></span>K
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6329"></span>Propagation Constant
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6332"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6335"></span>2.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6338"></span>1.2
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6341"></span>-
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-6344"></span>FMAX
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6347"></span>Maximum Frequency of interest
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6350"></span><span class="math inline"><em>H</em><em>z</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6353"></span>1.0 G
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6356"></span>6.5 Meg
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6359"></span>-
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-6362"></span>RPERL
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6365"></span>Resistance per unit length
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6368"></span><span class="math inline">$\frac{\Omega}{m}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6371"></span>1000
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6374"></span>10
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6377"></span>-
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-6380"></span>CPERL
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6383"></span>Capacitance per unit length
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6386"></span><span class="math inline">$\frac{F}{m}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6389"></span>10e-15
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6392"></span>1 p
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6395"></span>-
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-6398"></span>ISPERL
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6401"></span>Saturation Current per unit length
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6404"></span><span class="math inline">$\frac{A}{m}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6407"></span>0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6410"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6413"></span>-
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-6416"></span>RSPERL
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6419"></span>Diode Resistance per unit length
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6422"></span><span class="math inline">$\frac{\Omega}{m}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6425"></span>0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6428"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6431"></span>-
</div></td>
</tr>
</tbody>
</table>

