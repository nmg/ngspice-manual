# Lossy Transmission Line Model \(LTRA\)

The uniform RLC/RC/LC/RG transmission line model (referred to as the
LTRA model henceforth) models a uniform constant-parameter distributed
transmission line. The RC and LC cases may also be modeled using the URC
and TRA models; however, the newer LTRA model is usually faster and more
accurate than the others. The operation of the LTRA model is based on
the convolution of the transmission line's impulse responses with its
inputs (see \[8\]). The LTRA model takes a number of parameters, some of
which must be given and some of which are optional.

<table>
<colgroup>
<col width="20%" />
<col width="20%" />
<col width="20%" />
<col width="20%" />
<col width="20%" />
</colgroup>
<tbody>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-5983"></span>Name
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-5986"></span>Parameter
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-5989"></span>Units/Type
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-5992"></span>Default
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-5995"></span>Example
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-5998"></span>R
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6001"></span>resistance/length
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6004"></span><span class="math inline">$\frac{\Omega}{unit}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6007"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6010"></span>0.2
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-6013"></span>L
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6016"></span>inductance/length
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6019"></span><span class="math inline">$\frac{H}{unit}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6022"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6025"></span>9.13e-9
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-6028"></span>G
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6031"></span>conductance/length
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6034"></span><span class="math inline">$\frac{mhos}{unit}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6037"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6040"></span>0.0
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-6043"></span>C
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6046"></span>capacitance/length
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6049"></span><span class="math inline">$\frac{F}{unit}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6052"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6055"></span>3.65e-12
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-6058"></span>LEN
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6061"></span>length of line
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6064"></span><span class="math inline"><em>u</em><em>n</em><em>i</em><em>t</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6067"></span>no default
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6070"></span>1.0
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-6073"></span>REL
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6076"></span>breakpoint control
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6079"></span>arbitrary unit
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6082"></span>1
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6085"></span>0.5
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-6088"></span>ABS
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6091"></span>breakpoint control
</div></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-6097"></span>1
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6100"></span>5
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-6103"></span>NOSTEPLIMIT
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6106"></span>don't limit time-step to less than line delay
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6109"></span>flag
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6112"></span>not set
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6115"></span>set
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-6118"></span>NO CONTROL
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6121"></span>don't do complex time-step control
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6124"></span>flag
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6127"></span>not set
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6130"></span>set
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-6133"></span>LININTERP
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6136"></span>use linear interpolation
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6139"></span>flag
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6142"></span>not set
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6145"></span>set
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-6148"></span>MIXEDINTERP
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6151"></span>use linear when quadratic seems bad
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6154"></span>flag
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6157"></span>not set
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6160"></span>set
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-6163"></span>COMPACTREL
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6166"></span>special reltol for history compaction
</div></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-6172"></span>RELTOL
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6175"></span>1.0e-3
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-6178"></span>COMPACTABS
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6181"></span>special abstol for history compaction
</div></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-6187"></span>ABSTOL
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6190"></span>1.0e-9
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-6193"></span>TRUNCNR
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6196"></span>use Newton-Raphson method for time-step control
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6199"></span>flag
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6202"></span>not set
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6205"></span>set
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-6208"></span>TRUNCDONTCUT
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6211"></span>don't limit time-step to keep impulse-response errors low
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6214"></span>flag
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6217"></span>not set
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6220"></span>set
</div></td>
</tr>
</tbody>
</table>

The following types of lines have been implemented so far:

  - RLC (uniform transmission line with series loss only),
  - RC (uniform RC line),
  - LC (lossless transmission line),
  - RG (distributed series resistance and parallel conductance only).

Any other combination will yield erroneous results and should not be
tried. The length **LEN** of the line must be specified. **NOSTEPLIMIT**
is a flag that will remove the default restriction of limiting
time-steps to less than the line delay in the RLC case. **NO CONTROL**
is a flag that prevents the default limiting of the time-step based on
convolution error criteria in the RLC and RC cases. This speeds up
simulation but may in some cases reduce the accuracy of results.
**LININTERP** is a flag that, when specified, will use linear
interpolation instead of the default quadratic interpolation for
calculating delayed signals. **MIXEDINTERP** is a flag that, when
specified, uses a metric for judging whether quadratic interpolation is
not applicable and if so uses linear interpolation; otherwise it uses
the default quadratic interpolation. **TRUNCDONTCUT** is a flag that
removes the default cutting of the time-step to limit errors in the
actual calculation of impulse-response related quantities.
**COMPACTREL** and **COMPACTABS** are quantities that control the
compaction of the past history of values stored for convolution. Larger
values of these lower accuracy but usually increase simulation speed.
These are to be used with the **TRYTOCOMPACT** option, described in the
.**OPTIONS** section. **TRUNCNR** is a flag that turns on the use of
Newton-Raphson iterations to determine an appropriate time-step in the
time-step control routines. The default is a trial and error procedure
by cutting the previous time-step in half. **REL** and **ABS** are
quantities that control the setting of breakpoints.

The option most worth experimenting with for increasing the speed of
simulation is **REL**. The default value of 1 is usually safe from the
point of view of accuracy but occasionally increases computation time. A
value greater than 2 eliminates all breakpoints and may be worth trying
depending on the nature of the rest of the circuit, keeping in mind that
it might not be safe from the viewpoint of accuracy.

Breakpoints may usually be entirely eliminated if it is expected the
circuit will not display sharp discontinuities. Values between 0 and 1
are usually not required but may be used for setting many breakpoints.

**COMPACTREL** may also be experimented with when the option
**TRYTOCOMPACT** is specified in a .OPTIONS card. The legal range is
between 0 and 1. Larger values usually decrease the accuracy of the
simulation but in some cases improve speed. If **TRYTOCOMPACT** is not
specified on a .OPTIONS card, history compaction is not attempted and
accuracy is high.

**NO CONTROL**, **TRUNCDONTCUT** and **NOSTEPLIMIT** also tend to
increase speed at the expense of accuracy.

