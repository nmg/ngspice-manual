# Lossless Transmission Lines

General form:

``` listings
TXXXXXXX N1 N2 N3 N4 Z0=VALUE <TD=VALUE>
+  <F=FREQ <NL=NRMLEN>> <IC=V1, I1, V2, I2>
```

Examples:

``` listings
T1 1 0 2 0 Z0=50 TD=10NS
```

**n1** and **n2** are the nodes at port 1; **n3** and **n4** are the
nodes at port 2. **z0** is the characteristic impedance. The length of
the line may be expressed in either of two forms. The transmission
delay, **td**, may be specified directly (as td=10ns, for example).
Alternatively, a frequency **f** may be given, together with **nl**, the
normalized electrical length of the transmission line with respect to
the wavelength in the line at the frequency \`f'. If a frequency is
specified but **nl** is omitted, 0.25 is assumed (that is, the frequency
is assumed to be the quarter-wave frequency). Note that although both
forms for expressing the line length are indicated as optional, one of
the two must be specified.

Note that this element models only one propagating mode. If all four
nodes are distinct in the actual circuit, then two modes may be excited.
To simulate such a situation, two transmission-line elements are
required. (see the example in Chapt.
[21.7](#sec_Transmission_Line_Inverter) for further clarification.) The
(optional) initial condition specification consists of the voltage and
current at each of the transmission line ports. Note that the initial
conditions (if any) apply *only* if the **UIC** option is specified on
the .TRAN control line.

Note that a lossy transmission line (see below) with zero loss may be
more accurate than the lossless transmission line due to implementation
details.

