# Lossy Transmission Lines

General form:

``` listings
OXXXXXXX n1 n2 n3 n4 mname
```

Examples:

``` listings
O23 1 0 2 0 LOSSYMOD
OCONNECT 10 5 20 5 INTERCONNECT
```

This is a two-port convolution model for single conductor lossy
transmission lines. **n1** and **n2** are the nodes at port 1; **n3**
and **n4** are the nodes at port 2. Note that a lossy transmission line
with zero loss may be more accurate than the lossless transmission line
due to implementation details.

