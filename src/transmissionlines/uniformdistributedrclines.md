# Uniform Distributed RC Lines

General form:

``` listings
UXXXXXXX n1 n2 n3 mname l=len <n=lumps>
```

Examples:

``` listings
U1 1 2 0 URCMOD L=50U
URC2 1 12 2 UMODL l=1MIL N=6
```

**n1** and **n2** are the two element nodes the RC line connects, while
**n3** is the node the capacitances are connected to. **mname** is the
model name, **len** is the length of the RC line in meters. **lumps**,
if specified, is the number of lumped segments to use in modeling the RC
line (see the model description for the action taken if this parameter
is omitted).

