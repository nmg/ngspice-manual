# KSPICE Lossy Transmission Lines

Unlike SPICE3, which uses the state-based approach to simulate lossy
transmission lines, KSPICE simulates lossy transmission lines and
coupled multiconductor line systems using the recursive convolution
method. The impulse response of an arbitrary transfer function can be
determined by deriving a recursive convolution from the Pade
approximations of the function. NGSPICE is using this approach for
simulating each transmission line's characteristics and each
multiconductor line's modal functions. This method of lossy transmission
line simulation has shown to give a speedup of one to two orders of
magnitude over SPICE3E. Please note that the following two models will
support only **transient simulation**, no ac.

Additional Documentation Available:

  - S. Lin and E. S. Kuh, \`Pade Approximation Applied to Transient
    Simulation of Lossy Coupled Transmission Lines,' Proc. IEEE
    Multi-Chip Module Conference, 1992, pp. 52-55.
  - S. Lin, M. Marek-Sadowska, and E. S. Kuh, \`SWEC: A StepWise
    Equivalent Conductance Timing Simulator for CMOS VLSI Circuits,'
    European Design Automation Conf., February 1991, pp. 142-148.
  - S. Lin and E. S. Kuh, \`Transient Simulation of Lossy Interconnect,'
    Proc. Design Automation Conference, Anaheim, CA, June 1992, pp.
    81-86.

