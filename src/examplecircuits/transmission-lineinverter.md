# Transmission-Line Inverter

The following deck simulates a transmission-line inverter. Two
transmission-line elements are required since two propagation modes are
excited. In the case of a coaxial line, the first line (T1) models the
inner conductor with respect to the shield, and the second line (T2)
models the shield with respect to the outside world.

Example:

``` listings
Transmission-line inverter

v1 1 0 pulse(0 1 0 0.1n)
r1 1 2 50
x1 2 0 0 4 tline
r2 4 0 50

.subckt tline 1 2 3 4
t1 1 2 3 4 z0=50 td=1.5ns
t2 2 0 4 0 z0=100 td=1ns
.ends tline

.tran 0.1ns 20ns
.end
```

