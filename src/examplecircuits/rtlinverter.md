# RTL Inverter

The following deck determines the dc transfer curve and the transient
pulse response of a simple RTL inverter. The input is a pulse from 0 to
5 Volts with delay, rise, and fall times of 2ns and a pulse width of
30ns. The transient interval is 0 to 100ns, with printing to be done
every nanosecond.

Example:

``` listings
SIMPLE RTL INVERTER
VCC 4 0 5
VIN 1 0 PULSE 0 5 2NS 2NS 2NS 30NS
RB 1 2 10K
Q1 3 2 0 Q1
RC 3 4 1K
.MODEL Q1 NPN BF 20 RB 100 TF .1NS CJC 2PF
.DC VIN 0 5 0.1
.TRAN 1NS 100NS
.END
```

