#  Execution Procedures

This chapter covers operation of the XSPICE simulator and the Code Model
Subsystem. It begins with background material on simulation and modeling
and then discusses the analysis modes supported in XSPICE and the
circuit description syntax used for modeling. Detailed descriptions of
the predefined Code Models and Node Types provided in the XSPICE
libraries are also included.

