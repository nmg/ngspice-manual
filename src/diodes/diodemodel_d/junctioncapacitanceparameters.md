# Junction capacitance parameters

<table style="width:100%;">
<colgroup>
<col width="16%" />
<col width="16%" />
<col width="16%" />
<col width="16%" />
<col width="16%" />
<col width="16%" />
</colgroup>
<tbody>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-7265"></span><strong><em>Name</em></strong>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7268"></span><strong><em>Parameter</em></strong>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7271"></span><strong><em>Units</em></strong>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7274"></span><strong><em>Default</em></strong>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7277"></span><strong><em>Example</em></strong>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7280"></span><strong><em>Scale factor</em></strong>
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-7283"></span>CJO (CJ0)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7286"></span>Zero-bias junction bottom-wall capacitance
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7289"></span><span class="math inline"><em>F</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7292"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7295"></span>2pF
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7298"></span>area
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-7301"></span>CJP (CJSW)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7304"></span>Zero-bias junction sidewall capacitance
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7307"></span><span class="math inline"><em>F</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7310"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7313"></span>.1pF
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7316"></span>perimeter
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-7319"></span>FC
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7322"></span>Coefficient for forward-bias depletion bottom-wall capacitance formula
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7325"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7328"></span>0.5
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7331"></span>-
</div></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-7337"></span>FCS
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7340"></span>Coefficient for forward-bias depletion sidewall capacitance formula
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7343"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7346"></span>0.5
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7349"></span>-
</div></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-7355"></span>M (MJ)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7358"></span>Area junction grading coefficient
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7361"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7364"></span>0.5
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7367"></span>0.5
</div></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-7373"></span>MJSW
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7376"></span>Periphery junction grading coefficient
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7379"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7382"></span>0.33
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7385"></span>0.5
</div></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-7391"></span>VJ (PB)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7394"></span>Junction potential
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7397"></span><span class="math inline"><em>V</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7400"></span>1
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7403"></span>0.6
</div></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-7409"></span>PHP
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7412"></span>Periphery junction potential
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7415"></span><span class="math inline"><em>V</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7418"></span>1
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7421"></span>0.6
</div></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-7427"></span>TT
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7430"></span>Transit-time
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7433"></span>sec
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7436"></span>0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7439"></span>0.1ns
</div></td>
<td></td>
</tr>
</tbody>
</table>

