# Junction DC parameters

<table style="width:100%;">
<colgroup>
<col width="16%" />
<col width="16%" />
<col width="16%" />
<col width="16%" />
<col width="16%" />
<col width="16%" />
</colgroup>
<tbody>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-6887"></span><strong><em>Name</em></strong>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6890"></span><strong><em>Parameter</em></strong>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6893"></span><strong><em>Units</em></strong>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6896"></span><strong><em>Default</em></strong>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6899"></span><strong><em>Example</em></strong>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6902"></span><strong><em>Scale factor</em></strong>
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-6905"></span>IS (JS)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6908"></span>Saturation current
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6911"></span><span class="math inline"><em>A</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6914"></span>1.0e-14
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6917"></span>1.0e-16
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6920"></span>area
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-6923"></span>JSW
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6926"></span>Sidewall saturation current
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6929"></span><span class="math inline"><em>A</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6932"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6935"></span>1.0e-15
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6938"></span>perimeter
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-6941"></span>N
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6944"></span>Emission coefficient
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6947"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6950"></span>1
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6953"></span>1.5
</div></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-6959"></span>RS
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6962"></span>Ohmic resistance
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6965"></span><span class="math inline"><em>Ω</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6968"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6971"></span>100
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6974"></span><span class="math inline">$\frac{1}{area}$</span>
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-6977"></span>BV
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6980"></span>Reverse breakdown voltage
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6983"></span><span class="math inline"><em>V</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6986"></span><span class="math inline">∞</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6989"></span>40
</div></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-6995"></span>IBV
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-6998"></span>Current at breakdown voltage
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7001"></span><span class="math inline"><em>A</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7004"></span>1.0e-3
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7007"></span>1.0e-4
</div></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-7013"></span>NBV
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7016"></span>Breakdown Emission Coefficient
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7019"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7022"></span>N
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7025"></span>1.2
</div></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-7031"></span>IKF (IK)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7034"></span>Forward knee current
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7037"></span><span class="math inline"><em>A</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7040"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7043"></span>1.0e-3
</div></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-7049"></span>IKR
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7052"></span>Reverse knee current
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7055"></span><span class="math inline"><em>A</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7058"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7061"></span>1.0e-3
</div></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-7067"></span>JTUN
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7070"></span>Tunneling saturation current
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7073"></span><span class="math inline"><em>A</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7076"></span>0.0
</div></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-7082"></span>area
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-7085"></span>JTUNSW
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7088"></span>Tunneling sidewall saturation current
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7091"></span><span class="math inline"><em>A</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7094"></span>0.0
</div></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-7100"></span>perimeter
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-7103"></span>NTUN
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7106"></span>Tunneling emission coefficient
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7109"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7112"></span>30
</div></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-7121"></span>XTITUN
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7124"></span>Tunneling saturation current exponential
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7127"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7130"></span>3
</div></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-7139"></span>KEG
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7142"></span>EG correction factor for tunneling
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7145"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7148"></span>1.0
</div></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-7157"></span>ISR
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7160"></span>Recombination saturation current
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7163"></span><span class="math inline"><em>A</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7166"></span>1e-14
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7169"></span>1pA
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7172"></span>area
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-7175"></span>NR
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7178"></span>Recombination current emission coefficient
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7181"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7184"></span>1
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7187"></span>2
</div></td>
<td></td>
</tr>
</tbody>
</table>

