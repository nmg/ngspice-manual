# Noise modeling

<table style="width:100%;">
<colgroup>
<col width="16%" />
<col width="16%" />
<col width="16%" />
<col width="16%" />
<col width="16%" />
<col width="16%" />
</colgroup>
<tbody>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-7826"></span><strong><em>Name</em></strong>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7829"></span><strong><em>Parameter</em></strong>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7832"></span><strong><em>Units</em></strong>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7835"></span><strong><em>Default</em></strong>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7838"></span><strong><em>Example</em></strong>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7841"></span><strong><em>Scale factor</em></strong>
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-7844"></span>KF
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7847"></span>Flicker noise coefficient
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7850"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7853"></span>0
</div></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-7862"></span>AF
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7865"></span>Flicker noise exponent
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7868"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7871"></span>1
</div></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

Diode models may be described in the input file (or an file included by
.inc) according to the following example:

General form:

``` listings
.model mname type(pname1=pval1 pname2=pval2 ... )
```

Examples:

``` listings
.model DMOD D (bv=50 is=1e-13 n=1.05)
```

