# Temperature effects

<table>
<colgroup>
<col width="20%" />
<col width="20%" />
<col width="20%" />
<col width="20%" />
<col width="20%" />
</colgroup>
<tbody>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-7541"></span><strong><em>Name</em></strong>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7544"></span><strong><em>Parameter</em></strong>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7547"></span><strong><em>Units</em></strong>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7550"></span><strong><em>Default</em></strong>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7553"></span><strong><em>Example</em></strong>
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-7556"></span>EG
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7559"></span>Activation energy
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7562"></span><span class="math inline"><em>e</em><em>V</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7565"></span>1.11
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7568"></span><span class="math inline">$\begin{array}{ll}
1.11 &amp; {Si} \\
0.69 &amp; {Sbd} \\
0.67 &amp; {Ge} \\
\end{array}$</span>
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-7571"></span>TM1
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7574"></span>1st order tempco for MJ
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7577"></span><span class="math inline">$\frac{1}{C}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7580"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7583"></span>-
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-7586"></span>TM2
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7589"></span>2nd order tempco for MJ
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7592"></span><span class="math inline">$\frac{1}{C^{2}}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7595"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7598"></span>-
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-7601"></span>TNOM (TREF)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7604"></span>Parameter measurement temperature
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7607"></span><span class="math inline"><em>C</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7610"></span>27
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7613"></span>50
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-7616"></span>TRS1 (TRS)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7619"></span>1st order tempco for RS
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7622"></span><span class="math inline">$\frac{1}{C}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7625"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7628"></span>-
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-7631"></span>TRS2
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7634"></span>2nd order tempco for RS
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7637"></span><span class="math inline">$\frac{1}{C^{2}}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7640"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7643"></span>-
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-7646"></span>TM1
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7649"></span>1st order tempco for MJ
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7652"></span><span class="math inline">$\frac{1}{C}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7655"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7658"></span>-
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-7661"></span>TM2
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7664"></span>2nd order tempco for MJ
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7667"></span><span class="math inline">$\frac{1}{C^{2}}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7670"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7673"></span>-
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-7676"></span>TTT1
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7679"></span>1st order tempco for TT
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7682"></span><span class="math inline">$\frac{1}{C}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7685"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7688"></span>-
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-7691"></span>TTT2
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7694"></span>2nd order tempco for TT
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7697"></span><span class="math inline">$\frac{1}{C^{2}}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7700"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7703"></span>-
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-7706"></span>XTI
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7709"></span>Saturation current temperature exponent
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7712"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7715"></span>3.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7718"></span><span class="math inline">$\begin{array}{ll}
3.0 &amp; {pn} \\
2.0 &amp; {Sbd} \\
\end{array}$</span>
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-7721"></span>TLEV
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7724"></span>Diode temperature equation selector
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7727"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7730"></span>0
</div></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-7736"></span>TLEVC
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7739"></span>Diode capac. temperature equation selector
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7742"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7745"></span>0
</div></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-7751"></span>CTA (CTC)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7754"></span>Area junct. cap. temperature coefficient
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7757"></span><span class="math inline">$\frac{1}{C}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7760"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7763"></span>-
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-7766"></span>CTP
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7769"></span>Perimeter junct. cap. temperature coefficient
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7772"></span><span class="math inline">$\frac{1}{C}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7775"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7778"></span>-
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-7781"></span>TCV
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7784"></span>Breakdown voltage temperature coefficient
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7787"></span><span class="math inline">$\frac{1}{C}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7790"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-7793"></span>-
</div></td>
</tr>
</tbody>
</table>

