# Diode DC, Transient and AC model equations

The diode model has certain dc currents for bottom and sidewall
components. Exemplary here is the equation for the bottom part:

\[\begin{array}{ll}
{I_{D} = \begin{cases}
{IS_{eff}\left( {e^{\frac{qV_{D}}{NkT}} - 1} \right) + V_{D} \cdot GMIN,} & {{if} V_{D} \geq - 3\frac{NkT}{q}} \\
{- IS_{eff}\left\lbrack {1 + \left( \frac{3NkT}{qV_{D}e})^{3} \right.} \right\rbrack + V_{D} \cdot GMIN,} & {{if} - BV_{eff} < V_{D} < - 3\frac{NkT}{q}} \\
{- IS_{eff}\left( e^{\frac{- q({BV_{eff} + V_{D}})}{NkT}} \right) + V_{D} \cdot GMIN,} & {{if} V_{D} \leq - BV_{eff}} \\
\end{cases}} & \\
\end{array}\]

Two secondary effects are modelled if the appropriate parameters (see
table Junction DC parameters) are given: Recombination current and
bottom and sidewall tunnel current.

The breakdown region must be described with more depth since the
breakdown is not modeled physically. As written before, the breakdown
modeling is based on two model parameters: the \`nominal breakdown
voltage' **bv** and the current at the onset of breakdown **ibv**. For
the diode model to be consistent, the current value cannot be
arbitrarily chosen, since the reverse bias and breakdown regions must
match. When the diode enters breakdown region from reverse bias, the
current is calculated using the formula

1

if you look at the source code in file diotemp.c you will discover that
the exponential relation is replaced with a first order Taylor series
expansion.

:

\[\begin{array}{ll}
{I_{bdwn} = - IS_{eff}\left( {e^{\frac{- q{\lbrack font\ rm\ \lbrack char\ B\ mathalpha\rbrack\lbrack char\ V\ mathalpha\rbrack\rbrack}}{NkT}} - 1} \right)} & \\
\end{array}\]

The computed current is necessary to adjust the breakdown voltage making
the two regions match. The algorithm is a little bit convoluted and only
a brief description is given here:

if \(IBV_{eff} < I_{bdwn}\) then

\(\begin{array}{ll}
{IBV_{eff} = I_{bdwn}} & \\
{BV_{eff} = {\lbrack font\ rm\ \lbrack char\ B\ mathalpha\rbrack\lbrack char\ V\ mathalpha\rbrack\rbrack}} & \\
\end{array}\)

else

\(\begin{array}{ll}
{BV_{eff} = {\lbrack font\ rm\ \lbrack char\ B\ mathalpha\rbrack\lbrack char\ V\ mathalpha\rbrack\rbrack} - {\lbrack font\ rm\ \lbrack char\ N\ mathalpha\rbrack\rbrack}V_{t}\ln\left( \frac{IBV_{eff}}{I_{bdwn}} \right)} & \\
\end{array}\)

Algorithm 7.1: Diode breakdown current calculation

Most real diodes shows a current increase that, at high current levels,
does not follow the exponential relationship given above. This behavior
is due to high level of carriers injected into the junction. High
injection effects (as they are called) are modeled with **ik** and
**ikr**.

\[\begin{array}{ll}
{I_{Deff} = \begin{cases}
{\frac{I_{D}}{1 + \sqrt{\frac{I_{D}}{IK_{eff}}}},} & {{if} V_{D} \geq - 3\frac{NkT}{q}} \\
{\frac{I_{D}}{1 + \sqrt{\frac{I_{D}}{IKR_{eff}}}},} & {otherwise.} \\
\end{cases}} & \\
\end{array}\]

Diode capacitance is divided into two different terms:

  - Depletion capacitance
  - Diffusion capacitance

Depletion capacitance is composed by two different contributes, one
associated to the bottom of the junction (bottom-wall depletion
capacitance) and the other to the periphery (sidewall depletion
capacitance). The basic equations are:

\[C_{Diode} = C_{diffusion} + C_{depletion}\]

Where the depletion capacitance is defined as:

\[C_{depletion} = C_{depl_{bw}} + C_{depl_{sw}}\]

The diffusion capacitance, due to the injected minority carriers, is
modeled with the transit time**
tt**:

\[C_{diffusion} = {\lbrack font\ rm\ \lbrack char\ T\ mathalpha\rbrack\lbrack char\ T\ mathalpha\rbrack\rbrack}\frac{\partial I_{Deff}}{\partial V_{D}}\]

The depletion capacitance is more complex to model, since the function
used to approximate it diverges when the diode voltage become greater
than the junction built-in potential. To avoid function divergence, the
capacitance function is approximated with a linear extrapolation for
applied voltage greater than a fraction of the junction built-in
potential.

\[\begin{array}{llll}
C_{depl_{bw}} & = & \begin{cases}
{CJ_{eff}\left( 1 - \frac{V_{D}}{\lbrack font\ rm\ \lbrack char\ V\ mathalpha\rbrack\lbrack char\ J\ mathalpha\rbrack\rbrack})^{- {\lbrack font\ rm\ \lbrack char\ M\ mathalpha\rbrack\lbrack char\ J\ mathalpha\rbrack\rbrack}}, \right.} & {{if} V_{D} < {\lbrack font\ rm\ \lbrack char\ F\ mathalpha\rbrack\lbrack char\ C\ mathalpha\rbrack\rbrack} \cdot {\lbrack font\ rm\ \lbrack char\ V\ mathalpha\rbrack\lbrack char\ J\ mathalpha\rbrack\rbrack}} \\
{CJ_{eff}\frac{1 - {\lbrack font\ rm\ \lbrack char\ F\ mathalpha\rbrack\lbrack char\ C\ mathalpha\rbrack\rbrack}\left( {1 + {\lbrack font\ rm\ \lbrack char\ M\ mathalpha\rbrack\lbrack char\ J\ mathalpha\rbrack\rbrack}I} \right) + {\lbrack font\ rm\ \lbrack char\ M\ mathalpha\rbrack\lbrack char\ J\ mathalpha\rbrack\rbrack}\frac{V_{D}}{\lbrack font\ rm\ \lbrack char\ V\ mathalpha\rbrack\lbrack char\ J\ mathalpha\rbrack\rbrack}}{\left( 1 - {\lbrack font\ rm\ \lbrack char\ F\ mathalpha\rbrack\lbrack char\ C\ mathalpha\rbrack\rbrack})^{({1 + {\lbrack font\ rm\ \lbrack char\ M\ mathalpha\rbrack\lbrack char\ J\ mathalpha\rbrack\rbrack}})} \right.},} & {{otherwise}.} \\
\end{cases} & \\
\end{array}\]

\[\begin{array}{ll}
{C_{depl_{sw}} = \begin{cases}
{CJP_{eff}\left( 1 - \frac{V_{D}}{\lbrack font\ rm\ \lbrack char\ P\ mathalpha\rbrack\lbrack char\ H\ mathalpha\rbrack\lbrack char\ P\ mathalpha\rbrack\rbrack})^{- {\lbrack font\ rm\ \lbrack char\ M\ mathalpha\rbrack\lbrack char\ J\ mathalpha\rbrack\lbrack char\ S\ mathalpha\rbrack\lbrack char\ W\ mathalpha\rbrack\rbrack}}, \right.} & {if V_{D} < {\lbrack font\ rm\ \lbrack char\ F\ mathalpha\rbrack\lbrack char\ C\ mathalpha\rbrack\lbrack char\ S\ mathalpha\rbrack\rbrack} \cdot {\lbrack font\ rm\ \lbrack char\ P\ mathalpha\rbrack\lbrack char\ H\ mathalpha\rbrack\lbrack char\ P\ mathalpha\rbrack\rbrack}} \\
{CJP_{eff}\frac{1 - {\lbrack font\ rm\ \lbrack char\ F\ mathalpha\rbrack\lbrack char\ C\ mathalpha\rbrack\lbrack char\ S\ mathalpha\rbrack\rbrack}\left( {1 + {\lbrack font\ rm\ \lbrack char\ M\ mathalpha\rbrack\lbrack char\ J\ mathalpha\rbrack\lbrack char\ S\ mathalpha\rbrack\lbrack char\ W\ mathalpha\rbrack\rbrack}} \right) + {\lbrack font\ rm\ \lbrack char\ M\ mathalpha\rbrack\lbrack char\ J\ mathalpha\rbrack\lbrack char\ S\ mathalpha\rbrack\lbrack char\ W\ mathalpha\rbrack\rbrack} \cdot \frac{V_{D}}{\lbrack font\ rm\ \lbrack char\ P\ mathalpha\rbrack\lbrack char\ H\ mathalpha\rbrack\lbrack char\ P\ mathalpha\rbrack\rbrack}}{\left( 1 - {\lbrack font\ rm\ \lbrack char\ F\ mathalpha\rbrack\lbrack char\ C\ mathalpha\rbrack\lbrack char\ S\ mathalpha\rbrack\rbrack})^{({1 + {\lbrack font\ rm\ \lbrack char\ M\ mathalpha\rbrack\lbrack char\ J\ mathalpha\rbrack\lbrack char\ S\ mathalpha\rbrack\lbrack char\ W\ mathalpha\rbrack\rbrack}})} \right.},} & {{otherwise}.} \\
\end{cases}} & \\
\end{array}\]

