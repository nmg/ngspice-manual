# Noise model

The diode has three noise contribution, one due to the presence of the
parasitic resistance **rs** and the other two (shot and flicker) due to
the pn junction.

The thermal noise due to the parasitic resistance is:

\[\begin{array}{ll}
{\overline{i_{RS}^{2}} = \frac{4kT\Delta f}{RS}} & \\
\end{array}\]

The shot and flicker noise contributions are:

\[\begin{array}{ll}
{\overline{i_{d}^{2}} = 2qI_{D}\Delta f + \frac{KF \cdot I_{D}^{AF}}{f}\Delta f} & \\
\end{array}\]

