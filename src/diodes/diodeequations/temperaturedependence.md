# Temperature dependence

The temperature affects many of the parameters in the equations above,
and the following equations show how. One of the most significant
parameters that varies with the temperature for a semiconductor is the
band-gap energy:

\[\begin{array}{ll}
{EG_{nom} = 1.16 - 7.02e^{- 4}\frac{{\lbrack font\ rm\ \lbrack char\ T\ mathalpha\rbrack\lbrack char\ N\ mathalpha\rbrack\lbrack char\ O\ mathalpha\rbrack\lbrack char\ M\ mathalpha\rbrack\rbrack}^{2}}{{\lbrack font\ rm\ \lbrack char\ T\ mathalpha\rbrack\lbrack char\ N\ mathalpha\rbrack\lbrack char\ O\ mathalpha\rbrack\lbrack char\ M\ mathalpha\rbrack\rbrack} + 1108.0}} & \\
\end{array}\]

\[\begin{array}{ll}
{EG\left( T \right) = 1.16 - 7.02e^{- 4}\frac{T^{2}}{{\lbrack font\ rm\ \lbrack char\ T\ mathalpha\rbrack\lbrack char\ N\ mathalpha\rbrack\lbrack char\ O\ mathalpha\rbrack\lbrack char\ M\ mathalpha\rbrack\rbrack} + 1108.0}} & \\
\end{array}\]

The leakage current temperature's dependence is:

\[\begin{array}{ll}
{IS\left( T \right) = {\lbrack font\ rm\ \lbrack char\ I\ mathalpha\rbrack\lbrack char\ S\ mathalpha\rbrack\rbrack} e^{\frac{logfactor}{\lbrack font\ rm\ \lbrack char\ N\ mathalpha\rbrack\rbrack}}} & \\
\end{array}\]

\[\begin{array}{ll}
{JSW\left( T \right) = {\lbrack font\ rm\ \lbrack char\ J\ mathalpha\rbrack\lbrack char\ S\ mathalpha\rbrack\lbrack char\ W\ mathalpha\rbrack\rbrack} e^{\frac{logfactor}{\lbrack font\ rm\ \lbrack char\ N\ mathalpha\rbrack\rbrack}}} & \\
\end{array}\]

where \`logfactor' is defined as

\[\begin{array}{ll}
{logfactor = \frac{\lbrack font\ rm\ \lbrack char\ E\ mathalpha\rbrack\lbrack char\ G\ mathalpha\rbrack\rbrack}{V_{t}\left( {\lbrack font\ rm\ \lbrack char\ T\ mathalpha\rbrack\lbrack char\ N\ mathalpha\rbrack\lbrack char\ O\ mathalpha\rbrack\lbrack char\ M\ mathalpha\rbrack\rbrack} \right)} - \frac{\lbrack font\ rm\ \lbrack char\ E\ mathalpha\rbrack\lbrack char\ G\ mathalpha\rbrack\rbrack}{V_{t}\left( T \right)} + {\lbrack font\ rm\ \lbrack char\ X\ mathalpha\rbrack\lbrack char\ T\ mathalpha\rbrack\lbrack char\ I\ mathalpha\rbrack\rbrack}\ln\left( \frac{T}{\lbrack font\ rm\ \lbrack char\ T\ mathalpha\rbrack\lbrack char\ N\ mathalpha\rbrack\lbrack char\ O\ mathalpha\rbrack\lbrack char\ M\ mathalpha\rbrack\rbrack} \right)} & \\
\end{array}\]

The contact potentials (bottom-wall an sidewall) temperature dependence
is:

\[\begin{array}{ll}
{VJ\left( T \right) = {\lbrack font\ rm\ \lbrack char\ V\ mathalpha\rbrack\lbrack char\ J\ mathalpha\rbrack\rbrack}\left( \frac{T}{\lbrack font\ rm\ \lbrack char\ T\ mathalpha\rbrack\lbrack char\ N\ mathalpha\rbrack\lbrack char\ O\ mathalpha\rbrack\lbrack char\ M\ mathalpha\rbrack\rbrack} \right) - V_{t}\left( T \right)\left\lbrack {3 \cdot \ln\left( \frac{T}{\lbrack font\ rm\ \lbrack char\ T\ mathalpha\rbrack\lbrack char\ N\ mathalpha\rbrack\lbrack char\ O\ mathalpha\rbrack\lbrack char\ M\ mathalpha\rbrack\rbrack} \right) + \frac{\lbrack font\ rm\ \lbrack char\ E\ mathalpha\rbrack\lbrack sub\ \lbrack char\ G\ mathalpha\rbrack\ \lbrack char\ n\ mathalpha\rbrack\lbrack char\ o\ mathalpha\rbrack\lbrack char\ m\ mathalpha\rbrack\rbrack\rbrack}{V_{t}\left( {\lbrack font\ rm\ \lbrack char\ T\ mathalpha\rbrack\lbrack char\ N\ mathalpha\rbrack\lbrack char\ O\ mathalpha\rbrack\lbrack char\ M\ mathalpha\rbrack\rbrack} \right)} - \frac{\lbrack font\ rm\ \lbrack char\ E\ mathalpha\rbrack\lbrack char\ G\ mathalpha\rbrack\lbrack char\ (\ mathalpha\rbrack\lbrack char\ T\ mathalpha\rbrack\lbrack char\ )\ mathalpha\rbrack\rbrack}{V_{t}\left( T \right)}} \right\rbrack} & \\
\end{array}\]

\[\begin{array}{ll}
{PHP\left( T \right) = {\lbrack font\ rm\ \lbrack char\ P\ mathalpha\rbrack\lbrack char\ H\ mathalpha\rbrack\lbrack char\ P\ mathalpha\rbrack\rbrack}\left( \frac{T}{\lbrack font\ rm\ \lbrack char\ T\ mathalpha\rbrack\lbrack char\ N\ mathalpha\rbrack\lbrack char\ O\ mathalpha\rbrack\lbrack char\ M\ mathalpha\rbrack\rbrack} \right) - V_{t}\left( T \right)\left\lbrack {3 \cdot \ln\left( \frac{T}{\lbrack font\ rm\ \lbrack char\ T\ mathalpha\rbrack\lbrack char\ N\ mathalpha\rbrack\lbrack char\ O\ mathalpha\rbrack\lbrack char\ M\ mathalpha\rbrack\rbrack} \right) + \frac{\lbrack font\ rm\ \lbrack char\ E\ mathalpha\rbrack\lbrack sub\ \lbrack char\ G\ mathalpha\rbrack\ \lbrack char\ n\ mathalpha\rbrack\lbrack char\ o\ mathalpha\rbrack\lbrack char\ m\ mathalpha\rbrack\rbrack\rbrack}{V_{t}\left( {\lbrack font\ rm\ \lbrack char\ T\ mathalpha\rbrack\lbrack char\ N\ mathalpha\rbrack\lbrack char\ O\ mathalpha\rbrack\lbrack char\ M\ mathalpha\rbrack\rbrack} \right)} - \frac{\lbrack font\ rm\ \lbrack char\ E\ mathalpha\rbrack\lbrack char\ G\ mathalpha\rbrack\lbrack char\ (\ mathalpha\rbrack\lbrack char\ T\ mathalpha\rbrack\lbrack char\ )\ mathalpha\rbrack\rbrack}{V_{t}\left( T \right)}} \right\rbrack} & \\
\end{array}\]

The depletion capacitances temperature dependence is:

\[\begin{array}{ll}
{CJ\left( T \right) = {\lbrack font\ rm\ \lbrack char\ C\ mathalpha\rbrack\lbrack char\ J\ mathalpha\rbrack\rbrack}\left\lbrack {1 + {\lbrack font\ rm\ \lbrack char\ M\ mathalpha\rbrack\lbrack char\ J\ mathalpha\rbrack\rbrack}\left( {4.0e^{- 4}\left( {T - {\lbrack font\ rm\ \lbrack char\ T\ mathalpha\rbrack\lbrack char\ N\ mathalpha\rbrack\lbrack char\ O\ mathalpha\rbrack\lbrack char\ M\ mathalpha\rbrack\rbrack}} \right) - \frac{VJ\left( T \right)}{\lbrack font\ rm\ \lbrack char\ V\ mathalpha\rbrack\lbrack char\ J\ mathalpha\rbrack\rbrack} + 1} \right)} \right\rbrack} & \\
\end{array}\]

\[\begin{array}{ll}
{CJSW\left( T \right) = {\lbrack font\ rm\ \lbrack char\ C\ mathalpha\rbrack\lbrack char\ J\ mathalpha\rbrack\lbrack char\ S\ mathalpha\rbrack\lbrack char\ W\ mathalpha\rbrack\rbrack}\left\lbrack {1 + {\lbrack font\ rm\ \lbrack char\ M\ mathalpha\rbrack\lbrack char\ J\ mathalpha\rbrack\lbrack char\ S\ mathalpha\rbrack\lbrack char\ W\ mathalpha\rbrack\rbrack}\left( {4.0e^{- 4}\left( {T - {\lbrack font\ rm\ \lbrack char\ T\ mathalpha\rbrack\lbrack char\ N\ mathalpha\rbrack\lbrack char\ O\ mathalpha\rbrack\lbrack char\ M\ mathalpha\rbrack\rbrack}} \right) - \frac{PHP\left( T \right)}{\lbrack font\ rm\ \lbrack char\ P\ mathalpha\rbrack\lbrack char\ H\ mathalpha\rbrack\lbrack char\ P\ mathalpha\rbrack\rbrack} + 1} \right)} \right\rbrack} & \\
\end{array}\]

The transit time temperature dependence is:

\[\begin{array}{ll}
{TT\left( T \right) = {\lbrack font\ rm\ \lbrack char\ T\ mathalpha\rbrack\lbrack char\ T\ mathalpha\rbrack\rbrack}\left( 1 + {\lbrack font\ rm\ \lbrack char\ T\ mathalpha\rbrack\lbrack char\ T\ mathalpha\rbrack\lbrack char\ T\ mathalpha\rbrack\lbrack char\ 1\ mathalpha\rbrack\rbrack}\left( {T - {\lbrack font\ rm\ \lbrack char\ T\ mathalpha\rbrack\lbrack char\ N\ mathalpha\rbrack\lbrack char\ O\ mathalpha\rbrack\lbrack char\ M\ mathalpha\rbrack\rbrack}} \right) + {\lbrack font\ rm\ \lbrack char\ T\ mathalpha\rbrack\lbrack char\ T\ mathalpha\rbrack\lbrack char\ T\ mathalpha\rbrack\lbrack char\ 2\ mathalpha\rbrack\rbrack}\left( {T - {\lbrack font\ rm\ \lbrack char\ T\ mathalpha\rbrack\lbrack char\ N\ mathalpha\rbrack\lbrack char\ O\ mathalpha\rbrack\lbrack char\ M\ mathalpha\rbrack\rbrack})^{2}} \right) \right.} & \\
\end{array}\]

The junction grading coefficient temperature dependence is:

\[\begin{array}{ll}
{MJ\left( T \right) = {\lbrack font\ rm\ \lbrack char\ M\ mathalpha\rbrack\lbrack char\ J\ mathalpha\rbrack\rbrack}\left( 1 + {\lbrack font\ rm\ \lbrack char\ T\ mathalpha\rbrack\lbrack char\ M\ mathalpha\rbrack\lbrack char\ 1\ mathalpha\rbrack\rbrack}\left( {T - {\lbrack font\ rm\ \lbrack char\ T\ mathalpha\rbrack\lbrack char\ N\ mathalpha\rbrack\lbrack char\ O\ mathalpha\rbrack\lbrack char\ M\ mathalpha\rbrack\rbrack}} \right) + {\lbrack font\ rm\ \lbrack char\ T\ mathalpha\rbrack\lbrack char\ M\ mathalpha\rbrack\lbrack char\ 2\ mathalpha\rbrack\rbrack}\left( {T - {\lbrack font\ rm\ \lbrack char\ T\ mathalpha\rbrack\lbrack char\ N\ mathalpha\rbrack\lbrack char\ O\ mathalpha\rbrack\lbrack char\ M\ mathalpha\rbrack\rbrack})^{2}} \right) \right.} & \\
\end{array}\]

The series resistance temperature dependence is:

\[\begin{array}{ll}
{RS\left( T \right) = {\lbrack font\ rm\ \lbrack char\ R\ mathalpha\rbrack\lbrack char\ S\ mathalpha\rbrack\rbrack}\left( 1 + {\lbrack font\ rm\ \lbrack char\ T\ mathalpha\rbrack\lbrack char\ R\ mathalpha\rbrack\lbrack char\ S\ mathalpha\rbrack\rbrack}\left( {T - {\lbrack font\ rm\ \lbrack char\ T\ mathalpha\rbrack\lbrack char\ N\ mathalpha\rbrack\lbrack char\ O\ mathalpha\rbrack\lbrack char\ M\ mathalpha\rbrack\rbrack}} \right) + {\lbrack font\ rm\ \lbrack char\ T\ mathalpha\rbrack\lbrack char\ R\ mathalpha\rbrack\lbrack char\ S\ mathalpha\rbrack\lbrack char\ 2\ mathalpha\rbrack\rbrack}\left( {T - {\lbrack font\ rm\ \lbrack char\ T\ mathalpha\rbrack\lbrack char\ N\ mathalpha\rbrack\lbrack char\ O\ mathalpha\rbrack\lbrack char\ M\ mathalpha\rbrack\rbrack})^{2}} \right) \right.} & \\
\end{array}\]

