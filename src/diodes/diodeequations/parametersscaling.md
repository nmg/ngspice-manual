# Parameters Scaling

Model parameters are scaled using the unit-less parameters **area** and
**pj** and the multiplier **m** as depicted
below:

\(AREA_{eff} = {\lbrack font\ rm\ \lbrack char\ A\ mathalpha\rbrack\lbrack char\ R\ mathalpha\rbrack\lbrack char\ E\ mathalpha\rbrack\lbrack char\ A\ mathalpha\rbrack\rbrack} m\)

\(PJ_{eff} = {\lbrack font\ rm\ \lbrack char\ P\ mathalpha\rbrack\lbrack char\ J\ mathalpha\rbrack\rbrack} m\)

\(IS_{eff} = {\lbrack font\ rm\ \lbrack char\ I\ mathalpha\rbrack\lbrack char\ S\ mathalpha\rbrack\rbrack} AREA_{eff} + {\lbrack font\ rm\ \lbrack char\ J\ mathalpha\rbrack\lbrack char\ S\ mathalpha\rbrack\lbrack char\ W\ mathalpha\rbrack\rbrack} PJ_{eff}\)

\(IBV_{eff} = {\lbrack font\ rm\ \lbrack char\ I\ mathalpha\rbrack\lbrack char\ B\ mathalpha\rbrack\lbrack char\ V\ mathalpha\rbrack\rbrack} AREA_{eff}\)

\(IK_{eff} = {\lbrack font\ rm\ \lbrack char\ I\ mathalpha\rbrack\lbrack char\ K\ mathalpha\rbrack\rbrack} AREA_{eff}\)

\(IKR_{eff} = {\lbrack font\ rm\ \lbrack char\ I\ mathalpha\rbrack\lbrack char\ K\ mathalpha\rbrack\lbrack char\ R\ mathalpha\rbrack\rbrack} AREA_{eff}\)

\(CJ_{eff} = {\lbrack font\ rm\ \lbrack char\ C\ mathalpha\rbrack\lbrack char\ J\ mathalpha\rbrack\lbrack char\ 0\ mathalpha\rbrack\rbrack} AREA_{eff}\)

\(CJP_{eff} = {\lbrack font\ rm\ \lbrack char\ C\ mathalpha\rbrack\lbrack char\ J\ mathalpha\rbrack\lbrack char\ P\ mathalpha\rbrack\rbrack} PJ_{eff}\)

