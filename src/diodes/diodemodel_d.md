# Diode Model \(D\)

The dc characteristics of the diode are determined by the parameters
**is** and **n**. An ohmic resistance, **rs**, is included. Charge
storage effects are modeled by a transit time, **tt**, and a nonlinear
depletion layer capacitance that is determined by the parameters
**cjo**, **vj**, and **m**. The temperature dependence of the saturation
current is defined by the parameters **eg**, the energy, and **xti**,
the saturation current temperature exponent. The nominal temperature
where these parameters were measured is **tnom**, which defaults to the
circuit-wide value specified on the .options control line. Reverse
breakdown is modeled by an exponential increase in the reverse diode
current and is determined by the parameters **bv** and **ibv** (both of
which are positive numbers).

