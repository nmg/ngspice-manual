# Diode Equations

The junction diode is the basic semiconductor device and the simplest
one in ngspice, but its model is quite complex, even when not all the
physical phenomena affecting a pn junction are handled. The diode is
modeled in three different regions:

  - *Forward bias*: the anode is more positive than the cathode, the
    diode is \`on' and can conduct large currents. To avoid convergence
    problems and unrealistic high current, it is prudent to specify a
    series resistance to limit current with the **rs** model parameter.
  - *Reverse bias*: the cathode is more positive than the anode and the
    diode is \`off'. A reverse bias diode conducts a small leakage
    current.
  - *Breakdown*: the breakdown region is modeled only if the **bv**
    model parameter is given. When a diode enters breakdown the current
    increases exponentially (remember to limit it); **bv** is a positive
    value.

