# ngspice optimizer using tclspice

ngspice offers another scripting capability, namely the tcl/tk based
tclspice option (see Chapt. [20](#chap_TCLspice)). An optimization
procedure may be written using a tcl script. An example is provided in
Chapt. [20.5.2](#subsec_Optimization_of_a).

