# ngspice optimizer using a Python script

Werner Hoch has developed a ngspice optimization procedure based on the
'differential evolution' algorithm \[[21](#LyXCite-key_21)\]. On his
[web page](http://www.h-renrew.de/h/python_spice/optimisation.html) he
provides a Python script containing the control flow and algorithms.

