# amp3.sp

This is the basic circuit description. Entries like \#LM2\# are
ASCO-specific, defined in the \# Parameters \# section of file amp3.cfg.
ASCO will replace these parameter placeholders with real values for
simulation, determined by the optimization algorithm. The .control ...
.endc section is specific to ngspice. Entries to this section may
deliver workarounds of some commands not available in ngspice, but used
in other simulators. You may also define additional measurements, get
access to variables and vectors, or define some data manipulation. In
this example the .control section contains an op measurement, required
later for slew rate calculation, as well as the ac simulation, which has
to occur before any further data evaluation. Data from the op simulation
are stored in a plot op1. Its name is saved in variable dt. The ac
measurements sets another plot ac1. To retrieve op data from the former
plot, you have to use the {$dt}.\<vector\> notation (see file
/extract/amp3\_slew\_rate).

