# amp3.cfg

This file contains all configuration data for this optimization. Of
special interest is the following section, which sets the required
measurements and the constraints on the measured parameters:

\# Measurements \#

ac\_power:VDD:MIN:0

dc\_gain:VOUT:GE:122

unity\_gain\_frequency:VOUT:GE:3.15E6

phase\_margin:VOUT:GE:51.8

phase\_margin:VOUT:LE:70

amp3\_slew\_rate:VOUT:GE:0.777E6

\#

Each of these entries is linked to a file in the /extract subdirectory,
having exactly the same names as given here, e.g. ac\_power, dc\_gain,
unity\_gain, phase\_margin, and amp3\_slew\_rate. Each of these files
contains an \# Info \# section, which is currently not used. The \#
Commands \# section may contain a measurement command (including ASCO
parameter \#SYMBOL\#, see file /extract/unity\_gain\_frequency). It also
may contain a .control section (see file /extract/phase\_margin\_min).
During set-up \#SYMBOL\# is replaced by the file name, a leading \`z',
and a trailing number according to the above sequence, starting with 0.

