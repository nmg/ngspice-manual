# Class-E power amplifier

This example is taken from Chapt. 6.2.3 Tutorial \#3 from the ASCO
manual. In this example the ASCO post processing is applied in file
extract/P\_OUT (see Chapt. 7.4 of the ASCO manual.). In this example
.measure statements are used. They allow using parameters from .param
statements, because they will be located outside of .control sections,
but do not allow to do data post processing inside of ngspice. You may
use ASCO post processing instead.

