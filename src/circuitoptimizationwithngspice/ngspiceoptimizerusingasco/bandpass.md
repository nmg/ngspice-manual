# Bandpass

This example is taken from Chapt. 6.2.4 Tutorial \#4 from the ASCO
manual. S11 in the passband is to be maximised. S21 is used to extract
side lobe parameters. The .net command is not available in ngspice, so
S11 and S21 are derived with a script in file bandpass.sp as described
in Chapt. [17.9](#sec_Scattering_parameters). The measurements requested
in bandpass.cfg as

\# Measurements \#

Left\_Side\_Lobe:---:LE:-20

Pass\_Band\_Ripple:---:GE:-1

Right\_Side\_Lobe:---:LE:-20

S11\_In\_Band:---:MAX:---

\#

are realized as 'measure' commands inside of control sections (see files
in directory extract). The result of a measure statement is a vector,
which may be processed by commands in the following lines. In file
extract/S1\_In\_Band \#Symbol\# is made available only after a short
calculation (inversion of sign), using the print command. quit has been
added to this entry because it will become the final control section in
\<computer-name\>.sp. A disadvantage of measure inside of a .control
section is that parameters from .param statements may not be used (as is
done in example [23.5.4](#subsec_Class_E_power_amplifier)).

The bandpass example includes the calculation of RF parasitic elements
defined in rfmodule.cfg (see Chapt. 7.5 of the ASCO manual). This
calculation is invoked by setting

ExecuteRF:yes $Execute or no the RF module to add RF parasitics?

in bandpass.cfg. The two subcircuits LBOND\_sub and CSMD\_sub are
generated in \<computer-name\>.sp to simulate these effects.

