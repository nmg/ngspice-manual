# Testing the set-up

Copy asco-test.exe and ngspice\_c.exe (console executable of ngspice)
into the directory, and run

$ asco-test -ngspice amp3

from the console window. Several files will be created during checking.
If you look at \<computer-name\>.sp: this is the input file for
ngspice\_c, generated by ASCO. You will find the additional .measure
commands and .control sections. The quit command will be added
automatically just before the .endc command in its own .control section.
asco-test will display error messages on the console, if the simulation
or communication with ASCO is not ok. The output file
\<computer-name\>.out, generated by ngspice during each simulation,
contains symbols like zac\_power0, zdc\_gain1, zunity\_gain\_frequency2,
zphase\_margin3, zphase\_margin4, and zamp3\_slew\_rate5. These are used
to communicate the ngspice output data to ASCO. ASCO is searching for
something like zdc\_gain1 =, and then takes the next token as the input
value. Calling phase\_margin twice in amp3.cfg has lead to two
measurements in two .control sections with different symbols
(zphase\_margin3, zphase\_margin4).

A failing test may result in an error message from ASCO. Sometimes,
however, ASCO freezes after some output statements. This may happen if
ngspice issues an error message that cannot be handled by ASCO. Here it
may help calling ngspice directly with the input file generated by ASCO:

$ ngspice\_c \<computer-name\>.sp

Thus you may evaluate the ngspice messages directly.

