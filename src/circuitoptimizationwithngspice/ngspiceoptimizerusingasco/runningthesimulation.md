# Running the simulation

Copy (w)asco.exe, (w)asco-mpi.exe and ngspice\_c.exe (console executable
of ngspice) into the directory, and run

$ asco -ngspice amp3

or alternatively (if MPICH is installed)

$ mpiexec -n 7 asco-mpi -ngspice amp3

The following graph [23.1](#fig_Optimization_speed) shows the
acceleration of the optimization simulation on a multi-core processor
(i7 with 4 real or 8 virtual cores), 500 generations, if -n is varied.
Speed is tripled, a mere 15 min suffices to optimize 21 parameters of
the amplifier.

![image:
8\_home\_nmg\_Documents\_gitroot\_ngspice-ngspice-manuals\_Images\_optim-speed.png](8_home_nmg_Documents_gitroot_ngspice-ngspice-manuals_Images_optim-speed.png)
Figure 23.1:  Optimization speed

