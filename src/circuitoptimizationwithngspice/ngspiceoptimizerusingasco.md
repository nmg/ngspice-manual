# ngspice optimizer using ASCO

The [ASCO optimizer](http://asco.sourceforge.net/index.html), developed
by Joao Ramos, also applies the 'differential evolution' algorithm
\[[21](#LyXCite-key_21)\]. An enhanced version 0.4.7.1, adding ngspice
as a simulation engine, may be downloaded
[here](http://ngspice.sourceforge.net/optimizers/asco-dist.7z) (7z
archive format). Included are executable files (asco, asco-mpi,
ngspice-c for MS Windows). The source code should also compile and
function under Linux (not yet tested).

ASCO is a standalone executable, which communicates with ngspice via
ngspice input and output files. Several optimization examples,
originally provided by J. Ramos for other simulators, are prepared for
use with ngspice. Parallel processing on a multi-core computer has been
tested using MPI
([MPICH2](http://www.mcs.anl.gov/research/projects/mpich2/)) under MS
Windows. A processor network will be supported as well. A MS Windows
console application ngspice\_c.exe is included in the archive. Several
stand alone tools are provided, but not tested yet.

Setting up an optimization project with ASCO requires advanced know-how
of using ngspice. There are several sources of information. First of all
the examples provided with the distribution give hints how to start with
ASCO. The original ASCO manual is provided as well, or is available
[here](http://asco.sourceforge.net/manual.html). It elaborates on the
examples, using a commercial simulator, and provides a detailed
description how to set up ASCO. Installation of ASCO and MPI (under
Windows) is described in a file INSTALL.

Some remarks on how to set up ASCO for ngspice are given in the
following sections (more to be added). These a meant not as a complete
description, but are an addition the the ASCO manual.

