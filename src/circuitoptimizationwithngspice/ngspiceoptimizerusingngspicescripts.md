# ngspice optimizer using ngspice scripts

Friedrich Schmidt (see [his web
site](http://members.aon.at/fschmid7/page_2_1.html)) has intensively
used circuit optimization during his development of Nonlinear loadflow
computation with Spice based simulators. He has provided an optimizer
using the internal ngspice scripting language (see Chapt.
[17.8](#sec_SCRIPTS)). His original scripts are found
[here](http://members.aon.at/fschmid7/examples_new.zip). A slightly
modified and concentrated set of his scripts is available from the
[ngspice optimizer
directory](http://ngspice.sourceforge.net/optimizers/ngspice-optimizer.7z).

The simple example given in the scripts is ok with current ngspice. Real
circuits have still to be tested.

