# Model by Statz e.a.

The MESFET model **level 1** is derived from the GaAs FET model of Statz
et al. as described in \[[11](#LyXCite-key_11)\]. The dc characteristics
are defined by the parameters VTO, B, and BETA, which determine the
variation of drain current with gate voltage, ALPHA, which determines
saturation voltage, and LAMBDA, which determines the output conductance.
The formula are given by:

\[\begin{array}{ll}
{I_{d} = \begin{cases}
{\frac{B\left( V_{gs} - V_{T})^{2} \right.}{1 + b\left( {V_{gs} - V_{T}} \right)}\left| {1 - \left| {1 - A\frac{V_{ds}}{3}} \right|^{3}} \right|\left( {1 + LV_{ds}} \right)} & {{for}0 < V_{ds} < \frac{3}{A}} \\
{\frac{B\left( V_{gs} - V_{T})^{2} \right.}{1 + b\left( {V_{gs} - V_{T}} \right)}\left( {1 + LV_{ds}} \right)} & {{for}V > \frac{3}{A}} \\
\end{cases}} & \\
\end{array}\]

Two ohmic resistances, **rd** and **rs**, are included. Charge storage
is modeled by total gate charge as a function of gate-drain and
gate-source voltages and is defined by the parameters **cgs**, **cgd**,
and **pb**.

<table style="width:100%;">
<colgroup>
<col width="16%" />
<col width="16%" />
<col width="16%" />
<col width="16%" />
<col width="16%" />
<col width="16%" />
</colgroup>
<tbody>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-11564"></span>Name
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11567"></span>Parameter
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11570"></span>Units
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11573"></span>Default
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11576"></span>Example
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11579"></span>Area
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-11582"></span>VTO
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11585"></span>Pinch-off voltage
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11588"></span><span class="math inline"><em>V</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11591"></span>-2.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11594"></span>-2.0
</div></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-11600"></span>BETA
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11603"></span>Transconductance parameter
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11606"></span><span class="math inline">$\frac{A}{V^{2}}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11609"></span>1.0e-4
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11612"></span>1.0e-3
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11615"></span>*
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-11618"></span>B
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11621"></span>Doping tail extending parameter
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11624"></span><span class="math inline">$\frac{1}{V}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11627"></span>0.3
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11630"></span>0.3
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11633"></span>*
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-11636"></span>ALPHA
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11639"></span>Saturation voltage parameter
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11642"></span><span class="math inline">$\frac{1}{V}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11645"></span>2
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11648"></span>2
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11651"></span>*
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-11654"></span>LAMBDA
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11657"></span>Channel-length modulation parameter
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11660"></span><span class="math inline">$\frac{1}{V}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11663"></span>0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11666"></span>1.0e-4
</div></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-11672"></span>RD
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11675"></span>Drain ohmic resistance
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11678"></span><span class="math inline"><em>Ω</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11681"></span>0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11684"></span>100
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11687"></span>*
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-11690"></span>RS
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11693"></span>Source ohmic resistance
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11696"></span><span class="math inline"><em>Ω</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11699"></span>0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11702"></span>100
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11705"></span>*
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-11708"></span>CGS
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11711"></span>Zero-bias G-S junction capacitance
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11714"></span><span class="math inline"><em>F</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11717"></span>0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11720"></span>5pF
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11723"></span>*
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-11726"></span>CGD
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11729"></span>Zero-bias G-D junction capacitance
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11732"></span><span class="math inline"><em>F</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11735"></span>0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11738"></span>1pF
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11741"></span>*
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-11744"></span>PB
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11747"></span>Gate junction potential
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11750"></span><span class="math inline"><em>V</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11753"></span>1
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11756"></span>0.6
</div></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-11762"></span>KF
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11765"></span>Flicker noise coefficient
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11768"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11771"></span>0
</div></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-11780"></span>AF
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11783"></span>Flicker noise exponent
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11786"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11789"></span>1
</div></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-11798"></span>FC
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11801"></span>Coefficient for forward-bias depletion capacitance formula
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11804"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11807"></span>0.5
</div></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

Device instance:

``` listings
z1 2 3 0 mesmod area=1.4
```

Model:

``` listings
.model mesmod nmf level=1 rd=46 rs=46 vt0=-1.3 
+ lambda=0.03 alpha=3 beta=1.4e-3
```

