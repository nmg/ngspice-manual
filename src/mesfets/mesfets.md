# MESFETs

General form:

``` listings
ZXXXXXXX ND NG NS MNAME <AREA> <OFF> <IC=VDS, VGS>
```

Examples:

``` listings
Z1 7 2 3 ZM1 OFF
```

