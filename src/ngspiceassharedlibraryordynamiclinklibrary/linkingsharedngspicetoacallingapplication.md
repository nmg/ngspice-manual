# Linking shared ngspice to a calling application

Basically there are two methods (as with all \*.so, \*.dll libraries).
The caller may link to a (small) library file during compiling/linking,
and then immediately search for the shared library upon being started.
It is also possible to dynamically load the ngspice shared library at
runtime using the dlopen/LoadLibrary mechanisms.

