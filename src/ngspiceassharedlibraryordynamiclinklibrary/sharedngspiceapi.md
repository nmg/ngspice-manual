# Shared ngspice API

The sources for the ngspice shared library API are contained in a single
C file (sharedspice.c) and a corresponding header file sharedspice.h.
The type and function declarations are contained in sharedspice.h, which
may be directly added to the calling application, if written in C or
C++.

