# Pointers to callback functions \(details see \[19.3.3\]\(#subsec_Callback_functions\)\):

  - GetVSRCData\*  
    callback function for retrieving a voltage source value from caller
    (NULL allowed)
  - GetISRCData\*  
    callback function for retrieving a current source value from caller
    (NULL allowed)
  - GetSyncData\*  
    callback function for synchronization (NULL allowed)

