# typedef int \(GetISRCData\)\(double\*, double, char\*, int, void\*\)

  - double\*  
    return current value
  - double  
    actual time
  - char\*  
    node name
  - int  
    identification number of calling ngspice shared lib
  - void\*  
    return pointer received from caller

Ask for ISRC EXTERNAL value. The independent current source (see Chapt.
[4.1](#sec_Independent_Sources_for)) with EXTERNAL option allows to set
a current value to the node defined by the netlist and named here at the
time returned by the
simulator.

