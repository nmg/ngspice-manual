# Parallel ngspice example

A first
[example](http://ngspice.sourceforge.net/ngspice-shared-lib/ngspice_sync_win.7z)
is available as a compacted 7z archive. It contains the source code of a
controlling application, as well as its compiled executable and
ngspice.dll (for MS Windows). As the input circuit an inverter chain has
been divided into three parts. Three ngspice shared libraries are
loaded, each simulates one partition of the circuit. Interconnections
between the partitions are provided via a callback function. The
simulation time is synchronized among the three ngspice invocations by
another callback function.

