# typedef int \(GetVSRCData\)\(double\*, double, char\*, int, void\*\)

  - double\*  
    return voltage value
  - double  
    actual time
  - char\*  
    node name
  - int  
    identification number of calling ngspice shared lib
  - void\*  
    return pointer received from caller

Ask for a VSRC EXTERNAL voltage value. The independent voltage source
(see Chapt. [4.1](#sec_Independent_Sources_for)) with EXTERNAL option
allows to set a voltage value to the node defined in the netlist and
named here at the time returned by the
simulator.

