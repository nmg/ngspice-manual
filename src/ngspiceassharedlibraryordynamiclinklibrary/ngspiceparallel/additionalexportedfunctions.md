# Additional exported functions

The following functions (exported or callback) are designed to support
the parallel action of several ngspice invocations. They may be useful,
however, also when only a single library is loaded into a caller, if you
want to use external voltage or current sources or 'play' with advancing
simulation
time.

