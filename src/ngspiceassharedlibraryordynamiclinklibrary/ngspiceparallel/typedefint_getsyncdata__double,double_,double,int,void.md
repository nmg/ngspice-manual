# typedef int \(GetSyncData\)\(double, double\*, double, int, void\*\)

  - double  
    actual time (ckt-\>CKTtime)
  - double\*  
    delta time (ckt-\>CKTdelta)
  - double  
    old delta time (olddelta)
  - int  
    identification number of calling ngspice shared lib
  - int  
    location of call for synchronization in dctran.c
  - void\*  
    return pointer received from caller

Ask for new delta time depending on synchronization requirements. See
[19.6.1](#subsec_Go_parallel_) for an explanation.

