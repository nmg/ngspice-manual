# bool ngSpice\_SetBkpt\(double\)

Sets a breakpoint in ngspice, a time point that the simulator is
enforced to hit during the transient simulation. After the breakpoint
time has been hit, the next delta time starts with a small value and is
ramped up again. A breakpoint should be set only when the background
thread in ngspice is not running (before the simulation has started, or
after the simulation has been paused by bg\_halt). The time sent to
ngspice should be larger than the current time (which is either 0 before
start or given by the callback **GetSyncData**
([19.6.3.3](#subsec_typedef_int__GetSyncData__double)). Several
breakpoints may be
set.

