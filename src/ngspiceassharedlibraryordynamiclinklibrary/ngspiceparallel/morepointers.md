# More pointers

  - int\*  
    pointer to integer unique to this shared library (defaults to 0)
  - void\*  
    pointer to user-defined data, will not be modified, but handed over
    back to caller during Callback, e.g. address of calling object. If
    NULL is sent here, userdata info from ngSpice\_Init() will be kept,
    otherwise userdata will be overridden by new value from here.

