# MS Visual Studio

Compilation is similar to what has been described in Chapt.
[32.2.1](#subsec_make_ngspice_with_1). However, there is a dedicated
project file coming with the source code to generate ngspice.dll. Go to
the directory **visualc** and start the project with double clicking on
**sharedspice.vcxproj**.

