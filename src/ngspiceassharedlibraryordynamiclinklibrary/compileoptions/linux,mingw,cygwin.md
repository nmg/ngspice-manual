# Linux, MINGW, CYGWIN

Compilation is done as described in Chapts.
[32.1](#sec_Ngspice_Installation_under) or**
**[32.2.2](#subsec_How_to_make)**.** Use the configure option**
--with-ngshared** instead of **--with-x** or **--with-wingui**. In
addition you might add (optionally) **--enable-relpath** to avoid
absolute paths when searching for code models. For MINGW you may edit
compile\_min.sh accordingly and compile using this script in the MSYS2
window.

Other operation systems (Mac OS, BSD, ...) have not been tested so far.
Your input is welcome\!

