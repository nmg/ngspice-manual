# How to get the sources

Currently (as of ngspice-27 being the actual release), you will have to
use the direct loading of the sources from the git repository (see
Chapt. [32.1.2](#subsec_Install_from_Git)).

