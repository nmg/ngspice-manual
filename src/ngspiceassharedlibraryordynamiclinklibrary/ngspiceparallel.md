# ngspice parallel

The following chapter describes an offer to the advanced user and
developer community. If you are interested in evaluating the parallel
and synchronized operation of several ngspice instances, this may be one
way to go. However, no ready to use implementation is available. You
will find a toolbox and some hints how to use it. Parallelization and
synchronization is your task by developing a suitable caller\! And of
course another major input has to come from partitioning the circuit
into suitable, loosely coupled pieces, each with its own netlist, one
netlist per ngspice instance. And you have to define the coupling
between the circuit blocks. Both are not provided by ngspice, but are
again your responsibility. Both are under active research, and the
toolbox described below is an offer to join that research.

