# Loading from file

As with interactive ngspice, you may use the ngspice internal command
source ([17.5.75](#subsec_Source__Read_a)) to load a complete netlist
from a file.

Typical usage:

``` listings
ngSpice_Command("source ../examples/adder_mos.cir");
```

