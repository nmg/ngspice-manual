# Loading line by line

As with interactive ngspice, you may use the ngspice internal command
circbyline ([17.5.11](#subsec_Circbyline___Enter_a)) to send a netlist
line by line to the ngspice circuit parser.

Typical usage:

``` listings
ngSpice_Command("circbyline fail test");
ngSpice_Command("circbyline V1 1 0 1");
ngSpice_Command("circbyline R1 1 0 1");
ngSpice_Command("circbyline .dc V1 0 1 0.1");
ngSpice_Command("circbyline .end");
```

The first line is a title line, which will be ignored during circuit
parsing. As soon as the line .end has been sent to ngspice, circuit
parsing commences.

