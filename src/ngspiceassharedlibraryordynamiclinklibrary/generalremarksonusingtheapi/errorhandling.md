# Error handling

There are several occasions where standard ngspice suffers from an
error, cannot recover internally and then exits. If this is happening to
the shared module this would mean that the parent application, the
caller, is also forced to exit. Therefore (if not suffering from a
segfault) ngspice.dll will call the function controlled\_exit as usual,
this now calls the callback function 'ControlledExit'
([19.3.3.3](#subsec_typedef_int__ControlledExit__int)), which hands over
the request for exiting to the caller. The caller now has the task to
handle the exit code for ngspice.

If ngspice has been linked at runtime by dlopen/LoadLibrary (see
[19.2.2](#subsec_Linking_at_runtime)), the callback may close all
threads, and then detach ngspice.dll by invoking dlclose/FreeLibrary.
The caller may then restart ngspice by another loading and
initialization ([19.3.2.1](#subsec_int_ngSpice_Init_SendChar___Send)).

If ngspice is included during linking the caller (see
[19.2.1](#subsec_Linking_during_creating)), there is not yet a good and
general solution to error handling, if the error is non-recoverable from
inside ngspice.

