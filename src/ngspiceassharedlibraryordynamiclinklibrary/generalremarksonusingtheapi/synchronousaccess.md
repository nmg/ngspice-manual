# Synchronous access

The callback functions **SendInitData**
([19.3.3.5](#subsec_typedef_int__SendInitData__pveci)) and **SendData**
([19.3.3.4](#subsec_typedef_int__SendData__pvecvalue)) allow access to
simulator output data synchronized with the simulation progress.

Each time a new plot is generated during simulation, e.g. when a
sequence of op, ac and tran is used or commands like linearize or fft
are invoked, the callback **SendInitData** is called by ngspice.
Immediately after setting up the vector structure of the new plot, the
function is called once. Its parameter is a pointer to the structure
vecinfoall ([19.3.1](#subsec_structs_and_types)), which contains an
array of structures vecinfo, one for each vector in the actual plot. You
may simply use vecname to get the name of any vector. This time the
vectors are still empty, but pointers to the vector structure are
available.

Each time a new data point (e.g. time value and simulation output
value(s)) is added to the vector structure of the current plot, the
function **SendData** is called by ngspice. This allows you to
immediately access the simulation output synchronized with the
simulation time, e.g. to interface it to a runtime plot or to use it for
some controlled simulation by stopping the simulation based on a
condition, altering parameters and resume the simulation. **SendData**
returns a structure vecvaluesall as parameter, which contains an array
of structures vecvalues, one for each vector.

Some code to demonstrate the callback function usage is referenced below
([19.5](#sec_Example_applications)).

