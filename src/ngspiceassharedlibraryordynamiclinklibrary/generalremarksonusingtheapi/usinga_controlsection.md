# Using a .control section

If the simulation is started with the background thread (command
bg\_run), the .control section commands are executed immediately after
bg\_run has been given, i.e. typically before the simulation has
finished. This often is not very useful because you want to evaluate the
simulation results. If the predefined variable controlswait is set in
.spiceinit or spice.rc, the command execution is delayed until the
background thread has returned (aka the simulation has finished). If set
controlswait is given inside of the .control section, only the commands
following this statement are delayed.

