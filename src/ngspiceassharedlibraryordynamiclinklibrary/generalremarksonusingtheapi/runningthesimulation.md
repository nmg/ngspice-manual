# Running the simulation

The following commands are used to start the simulator in its own
thread, halt the simulation and resume it again. The extra (background)
thread enables the caller to continue with other tasks in the main
thread, e.g. watching its own event loop. Of course you have to take
care that the caller will not exit before ngspice is finished, otherwise
you immediately will loose all data. After having halted the simulator
by suspending the background thread, you may assess data, change ngspice
parameters, or read output data using the caller's main thread, before
you resume simulation using a background thread again. While the
background thread is running, ngspice will reject any other command sent
by ngSpice\_Command.

Typical usage:

``` listings
ngSpice_Command("bg_run");
...
ngSpice_Command("bg_halt");
...
ngSpice_Command("bg_resume");
```

Basically you may send the commands 'run' or 'resume' (no prefix bg\_),
starting ngspice within the main thread. The caller then has to wait
until ngspice returns from simulation. A command 'halt' is not available
then.

After simulation is finished (test with callback
[19.3.3.6](#subsec_typedef_int__BGThreadRunning__bo)), you may send
other commands from Chapt. [17.5](#sec_Commands), emulating any .control
script. These commands are executed in the main thread, which should be
okay because execution time is typically short.

