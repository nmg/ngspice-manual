# Altering model or device parameters

After halting ngspice by stopping the background thread
([19.4.2](#subsec_Running_the_simulation)), nearly all ngspice commands
are available. Especially **alter** ([17.5.3](#subsec_Alter___Change_a))
and **altermod** ([17.5.4](#subsec_Altermod___Change_a)) may be used to
change device or model parameters. After the modification, the
simulation may be resumed immediately. Changes to a circuit netlist,
however, are not possible. You would need to load a complete new netlist
([19.4.1](#subsec_Loading_a_netlist)) and restart the simulation from
the beginning.

