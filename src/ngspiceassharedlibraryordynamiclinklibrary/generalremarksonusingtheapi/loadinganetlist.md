# Loading a netlist

Basically the input to shared ngspice is the same as if you would start
a ngspice batch job, e.g. you enter a netlist and the simulation command
(any .dot analysis command like .tran, .op, or .dc etc. as found in
Chapt. [15.3](#sec_Analyses)), as well as suitable options.

Typically you should **not** include a .control section in your input
file. Any script described in a .control section for standard ngspice
should better be emulated by the caller and be sent directly to
ngspice.dll. Start the simulation according to Chapt.
[19.4.2](#subsec_Running_the_simulation) in an extra thread.

As an alternative, only the netlist has to be entered (without analysis
command), then you may use any interactive command as listed in Chapt.
[17.5](#sec_Commands) (except for the plot command).

However, for users without direct access to source code commands (e.g.
KiCad users), it might be advantageous to add a .control section to
their netlist simulation dot commands. please be careful and check for
chapter [19.4.1.4](#subsec_Using_a__control).

The \`typical usage' examples given below are part of a caller written
in C.

