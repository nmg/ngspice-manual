# XSPICE event node data

After starting the simulation, in a first step the callback function
**SendInitEvtData** is called once for each event node. All nodes are
numbered in ascending order. The first function argument is the actual
node number, the second sets the total amount of nodes, then node name
and node type follow. You may set up an array to store name and type,
indexed by the node number.

During simulation, after each time step ngspice checks if a node has
changed. If so, **SendEvtData** is called for each node that changed,
returning the simulation time, the node number, and the node value as a
char\* string, consisting of one out of 0s, 1s, Us, 0r, 1r, Ur, 0z, 1z,
Uz, 0u, 1u, Uu (see [12.5.1](#subsec_Digital_Node_Type)). The double
real value and the void\* binary data structure arguments are for future
enhancements of the data interface. The int mode returns 0 for op, 1 for
dc, 2 for ac , and 3 for tran simulation. The final int is useful to
identify the ngspice lib by number if you run several in parallel (see
[19.6](#sec_ngspice_parallel)). The final \*void just returns the
pointer received from caller. e.g. to identify the calling object.

