# Asynchronous access

During simulation, while the background thread is running, or after it
is finished, you may use the functions **ngSpice\_CurPlot**
([19.3.2.7](#subsec_char__ngSpice_CurPlot_void_)), **ngSpice\_AllPlots**
([19.3.2.8](#subsec_char___ngSpice_AllPlots_void_)),
**ngSpice\_AllVecs** ([19.3.2.9](#subsec_char___ngSpice_AllVecs_char__))
to retrieve information about vectors available, and function
**ngGet\_Vec\_Info**
([19.3.2.5](#subsec_pvector_info_ngGet_Vec_Info_char)) to obtain data
from a vector and its corresponding scale vector. The timing of the
caller and the simulation progress are independent from each other and
not synchronized.

Again some code to demonstrate the callback function usage is referenced
below ([19.5](#sec_Example_applications)).

