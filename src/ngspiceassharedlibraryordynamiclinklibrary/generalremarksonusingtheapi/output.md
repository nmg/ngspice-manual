# Output

After the simulation is finished, use the ngspice commands **write**
([17.5.93](#subsec_Write__Write_data)) or **wrdata**
([17.5.92](#subsec_Wrdata__Write_data)) to output data to a file as
usual, use the **print** command
([17.5.50](#subsec_Print__Print_values)) to retrieve data via callback
**SendChar** ([19.3.3.1](#subsec_typedef_int__SendChar__char__)), or
refer to accessing the data as described in Chapt.
[19.4.3](#subsec_Accessing_data).

Typical usage:

``` listings
ngSpice_Command("write testout.raw V(2)");
ngSpice_Command("print V(2)");
```

