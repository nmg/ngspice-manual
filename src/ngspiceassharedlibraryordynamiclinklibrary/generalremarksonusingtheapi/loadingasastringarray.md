# Loading as a string array

Typical usage:

``` listings
circarray = (char**)malloc(sizeof(char*) * 7);
circarray[0] = strdup("test array");
circarray[1] = strdup("V1 1 0 1");
circarray[2] = strdup("R1 1 2 1");
circarray[3] = strdup("C1 2 0 1 ic=0");
circarray[4] = strdup(".tran 10u 3 uic");
circarray[5] = strdup(".end");
circarray[6] = NULL;
ngSpice_Circ(circarray);
```

An array of char pointers is malloc'd, each netlist line is then copied
to the array. strdup will care for the memory allocation. The first
entry to the array is a title line, the last entry has to contain NULL.
ngSpice\_Circ(circarray); sends the array to ngspice, where circuit
parsing is started immediately. Don't forget to free the array after
sending it, to avoid a memory leak.

