# Loading at runtime

dlopen (Linux) or LoadLibrary (MS Windows) will load libngspice.so or
ngspice.dll into the address space of the caller at runtime. The
functions return a handle that may be used to acquire the pointers to
the functions exported by libngspice.so. Detaching ngspice at runtime is
equally possible (using dlclose/FreeLibrary), after the background
thread has been stopped and all callbacks have returned.

