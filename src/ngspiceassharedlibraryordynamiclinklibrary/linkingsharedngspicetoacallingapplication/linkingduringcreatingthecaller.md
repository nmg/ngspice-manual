# Linking during creating the caller

While creating the ngspice shared lib, not only the \*.so (\*.dll) file
is created, but also a small library file, which just includes
references to the exported symbols. Depending on the OS, these may be
called libngspice.dll.a, ngspice.lib. Linux and MINGW also allow linking
to the shared object itself. The shared object is not included into the
executable component but is tied to the execution.

