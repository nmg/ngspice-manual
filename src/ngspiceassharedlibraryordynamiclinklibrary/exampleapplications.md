# Example applications

Three executables (coming with source code) serve as examples for
controlling ngspice. These are not meant to be \`production' programs,
but just give some commented example usages of the interface.

ng\_start.exe is a MS Windows application loading ngspice.dll
dynamically. All functions and callbacks of the interface are assessed.
The source code, generated with Turbo Delphi 2006, may be found
[here](http://ngspice.sourceforge.net/ngspice-shared-lib/ng_dll_src_delphi.7z),
the binaries compiled for 32 Bit are
[here](http://ngspice.sourceforge.net/ngspice-shared-lib/ngspice-sh_bin_win32.7z).

Two console applications, compilable with Linux, CYGWIN, MINGW or MS
Visual Studio, are available
[here](http://ngspice.sourceforge.net/ngspice-shared-lib/ngspice_cb.7z),
demonstrating either linking upon start-up or loading shared ngspice
dynamically at runtime. A simple feedback loop is shown in tests 3 and
4, where a device parameter is changed upon having an output vector
value crossing a limit.

An XSPICE event node example may be assessed at
ngspice/visualc/ng\_shared\_xspice\_v, currently tested only with MS
Windows and compiled with Visual Studio.

