# Callback functions

Callback functions are a means to return data from ngspice to the
caller. These functions are defined as global functions in the caller,
so to be reachable by the C-coded ngspice. They are declared according
to the typedefs given below. ngspice receives their addresses from the
caller upon initialization with the ngSpice\_Init(...) function (see
[19.3.2.1](#subsec_int_ngSpice_Init_SendChar___Send)). If the caller
will not make use of a callback, it may send NULL instead of the address
(except for ControlledExit, which is always required).

If XSPICE is enabled, additional callback functions are made accessible
by ngSpice\_Init\_Evt(...) to obtain digital event node data.

If ngspice is run in the background thread
([19.4.2](#subsec_Running_the_simulation)), the callback functions
(defined in the caller) also are called from within that thread. One has
to be carefully judging how this behavior might influence the caller,
where now you have the primary and the background thread running in
parallel. So make the callback function thread safe. The integer
identification number is only used if you run several shared libraries
in parallel (see Chapt. [19.6](#sec_ngspice_parallel)). Three additional
callback function are described in Chapt.
[19.6.3](#subsec_Callback_functions_1).

