# structs and types defined for transporting data

pvector\_info is returned by the exported function ngGet\_Vec\_Info (see
[19.3.2.5](#subsec_pvector_info_ngGet_Vec_Info_char)). Addresses of the
vector name, type, real or complex data are transferred and may be read
asynchronously during or after the simulation.

vector\_info

``` listings
typedef struct vector_info {
    char *v_name;           /* Same as so_vname */
    int v_type;             /* Same as so_vtype */
    short v_flags;          /* Flags (a combination of VF_*) */
    double *v_realdata;     /* Real data */
    ngcomplex_t *v_compdata;/* Complex data */
    int v_length;           /* Length of the vector */
} vector_info, *pvector_info;
```

The next two structures are used by the callback function SendInitData
(see [19.3.3.5](#subsec_typedef_int__SendInitData__pveci)). Each time a
new plot is generated during simulation, e.g. when a sequence of op, ac
or tran is used, or commands like linearize or fft are invoked, the
function is called once by ngspice. Among its parameters you find a
pointer to a struct vecinfoall, which includes an array of vecinfo, one
for each vector. Pointers to the struct dvec, containing the vector, are
included.

vecinfo

``` listings
typedef struct vecinfo
{
   int number;       /* number of vector, as position in the
                        linked list of vectors, starts with 0 */
   char *vecname;    /* name of the actual vector */
   bool is_real;     /* TRUE if the actual vector has real data */
   void *pdvec;      /* a void pointer to struct dvec *d, the
                        actual vector */
   void *pdvecscale; /* a void pointer to struct dvec *ds,
                        the scale vector */
} vecinfo, *pvecinfo;
```

vecinfoall

``` listings
typedef struct vecinfoall
{
    /* the plot */
    char *name;
    char *title;
    char *date;
    char *type;
    int veccount;

    /* the data as an array of vecinfo with 
       length equal to the number of vectors 
       in the plot */
    pvecinfo *vecs; 

} vecinfoall, *pvecinfoall;
```

The next two structures are used by the callback function SendData (see
[19.3.3.4](#subsec_typedef_int__SendData__pvecvalue)). Each time a new
data point (e.g. time value and simulation output value(s)) is added to
the vector structure of the current plot, the function **SendData** is
called by ngspice, among its parameters the actual pointer
pvecvaluesall, which contains an array of pointers to pvecvalues, one
for each vector.

vecvalues

``` listings
typedef struct vecvalues {
    char* name;      /* name of a specific vector */
    double creal;    /* actual data value */
    double cimag;    /* actual data value */
    bool is_scale;   /* if 'name' is the scale vector */
    bool is_complex; /* if the data are complex numbers */
} vecvalues, *pvecvalues;
```

Pointer vecvaluesall to be found as parameter to callback function
**SendData.**

vecvaluesall

``` listings
typedef struct vecvaluesall {
    int veccount;      /* number of vectors in plot */
    int vecindex;      /* index of actual set of vectors, i.e. 
                          the number of accepted data points */
    pvecvalues *vecsa; /* values of actual set of vectors, 
                          indexed from 0 to veccount - 1 */
} vecvaluesall, *pvecvaluesall;
```

