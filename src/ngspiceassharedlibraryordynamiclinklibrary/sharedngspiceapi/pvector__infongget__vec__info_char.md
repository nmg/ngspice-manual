# pvector\_info ngGet\_Vec\_Info\(char\*\)

uses the name of a vector (may be in the form 'vectorname' or
\<plotname\>.vectorname) as parameter and returns a pointer to a
vector\_info struct. The caller may then directly assess the vector data
(but better should not modify them).

