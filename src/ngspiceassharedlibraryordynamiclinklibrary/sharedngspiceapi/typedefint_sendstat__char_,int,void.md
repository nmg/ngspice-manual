# typedef int \(SendStat\)\(char\*, int, void\*\)

  - char\*  
    simulation status and value (in percent) to be sent to caller
  - int  
    identification number of calling ngspice shared lib (default is 0,
    see Chapt. [19.6](#sec_ngspice_parallel))
  - void\*  
    return pointer received from caller

sending simulation status to caller, e.g. the string tran
34.5%.

