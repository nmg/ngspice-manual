# typedef int \(BGThreadRunning\)\(bool, int, void\*\)

  - bool  
    true if background thread is running
  - int  
    identification number of calling ngspice shared lib (default is 0,
    see Chapt. [19.6](#sec_ngspice_parallel))
  - void\*  
    return pointer received from caller

indicate if background thread is
running

