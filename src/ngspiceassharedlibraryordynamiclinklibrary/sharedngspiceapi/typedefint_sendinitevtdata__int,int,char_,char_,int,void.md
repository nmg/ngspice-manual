# typedef int \(SendInitEvtData\)\(int, int, char\*, char\*, int, void\*\)

  - int  
    node index
  - int  
    maximum node index, number of nodes
  - char\*  
    node name
  - char\*  
    udn-name, node type
  - int  
    identification number of calling ngspice shared lib
  - void\*  
    return pointer received from caller

Upon initialization, called once per event node to build up a dictionary
of nodes.

