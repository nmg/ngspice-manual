# int ngSpice\_Init\(SendChar\*, SendStat\*, ControlledExit\*, SendData\*, SendInitData\*, BGThreadRunning\*, void\)

After caller has loaded ngspice.dll, the simulator has to be initialized
by calling ngSpice\_Init(...). Address pointers of several callback
functions (see [19.3.3](#subsec_Callback_functions)), which are to be
defined in the caller, are sent to ngspice.dll. The int return value is
not
used.

