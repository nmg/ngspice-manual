# int ngSpice\_Circ\(char\*\*\)

sends an array of null-terminated char\* to ngspice.dll. Each char\*
contains a single line of a circuit (Each line is like it is found in an
input file \*.sp.). The last entry to char\*\* has to be NULL. Upon
receiving the array, ngspice.dll will immediately parse the input and
set up the circuit structure (as if the circuit is loaded from a file by
the 'source' command). The function returns a '1' upon error, otherwise
'0'.

