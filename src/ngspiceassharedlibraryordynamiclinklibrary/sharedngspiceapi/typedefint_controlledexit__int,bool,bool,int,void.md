# typedef int \(ControlledExit\)\(int, bool, bool, int, void\*\)

  - int  
    exit status
  - bool  
    if true: immediate unloading dll, if false: just set flag, unload is
    done when function has returned
  - bool  
    if true: exit upon 'quit', if false: exit due to ngspice.dll error
  - int  
    identification number of calling ngspice shared lib (default is 0,
    see Chapt. [19.6](#sec_ngspice_parallel))
  - void\*  
    return pointer received from caller

asking for a reaction after controlled exit.

