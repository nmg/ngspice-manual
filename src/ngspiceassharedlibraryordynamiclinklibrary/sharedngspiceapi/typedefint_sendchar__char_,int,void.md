# typedef int \(SendChar\)\(char\*, int, void\*\)

  - char\*  
    string to be sent to caller output
  - int  
    identification number of calling ngspice shared lib (default is 0,
    see Chapt. [19.6](#sec_ngspice_parallel))
  - void\*  
    return pointer received from caller during initialization, e.g.
    pointer to object having sent the request

Sending output from stdout, stderr to caller. ngspice printf, fprintf,
fputs, fputc functions are redirected to this function. The char\*
string is generated by assembling the print outputs of the above
mentioned functions according to the following rules: The string
commences with \`stdout ', if directed to stdout by ngspice (with
\`stderr ' respectively); all tokens are assembled in sequence, taking
the printf format specifiers into account, until \`\\n' is hit. If set
addescape is given in .spiceinit, the escape character \\ is added to
any character from $\[\]\\" found in the string.

Each callback function has a void pointer as the last parameter. This is
useful in object oriented programming. You may have sent the this (or
self) pointer of the caller's class object to ngspice.dll during calling
ngSpice\_Init ([19.3.2.1](#subsec_int_ngSpice_Init_SendChar___Send)).
The pointer is returned unmodified by each callback, so the callback
function may identify the class object that has initialized ngspice.dll.

