# int ngSpice\_Command\(char\*\)

Send a valid command (see the control or interactive commands) from
caller to ngspice.dll. Will be executed immediately (as if in
interactive mode). Some commands are rejected (e.g. 'plot', because
there is no graphics interface). Command 'quit' will remove internal
data, and then send a notice to caller via ngexit(). The function
returns a '1' upon error, otherwise '0'.

