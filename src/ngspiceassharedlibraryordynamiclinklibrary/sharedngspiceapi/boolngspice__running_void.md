# bool ngSpice\_running \(void\)

Checks if ngspice is running in its background thread (returning
'true').

