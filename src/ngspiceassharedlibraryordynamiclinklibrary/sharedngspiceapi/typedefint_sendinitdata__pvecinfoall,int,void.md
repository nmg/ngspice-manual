# typedef int \(SendInitData\)\(pvecinfoall, int, void\*\)

  - vecinfoall\*  
    pointer to array of structs containing data from all vectors right
    after initialization
  - int  
    identification number of calling ngspice shared lib (default is 0,
    see Chapt. [19.6](#sec_ngspice_parallel))
  - void\*  
    return pointer received from caller

send back initialization vector data.

