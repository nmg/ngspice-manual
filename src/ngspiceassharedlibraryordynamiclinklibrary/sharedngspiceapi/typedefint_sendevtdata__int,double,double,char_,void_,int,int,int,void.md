# typedef int \(SendEvtData\)\(int, double, double, char \*, void \*, int, int, int, void\*\)

  - int  
    node index
  - double  
    actual simulation time
  - double  
    a real value for specified structure component for plotting purposes
  - char\*  
    a string value for specified structure component for printing
  - void\*  
    a binary data structure
  - int  
    size of the binary data structure
  - int  
    the mode (op, dc, tran) we are in
  - int  
    identification number of calling ngspice shared lib
  - void\*  
    return pointer received from caller

Upon a time step finished, called per
node.

