# typedef int \(SendData\)\(pvecvaluesall, int, int, void\*\)

  - vecvaluesall\*  
    pointer to array of structs containing actual values from all
    vectors
  - int  
    number of structs (one per vector)
  - int  
    identification number of calling ngspice shared lib (default is 0,
    see Chapt. [19.6](#sec_ngspice_parallel))
  - void\*  
    return pointer received from caller

send back actual vector data.

