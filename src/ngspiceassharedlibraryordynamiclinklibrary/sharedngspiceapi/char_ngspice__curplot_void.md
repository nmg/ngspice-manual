# char\* ngSpice\_CurPlot\(void\)

returns to the caller a pointer to the name of the current plot. For a
definition of the term 'plot' see Chapt. [17.3](#sec_Plots).

