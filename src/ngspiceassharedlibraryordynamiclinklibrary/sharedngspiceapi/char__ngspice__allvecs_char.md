# char\*\* ngSpice\_AllVecs\(char\*\)

returns to the caller a pointer to an array of all vector names in the
plot named by the string in the argument.

