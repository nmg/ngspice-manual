# tclspice framework

The technical difference between the ngspice CLI interface and tclspice
is that the CLI interface is compiled as a standalone program, whereas
tclspice is a shared object. Tclspice is designed to work with tools
that expand the capabilities of ngspice: TCL for the scripting and
programming language interface and BLT for data processing and display.
This two tools give tclspice all of its relevance, with the insurance
that the functionality is maintained by competent people.

Making tclspice (see [20.6](#sec_Compiling)) produces two files:
libspice.so and pkgIndex.tcl. libspice.so is the executable binary that
the TCL interpreter calls to handle SPICE commands. pkgIndex.tcl take
place in the TCL directory tree, providing the SPICE package

1

package has to be understood as the TCL package

to the TCL user.

BLT is a TCL package. It is quite well documented. It permits to handle
mathematical vector data structures for calculus and display, in a Tk
interpreter like wish.

