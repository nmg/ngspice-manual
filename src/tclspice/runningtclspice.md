# Running TCLspice

TCLspice consists of a library or a package to include in your tcl
console or script:

``` listings
load /somepath/libspice.so
package require spice
```

Then you can execute any native SPICE command by preceding it with
spice::. For example if you want to source the testCapa.cir netlist,
type the following:

``` listings
spice::source testCapa.cir
spice::spicetoblt example...
```

Plotting data is not a matter of SPICE, but of tcl. Once the data is
stored in a blt vector, it can be plotted. Example:

``` listings
blt::graph .cimvd -title "Cim = f(Vd)"
pack .cimvd
.cimvd element create line1 -xdata Vcmd -ydata Cim
```

With blt::graph a plotting structure is allocated in memory. With pack
it is placed into the output window, and becomes visible. The last
command, and not the least, plots the function
\(C_{im} = f\left( V_{cmd} \right)\), where \(C_{im}\) and \(V_{cmd}\)
are two BLT vectors.

