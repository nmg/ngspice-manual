# blt

blt source files have been downloaded from the [blt web
page](ftp://www.sourceforge.net/projects/blt/files/BLT2.4z.tar.gz) and
modified for compatibility with TCL8.6. To facilitate making blt24.dll,
the modified source code is available as a [7z compressed
file](http://ngspice.sourceforge.net/experimental/blt2.4z.7z), including
a project file for MS Visual Studio 2008.

