# tclspice

ngspice is compiled and linked into a dll called spice.dll that may be
loaded by wish86t.exe. MS Visual Studio 2008 is the compiler applied. A
project file may be downloaded as a [7z compressed
file](http://ngspice.sourceforge.net/experimental/visualc-tcl.7z).
Expand this file in the ngspice main directory. The links to tcl and tk
are hard-coded, so both have to be installed in the places described
above (d:\\software\\...). spice.dll may be generated in Debug, Release
or ReleaseOMP mode.

