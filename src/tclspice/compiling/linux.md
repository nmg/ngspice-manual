# Linux

Get tcl8.4 from your distribution. You will need the blt plotting
package (compatible to the old tcl 8.4 only) from
[here](http://sourceforge.net/projects/blt/files/BLT/BLT%202.4z/). See
also the actual [blt wiki](http://wiki.tcl.tk/199).

./configure --with-tcl ..

make

sudo make install

