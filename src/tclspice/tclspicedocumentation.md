# tclspice documentation

A detailed documentation on [tclspice
commands](http://tclspice.sourceforge.net/docs/tclspice_com.html) is
available on the [original tclspice web
page](http://tclspice.sourceforge.net/).

