# Optimization of a linearization circuit for a Thermistor

This example is both the first and the last optimization program written
for an electronic circuit. It is far from perfect.

The temperature response of a CTN is exponential. It is thus nonlinear.
In a battery charger application floating voltage varies linearly with
temperature. A TL431 voltage reference sees its output voltage
controlled by two resistors (r10, r12) and a thermistor (r11). The
simulation is run at a given temperature. The thermistor is modeled in
SPICE by a regular resistor. Its resistivity is assessed by the TCL
script. It is set with a spice::alter command before running the
simulation. This script uses an iterative optimization approach to try
to converge to a set of two resistor values that minimizes the error
between the expected floating voltage and the TL431 output.

