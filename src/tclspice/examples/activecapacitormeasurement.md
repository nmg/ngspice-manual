# Active capacitor measurement

This is a crude implementation of a circuit described by Marc Kodrnja,
in his PhD thesis that was found on the Internet. The simulation outputs
a graph representing virtual capacitance versus a control voltage. The
function \(C = f\left( V \right)\) is calculated point by point. For
each control voltage value, the virtual capacitance is calculated in a
frequency simulation. A control value that should be as close to zero as
possible is calculated to assess simulation success.

