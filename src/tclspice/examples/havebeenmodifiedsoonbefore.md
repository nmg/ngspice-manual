# have been modified soon before
proc iteration { t } { set tzero 273.15 spice::alter
   r11 = [ thermistance_calc 10000 3900 $t ]
# Temperature simulation often crashes. Comment it out...
#spice::set temp = [ expr " $tzero + $t " ]
spice::op
spice::vectoblt vref_temp tref_tmp
