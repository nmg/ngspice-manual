# testbench1.tcl

This line loads the simulator capabilities

``` listings
package require spice
```

This is a comment (Quite useful if you intend to live with other Human
beings)

``` listings
# Test of virtual capacitor circuit
# Vary the control voltage and log the resulting capacitance
```

A good example of the calling of a SPICE command: precede it with
spice::

``` listings
spice::source "testCapa.cir"
```

This reminds that any regular TCL command is of course possible

``` listings
set n 30 set dv 0.2
set vmax [expr $dv/2]
set vmin [expr -1 * $dv/2]
set pas [expr $dv/ $n]
```

BLT vector is the structure used to manipulate data. Instantiate the
vectors

``` listings
blt::vector create Ctmp
blt::vector create Cim
blt::vector create check
blt::vector create Vcmd
```

Data is, in my coding style, plotted into graph objects. Instantiate the
graph

``` listings
blt::graph .cimvd -title "Cim = f(Vd)"
blt::graph .checkvd -title "Rim = f(Vd)"
blt::vector create Iex
blt::vector create freq
blt::graph .freqanal -title "Analyse frequentielle"
#
# First simulation: A simple AC plot
#
set v [expr {$vmin + $n * $pas / 4}]
spice::alter vd = $v
spice::op
spice::ac dec 10 100 100k
```

Retrieve a the intensity of the current across Vex source

``` listings
spice::vectoblt {Vex#branch} Iex
```

Retrieve the frequency at which the current have been assessed

``` listings
spice::vectoblt {frequency} freq
```

Room the graph in the display window

``` listings
pack .freqanal
```

Plot the function Iex =f(V)

``` listings
.freqanal element create line1 -xdata freq -ydata Iex
#
# Second simulation: Capacitance versus voltage control
# for {set i 0} {[expr $n - $i]} {incr i } 
#     { set v [expr {$vmin + $i * $pas}]
spice::alter vd = $v
spice::op spice::ac dec 10 100 100k
```

Image capacitance is calculated by SPICE, instead of TCL there is no
objective reason

``` listings
spice::let Cim = real(mean(Vex#branch/(2*Pi*i*frequency*(V(5)-V(6)))))
spice::vectoblt Cim Ctmp
```

Build function vector point by point

``` listings
Cim append $Ctmp(0:end)
```

Build a control vector to check simulation success

``` listings
spice::let err = real(mean(sqrt((Vex#branch-  
        (2*Pi*i*frequency*Cim*V(5)-V(6)))^2)))
spice::vectoblt err Ctmp check
append $Ctmp(0:end)
```

Build abscissa vector

``` listings
FALTA ALGO... Vcmd append $v }
```

Plot

``` listings
pack .cimvd
.cimvd element create line1 -xdata Vcmd -ydata Cim
pack .checkvd
.checkvd element create line1 -xdata Vcmd -ydata check
```

