# Progressive display

This example is quite simple but it is very interesting. It displays a
transient simulation result on the fly. You may now be familiar with
most of the lines of this script. It uses the ability of BLT objects to
automatically update. When the vector data is modified, the strip-chart
display is modified accordingly.

