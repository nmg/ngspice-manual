# computed here will not be reused. Clean it
spice::destroy all return [ tref_tmp range 0 0 ] } 
```

This is the cost function optimization algorithm will try to minimize.
It is a 2-norm (Euclidean norm) of the error across the temperature
range \[-25:75\]°C.

``` listings
proc cost { r10 r12 } {
tref_blt length 0
spice::alter r10 = $r10
spice::alter r12 = $r12
foreach point [ temperatures_blt range 0
+       [ expr " [temperatures_blt length ] - 1" ] ] {
+       tref_blt append [ iteration $point ]
}
set result [ blt::vector expr " 1000 *
        sum(( tref_blt - expected_blt )^2 )" ]
disp_curve $r10 $r12
return $result }
```

This function displays the expected and effective value of the voltage,
as well as the r10 and r12 resistor values

``` listings
proc disp_curve { r10 r12 }
+  { .g configure -title "Valeurs optimales: R10 = $r10 R12 = $r12" }
```

Main loop starts here

``` listings
#
# Optimization
# blt::vector create tref_tmp
blt::vector create tref_blt
blt::vector create expected_blt
blt::vector create temperatures_blt temperatures_blt
append [ temperatures_calc -25 75 30 ] expected_blt
append [ tref_calc [temperatures_blt range 0
+      [ expr " [ temperatures_blt length ] - 1" ] ] ]
blt::graph .g
pack .g -side top -fill both -expand true
.g element create real -pixels 4 -xdata temperatures_blt
+       -ydata tref_blt
.g element create expected -fill red -pixels 0 -dashes
+       dot -xdata temperatures_blt -ydata expected_blt
```

Source the circuit and optimize it. The result is retrieved in the
variable r10r12e and put into r10 and r12 with a regular expression. A
bit ugly.

``` listings
spice::source FB14.cir
set r10r12 [ ::math::optimize::minimumSteepestDescent
+            cost { 10000 10000 } 0.1 50 ]
regexp {([0-9.]*) ([0-9.]*)} $r10r12 r10r12 r10 r12
```

Outputs optimization result

``` listings
#
# Results
# spice::alter r10 = $r10
spice::alter r12 = $r12
foreach point [ temperatures_blt range 0
+   [ expr " [temperatures_blt length ] - 1" ] ] {
        tref_blt append [ iteration $point ]
}
disp_curve $r10 $r12
```

