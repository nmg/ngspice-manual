# Invocation:

This script can be invoked by typing wish testbench1.tcl

# Invocation:

This script can be executed by the user by simply executing the file in
a terminal.

``` listings
./testbench3.tcl
```

Two issues

2

For those who are really interested in optimizing circuits: Some
parameters are very important for quick and correct convergence. The
optimizer walks step by step to a local minimum of the cost function you
define. Starting from an initial vector *you* provide, it converges step
by step. Consider trying another start vector if the result is not the
one you expected.

The optimizer will carry on walking until it reaches a vector whose
resulting cost is smaller than the target cost *you* provided. You must
also provide a maximum iteration count in case the target can not be
achieved. Balance time, specifications, and every other parameter. For a
balance between quick and accurate convergence adjust the \`factor'
variable, at the beginning of minimumSteepestDescent in the file
differentiate.tcl.

are important to point out:

  - During optimization loop, graphical display of the current
    temperature response is not yet possible and I don't know why. Each
    time a simulation is performed, some memory is allocated for it.
  - The simulation result remains in memory until the libspice library
    is unloaded (typically: when the tcl script ends) or when a
    spice::clean command is performed. In this kind of simulation, not
    cleaning the memory space will freeze your computer and you'll have
    to restart it. Be aware of that.

