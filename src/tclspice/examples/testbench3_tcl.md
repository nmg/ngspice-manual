# testbench3.tcl

This calls the shell sh who then runs wish with the file itself.

``` listings
#!/bin/sh
# WishFix \
exec wish "$0" ${1+"$@"}
#
#
#
```

Regular package for simulation

``` listings
package require spice
```

Here the important line is source differentiate.tcl that contains the
optimization library

``` listings
source differentiate.tcl
```

Generates a temperature vector

``` listings
proc temperatures_calc {temp_inf temp_sup points} {
set tstep [ expr " ( $temp_sup - $temp_inf ) / $points " ]
set t $temp_inf
set temperatures "" 
for { set i 0 } { $i < $points } { incr i } {
    set t [ expr { $t + $tstep } ]
    set temperatures "$temperatures $t"
}
return $temperatures }
```

generates thermistor resistivity as a vector, typically run:
thermistance\_calc res B \[ temperatures\_calc temp\_inf temp\_sup
points \]

``` listings
proc thermistance_calc { res B points } {
set tzero 273.15
set tref 25
set thermistance ""
foreach t $points {
        set res_temp [expr " $res *
+       exp ( $B * ( 1 / ($tzero + $t) -
+       1 / ( $tzero + $tref ) ) ) " ]
        set thermistance "$thermistance $res_temp"
}
return $thermistance }
```

generates the expected floating value as a vector, typically run:
tref\_calc res B \[ temperatures\_calc temp\_inf temp\_sup points \]

``` listings
proc tref_calc { points } {
set tref ""
foreach t $points {
    set tref "$tref[expr "6*(2.275-0.005*($t-20))-9"]"
}
return $tref }
```

In the optimization algorithm, this function computes the effective
floating voltage at the given temperature.

``` listings
