# spicetoblt

Tclspice opens its doors to TCL and BLT with a single specific command
spicetoblt.

TCLspice gets its identity in the command spice::vectoblt . This command
copies data computed by the simulation engine into a tcl variable.
vectoblt is composed of three words: vec, to and blt. Vec means SPICE
vector data. To is the English preposition, and blt is a useful tcl
package providing a vector data structure. Example:

``` listings
blt::vector create Iex
spice::vectoblt Vex#branch Iex
```

Here an empty blt vector is created. It is then filled with the vector
representation of the current flowing out of source Vex. Vex\#branch is
native SPICE syntax. Iex is the name of the BLT vector.

The reverse operation is handled by native SPICE commands, such as
alter, let and set.

