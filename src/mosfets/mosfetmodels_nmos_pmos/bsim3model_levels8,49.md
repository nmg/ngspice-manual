# BSIM3 model \(levels 8, 49\)

BSIM3 solves the numerical problems of previous models with the
introduction of smoothing functions. It adopts a single equation to
describe device characteristics in the operating regions. This approach
eliminates the discontinuities in the I-V and C-V characteristics. The
original model, [BSIM3](http://bsim.berkeley.edu/models/bsim3/) evolved
through three versions: BSIM3v1, BSIM3v2 and BSIM3v3. Both BSIM3v1 and
BSIM3v2 had suffered from many mathematical problems and were replaced
by BSIM3v3. The latter is the only surviving release and has itself a
long revision history.

The following table summarizes the story of this model:

<table>
<colgroup>
<col width="25%" />
<col width="25%" />
<col width="25%" />
<col width="25%" />
</colgroup>
<tbody>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-14256"></span><strong>Release</strong>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14259"></span><strong>Date</strong>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14262"></span><strong>Notes</strong>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14265"></span><strong>Version flag</strong>
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-14268"></span>BSIM3v3.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14271"></span>10/30/1995
</div></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-14277"></span>3.0
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-14280"></span>BSIM3v3.1
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14283"></span>12/09/1996
</div></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-14289"></span>3.1
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-14292"></span>BSIM3v3.2
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14295"></span>06/16/1998
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14298"></span>Revisions available: BSIM3v3.2.2, BSIM3v3.2.3, and BSIM3v3.2.4
</div>
<div class="plain_layout">
<span id="magicparlabel-14299"></span>Parallel processing with OpenMP is available for BSIM3v3.2.4.
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14302"></span>3.2, 3.2.2, 3.2.3, 3.2.4
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-14305"></span>BSIM3v3.3
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14308"></span>07/29/2005
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14311"></span>Parallel processing with OpenMP is available for this model.
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14314"></span>3.3.0
</div></td>
</tr>
</tbody>
</table>

BSIM3v2 and 3v3 models has proved for accurate use in 0.18 \(\mu m\)
technologies. The model is publicly available as [source
code](http://bsim.berkeley.edu/BSIM4/BSIM3/ftpv330.zip) form from
University of California, Berkeley.

A detailed description is given in the user's manual available from
[here](http://ngspice.sourceforge.net/external-documents/models/bsim330_manual.pdf)
.

We recommend that you use only the most recent BSIM3 models (version
3.3.0), because it contains corrections to all known bugs. To achieve
that, change the version parameter in your modelcard files to

VERSION = 3.3.0.

If no version number is given in the .model card, this (newest) version
is selected as the default.

BSIM3v3.2.4 supports the extra model parameter lmlt on channel length
scaling and is still used by many foundries today.

The older models will not be supported, they are made available for
reference only.

