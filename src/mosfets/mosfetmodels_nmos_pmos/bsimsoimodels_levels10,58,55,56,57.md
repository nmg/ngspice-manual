# BSIMSOI models \(levels 10, 58, 55, 56, 57\)

BSIMSOI is a SPICE compact model for SOI (Silicon-On-Insulator) circuit
design, created by [University of California at
Berkeley](http://bsim.berkeley.edu/models/bsimsoi/). This model is
formulated on top of the BSIM3 framework. It shares the same basic
equations with the bulk model so that the physical nature and smoothness
of BSIM3v3 are retained. Four models are supported in ngspice, those
based on BSIM3 and modeling fully depleted (FD, level 55), partially
depleted (PD, level 57) and both (DD, level 56), as well as the modern
BSIMSOI version 4 model (levels 10, 58). Detailed descriptions are
beyond the scope of this manual, but see e.g. [BSIMSOIv4.4 User
Manual](http://ngspice.sourceforge.net/external-documents/models/BSIMSOIv4.4_UsersManual.pdf)
for a very extensive description of the recent model version. OpenMP
support is available for levels 10, 58, version 4.4.

