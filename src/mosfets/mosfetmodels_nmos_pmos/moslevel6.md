# MOS Level 6

This model is described in \[[2](#LyXCite-key_2)\]. The model can
express the current characteristics of short-channel MOSFETs at least
down to 0.25 \(\mu m\) channel-length, GaAs FET, and resistance inserted
MOSFETs. The model evaluation time is about 1/3 of the evaluation time
of the SPICE3 mos level 3 model. The model also enables analytical
treatments of circuits in short-channel region and makes up for a
missing link between a complicated MOSFET current characteristics and
circuit behaviors in the deep submicron region.

