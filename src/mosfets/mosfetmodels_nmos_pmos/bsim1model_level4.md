# BSIM1 model \(level 4\)

BSIM1 model (the first is a long series) is an empirical model.
Developers placed less emphasis on device physics and based the model on
parametrical polynomial equations to model the various physical effects.
This approach pays in terms of circuit simulation behavior but the
accuracy degrades in the submicron region. A known problem of this model
is the negative output conductance and the convergence problems, both
related to poor behavior of the polynomial equations.

