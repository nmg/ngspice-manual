# Ngspice BSIM \(level 4\) parameters

<table>
<colgroup>
<col width="25%" />
<col width="25%" />
<col width="25%" />
<col width="25%" />
</colgroup>
<thead>
<tr class="header">
<th><div class="plain_layout">
<span id="magicparlabel-13741"></span><strong>Name</strong>
</div></th>
<th><div class="plain_layout">
<span id="magicparlabel-13744"></span><strong>Parameter</strong>
</div></th>
<th><div class="plain_layout">
<span id="magicparlabel-13747"></span><strong>Units</strong>
</div></th>
<th><div class="plain_layout">
<span id="magicparlabel-13750"></span><strong>l/w</strong>
</div></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-13753"></span>VFB
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13756"></span>Flat-band voltage
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13759"></span><span class="math inline"><em>V</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13762"></span>*
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-13765"></span>PHI
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13768"></span>Surface inversion potential
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13771"></span><span class="math inline"><em>V</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13774"></span>*
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-13777"></span>K1
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13780"></span>Body effect coefficient
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13783"></span><span class="math inline">$\sqrt{V}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13786"></span>*
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-13789"></span>K2
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13792"></span>Drain/source depletion charge-sharing coefficient
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13795"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13798"></span>*
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-13801"></span>ETA
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13804"></span>Zero-bias drain-induced barrier-lowering coefficient
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13807"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13810"></span>*
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-13813"></span>MUZ
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13816"></span>Zero-bias mobility
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13819"></span><span class="math inline">$\frac{cm^{2}}{V \cdot sec}$</span>
</div></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-13825"></span>DL
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13828"></span>Shortening of channel
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13831"></span><span class="math inline"><em>μ</em><em>m</em></span>
</div></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-13837"></span>DW
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13840"></span>Narrowing of channel
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13843"></span><span class="math inline"><em>μ</em><em>m</em></span>
</div></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-13849"></span>U0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13852"></span>Zero-bias transverse-field mobility degradation coefficient
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13855"></span><span class="math inline">$\frac{1}{V}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13858"></span>*
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-13861"></span>U1
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13864"></span>Zero-bias velocity saturation coefficient
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13867"></span><span class="math inline">$\frac{\mu}{V}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13870"></span>*
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-13873"></span>X2MZ
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13876"></span>Sens. of mobility to substrate bias at v=0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13879"></span><span class="math inline">$\frac{cm^{2}}{V^{2} \cdot sec}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13882"></span>*
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-13885"></span>X2E
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13888"></span>Sens. of drain-induced barrier lowering effect to substrate bias
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13891"></span><span class="math inline">$\frac{1}{V}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13894"></span>*
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-13897"></span>X3E
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13900"></span>Sens. of drain-induced barrier lowering effect to drain bias at <span class="math inline"><em>V</em><sub><em>d</em><em>s</em></sub> = <em>V</em><sub><em>d</em><em>d</em></sub></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13903"></span><span class="math inline">$\frac{1}{V}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13906"></span>*
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-13909"></span>X2U0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13912"></span>Sens. of transverse field mobility degradation effect to substrate bias
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13915"></span><span class="math inline">$\frac{1}{V^{2}}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13918"></span>*
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-13921"></span>X2U1
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13924"></span>Sens. of velocity saturation effect to substrate bias
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13927"></span><span class="math inline">$\frac{\mu m}{V^{2}}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13930"></span>*
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-13933"></span>MUS
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13936"></span>Mobility at zero substrate bias and at <span class="math inline"><em>V</em><sub><em>d</em><em>s</em></sub> = <em>V</em><sub><em>d</em><em>d</em></sub></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13939"></span><span class="math inline">$\frac{cm^{2}}{V^{2}sec}$</span>
</div></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-13945"></span>X2MS
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13948"></span>Sens. of mobility to substrate bias at <span class="math inline"><em>V</em><sub><em>d</em><em>s</em></sub> = <em>V</em><sub><em>d</em><em>d</em></sub></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13951"></span><span class="math inline">$\frac{cm^{2}}{V^{2}sec}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13954"></span>*
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-13957"></span>X3MS
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13960"></span>Sens. of mobility to drain bias at <span class="math inline"><em>V</em><sub><em>d</em><em>s</em></sub> = <em>V</em><sub><em>d</em><em>d</em></sub></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13963"></span><span class="math inline">$\frac{cm^{2}}{V^{2}sec}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13966"></span>*
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-13969"></span>X3U1
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13972"></span>Sens. of velocity saturation effect on drain bias at Vds=Vdd
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13975"></span><span class="math inline">$\frac{\mu m}{V^{2}}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13978"></span>*
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-13981"></span>TOX
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13984"></span>Gate oxide thickness
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13987"></span><span class="math inline"><em>μ</em><em>m</em></span>
</div></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-13993"></span>TEMP
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13996"></span>Temperature where parameters were measured
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13999"></span><span class="math inline"><em>C</em></span>
</div></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-14005"></span>VDD
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14008"></span>Measurement bias range
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14011"></span><span class="math inline"><em>V</em></span>
</div></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-14017"></span>CGDO
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14020"></span>Gate-drain overlap capacitance per meter channel width
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14023"></span><span class="math inline">$\frac{F}{m}$</span>
</div></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-14029"></span>CGSO
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14032"></span>Gate-source overlap capacitance per meter channel width
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14035"></span><span class="math inline">$\frac{F}{m}$</span>
</div></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-14041"></span>CGBO
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14044"></span>Gate-bulk overlap capacitance per meter channel length
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14047"></span><span class="math inline">$\frac{F}{m}$</span>
</div></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-14053"></span>XPART
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14056"></span>Gate-oxide capacitance-charge model flag
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14059"></span>-
</div></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-14065"></span>N0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14068"></span>Zero-bias subthreshold slope coefficient
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14071"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14074"></span>*
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-14077"></span>NB
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14080"></span>Sens. of subthreshold slope to substrate bias
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14083"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14086"></span>*
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-14089"></span>ND
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14092"></span>Sens. of subthreshold slope to drain bias
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14095"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14098"></span>*
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-14101"></span>RSH
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14104"></span>Drain and source diffusion sheet resistance
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14107"></span><span class="math inline">$\frac{\Omega}{\square}$</span>
</div></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-14113"></span>JS
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14116"></span>Source drain junction current density
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14119"></span><span class="math inline">$\frac{A}{m^{2}}$</span>
</div></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-14125"></span>PB
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14128"></span>Built in potential of source drain junction
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14131"></span><span class="math inline"><em>V</em></span>
</div></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-14137"></span>MJ
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14140"></span>Grading coefficient of source drain junction
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14143"></span>-
</div></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-14149"></span>PBSW
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14152"></span>Built in potential of source, drain junction sidewall
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14155"></span><span class="math inline"><em>V</em></span>
</div></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-14161"></span>MJSW
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14164"></span>Grading coefficient of source drain junction sidewall
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14167"></span>-
</div></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-14173"></span>CJ
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14176"></span>Source drain junction capacitance per unit area
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14179"></span><span class="math inline">$\frac{F}{m^{2}}$</span>
</div></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-14185"></span>CJSW
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14188"></span>source drain junction sidewall capacitance per unit length
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14191"></span><span class="math inline">$\frac{F}{m}$</span>
</div></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-14197"></span>WDF
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14200"></span>Source drain junction default width
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14203"></span><span class="math inline"><em>m</em></span>
</div></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-14209"></span>DELL
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14212"></span>Source drain junction length reduction
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14215"></span><span class="math inline"><em>m</em></span>
</div></td>
<td></td>
</tr>
</tbody>
</table>

**xpart** = 0 selects a 40/60 drain/source charge partition in
saturation, while **xpart**=1 selects a 0/100 drain/source charge
partition. **nd**, **ng**, and **ns** are the drain, gate, and source
nodes, respectively. **mname** is the model name, **area** is the area
factor, and **off** indicates an (optional) initial condition on the
device for dc analysis. If the area factor is omitted, a value of 1.0 is
assumed. The (optional) initial condition specification, using
**ic=vds,vgs** is intended for use with the **uic** option on the .tran
control line, when a transient analysis is desired starting from other
than the quiescent operating point. See the .ic control line for a
better way to set initial conditions.

