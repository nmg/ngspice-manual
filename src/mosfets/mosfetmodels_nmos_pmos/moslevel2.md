# MOS Level 2

This model tries to overcome the limitations of the Level 1 model
addressing several short-channel effects, like velocity saturation. The
implementation of this model is complicated and this leads to many
convergence problems. C-V calculations can be done with the original
Meyer model (non charge conserving).

