# HiSIM models of the University of Hiroshima

There are two model implementations available - see also [HiSIM Research
Center](https://www.hisim.hiroshima-u.ac.jp/index.php?id=87):

1.  HiSIM2 model: Surface-Potential-Based MOSFET Model for Circuit
    Simulation version 2.8.0 - level 68 (see [link to
    HiSIM2](http://home.hiroshima-u.ac.jp/usdl/HiSIM2/HiSIM_2.5.1_Release_20110407.zip)
    for source code and manual).
2.  HiSIM\_HV model: Surface-Potential-Based HV/LD-MOSFET Model for
    Circuit Simulation version 1.2.4 and 2.2.0 - level 73 (see [link to
    HiSIM\_HV](http://home.hiroshima-u.ac.jp/usdl/HiSIM_HV/C-Code/HiSIM_HV_1.2.2_Release_20110629.zip)
    for source code and manual).

