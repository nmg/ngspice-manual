# EKV model

Level 44 model (EKV) is not available in the standard distribution since
it is not released in source form by the EKV group. To obtain the code
please refer to the ([EKV model home page](http://ekv.epfl.ch/), EKV
group home page). A verilog-A version is available contributed by Ivan
Riis Nielsen 11/2006.

