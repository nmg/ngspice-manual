# MOS Level 1

This model is also known as the \`Shichman-Hodges' model. This is the
first model written and the one often described in the introductory
textbooks for electronics. This model is applicable only to long channel
devices. The use of Meyer's model for the C-V part makes it non charge
conserving.

