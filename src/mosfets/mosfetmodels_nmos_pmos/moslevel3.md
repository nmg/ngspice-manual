# MOS Level 3

This is a semi-empirical model derived from the Level 2 model. In the
80s this model has often been used for digital design and, over the
years, has proved to be robust. A discontinuity in the model with
respect to the KAPPA parameter has been detected (see \[10\]). The
supplied fix has been implemented in Spice3f2 and later. Since this fix
may affect parameter fitting, the option **badmos3** may be set to use
the old implementation (see the section on simulation variables and the
.options line). Ngspice level 3 implementation takes into account length
and width mask adjustments (**xl** and **xw**) and device width
narrowing due to diffusion (**wd**).

