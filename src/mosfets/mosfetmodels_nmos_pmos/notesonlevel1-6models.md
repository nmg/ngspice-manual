# Notes on Level 1-6 models

The dc characteristics of the level 1 through level 3 MOSFETs are
defined by the device parameters **vto**, **kp**, **lambda**, **phi**
and **gamma**. These parameters are computed by ngspice if process
parameters (**nsub**, **tox**, ...) are given, but users specified
values always override. **vto** is positive (negative) for enhancement
mode and negative (positive) for depletion mode N-channel (P-channel)
devices.

Charge storage is modeled by three constant capacitors, **cgso**,
**cgdo**, and **cgbo**, which represent overlap capacitances, by the
nonlinear thin-oxide capacitance that is distributed among the gate,
source, drain, and bulk regions, and by the nonlinear depletion-layer
capacitances for both substrate junctions divided into bottom and
periphery, which vary as the **mj** and **mjsw** power of junction
voltage respectively, and are determined by the parameters **cbd**,
**cbs**, **cj**, **cjsw**, **mj**, **mjsw** and **pb**.

Charge storage effects are modeled by the piecewise linear
voltages-dependent capacitance model proposed by Meyer. The thin-oxide
charge-storage effects are treated slightly different for the level 1
model. These voltage-dependent capacitances are included only if **tox**
is specified in the input description and they are represented using
Meyer's formulation.

There is some overlap among the parameters describing the junctions,
e.g. the reverse current can be input either as **is** (in A) or as
**js** (in \(\frac{A}{m^{2}}\)). Whereas the first is an absolute value
the second is multiplied by **ad** and **as** to give the reverse
current of the drain and source junctions respectively.

This methodology has been chosen since there is no sense in relating
always junction characteristics with **ad** and **as** entered on the
device line; the areas can be defaulted. The same idea applies also to
the zero-bias junction capacitances **cbd** and **cbs** (in F) on one
hand, and **cj** (in \(\frac{F}{m^{2}}\)) on the other.

The parasitic drain and source series resistance can be expressed as
either **rd** and **rs** (in ohms) or **rsh** (in ohms/sq.), the latter
being multiplied by the number of squares **nrd** and **nrs** input on
the device line.

