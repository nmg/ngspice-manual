# NGSPICE level 1, 2, 3 and 6 parameters

 

<table>
<colgroup>
<col width="20%" />
<col width="20%" />
<col width="20%" />
<col width="20%" />
<col width="20%" />
</colgroup>
<thead>
<tr class="header">
<th><div class="plain_layout">
<span id="magicparlabel-12912"></span><strong>Name</strong>
</div></th>
<th><div class="plain_layout">
<span id="magicparlabel-12915"></span><strong>Parameter</strong>
</div></th>
<th><div class="plain_layout">
<span id="magicparlabel-12918"></span><strong>Units</strong>
</div></th>
<th><div class="plain_layout">
<span id="magicparlabel-12921"></span><strong>Default</strong>
</div></th>
<th><div class="plain_layout">
<span id="magicparlabel-12924"></span><strong>Example</strong>
</div></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-12927"></span>LEVEL
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-12930"></span>Model index
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-12933"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-12936"></span>1
</div></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-12942"></span>VTO
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-12945"></span>Zero-bias threshold voltage (<span class="math inline"><em>V</em><sub><em>T</em>0</sub>)</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-12948"></span><span class="math inline"><em>V</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-12951"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-12954"></span>1.0
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-12957"></span>KP
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-12960"></span>Transconductance parameter
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-12963"></span><span class="math inline">$\frac{A}{V^{2}}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-12966"></span>2.0e-5
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-12969"></span>3.1e-5
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-12972"></span>GAMMA
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-12975"></span>Bulk threshold parameter
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-12978"></span><span class="math inline">$\sqrt{V}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-12981"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-12984"></span>0.37
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-12987"></span>PHI
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-12990"></span>Surface potential (U)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-12993"></span><span class="math inline"><em>V</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-12996"></span>0.6
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-12999"></span>0.65
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-13002"></span>LAMBDA
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13005"></span>Channel length modulation (MOS1 and MOS2 only) (<span class="math inline"><em>λ</em>)</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13008"></span><span class="math inline">$\frac{1}{V}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13011"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13014"></span>0.02
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-13017"></span>RD
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13020"></span>Drain ohmic resistance
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13023"></span><span class="math inline"><em>Ω</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13026"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13029"></span>1.0
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-13032"></span>RS
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13035"></span>Source ohmic resistance
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13038"></span><span class="math inline"><em>Ω</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13041"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13044"></span>1.0
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-13047"></span>CBD
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13050"></span>Zero-bias B-D junction capacitance
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13053"></span><span class="math inline"><em>F</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13056"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13059"></span>20fF
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-13062"></span>CBS
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13065"></span>Zero-bias B-S junction capacitance
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13068"></span><span class="math inline"><em>F</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13071"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13074"></span>20fF
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-13077"></span>IS
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13080"></span>Bulk junction saturation current (<span class="math inline"><em>I</em><sub><em>S</em></sub></span>)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13083"></span><span class="math inline"><em>A</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13086"></span>1.0e-14
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13089"></span>1.0e-15
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-13092"></span>PB
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13095"></span>Bulk junction potential
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13098"></span><span class="math inline"><em>V</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13101"></span>0.8
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13104"></span>0.87
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-13107"></span>CGSO
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13110"></span>Gate-source overlap capacitance per meter channel width
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13113"></span><span class="math inline">$\frac{F}{m}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13116"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13119"></span>4.0e-11
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-13122"></span>CGDO
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13125"></span>Gate-drain overlap capacitance per meter channel width
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13128"></span><span class="math inline">$\frac{F}{m}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13131"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13134"></span>4.0e-11
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-13137"></span>CGBO
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13140"></span>Gate-bulk overlap capacitance per meter channel width
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13143"></span><span class="math inline">$\frac{F}{m}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13146"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13149"></span>2.0e-11
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-13152"></span>RSH
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13155"></span>Drain and source diffusion sheet resistance
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13158"></span><span class="math inline">$\frac{\Omega}{\square}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13161"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13164"></span>10
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-13167"></span>CJ
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13170"></span>Zero-bias bulk junction bottom cap. per sq-meter of junction area
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13173"></span><span class="math inline">$\frac{F}{m^{2}}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13176"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13179"></span>2.0e-4
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-13182"></span>MJ
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13185"></span>Bulk junction bottom grading coeff.
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13188"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13191"></span>0.5
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13194"></span>0.5
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-13197"></span>CJSW
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13200"></span>Zero-bias bulk junction sidewall cap. per meter of junction perimeter
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13203"></span><span class="math inline">$\frac{F}{m}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13206"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13209"></span>1.0e-9
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-13212"></span>MJSW
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13215"></span>Bulk junction sidewall grading coeff.
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13218"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13221"></span><span class="math inline">$\begin{array}{ll}
0.50 &amp; \left. \left( level \right.1 \right) \\
0.33 &amp; \left. \left( level \right.2,3 \right) \\
\end{array}$</span>
</div></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-13227"></span>JS
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13230"></span>Bulk junction saturation current
</div></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-13242"></span>TOX
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13245"></span>Oxide thickness
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13248"></span><span class="math inline"><em>m</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13251"></span>1.0e-7
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13254"></span>1.0e-7
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-13257"></span>NSUB
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13260"></span>Substrate doping
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13263"></span><span class="math inline"><em>c</em><em>m</em><sup>−3</sup></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13266"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13269"></span>4.0e15
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-13272"></span>NSS
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13275"></span>Surface state density
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13278"></span><span class="math inline"><em>c</em><em>m</em><sup>−2</sup></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13281"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13284"></span>1.0e10
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-13287"></span>NFS
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13290"></span>Fast surface state density
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13293"></span><span class="math inline"><em>c</em><em>m</em><sup>−2</sup></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13296"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13299"></span>1.0e10
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-13302"></span>TPG
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13305"></span>Type of gate material: +1 opp. to substrate, -1 same as substrate, 0 Al gate
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13308"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13311"></span>1.0
</div></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-13317"></span>XJ
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13320"></span>Metallurgical junction depth
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13323"></span><span class="math inline"><em>m</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13326"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13329"></span>1M
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-13332"></span>LD
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13335"></span>Lateral diffusion
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13338"></span><span class="math inline"><em>m</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13341"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13344"></span>0.8M
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-13347"></span>UO
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13350"></span>Surface mobility
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13353"></span><span class="math inline">$\frac{cm^{2}}{V \cdot sec}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13356"></span>600
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13359"></span>700
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-13362"></span>UCRIT
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13365"></span>Critical field for mobility degradation (MOS2 only)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13368"></span><span class="math inline">$\frac{V}{cm}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13371"></span>1.0e4
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13374"></span>1.0e4
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-13377"></span>UEXP
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13380"></span>Critical field exponent in mobility degradation (MOS2 only)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13383"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13386"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13389"></span>0.1
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-13392"></span>UTRA
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13395"></span>Transverse field coeff. (mobility) (deleted for MOS2)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13398"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13401"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13404"></span>0.3
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-13407"></span>VMAX
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13410"></span>Maximum drift velocity of carriers
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13413"></span><span class="math inline">$\frac{m}{s}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13416"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13419"></span>5.0e4
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-13422"></span>NEFF
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13425"></span>Total channel-charge (fixed and mobile) coefficient (MOS2 only)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13428"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13431"></span>1.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13434"></span>5.0
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-13437"></span>KF
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13440"></span>Flicker noise coefficient
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13443"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13446"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13449"></span>1.0e-26
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-13452"></span>AF
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13455"></span>Flicker noise exponent
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13458"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13461"></span>1.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13464"></span>1.2
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-13467"></span>FC
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13470"></span>Coefficient for forward-bias depletion capacitance formula
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13473"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13476"></span>0.5
</div></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-13482"></span>DELTA
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13485"></span>Width effect on threshold voltage (MOS2 and MOS3)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13488"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13491"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13494"></span>1.0
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-13497"></span>THETA
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13500"></span>Mobility modulation (MOS3 only)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13503"></span><span class="math inline">$\frac{1}{V}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13506"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13509"></span>0.1
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-13512"></span>ETA
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13515"></span>Static feedback (MOS3 only)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13518"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13521"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13524"></span>1.0
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-13527"></span>KAPPA
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13530"></span>Saturation field factor (MOS3 only)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13533"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13536"></span>0.2
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13539"></span>0.5
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-13542"></span>TNOM
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13545"></span>Parameter measurement temperature
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13548"></span><span class="math inline"><em>C</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13551"></span>27
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-13554"></span>50
</div></td>
</tr>
</tbody>
</table>

