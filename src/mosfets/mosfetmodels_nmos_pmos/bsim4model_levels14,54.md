# BSIM4 model \(levels 14, 54\)

This is the newest class of the BSIM family and introduces noise
modeling and extrinsic parasitics. BSIM4, as the extension of BSIM3
model, addresses the MOSFET physical effects into sub-100nm regime. It
is a physics-based, accurate, scalable, robust and predictive MOSFET
SPICE model for circuit simulation and CMOS technology development. It
is developed by the BSIM Research Group in the Department of Electrical
Engineering and Computer Sciences (EECS) at the University of
California, Berkeley (see [BSIM4 home
page](http://bsim.berkeley.edu/models/bsim4/)). BSIM4 has a long
revision history, which is summarized below.

<table>
<colgroup>
<col width="25%" />
<col width="25%" />
<col width="25%" />
<col width="25%" />
</colgroup>
<tbody>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-14387"></span><strong>Release</strong>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14390"></span><strong>Date</strong>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14393"></span><strong>Notes</strong>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14396"></span><strong>Version flag</strong>
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-14399"></span>BSIM4.0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14402"></span>03/24/2000
</div></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-14411"></span>BSIM4.1.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14414"></span>10/11/2000
</div></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-14423"></span>BSIM4.2.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14426"></span>04/06/2001
</div></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-14435"></span>BSIM4.2.1
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14438"></span>10/05/2001
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14441"></span>*
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14444"></span>4.2.1
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-14447"></span>BSIM4.3.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14450"></span>05/09/2003
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14453"></span>*
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14456"></span>4.3.0
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-14459"></span>BSIM4.4.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14462"></span>03/04/2004
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14465"></span>*
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14468"></span>4.4.0
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-14471"></span>BSIM4.5.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14474"></span>07/29/2005
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14477"></span>* **
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14480"></span>4.5.0
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-14483"></span>BSIM4.6.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14486"></span>12/13/2006
</div></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-14495"></span>...
</div></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-14507"></span>BSIM4.6.5
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14510"></span>09/09/2009
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14513"></span>* **
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14516"></span>4.6.5
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-14519"></span>BSIM4.7.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14522"></span>04/08/2011
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14525"></span>* **
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14528"></span>4.7
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-14531"></span>BSIM4.8.1
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14534"></span>15/02/2017
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14537"></span>* **
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14540"></span>4.8
</div></td>
</tr>
</tbody>
</table>

\*) supported in ngspice, using e.g. the version=\<version flag\> flag
in the parameter file.

\*\*) Parallel processing using OpenMP support is available for this
model.

Details of any revision are to be found in the Berkeley user's manuals,
a pdf download of the most recent edition is to be found
[here](http://ngspice.sourceforge.net/external-documents/models/BSIM480_Manual.pdf).

We recommend that you use only the most recent BSIM4 model (version
4.8.1), because it contains corrections to all known bugs. To achieve
that, change the version parameter in your modelcard files to

VERSION = 4.8.

If no version number is given in the .model card, this (newest) version
is selected as the default. The older models will typically not be
supported, they are made available for reference only.

