# BSIM Models

Ngspice implements many of the BSIM models developed by [Berkeley's BSIM
group](http://bsim.berkeley.edu/). BSIM stands for Berkeley
Short-Channel IGFET Model and groups a class of models that is
continuously updated. BSIM3 ([11.2.10](#subsec_BSIM3_model)) and BSIM4
([11.2.11](#subsec_BSIM4_model)) are industry standards for CMOS
processes down to 0.15 µm (BSIM3) and below (BSIM4), are very stable and
are supported by model parameter sets from foundries all over the world.
BSIM1 and BSIM2 are obsolete today.

In general, all parameters of BSIM models are obtained from process
characterization, in particular level 4 and level 5 (BSIM1 and BSIM2)
parameters can be generated automatically. J. Pierret \[4\] describes a
means of generating a \`process' file, and the program ngproc2mod
provided with ngspice converts this file into a sequence of BSIM1 .model
lines suitable for inclusion in an ngspice input file.

Parameters marked below with an \* in the l/w column also have
corresponding parameters with a length and width dependency. For
example, **vfb** is the basic parameter with units of Volts, and
**lvfb** and **wvfb** also exist and have units of Volt-meter.

The formula

\[\begin{array}{ll}
{P = P_{0} + \frac{P_{L}}{L_{\lbrack font\ rm\ \lbrack char\ e\ mathalpha\rbrack\lbrack char\ f\ mathalpha\rbrack\lbrack char\ f\ mathalpha\rbrack\lbrack char\ e\ mathalpha\rbrack\lbrack char\ c\ mathalpha\rbrack\lbrack char\ t\ mathalpha\rbrack\lbrack char\ i\ mathalpha\rbrack\lbrack char\ v\ mathalpha\rbrack\lbrack char\ e\ mathalpha\rbrack\rbrack}} + \frac{P_{W}}{W_{\lbrack font\ rm\ \lbrack char\ e\ mathalpha\rbrack\lbrack char\ f\ mathalpha\rbrack\lbrack char\ f\ mathalpha\rbrack\lbrack char\ e\ mathalpha\rbrack\lbrack char\ c\ mathalpha\rbrack\lbrack char\ t\ mathalpha\rbrack\lbrack char\ i\ mathalpha\rbrack\lbrack char\ v\ mathalpha\rbrack\lbrack char\ e\ mathalpha\rbrack\rbrack}}} & \\
\end{array}\]

is used to evaluate the parameter for the actual device specified with

\[\begin{array}{ll}
{L_{\lbrack font\ rm\ \lbrack char\ e\ mathalpha\rbrack\lbrack char\ f\ mathalpha\rbrack\lbrack char\ f\ mathalpha\rbrack\lbrack char\ e\ mathalpha\rbrack\lbrack char\ c\ mathalpha\rbrack\lbrack char\ t\ mathalpha\rbrack\lbrack char\ i\ mathalpha\rbrack\lbrack char\ v\ mathalpha\rbrack\lbrack char\ e\ mathalpha\rbrack\rbrack} = L_{\lbrack font\ rm\ \lbrack char\ i\ mathalpha\rbrack\lbrack char\ n\ mathalpha\rbrack\lbrack char\ p\ mathalpha\rbrack\lbrack char\ u\ mathalpha\rbrack\lbrack char\ t\ mathalpha\rbrack\rbrack} - DL} & \\
\end{array}\]

\[\begin{array}{ll}
{W_{\lbrack font\ rm\ \lbrack char\ e\ mathalpha\rbrack\lbrack char\ f\ mathalpha\rbrack\lbrack char\ f\ mathalpha\rbrack\lbrack char\ e\ mathalpha\rbrack\lbrack char\ c\ mathalpha\rbrack\lbrack char\ t\ mathalpha\rbrack\lbrack char\ i\ mathalpha\rbrack\lbrack char\ v\ mathalpha\rbrack\lbrack char\ e\ mathalpha\rbrack\rbrack} = W_{\lbrack font\ rm\ \lbrack char\ i\ mathalpha\rbrack\lbrack char\ n\ mathalpha\rbrack\lbrack char\ p\ mathalpha\rbrack\lbrack char\ u\ mathalpha\rbrack\lbrack char\ t\ mathalpha\rbrack\rbrack} - DW} & \\
\end{array}\]

Note that unlike the other models in ngspice, the BSIM models are
designed for use with a process characterization system that provides
all the parameters, thus there are no defaults for the parameters, and
leaving one out is considered an error. For an example set of parameters
and the format of a process file, see the SPICE2 implementation notes
\[3\]. For more information on BSIM2, see reference \[5\]. BSIM3
([11.2.10](#subsec_BSIM3_model)) and BSIM4
([11.2.11](#subsec_BSIM4_model)) represent state of the art for
submicron and deep submicron IC design.

