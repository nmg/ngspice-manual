# BSIM2 model \(level 5\)

This model contains many improvements over BSIM1 and is suitable for
analog simulation. Nevertheless, even BSIM2 breaks transistor operation
into several distinct regions and this leads to discontinuities in the
first derivative in C-V and I-V characteristics that can cause numerical
problems during simulation.

