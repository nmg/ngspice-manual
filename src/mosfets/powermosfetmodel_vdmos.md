# Power MOSFET model \(VDMOS\)

The VDMOS model is a relativly simple power MOS model with 3 terminals
drain, gate, and source. Its current equations are based on the MOS1
model. The gate-source capacitance is set to a constant value by
parameter Cgs. The drain-source capacitance is evaluated from parameters
Cgdmax, Cgdmin, and A. The drain-source capacitance is that of a
parallel pn diode and calculated by Cjo, fc, and m. Leakage and
breakdown are modelled by the parallel pn diodes as well, using is and
other parameters. A subthreshold current model is available, using a
single parameter ksubthres. Quasi-saturation is modelled with parameters
rq and vq. Mtriode may be used here as well.

This model does not have a level parameter. It is invoked by the VDMOS
token preceeding the parameters on the .model line. P-channel or
n-channel are selected by the flags Pchan and Nchan. If no flag is
given, n-channel is the default. Standard MOS instance parameters W and
L are not aknowledged because they are no design parameters and are not
provided by the device manufacturers.

Please note that the device multiplier for this model is named mu\!

The following 'parameters' in the .model line are no model parameters,
but serve information purposes for the user: mfg=..., Vds=..., Ron=...,
and Qg=... They are ignored by ngspice.

General form:

``` listings
MXXXXXXX nd ng ns mname <mu=val> <temp=t> <dtemp=t>
```

Example:

``` listings
M1 24 2 0 IXTH48P20P
.MODEL IXTH48P20P VDMOS Pchan Vds=200 VTO=-4 KP=10 Lambda=5m
+ Mtriode=0.3 Ksubthres=120m Rs=10m Rd=20m Rds=200e6
+ Cgdmax=6000p Cgdmin=100p A=0.25 Cgs=5000p Cjo=9000p
+ Is=2e-6 Rb=20m BV=200 IBV=250e-6 NBV=4  TT=260e-9
```

