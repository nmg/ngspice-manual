# MOSFET models \(NMOS/PMOS\)

MOSFET models are the central part of ngspice, probably because they are
the most widely used devices in the electronics world. Ngspice provides
all the MOSFETs implemented in the original Spice3f and adds several
models developed by [UC Berkeley's Device
Group](http://www-device.eecs.berkeley.edu/bsim/) and other independent
groups.

Each model is invoked with a .model card. A minimal version is:

.model MOSN NMOS level=8 version=3.3.0

The model name MOSN corresponds to the model name in the instance card
(see [11.1](#sec_MOSFET_devices)). Parameter NMOS selects an n-channel
device, PMOS would point to a p-channel transistor. The **level** and
**version** parameters select the specific model. Further model
parameters are optional and replace ngspice default values. Due to the
large number of parameters (more than 100 for modern models), model
cards may be stored in extra files and loaded into the netlist by the
.include ([2.6](#sec__INCLUDE)) command. Model cards are specific for a
an IC manufacturing process and are typically provided by the IC
foundry. Some generic parameter sets, not linked to a specific process,
are made available by the model developers, e.g. [UC Berkeley's Device
Group](http://www-device.eecs.berkeley.edu/bsim/) for BSIM4 and BSIMSOI.

Ngspice provides several MOSFET device models, which differ in the
formulation of the I-V characteristic, and are of varying complexity.
Models available are listed in table [11.1](#tab_MOSFET_model_summary).
Current models for IC design are BSIM3 ([11.2.10](#subsec_BSIM3_model),
down to channel length of 0.25 µm), BSIM4
([11.2.11](#subsec_BSIM4_model), below 0.25 µm), BSIMSOI
([11.2.13](#subsec_BSIMSOI_models), silicon-on-insulator devices),
HiSIM2 and HiSIM\_HV ([11.2.15](#subsec_HiSIM_models_of), surface
potential models for standard and high voltage/high power MOS devices).

<table style="width:100%;">
<colgroup>
<col width="14%" />
<col width="14%" />
<col width="14%" />
<col width="14%" />
<col width="14%" />
<col width="14%" />
<col width="14%" />
</colgroup>
<tbody>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-12140"></span><strong>Level</strong>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-12143"></span><strong>Name</strong>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-12146"></span><strong>Model</strong>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-12149"></span><strong>Version</strong>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-12152"></span><strong>Developer</strong>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-12155"></span><strong>References</strong>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-12158"></span><strong>Notes</strong>
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-12161"></span>1
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-12164"></span>MOS1
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-12167"></span>Shichman-Hodges
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-12170"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-12173"></span>Berkeley
</div></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-12179"></span>This is the classical quadratic model.
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-12182"></span>2
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-12185"></span>MOS2
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-12188"></span>Grove-Frohman
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-12191"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-12194"></span>Berkeley
</div></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-12200"></span>Described in [2]
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-12203"></span>3
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-12206"></span>MOS3
</div></td>
<td></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-12215"></span>Berkeley
</div></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-12221"></span>A semi-empirical model (see [1])
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-12224"></span>4
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-12227"></span>BSIM1
</div></td>
<td></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-12236"></span>Berkeley
</div></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-12242"></span>Described in [3]
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-12245"></span>5
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-12248"></span>BSIM2
</div></td>
<td></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-12257"></span>Berkeley
</div></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-12263"></span>Described in [5]
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-12266"></span>6
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-12269"></span>MOS6
</div></td>
<td></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-12278"></span>Berkeley
</div></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-12284"></span>Described in [2]
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-12287"></span>9
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-12290"></span>MOS9
</div></td>
<td></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-12299"></span>Alan Gillespie
</div></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-12308"></span>8, 49
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-12311"></span>BSIM3v0
</div></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-12317"></span>3.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-12320"></span>Berkeley
</div></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-12326"></span>extensions by Alan Gillespie
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-12329"></span>8, 49
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-12332"></span>BSIM3v1
</div></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-12338"></span>3.1
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-12341"></span>Berkeley
</div></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-12347"></span>extensions by Serban Popescu
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-12350"></span>8, 49
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-12353"></span>BSIM3v32
</div></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-12359"></span>3.2 - 3.2.4
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-12362"></span>Berkeley
</div></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-12368"></span>Multi version code
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-12371"></span>8, 49
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-12374"></span>BSIM3
</div></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-12380"></span>3.3.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-12383"></span>Berkeley
</div></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-12389"></span>Described in [13]
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-12392"></span>10, 58
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-12395"></span>B4SOI
</div></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-12401"></span>4.3.1
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-12404"></span>Berkeley
</div></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-12413"></span>14, 54
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-12416"></span>BSIM4v5
</div></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-12422"></span>4.0 - 4.5
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-12425"></span>Berkeley
</div></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-12431"></span>Multi version code
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-12434"></span>14, 54
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-12437"></span>BSIM4v6
</div></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-12443"></span>4.6.5
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-12446"></span>Berkeley
</div></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-12455"></span>14, 54
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-12458"></span>BSIM4v7
</div></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-12464"></span>4.7.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-12467"></span>Berkeley
</div></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-12476"></span>14, 54
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-12479"></span>BSIM4
</div></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-12485"></span>4.8.1
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-12488"></span>Berkeley
</div></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-12497"></span>44
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-12500"></span>EKV
</div></td>
<td></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-12509"></span>EPFL
</div></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-12515"></span>adms configured
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-12518"></span>45
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-12521"></span>PSP
</div></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-12527"></span>1.0.2
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-12530"></span>Gildenblatt
</div></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-12536"></span>adms configured
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-12539"></span>55
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-12542"></span>B3SOIFD
</div></td>
<td></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-12551"></span>Berkeley
</div></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-12560"></span>56
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-12563"></span>B3SOIDD
</div></td>
<td></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-12572"></span>Berkeley
</div></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-12581"></span>57
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-12584"></span>B3SOIPD
</div></td>
<td></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-12593"></span>Berkeley
</div></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-12602"></span>60
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-12605"></span>STAG
</div></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-12611"></span>SOI3
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-12614"></span>Southampton
</div></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-12623"></span>68
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-12626"></span>HiSIM2
</div></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-12632"></span>2.8.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-12635"></span>Hiroshima
</div></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-12644"></span>73
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-12647"></span>HiSIM_HV
</div></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-12653"></span>1.2.4/2.2.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-12656"></span>Hiroshima
</div></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-12662"></span>High Voltage Version for LDMOS
</div></td>
</tr>
</tbody>
</table>

Table 11.1:  MOSFET model summary

