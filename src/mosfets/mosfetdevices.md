# MOSFET devices

General form:

``` listings
MXXXXXXX nd ng ns nb mname <m=val> <l=val> <w=val>
+ <ad=val> <as=val> <pd=val> <ps=val> <nrd=val>
+ <nrs=val> <off> <ic=vds, vgs, vbs> <temp=t>
```

Examples:

``` listings
M1 24 2 0 20 TYPE1
M31 2 17 6 10 MOSN L=5U W=2U
M1 2 9 3 0 MOSP L=10U W=5U AD=100P AS=100P PD=40U PS=40U
```

Note the suffixes in the example: the suffix \`u' specifies microns
(1e-6 \(m\)) and \`p' sq-microns (1e-12 \(m^{2}\)).

The instance card for MOS devices starts with the letter '**M**'.
**nd**, **ng**, **ns**, and **nb** are the drain, gate, source, and bulk
(substrate) nodes, respectively. **mname** is the model name and **m**
is the multiplicity parameter, which simulates \`m' paralleled devices.
All MOS models support the \`**m**' multiplier parameter. Instance
parameters **l** and **w**, channel length and width respectively, are
expressed in meters. The areas of drain and source diffusions: **ad**
and **as**, in squared meters (\(m^{2}\)).

If any of **l**, **w**, **ad**, or **as** are not specified, default
values are used. The use of defaults simplifies input file preparation,
as well as the editing required if device geometries are to be changed.
**pd** and **ps** are the perimeters of the drain and source junctions,
in meters. **nrd** and **nrs** designate the equivalent number of
squares of the drain and source diffusions; these values multiply the
sheet resistance **rsh** specified on the .model control line for an
accurate representation of the parasitic series drain and source
resistance of each transistor. **pd** and **ps** default to 0.0 while
**nrd** and **nrs** to 1.0. **off** indicates an (optional) initial
condition on the device for dc analysis. The (optional) initial
condition specification using **ic=vds,vgs,vbs** is intended for use
with the **uic** option on the .tran control line, when a transient
analysis is desired starting from other than the quiescent operating
point. See the .ic control line for a better and more convenient way to
specify transient initial conditions. The (optional) **temp** value is
the temperature at which this device is to operate, and overrides the
temperature specification on the .option control line.

The temperature specification is ONLY valid for level 1, 2, 3, and 6
MOSFETs, not for level 4 or 5 (BSIM) devices.

BSIM3 (v3.2 and v3.3.0), BSIM4 (v4.7 and v4.8) and BSIMSOI models are
also supporting the instance parameter **delvto** and **mulu0** for
local mismatch and NBTI (negative bias temperature instability)
modeling:

<table>
<colgroup>
<col width="20%" />
<col width="20%" />
<col width="20%" />
<col width="20%" />
<col width="20%" />
</colgroup>
<tbody>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-11898"></span>Name
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11901"></span>Parameter
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11904"></span>Units
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11907"></span>Default
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11910"></span>Example
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-11913"></span>delvto (delvt0)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11916"></span>Threshold voltage shift
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11919"></span><span class="math inline"><em>V</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11922"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11925"></span>0.07
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-11928"></span>mulu0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11931"></span>Low-field mobility multiplier (U0)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11934"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11937"></span>1.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11940"></span>0.9
</div></td>
</tr>
</tbody>
</table>

