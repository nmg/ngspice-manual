# NGSPICE VDMOS parameters

 

<table>
<colgroup>
<col width="20%" />
<col width="20%" />
<col width="20%" />
<col width="20%" />
<col width="20%" />
</colgroup>
<thead>
<tr class="header">
<th><div class="plain_layout">
<span id="magicparlabel-14806"></span><strong>Name</strong>
</div></th>
<th><div class="plain_layout">
<span id="magicparlabel-14809"></span><strong>Parameter</strong>
</div></th>
<th><div class="plain_layout">
<span id="magicparlabel-14812"></span><strong>Units</strong>
</div></th>
<th><div class="plain_layout">
<span id="magicparlabel-14815"></span><strong>Default</strong>
</div></th>
<th><div class="plain_layout">
<span id="magicparlabel-14818"></span><strong>Example</strong>
</div></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-14821"></span>NCHAN
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14824"></span>NMOS
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14827"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14830"></span>default, if not given
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14833"></span>-
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-14836"></span>PCHAN
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14839"></span>PMOS
</div></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-14845"></span>required, if PMOS
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14848"></span>-
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-14851"></span>VTO
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14854"></span>Zero-bias threshold voltage (<span class="math inline"><em>V</em><sub><em>T</em>0</sub>)</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14857"></span><span class="math inline"><em>V</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14860"></span>0.0
</div></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-14866"></span>KP
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14869"></span>Transconductance parameter
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14872"></span><span class="math inline">$\frac{A}{V^{2}}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14875"></span>1.0
</div></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-14881"></span>LAMBDA
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14884"></span>Channel length modulation (<span class="math inline"><em>λ</em>)</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14887"></span><span class="math inline">$\frac{1}{V}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14890"></span>0.0
</div></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-14896"></span>THETA
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14899"></span>Vgs influence on mobility
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14902"></span><span class="math inline">$\frac{1}{V}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14905"></span>0.0
</div></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-14911"></span>RD
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14914"></span>Drain ohmic resistance
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14917"></span><span class="math inline"><em>Ω</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14920"></span>0.0
</div></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-14926"></span>RS
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14929"></span>Source ohmic resistance
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14932"></span><span class="math inline"><em>Ω</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14935"></span>0.0
</div></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-14941"></span>RG
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14944"></span>Gate ohmic resistance
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14947"></span><span class="math inline"><em>Ω</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14950"></span>0.0
</div></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-14956"></span>KF
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14959"></span>Flicker noise coefficient
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14962"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14965"></span>0.0
</div></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-14971"></span>AF
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14974"></span>Flicker noise exponent
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14977"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14980"></span>1.0
</div></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-14986"></span>TNOM
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14989"></span>Parameter measurement temperature
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14992"></span><span class="math inline"><em>C</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-14995"></span>27
</div></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-15016"></span>RQ
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-15019"></span>Quasi saturation resistance fitting parameter
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-15022"></span><span class="math inline"><em>Ω</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-15025"></span>0.0
</div></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-15031"></span>VQ
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-15034"></span>Quasi saturation voltage fitting parameter
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-15037"></span><span class="math inline"><em>V</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-15040"></span>1.0e-14
</div></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-15046"></span>MTRIODE
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-15049"></span>Conductance multiplier in triode region
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-15052"></span><span class="math inline">−</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-15055"></span>1.0
</div></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-15076"></span>SUBSLOPE
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-15079"></span>slope in the dual parameter subthreshold model
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-15082"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-15085"></span>0.0
</div></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-15091"></span>SUBSHIFT
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-15094"></span>shift along gate voltage axis in the dual parameter subthreshold model
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-15097"></span><span class="math inline"><em>V</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-15100"></span>0.0
</div></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-15106"></span>KSUBTHRES
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-15109"></span>slope in the single parameter subthreshold model
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-15112"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-15115"></span>0.0
</div></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-15136"></span>BV
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-15139"></span>Vds breakdown voltage
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-15142"></span><span class="math inline"><em>V</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-15145"></span><span class="math inline">∞</span>
</div></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-15151"></span>IBV
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-15154"></span>Current at Vds=bv
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-15157"></span><span class="math inline"><em>A</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-15160"></span>1.0e-10
</div></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-15166"></span>NBV
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-15169"></span>Vds breakdown emission coefficient
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-15172"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-15175"></span>1.0
</div></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-15181"></span>RDS
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-15184"></span>Drain-source shunt resistance
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-15187"></span><span class="math inline"><em>Ω</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-15190"></span><span class="math inline">∞</span>
</div></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-15196"></span>RB
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-15199"></span>Body diode ohmic resistance
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-15202"></span><span class="math inline"><em>Ω</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-15205"></span>0.0
</div></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-15211"></span>N
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-15214"></span>Body diode emission coefficient
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-15217"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-15220"></span>0.0
</div></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-15226"></span>TT
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-15229"></span>Body diode transit time
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-15232"></span><span class="math inline"><em>s</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-15235"></span>0.0
</div></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-15241"></span>EG
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-15244"></span>Body diode activation energy for temperature effect on IS
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-15247"></span><span class="math inline"><em>e</em><em>V</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-15250"></span>1.11
</div></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-15256"></span>XTI
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-15259"></span>Body diode saturation current temperature exponent
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-15262"></span>-
</div></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-15271"></span>IS
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-15274"></span>Body diode saturation current
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-15277"></span><span class="math inline"><em>A</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-15280"></span>1e-14
</div></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-15286"></span>VJ
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-15289"></span>Body diode junction potential
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-15292"></span><span class="math inline"><em>V</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-15295"></span>0.8
</div></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-15316"></span>FC
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-15319"></span>Body diode coefficient for forward-bias depletion capacitance formula
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-15322"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-15325"></span>0.0
</div></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-15331"></span>CJO
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-15334"></span>Zero-bias body diode junction capacitance
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-15337"></span><span class="math inline"><em>F</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-15340"></span>0.0
</div></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-15346"></span>M
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-15349"></span>Body diode grading coefficient
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-15352"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-15355"></span>1.0
</div></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-15376"></span>CGDMIN
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-15379"></span>Minimum non-linear G-D capacitance
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-15382"></span><span class="math inline"><em>F</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-15385"></span>0.0
</div></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-15391"></span>CGDMAX
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-15394"></span>Maximum non-linear G-D capacitance
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-15397"></span><span class="math inline"><em>F</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-15400"></span>0.0
</div></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-15406"></span>A
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-15409"></span>Non-linear Cgd capacitance parameter
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-15412"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-15415"></span>1
</div></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-15421"></span>CGS
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-15424"></span>Gate-source capacitance
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-15427"></span><span class="math inline"><em>F</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-15430"></span>0.0
</div></td>
<td></td>
</tr>
</tbody>
</table>

