#  Circuit Elements and Models

Data fields that are enclosed in less-than and greater-than signs (\`\<
\>') are optional. All indicated punctuation (parentheses, equal signs,
etc.) is optional but indicate the presence of any delimiter. Further,
future implementations may require the punctuation as stated. A
consistent style adhering to the punctuation shown here makes the input
easier to understand. With respect to branch voltages and currents,
ngspice uniformly uses the associated reference convention (current
flows in the direction of voltage drop).

