# Example 2

A more sophisticated input file for Monte Carlo simulation is
distributed with the file /examples/Monte\_Carlo/MC\_ring.sp ([or git
repository](http://ngspice.git.sourceforge.net/git/gitweb.cgi?p=ngspice/ngspice;a=blob;f=examples/Monte_Carlo/MC_ring.sp;h=58e5c141f5abcb6aa1e22cfcc0d22acabae56170;hb=HEAD)).
Due to its length it is not reproduced here, but some comments on its
enhancements over example 1 ([22.5.1](#subsec_Example_1_1)) are
presented in the following.

A 25-stage ring oscillator is the circuit used with a transient
simulation. It comprises of CMOS inverters, modeled with BSIM3. Several
model parameters (vth, u0, tox, L, and W) shall be varied statistically
between each simulation run. The frequency of oscillation will be
measured by a fft and stored. Finally a histogram of all measured
frequencies will be plotted.

The function calls to sunif(0) and sgauss(0) return uniformly or
Gaussian distributed random numbers. A function unif, defined by the
line

define unif(nom, var) (nom + (nom\*var) \* sunif(0))

will return a value with mean nom and deviation var relative to nom.

The line

set n1vth0=@n1\[vth0\]

will store the threshold voltage vth0, given by the model parameter set,
into a variable n1vth0, ready to be used by unif, aunif, gauss, or
agauss function calls.

In the simulation loop the altermod command changes the model parameters
before a call to tran. After the transient simulation the resulting
vector is linearized, a fft is calculated, and the maximum of the fft
signal is measured by the meas command and stored in a vector maxffts.
Finally the contents of the vector maxffts is plotted in a histogram.

For more details, please have a look at the strongly commented input
file MC\_ring.sp.

