# Example 3

The next example is contained in the files MC\_2\_control.sp and
MC\_2\_circ.sp from folder /examples/Monte\_Carlo/. MC\_2\_control.sp is
a ngspice script (see [17.8](#sec_SCRIPTS)). It starts a loop by setting
the random number generator seed value to the value of the loop counter,
sources the circuit file MC\_2\_circ.sp, runs the simulation, stores a
raw file, makes an fft, saves the oscillator frequency thus measured,
deletes all outputs, increases the loop counter and restarts the loop.
The netlist file MC\_2\_circ.sp contains the circuit, which is the same
ring oscillator as of example 2. However, now the MOS model parameter
set, which is included with this netlist file, inherits some AGAUSS
functions (see [2.8.5](#subsec_Syntax_of_expressions)) to vary threshold
voltage, mobility and gate oxide thickness of the NMOS and PMOS
transistors. This is an approach similar to what commercial foundries
deliver within their device libraries. So this example may be your
source for running Monte Carlo with commercial libs. Start example 3 by
calling

ngspice -o MC\_2\_control.log MC\_2\_control.sp

