# ngspice scripting language

The ngspice scripting language is described in detail in Chapt.
[17.8](#sec_SCRIPTS). All commands listed in Chapt.
[17.5](#sec_Commands) are available, as well as the built-in functions
described in Chapt. [17.2](#sec_Expressions__Functions__and), the
control structures listed in Chapt. [17.6](#sec_Control_Structures), and
the predefined variables from Chapt. [17.7](#sec_Variables). Variables
and functions are typically evaluated after a simulation run. You may
created loops with several simulation runs and change device and model
parameters with the **alter** ([17.5.3](#subsec_Alter___Change_a)) or
**altermod** ([17.5.4](#subsec_Altermod___Change_a)) commands, as shown
in the next section [22.5](#sec_Monte_Carlo_Simulation). You may even
interrupt a simulation run by proper usage of the **stop**
([17.5.79](#subsec_Stop___Set_a)) and **resume**
([17.5.57](#subsec_Resume___Continue_a)) commands. After stop you may
change device or model parameters and then go on with resume, continuing
the simulation with the new parameter values.

The statistical functions provided for scripting are listed in the
following table:

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-27025"></span>Name
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-27028"></span>Function
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-27031"></span>rnd(vector)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-27034"></span>A vector with each component a random integer between 0 and the absolute value of the input vector's corresponding integer element value.
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-27037"></span>sgauss(vector)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-27040"></span>Returns a vector of random numbers drawn from a Gaussian distribution (real value, mean = 0 , standard deviation = 1). The length of the vector returned is determined by the input vector. The contents of the input vector will not be used. A call to sgauss(0) will return a single value of a random number as a vector of length 1..
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-27043"></span>sunif(vector)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-27046"></span>Returns a vector of random real numbers uniformly distributed in the interval [-1 .. 1[. The length of the vector returned is determined by the input vector. The contents of the input vector will not be used. A call to sunif(0) will return a single value of a random number as a vector of length 1.
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-27049"></span>poisson(vector)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-27052"></span>Returns a vector with its elements being integers drawn from a Poisson distribution. The elements of the input vector (real numbers) are the expected numbers <span class="math inline"><em>λ</em></span>. Complex vectors are allowed, real and imaginary values are treated separately.
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-27055"></span>exponential(vector)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-27058"></span>Returns a vector with its elements (real numbers) drawn from an exponential distribution. The elements of the input vector are the respective mean values (real numbers). Complex vectors are allowed, real and imaginary values are treated separately.
</div></td>
</tr>
</tbody>
</table>

