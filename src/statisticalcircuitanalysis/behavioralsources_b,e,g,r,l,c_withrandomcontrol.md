# Behavioral sources \(B, E, G, R, L, C\) with random control

All sources listed in the section header may contain parameters, which
will be evaluated **before** simulation starts, as described in the
previous section ([22.2](#sec_Using_random_param_eters_)). In addition
the nonlinear voltage or current sources (B-source,
[5](#sec_Non_linear_Dependent_Sources)) as well as their derivatives E
and G, but also the behavioral R, L, and C may be controlled **during**
simulation by a random independent voltage source V with TRRANDOM option
(Chapt. [4.1.8](#subsec_Random_voltage_source)).

An example circuit, a Wien bridge oscillator from input file
/examples/Monte\_Carlo/OpWien.sp is distributed with ngspice or
available at Git. The two frequency determining pairs of R and C are
varied statistically using four independent Gaussian voltage sources as
the controlling units. An excerpt of this command sequence is shown
below. The total simulation time ttime is divided into 100 equally
spaced blocks. Each block will get a new set of control voltages, e.g.
VR2, which is Gaussian distributed, mean 0 and absolute deviation 1. The
resistor value is calculated with ±10% spread, the factor 0.033 will set
this 10% to be a deviation of 1 sigma from nominal value.

Examples for control of a behavioral resistor:

``` listings
* random resistor
.param res = 10k
.param ttime=12000m
.param varia=100 
.param ttime10 = 'ttime/varia'
* random control voltage (Gaussian distribution)
VR2 r2  0 dc 0 trrandom (2 'ttime10' 0 1)
* behavioral resistor
R2 4 6  R = 'res + 0.033 * res*V(r2)'
```

So within a single simulation run you will obtain 100 different
frequency values issued by the Wien bridge oscillator. The voltage
sequence VR2 is shown below.

![image:
6\_home\_nmg\_Documents\_gitroot\_ngspice-ngspice-manuals\_Images\_vr2-trrandom.png](6_home_nmg_Documents_gitroot_ngspice-ngspice-manuals_Images_vr2-trrandom.png)

