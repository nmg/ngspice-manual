# Monte-Carlo Simulation

The ngspice scripting language may be used to run Monte-Carlo
simulations with statistically varying device or model parameters. Calls
to the functions sgauss(0) or sunif(0) (see
[17.2](#sec_Expressions__Functions__and)) will return Gaussian or
uniform distributed random numbers (real numbers), stored in a vector.
You may define (see [17.5.15](#subsec_Define__Define_a)) your own
function using sgauss or sunif, e.g. to change the mean or range. In a
loop (see [17.6](#sec_Control_Structures)) then you may call the alter
([17.5.3](#subsec_Alter___Change_a)) or altermod
([17.5.4](#subsec_Altermod___Change_a)) statements with random
parameters followed by an analysis like op, dc, ac, tran or other.

