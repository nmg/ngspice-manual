# Data evaluation with Gnuplot

Run the example file /examples/Monte\_Carlo/OpWien.sp, described in
Chapt. [22.3](#sec_Behavioral_sources__B_). Generate a plot with Gnuplot
by the ngspice command

gnuplot pl4mag v4mag xlimit 500 1500

Open and run the command file in the Gnuplot command line window by

load 'pl-v4mag.p'

A Gaussian curve will be fitted to the simulation data. The mean
oscillator frequency and its deviation are printed in the curve fitting
log in the Gnuplot window.

Gnuplot script for data evaluation:

``` listings
 # This file: pl-v4mag.p
 # ngspice file OpWien.sp
 # ngspice command: 
 #  gnuplot pl4mag v4mag xlimit 500 1500
 # a gnuplot manual:
 # http://www.duke.edu/~hpgavin/gnuplot.html
 
 # Gauss function to be fitted
 f1(x)=(c1/(a1*sqrt(2*3.14159))*exp(-((x-b1)**2)/(2*a1**2)))
 # Gauss function to plot start graph
 f2(x)=(c2/(a2*sqrt(2*3.14159))*exp(-((x-b2)**2)/(2*a2**2)))
 # start values
 a1=50 ; b1=900 ; c1=50
 # keep start values in  a2, b2, c2
 a2=a1  b2=b1 ; c2=c1
 # curve fitting
 fit f1(x) 'pl4mag.data' using 1:2 via a1, b1, c1
 # plot original and fitted curves with new a1, b1, c1
 plot "pl4mag.data" using 1:2 with lines, f1(x), f2(x)
```

![image:
7\_home\_nmg\_Documents\_gitroot\_ngspice-ngspice-manuals\_Images\_gnuplot-stat-an.png](7_home_nmg_Documents_gitroot_ngspice-ngspice-manuals_Images_gnuplot-stat-an.png)

pl4mag.data is the simulation data, f2(x) the starting curve, f1(x) the
fitted Gaussian distribution.

This is just a simple example. You might explore the powerful built-in
functions of Gnuplot to do a much more sophisticated statistical data
analysis.

