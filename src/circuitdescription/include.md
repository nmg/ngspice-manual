# .INCLUDE

General form:

``` listings
.INCLUDE filename
```

Examples:

``` listings
.INCLUDE /users/spice/common/bsim3-param.mod
```

Frequently, portions of circuit descriptions will be reused in several
input files, particularly with common models and subcircuits. In any
ngspice input file, the .INCLUDE line may be used to copy some other
file as if that second file appeared in place of the .INCLUDE line in
the original file.

There is no restriction on the file name imposed by ngspice beyond those
imposed by the local operating system.

