# .LIB

General form:

``` listings
.LIB filename libname
```

Examples:

``` listings
.LIB /users/spice/common/mosfets.lib mos1
```

The .LIB statement allows to include library descriptions into the input
file. Inside the \*.lib file a library **libname** will be selected. The
statements of each library inside the \*.lib file are enclosed in .LIB
libname \<...\> .ENDL statements.

If the compatibility mode ([16.13](#sec_Compatibility)) is set to 'ps'
by set ngbehavior=ps ([17.7](#sec_Variables)) in spinit
([16.5](#sec_Standard_configuration_file)) or .spiceinit
([16.6](#sec_User_defined_configuration)), then a simplified syntax .LIB
filename is available: a warning is issued and filename is simply
included as described in Chapt. [2.6](#sec__INCLUDE).

