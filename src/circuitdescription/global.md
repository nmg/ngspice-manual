# .GLOBAL

General form:

``` listings
.GLOBAL nodename
```

Examples:

``` listings
.GLOBAL gnd vcc
```

Nodes defined in the .GLOBAL statement are available to all circuit and
subcircuit blocks independently from any circuit hierarchy. After
parsing the circuit, these nodes are accessible from top level.

