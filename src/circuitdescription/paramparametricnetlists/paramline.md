# .param line

General form:

``` listings
.param <ident> = <expr>  <ident> = <expr> ...
```

Examples:

``` listings
.param pippo=5
.param po=6 pp=7.8 pap={AGAUSS(pippo, 1, 1.67)}
.param pippp={pippo + pp}
.param p={pp}
.param pop='pp+p'
```

This line assigns numerical values to identifiers. More than one
assignment per line is possible using a separating space. Parameter
identifier names must begin with an alphabetic character. The other
characters must be either alphabetic, a number, or \! \# $ % \[ \] \_ as
special characters. The variables **time**, **temper**, and **hertz**
(see [5.1.1](#subsec_Syntax_and_usage)) are not valid identifier names.
Other restrictions on naming conventions apply as well, see
[2.8.6](#subsec_Reserved_words).

The .param lines inside subcircuits are copied per call, like any other
line. All assignments are executed sequentially through the expanded
circuit. Before its first use, a parameter name must have been assigned
a value. Expressions defining a parameter should be put within braces
{p+p2}, or alternatively within single quotes 'AGAUSS(pippo, 1, 1.67)'.
An assignment cannot be self-referential, something like .param pip =
'pip+3' will not work.

The current ngspice version does not always need quotes or braces in
expressions, especially when spaces are used sparingly. However, it is
recommended to do so, as the following examples demonstrate.

``` listings
.param a = 123 * 3    b = sqrt(9) $ doesn't work, a <= 123 
.param a = '123 * 3'  b = sqrt(9) $ ok.
.param c = a + 123   $ won't work
.param c = 'a + 123' $ ok. 
.param c = a+123     $ ok. 
```

