# Symbol scope

*All subcircuit and model names are considered global and must be
unique.* The .param symbols that are defined outside of any .subckt ...
.ends section are global. Inside such a section, the pertaining params:
symbols and any .param assignments are considered local: they mask any
global identical names, until the .ends line is encountered. You cannot
reassign to a global number inside a .subckt, a local copy is created
instead. Scope nesting works up to a level of 10. For example, if the
main circuit calls A that has a formal parameter xx, A calls B that has
a param. xx, and B calls C that also has a formal param. xx, there will
be three versions of \`xx' in the symbol table but only the most local
one - belonging to C - is visible.

