# Subcircuit parameters

General form:

``` listings
.subckt <identn> node node ...  <ident>=<value> <ident>=<value> ...
```

Examples:

``` listings
.subckt myfilter in out rval=100k cval=100nF
```

**\<identn\>** is the name of the subcircuit given by the user.** node**
is an integer number or an identifier, for one of the external nodes.
The first **\<ident\>=\<value\>** introduces an optional section of the
line. Each **\<ident\>** is a formal parameter, and each **\<value\>**
is either a SPICE number or a brace expression. Inside the .subckt ...
.ends context, each formal parameter may be used like any identifier
that was defined on a .param control line. The **\<value\>** parts are
supposed to be default values of the parameters. However, in the current
version of , they are not used and each invocation of the subcircuit
must supply the \_exact\_ number of actual parameters.

The syntax of a subcircuit call (invocation) is:

General form:

``` listings
X<name> node node ... <identn> <ident>=<value> <ident>=<value> ...
```

Examples:

``` listings
X1 input output myfilter rval=1k cval=1n
```

Here **\<name\>** is the symbolic name given to that instance of the
subcircuit, **\<identn\>** is the name of a subcircuit defined
beforehand. **node node ...** is the list of actual nodes where the
subcircuit is connected. **\<value\>** is either a SPICE number or a
brace expression **{ \<expr\> }** . The sequence of **\<value\>** items
on the X line must exactly match the number and the order of formal
parameters of the subcircuit.

Subcircuit example with parameters:

``` listings
* Param-example 
.param amplitude= 1V
*
.subckt myfilter in out rval=100k  cval=100nF 
Ra in p1   {2*rval} 
Rb p1 out  {2*rval} 
C1 p1 0    {2*cval} 
Ca in p2   {cval} 
Cb p2 out  {cval} 
R1 p2 0    {rval} 
.ends myfilter
*
X1 input output myfilter rval=1k cval=1n 
V1 input 0 AC {amplitude}
.end
```

