# A word of caution on the three ngspice expression parsers

The historical parameter notation using & as the first character of a
line as equivalence to .param. is deprecated and will be removed in a
coming release.

Confusion may arise in ngspice because of its multiple numerical
expression features. The .param lines and the brace expressions (see
Chapt. [2.9](#sec_func)) are evaluated in the front-end, that is, just
after the subcircuit expansion. (Technically, the X lines are kept as
comments in the expanded circuit so that the actual parameters can be
correctly substituted). Therefore, after the netlist expansion and
before the internal data setup, all number attributes in the circuit are
known constants. However, there are circuit elements in Spice that
accept arithmetic expressions *not* evaluated at this point, but only
later during circuit analysis. These are the arbitrary current and
voltage sources (B-sources, [5](#sec_Non_linear_Dependent_Sources)), as
well as E- and G-sources and R-, L-, or C-devices. The syntactic
difference is that \`compile-time' expressions are within braces, but
\`run-time' expressions have no braces. To make things more complicated,
the back-end ngspice scripting language accepts arithmetic/logic
expressions that operate only on its own scalar or vector data sets
([17.2](#sec_Expressions__Functions__and)). Please see Chapt.
[2.13](#sec_Parameters__functions__expressions_).

It would be desirable to have the same expression syntax, operator and
function set, and precedence rules, for the three contexts mentioned
above. In the current Numparam implementation, that goal is not
achieved.

