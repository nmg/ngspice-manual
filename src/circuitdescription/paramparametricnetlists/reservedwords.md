# Reserved words

In addition to the above function names and to the verbose operators (
not and or div mod ), other words are reserved and cannot be used as
parameter names: or, defined, sqr, sqrt, sin, cos, exp, ln, log, log10,
arctan, abs, pwr, time, temper, hertz.

