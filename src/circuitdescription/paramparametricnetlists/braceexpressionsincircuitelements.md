# Brace expressions in circuit elements:

General form:

``` listings
{ <expr> }
```

Examples:

These are allowed in .model lines and in device lines. A SPICE number is
a floating point number with an optional scaling suffix, immediately
glued to the numeric tokens (see Chapt.
[2.8.5](#subsec_Syntax_of_expressions)). Brace expressions ({..}) cannot
be used to parametrize node names or parts of names. All identifiers
used within an \<expr\> must have known values at the time when the line
is evaluated, else an error is flagged.

