# Syntax of expressions

\<expr\> ( optional parts within \[...\] )

An expression may be one of:

``` listings
<atom> where <atom> is either a spice number or an identifier 
<unary-operator> <atom> 
<function-name> ( <expr> [ , <expr> ...] ) 
<atom> <binary-operator> <expr> 
( <expr> )
```

As expected, atoms, built-in function calls and stuff within parentheses
are evaluated before the other operators. The operators are evaluated
following a list of precedence close to the one of the C language. For
equal precedence binary ops, evaluation goes left to right. Functions
operate on real values only\!

<table>
<colgroup>
<col width="25%" />
<col width="25%" />
<col width="25%" />
<col width="25%" />
</colgroup>
<tbody>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-1404"></span>Operator
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-1407"></span>Alias
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-1410"></span>Precedence
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-1413"></span>Description
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-1416"></span><span style="font-family:monospace;">-</span>
</div></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-1422"></span>1
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-1425"></span>unary -
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-1428"></span><span style="font-family:monospace;">!</span>
</div></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-1434"></span>1
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-1437"></span>unary not
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-1440"></span><span style="font-family:monospace;">**</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-1443"></span><span style="font-family:monospace;">^</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-1446"></span>2
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-1449"></span>power, like pwr
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-1452"></span><span style="font-family:monospace;">*</span>
</div></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-1458"></span>3
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-1461"></span>multiply
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-1464"></span><span style="font-family:monospace;">/</span>
</div></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-1470"></span>3
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-1473"></span>divide
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-1476"></span><span style="font-family:monospace;">%</span>
</div></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-1482"></span>3
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-1485"></span>modulo
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-1488"></span><span style="font-family:monospace;">\</span>
</div></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-1494"></span>3
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-1497"></span>integer divide
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-1500"></span><span style="font-family:monospace;">+</span>
</div></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-1506"></span>4
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-1509"></span>add
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-1512"></span><span style="font-family:monospace;">-</span>
</div></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-1518"></span>4
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-1521"></span>subtract
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-1524"></span><span style="font-family:monospace;">==</span>
</div></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-1530"></span>5
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-1533"></span>equality
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-1536"></span><span style="font-family:monospace;">!=</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-1539"></span><span style="font-family:monospace;">&lt;&gt;</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-1542"></span>5
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-1545"></span>non-equal
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-1548"></span><span style="font-family:monospace;">&lt;=</span>
</div></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-1554"></span>5
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-1557"></span>less or equal
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-1560"></span><span style="font-family:monospace;">&gt;=</span>
</div></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-1566"></span>5
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-1569"></span>greater or equal
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-1572"></span><span style="font-family:monospace;">&lt;</span>
</div></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-1578"></span>5
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-1581"></span>less than
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-1584"></span><span style="font-family:monospace;">&gt;</span>
</div></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-1590"></span>5
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-1593"></span>greater than
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-1596"></span><span style="font-family:monospace;">&amp;&amp;</span>
</div></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-1602"></span>6
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-1605"></span>boolean and
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-1608"></span><span style="font-family:monospace;">||</span>
</div></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-1614"></span>7
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-1617"></span>boolean or
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-1620"></span><span style="font-family:monospace;">c?x:y</span>
</div></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-1626"></span>8
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-1629"></span>ternary operator
</div></td>
</tr>
</tbody>
</table>

The number zero is used to represent boolean False. Any other number
represents boolean True. The result of logical operators is 1 or 0. An
example input file is shown below:

Example input file with logical operators:

``` listings
* Logical operators

v1or   1 0  {1 || 0}
v1and  2 0  {1 && 0}
v1not  3 0  {! 1}
v1mod  4 0  {5 % 3}
v1div  5 0  {5 \ 3}
v0not  6 0  {! 0}  

.control
op
print allv
.endc

.end
```

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-1715"></span>Built-in function
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-1718"></span>Notes
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-1721"></span>sqrt(x)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-1724"></span><span style="font-family:monospace;">y = sqrt(x)</span>
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-1727"></span>sin(x), cos(x), tan(x)
</div></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-1733"></span>sinh(x), cosh(x), tanh(x)
</div></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-1739"></span>asin(x), acos(x), atan(x)
</div></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-1745"></span>asinh(x), acosh(x), atanh(x)
</div></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-1751"></span>arctan(x)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-1754"></span><span style="font-family:monospace;">atan(x)</span>, kept for compatibility
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-1757"></span>exp(x)
</div></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-1763"></span>ln(x), log(x)
</div></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-1769"></span>abs(x)
</div></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-1775"></span>nint(x)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-1778"></span>Nearest integer, half integers towards even
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-1781"></span>int(x)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-1784"></span>Nearest integer rounded towards 0
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-1787"></span>floor(x)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-1790"></span>Nearest integer rounded towards <span style="font-family:monospace;">-∞</span>
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-1793"></span>ceil(x)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-1796"></span>Nearest integer rounded towards <span style="font-family:monospace;">+∞</span>
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-1799"></span>pow(x,y)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-1802"></span>x raised to the power of y (pow from C runtime library)
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-1805"></span>pwr(x,y)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-1808"></span><span style="font-family:monospace;">pow(fabs(x), y)</span>
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-1811"></span>min(x, y)
</div></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-1817"></span>max(x, y)
</div></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-1823"></span>sgn(x)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-1826"></span><span style="font-family:monospace;">1.0 for x &gt; 0, 0.0 for x == 0, -1.0 for x &lt; 0</span>
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-1829"></span>ternary_fcn(x, y, z)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-1832"></span><span style="font-family:monospace;">x ? y : z</span>
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-1835"></span>gauss(nom, rvar, sigma)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-1838"></span>nominal value plus variation drawn from Gaussian distribution with mean 0 and standard deviation rvar (relative to nominal), divided by sigma
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-1841"></span>agauss(nom, avar, sigma)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-1844"></span>nominal value plus variation drawn from Gaussian distribution with mean 0 and standard deviation avar (absolute), divided by sigma
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-1847"></span>unif(nom, rvar)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-1850"></span>nominal value plus relative variation (to nominal) uniformly distributed between +/-rvar
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-1853"></span>aunif(nom, avar)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-1856"></span>nominal value plus absolute variation uniformly distributed between +/-avar
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-1859"></span>limit(nom, avar)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-1862"></span>nominal value +/-avar, depending on random number in [-1, 1[ being <span style="font-family:monospace;">&gt; 0</span> or <span style="font-family:monospace;">&lt; 0</span>
</div></td>
</tr>
</tbody>
</table>

The scaling suffixes (any decorative alphanumeric string may follow):

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-1891"></span>suffix
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-1894"></span>value
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-1897"></span>g
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-1900"></span>1e9
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-1903"></span>meg
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-1906"></span>1e6
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-1909"></span>k
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-1912"></span>1e3
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-1915"></span>m
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-1918"></span>1e-3
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-1921"></span>u
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-1924"></span>1e-6
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-1927"></span>n
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-1930"></span>1e-9
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-1933"></span>p
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-1936"></span>1e-12
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-1939"></span>f
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-1942"></span>1e-15
</div></td>
</tr>
</tbody>
</table>

Note: there are intentional redundancies in expression syntax, e.g. x^y
, x\*\*y and pwr(x,y) all have nearly the same result.

