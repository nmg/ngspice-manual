# .IF Condition-Controlled Netlist

A simple .IF-.ELSE(IF) block allows condition-controlling of the
netlist. boolean expression is any expression according to Chapt.
[2.8.5](#subsec_Syntax_of_expressions) that evaluates parameters and
returns a boolean 1 or 0. The netlist block in between the **.if** ...
**.endif** statements may contain device instances or .model cards that
are selected according to the logic condition.

General form:

``` listings
.if(boolean expression)
...
.elseif(boolean expression)
...
.else
...
.endif
```

Example 1:

``` listings
* device instance in IF-ELSE block
.param ok=0 ok2=1

v1 1 0 1
R1 1 0 2

.if (ok && ok2)
R11 1 0 2
.else
R11 1 0 0.5   $ <-- selected
.endif
```

Example 2:

``` listings
* .model in IF-ELSE block
.param m0=0 m1=1

M1 1 2 3 4 N1 W=1 L=0.5

.if(m0==1)
.model N1 NMOS level=49 Version=3.1
.elseif(m1==1)
.model N1 NMOS level=49 Version=3.2.4  $ <-- selected
.else
.model N1 NMOS level=49 Version=3.3.0
.endif
```

Nesting of .IF-.ELSE(IF)-.ENDIF blocks is possible. Several .elseif are
allowed per block, of course only one .else (please see example
ngspice/tests/regression/misc/if-elseif.cir). However some restrictions
apply, as the following netlist components are *not* supported within
the .IF-.ENDIF block: .SUBCKT, .INC, .LIB, and .PARAM.

