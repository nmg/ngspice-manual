# Control commands, Command scripts

Commands, as described in detail in Chapt. [17.5](#sec_Commands), may be
used interactively, but also as a command script enclosed in .control
... .endc lines. The scripts may contain expressions (see Chapt.
[17.2](#sec_Expressions__Functions__and)). The expressions may work upon
simulation output vectors (of node voltages, branch currents), as well
as upon predefined or user defined vectors and variables, and are
invoked **after** the simulation. Parameters from
[2.8.1](#subsec__param_line) defined by the .param statement are not
allowed in these expressions. However you may define such parameters
with .csparam ([2.10](#sec__csparam)). Again the expression syntax (see
Chapt. [17.2](#sec_Expressions__Functions__and)) will deviate from the
one for parameters or B sources listed in [2.8.1](#subsec__param_line)
and [5.1](#sec_B_source__ASRC_).

If you want to use parameters from [2.8.1](#subsec__param_line) inside
your control script, you may use .csparam ([2.10](#sec__csparam)) or
apply a trick by defining a voltage source with the parameter as its
value, and then have it available as a vector (e.g. after a transient
simulation) with a then constant output (the parameter). A feedback from
here back into parameters ([2.13.1](#subsec_Parameters)) is never
possible. Also you cannot access non-linear sources of the preceding
simulation. However you may start a first simulation inside your control
script, then evaluate its output using expressions, change some of the
element or model parameters with the alter and altermod statements (see
Chapt. [17.5.3](#subsec_Alter___Change_a)) and then automatically start
a new simulation.

Expressions and scripting are powerful tools within ngspice, and we will
enhance the examples given in Chapt. [21](#cha_Example_Circuits)
continuously to describe these features.

