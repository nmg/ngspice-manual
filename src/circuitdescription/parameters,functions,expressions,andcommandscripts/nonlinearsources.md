# Nonlinear sources

During the simulation, the B source (Chapt.
[5](#sec_Non_linear_Dependent_Sources)) and their associated E and G
sources, as well as some devices (R, C, L) may contain expressions.
These expressions may contain parameters from above (evaluated
immediately upon ngspice start up), numerical data, predefined
functions, but also node voltages and branch currents resulting from the
simulation. The source or device values are continuously updated
**during** the simulation. Therefore the sources are powerful tools to
define non-linear behavior, you may even create new \`devices' by
yourself. Unfortunately the expression syntax (see Chapt.
[5.1](#sec_B_source__ASRC_)) and the predefined functions may deviate
from the ones for parameters listed in [2.8.1](#subsec__param_line).

