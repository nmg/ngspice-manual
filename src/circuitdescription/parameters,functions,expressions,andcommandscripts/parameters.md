# Parameters

Parameters (Chapt. [2.8.1](#subsec__param_line)) and functions, either
defined within the .param statement or with the .func statement (Chapt.
[2.9](#sec_func)) are evaluated **before** any simulation is started,
that is during the setup of the input and the circuit. Therefore these
statements may not contain any simulation output (voltage or current
vectors), because it is simply not yet available. The syntax is
described in Chapt. [2.8.5](#subsec_Syntax_of_expressions). During the
circuit setup all functions are evaluated, all parameters are replaced
by their resulting numerical values. Thus it will not be possible to get
feedback from a later stage (during or after simulation) to change any
of the parameters.

