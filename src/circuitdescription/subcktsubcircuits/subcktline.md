# .SUBCKT Line

General form:

``` listings
.SUBCKT subnam N1 <N2 N3 ...>
```

Examples:

``` listings
.SUBCKT OPAMP 1 2 3 4
```

A circuit definition is begun with a .SUBCKT line. subnam is the
subcircuit name, and N1, N2, ... are the external nodes, which cannot be
zero. The group of element lines that immediately follow the .SUBCKT
line define the subcircuit. The last line in a subcircuit definition is
the .ENDS line (see below). Control lines may not appear within a
subcircuit definition; however, subcircuit definitions may contain
anything else, including other subcircuit definitions, device models,
and subcircuit calls (see below). Note that any device models or
subcircuit definitions included as part of a subcircuit definition are
strictly local (i.e., such models and definitions are not known outside
the subcircuit definition). Also, any element nodes not included on the
.SUBCKT line are strictly local, with the exception of 0 (ground) that
is always global. If you use parameters, the .SUBCKT line will be
extended (see [2.8.3](#subsec_Subcircuit_parameters)).

