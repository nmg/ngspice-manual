# Subcircuit Calls

General form:

``` listings
XYYYYYYY N1 <N2 N3 ...> SUBNAM
```

Examples:

``` listings
X1 2 4 17 3 1 MULTI
```

Subcircuits are used in ngspice by specifying pseudo-elements beginning
with the letter X, followed by the circuit nodes to be used in expanding
the subcircuit. If you use parameters, the subcircuit call will be
modified (see [2.8.3](#subsec_Subcircuit_parameters)).

