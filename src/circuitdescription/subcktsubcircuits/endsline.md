# .ENDS Line

General form:

``` listings
.ENDS <SUBNAM>
```

Examples:

``` listings
.ENDS OPAMP
```

The .ENDS line must be the last one for any subcircuit definition. The
subcircuit name, if included, indicates which subcircuit definition is
being terminated; if omitted, all subcircuits being defined are
terminated. The name is needed only when nested subcircuit definitions
are being made.

