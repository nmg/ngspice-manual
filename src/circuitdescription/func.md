# .FUNC

This keyword defines a function. The syntax of the expression is the
same as for a .param ([2.8.5](#subsec_Syntax_of_expressions)).

General form:

``` listings
.func <ident> { <expr> }
.func <ident> = { <expr> }
```

Examples:

``` listings
.func icos(x) {cos(x) - 1}
.func f(x,y) {x*y}
.func foo(a,b) = {a + b}
```

.func will initiate a replacement operation. After reading the input
files, and before parameters are evaluated, all occurrences of the
icos(x) function will be replaced by cos(x)-1. All occurrences of f(x,y)
will be replaced by x\*y. Function statements may be nested to a depth
of t.b.d..

