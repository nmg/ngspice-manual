# .CSPARAM

Create a constant vector (see [17.8.2](#subsec_Vectors)) from a
parameter in plot ([17.3](#sec_Plots)) const.

General form:

``` listings
.csparam <ident> = <expr>
```

Examples:

``` listings
.param pippo=5
.param pp=6
.csparam pippp={pippo + pp}
.param p={pp}
.csparam pap='pp+p'
```

In the example shown, vectors pippp, and pap are added to the constants
that already reside in plot const, having length one and real values.
These vectors are generated during circuit parsing and thus cannot be
changed later (same as with ordinary parameters). They may be used in
ngspice scripts and .control sections (see Chapt.
[17](#chap_Interactive_Interpreter)).

The use of .csparam is still experimental and has to be tested. A simple
usage is shown below.

\* test csparam

.param TEMPS = 27

.csparam newt = {3\*TEMPS}

.csparam mytemp = '2 + TEMPS'

.control

echo $\&newt $\&mytemp

.endc

.end

