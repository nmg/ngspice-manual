# Parameters, functions, expressions, and command scripts

In ngspice there are several ways to describe functional dependencies.
In fact there are three independent function parsers, being active
before, during, and after the simulation. So it might be due to have a
few words on their interdependence.

