# Circuit elements \(device instances\)

Each element in the circuit is a device instance specified by an
**instance line** that contains:

  - the element instance name,
  - the circuit nodes to which the element is connected,
  - and the values of the parameters that determine the electrical
    characteristics of the element.

The first letter of the element instance name specifies the element
type. The format for the ngspice element types is given in the following
manual chapters. In the rest of the manual, the strings XXXXXXX,
YYYYYYY, and ZZZZZZZ denote arbitrary alphanumeric strings.

For example, a resistor instance name must begin with the letter R and
can contain one or more characters. Hence, R, R1,** **RSE, ROUT, and
R3AC2ZY are valid resistor names. Details of each type of device are
supplied in a following section [3](#cha_Circuit_Elements_and). Table
[2.1](#tab_ngspice_element_types) lists the element types available in
ngspice, sorted by their first letter.

<table>
<colgroup>
<col width="33%" />
<col width="33%" />
<col width="33%" />
</colgroup>
<tbody>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-364"></span>First letter
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-367"></span>Element description
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-370"></span>Comments, links
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-373"></span>A
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-376"></span>XSPICE code model
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-379"></span><a href="#cha_Behavioral_Modeling">12</a><br />
analog (<a href="#sec_XSPICE_Analog_Models">12.2</a>)<br />
digital (<a href="#sec_XSPICE_Digital_Models">12.4</a>)<br />
mixed signal (<a href="#sec_XSPICE_Hybrid_Models">12.3</a>)
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-382"></span>B
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-385"></span>Behavioral (arbitrary) source
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-388"></span><a href="#sec_B_source__ASRC_">5.1</a>
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-391"></span>C
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-394"></span>Capacitor
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-397"></span><a href="#subsec_Capacitors">3.2.5</a>
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-400"></span>D
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-403"></span>Diode
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-406"></span><a href="#cha_DIODEs">7</a>
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-409"></span>E
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-412"></span>Voltage-controlled voltage source (VCVS)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-415"></span>linear (<a href="#subsec_Exxxx__Linear_Voltage_Controlled">4.2.2</a>),<br />
non-linear (<a href="#sec_E_source__non_linear">5.2</a>)
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-418"></span>F
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-421"></span>Current-controlled current source (CCCs)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-424"></span>linear (<a href="#subsec_Fxxxx__Linear_Current_Controlled">4.2.3</a>)
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-427"></span>G
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-430"></span>Voltage-controlled current source (VCCS)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-433"></span>linear (<a href="#subsec_Gxxxx__Linear_Voltage_Controlled">4.2.1</a>),<br />
non-linear (<a href="#sec_G_source__non_linear">5.3</a>)
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-436"></span>H
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-439"></span>Current-controlled voltage source (CCVS)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-442"></span>linear (<a href="#subsec_Hxxxx__Linear_Current_Controlled">4.2.4</a>)
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-445"></span>I
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-448"></span>Current source
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-451"></span><a href="#sec_Independent_Sources_for">4.1</a>
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-454"></span>J
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-457"></span>Junction field effect transistor (JFET)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-460"></span><a href="#cha_JFETs">9</a>
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-463"></span>K
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-466"></span>Coupled (Mutual) Inductors
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-469"></span><a href="#subsec_Coupled__Mutual__Inductors">3.2.11</a>
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-472"></span>L
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-475"></span>Inductor
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-478"></span><a href="#subsec_Inductors">3.2.9</a>
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-481"></span>M
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-484"></span>Metal oxide field effect transistor (MOSFET)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-487"></span><a href="#cha_MOSFETs">11</a><br />
BSIM3 (<a href="#subsec_BSIM3_model">11.2.10</a>)<br />
BSIM4 (<a href="#subsec_BSIM4_model">11.2.11</a>)
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-490"></span>N
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-493"></span>Numerical device for GSS
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-496"></span><a href="#sec_GSS__Genius">14.2</a>
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-499"></span>O
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-502"></span>Lossy transmission line
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-505"></span><a href="#sec_Lossy_Transmission_Lines">6.2</a>
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-508"></span>P
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-511"></span>Coupled multiconductor line (CPL)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-514"></span><a href="#subsec_Coupled_Multiconductor_Line">6.4.2</a>
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-517"></span>Q
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-520"></span>Bipolar junction transistor (BJT)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-523"></span><a href="#cha_BJTs">8</a>
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-526"></span>R
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-529"></span>Resistor
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-532"></span><a href="#subsec_Resistors">3.2.1</a>
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-535"></span>S
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-538"></span>Switch (voltage-controlled)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-541"></span><a href="#subsec_Switches">3.2.14</a>
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-544"></span>T
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-547"></span>Lossless transmission line
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-550"></span><a href="#sec_Lossless_Transmission_Lines">6.1</a>
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-553"></span>U
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-556"></span>Uniformly distributed RC line
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-559"></span><a href="#sec_Uniform_Distributed_RC">6.3</a>
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-562"></span>V
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-565"></span>Voltage source
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-568"></span><a href="#sec_Independent_Sources_for">4.1</a>
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-571"></span>W
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-574"></span>Switch (current-controlled)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-577"></span><a href="#subsec_Switches">3.2.14</a>
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-580"></span>X
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-583"></span>Subcircuit
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-586"></span><a href="#subsec_Subcircuit_Calls">2.4.3</a>
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-589"></span>Y
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-592"></span>Single lossy transmission line (TXL)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-595"></span><a href="#subsec_Single_Lossy_Transmission">6.4.1</a>
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-598"></span>Z
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-601"></span>Metal semiconductor field effect transistor (MESFET)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-604"></span><a href="#cha_MESFETs">10</a>
</div></td>
</tr>
</tbody>
</table>

Table 2.1:  ngspice element types

