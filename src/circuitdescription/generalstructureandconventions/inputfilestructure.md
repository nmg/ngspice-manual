# Input file structure

The circuit to be analyzed is described to ngspice by a set of element
instance lines, which define the circuit topology and element instance
values, and a set of control lines, which define the model parameters
and the run controls. All lines are assembled in an input file to be
read by ngspice. Two lines are essential:

  - The first line in the input file must be the title, which is the
    only comment line that does not need any special character in the
    first place.
  - The last line must be .end.

The order of the remaining lines is arbitrary (except, of course, that
continuation lines must immediately follow the line being continued).
This feature in the ngspice input language dates back to the punched
card times where elements were written on separate cards (and cards
frequently fell off). Leading white spaces in a line are ignored, as
well as empty lines.

The lines described in sections 2.1 to 2.12 are typically used in the
core of the input file, outside of a .control section (see
[16.4.3](#subsec_Interactive_mode_with)). An exception is the .include
includefile line ([2.6](#sec__INCLUDE)) that may be placed anywhere in
the input file. The contents of includefile will be inserted exactly in
place of the .include line.

