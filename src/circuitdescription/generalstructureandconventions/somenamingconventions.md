# Some naming conventions

Fields on a line are separated by one or more blanks, a comma, an equal
(=) sign, or a left or right parenthesis; extra spaces are ignored. A
line may be continued by entering a \`+' (plus) in column 1 of the
following line; ngspice continues reading beginning with column 2. A
name field must begin with a letter (A through Z) and cannot contain any
delimiters. A number field may be an integer field (12, -44), a floating
point field (3.14159), either an integer or floating point number
followed by an integer exponent (1e-14, 2.65e3), or either an integer or
a floating point number followed by one of the following scale factors:

<table>
<colgroup>
<col width="33%" />
<col width="33%" />
<col width="33%" />
</colgroup>
<tbody>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-659"></span>Suffix
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-662"></span>Name
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-665"></span>Factor
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-668"></span>T
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-671"></span>Tera
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-674"></span><span class="math inline">10<sup>12</sup></span>
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-677"></span>G
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-680"></span>Giga
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-683"></span><span class="math inline">10<sup>9</sup></span>
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-686"></span>Meg
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-689"></span>Mega
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-692"></span><span class="math inline">10<sup>6</sup></span>
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-695"></span>K
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-698"></span>Kilo
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-701"></span><span class="math inline">10<sup>3</sup></span>
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-704"></span>mil
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-707"></span>Mil
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-710"></span><span class="math inline">25.4 × 10<sup>−6</sup></span>
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-713"></span>m
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-716"></span>milli
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-719"></span><span class="math inline">10<sup>−3</sup></span>
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-722"></span>u
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-725"></span>micro
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-728"></span><span class="math inline">10<sup>−6</sup></span>
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-731"></span>n
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-734"></span>nano
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-737"></span><span class="math inline">10<sup>−9</sup></span>
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-740"></span>p
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-743"></span>pico
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-746"></span><span class="math inline">10<sup>−12</sup></span>
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-749"></span>f
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-752"></span>femto
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-755"></span><span class="math inline">10<sup>−15</sup></span>
</div></td>
</tr>
</tbody>
</table>

Table 2.2: Ngspice scale factors

Letters immediately following a number that are not scale factors are
ignored, and letters immediately following a scale factor are ignored.
Hence, 10, 10V, 10Volts, and 10Hz all represent the same number, and M,
MA, MSec, and MMhos all represent the same scale factor. Note that 1000,
1000.0, 1000Hz, 1e3, 1.0e3, 1kHz, and 1k all represent the same number.
Note that *\`M'* or *\`m'* denote \`milli', i.e. \(10^{- 3}\). Suffix
*meg* has to be used for \(10^{6}\).

Nodes names may be arbitrary character strings and are case insensitive,
if ngspice is used in batch mode ([16.4.1](#subsec_Batch_mode)). If in
interactive ([16.4.2](#subsec_Interactive_mode)) or control
([16.4.3](#subsec_Interactive_mode_with)) mode, node names may either be
plain numbers or arbitrary character strings, **not** starting with a
number. The ground node must be named \`0' (zero). For compatibility
reason gnd is accepted as ground node, and will internally be treated as
a global node and be converted to \`0'. If this is not feasible, you may
switch the conversion off by setting set no\_auto\_gnd in one of the
configuration files spinit or .spiceinit. *Each circuit has to have a
ground node (gnd or 0)\!* Note the difference in ngspice where the nodes
are treated as character strings and not evaluated as numbers, thus \`0'
and 00 are distinct nodes in ngspice but not in SPICE2.

Ngspice requires that the following topological constraints are
satisfied:

  - The circuit cannot contain a loop of voltage sources and/or
    inductors and cannot contain a cut-set of current sources and/or
    capacitors.
  - Each node in the circuit must have a dc path to ground.
  - Every node must have at least two connections except for
    transmission line nodes (to permit unterminated transmission lines)
    and MOSFET substrate nodes (which have two internal connections
    anyway).

