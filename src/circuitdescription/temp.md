# .TEMP

Sets the circuit temperature in degrees Celsius.

General form:

``` listings
.temp value
```

Examples:

``` listings
.temp 27
```

This card overrides the circuit temperature given in an .option line
([15.1.1](#subsec_General_Options)).

