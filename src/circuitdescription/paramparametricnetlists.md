# .PARAM Parametric netlists

Ngspice allows for the definition of parametric attributes in the
netlists. This is an enhancement of the ngspice front-end that adds
arithmetic functionality to the circuit description language.

