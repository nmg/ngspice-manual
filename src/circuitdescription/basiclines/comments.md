# Comments

General Form:

``` listings
* <any comment>
```

Examples:

``` listings
* RF=1K Gain should be 100
* Check open-loop gain and phase margin
```

The asterisk in the first column indicates that this line is a comment
line. Comment lines may be placed anywhere in the circuit description.

