# .TITLE line

Examples:

``` listings
POWER AMPLIFIER CIRCUIT
* additional lines following
*...

Test of CAM cell
* additional lines following
*...
```

The title line must be the first in the input file. Its contents are
printed verbatim as the heading for each section of output.

As an alternative you may place a .TITLE \<any title\> line anywhere in
your input deck. The first line of your input deck will be overridden by
the contents of this line following the .TITLE statement.

.TITLE line example:

``` listings
******************************
* additional lines following
*...
.TITLE Test of CAM cell
* additional lines following
*...
```

will internally be replaced by

Internal input deck:

``` listings
Test of CAM cell
* additional lines following
*...
*TITLE Test of CAM cell
* additional lines following
*...
```

