# End-of-line comments

General Form:

``` listings
<any command> $ <any comment>
```

Examples:

``` listings
RF2=1K $ Gain should be 100
C1=10p $ Check open-loop gain and phase margin
.param n1=1 //new value
```

ngspice supports comments that begin with double characters \`$ '
(dollar plus space) or \`//'. For readability you should precede each
comment character with a space. ngspice will accept the single character
\`$'.

Please note that in .control sections the \`;' character means
\`continuation' and can be used to put more than one statement on a
line.

