# .END Line

Examples:

``` listings
.end
```

The .end line must always be the last in the input file. Note that the
period is an integral part of the name.

