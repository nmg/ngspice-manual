# .SUBCKT Subcircuits

A subcircuit that consists of ngspice elements can be defined and
referenced in a fashion similar to device models. Subcircuits are the
way ngspice implements hierarchical modeling, but this is not entirely
true because each subcircuit instance is flattened during parsing, and
thus ngspice is not a hierarchical simulator.

The subcircuit is defined in the input deck by a grouping of element
cards delimited by the .subckt and the .ends cards (or the keywords
defined by the substart and subend options (see
[17.7](#sec_Variables))); the program then automatically inserts the
defined group of elements wherever the subcircuit is referenced.
Instances of subcircuits within a larger circuit are defined through the
use of an instance card that begins with the letter \`X'. A complete
example of all three of these cards follows:

Example:

``` listings
* The following is the instance card: 
*
xdiv1 10 7 0 vdivide

* The following are the subcircuit definition cards: 
*
.subckt vdivide 1 2 3 
r1 1 2 10K 
r2 2 3 5K 
.ends
```

The above specifies a subcircuit with ports numbered \`1', \`2' and
\`3':

  - Resistor \`R1' is connected from port \`1' to port \`2', and has
    value 10 kOhms.
  - Resistor \`R2' is connected from port \`2' to port \`3', and has
    value 5 kOhms.

The instance card, when placed in an ngspice deck, will cause subcircuit
port \`1' to be equated to circuit node \`10', while port \`2' will be
equated to node \`7' and port \`3' will equated to node \`0'.

There is no limit on the size or complexity of subcircuits, and
subcircuits may contain other subcircuits. An example of subcircuit
usage is given in Chapt. [21.6](#sec_MOS_Four_Bit).

