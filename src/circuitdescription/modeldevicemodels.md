# .MODEL Device Models

General form:

``` listings
.model mname type(pname1=pval1 pname2=pval2 ... )
```

Examples:

``` listings
.model MOD1 npn (bf=50 is=1e-13 vbf=50)
```

Most simple circuit elements typically require only a few parameter
values. However, some devices (semiconductor devices in particular) that
are included in ngspice require many parameter values. Often, many
devices in a circuit are defined by the same set of device model
parameters. For these reasons, a set of device model parameters is
defined on a separate .model line and assigned a unique model name. The
device element lines in ngspice then refer to the model name.

For these more complex device types, each device element line contains
the device name, the nodes the device is connected to, and the device
model name. In addition, other optional parameters may be specified for
some devices: geometric factors and an initial condition (see the
following section on Transistors ([8](#cha_BJTs) to [11](#cha_MOSFETs))
and Diodes ([7](#cha_DIODEs)) for more details). mname in the above is
the model name, and type is one of the following fifteen types:

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-935"></span>Code
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-938"></span>Model Type
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-941"></span>R
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-944"></span>Semiconductor resistor model
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-947"></span>C
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-950"></span>Semiconductor capacitor model
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-953"></span>L
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-956"></span>Inductor model
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-959"></span>SW
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-962"></span>Voltage controlled switch
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-965"></span>CSW
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-968"></span>Current controlled switch
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-971"></span>URC
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-974"></span>Uniform distributed RC model
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-977"></span>LTRA
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-980"></span>Lossy transmission line model
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-983"></span>D
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-986"></span>Diode model
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-989"></span>NPN
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-992"></span>NPN BJT model
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-995"></span>PNP
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-998"></span>PNP BJT model
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-1001"></span>NJF
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-1004"></span>N-channel JFET model
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-1007"></span>PJF
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-1010"></span>P-channel JFET model
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-1013"></span>NMOS
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-1016"></span>N-channel MOSFET model
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-1019"></span>PMOS
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-1022"></span>P-channel MOSFET model
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-1025"></span>NMF
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-1028"></span>N-channel MESFET model
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-1031"></span>PMF
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-1034"></span>P-channel MESFET model
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-1037"></span>VDMOS
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-1040"></span>Power MOS model
</div></td>
</tr>
</tbody>
</table>

Table 2.3: Ngspice model types

Parameter values are defined by appending the parameter name followed by
an equal sign and the parameter value. Model parameters that are not
given a value are assigned the default values given below for each model
type. Models are listed in the section on each device along with the
description of device element lines. Model parameters and their default
values are given in Chapt. [31](#cha_Model_and_Device).

