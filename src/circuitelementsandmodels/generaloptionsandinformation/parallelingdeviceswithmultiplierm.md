# Paralleling devices with multiplier m

When it is needed to simulate several devices of the same kind in
parallel, use the \`m' (parallel multiplier) instance parameter
available for the devices listed in Table [3.1](#tab_multiplier). This
multiplies the value of the element's matrix stamp with m's value. The
netlist below shows how to correctly use the parallel multiplier:

Multiple device example:

``` listings
d1 2 0 mydiode m=10
d01 1 0 mydiode
d02 1 0 mydiode
d03 1 0 mydiode
d04 1 0 mydiode
d05 1 0 mydiode
d06 1 0 mydiode
d07 1 0 mydiode
d08 1 0 mydiode
d09 1 0 mydiode
d10 1 0 mydiode
...
```

The d1 instance connected between nodes 2 and 0 is equivalent to the 10
parallel devices d01-d10 connected between nodes 1 and 0.

The following devices support the multiplier m:

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-2152"></span>First letter
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2155"></span>Element description
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-2158"></span>C
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2161"></span>Capacitor
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-2164"></span>D
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2167"></span>Diode
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-2170"></span>F
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2173"></span>Current-controlled current source (CCCs)
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-2176"></span>G
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2179"></span>Voltage-controlled current source (VCCS)
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-2182"></span>I
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2185"></span>Current source
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-2188"></span>J
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2191"></span>Junction field effect transistor (JFET)
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-2194"></span>L
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2197"></span>Inductor
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-2200"></span>M
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2203"></span>Metal oxide field effect transistor (MOSFET)
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-2206"></span>Q
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2209"></span>Bipolar junction transistor (BJT)
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-2212"></span>R
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2215"></span>Resistor
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-2218"></span>X
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2221"></span>Subcircuit (for details see below)
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-2224"></span>Z
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2227"></span>Metal semiconductor field effect transistor (MESFET)
</div></td>
</tr>
</tbody>
</table>

Table 3.1:  ngspice elements supporting multiplier 'm'

When the X line (e.g. x1 a b sub1 m=5) contains the token m=value (as
shown) or m=expression, subcircuit invocation is done in a special way.
If an instance line of the subcircuit sub1 contains any of the elements
shown in table [3.1](#tab_multiplier), then these elements are
instantiated with the additional parameter m (in this example having the
value 5). If such an element already has an m multiplier parameter, the
element m is multiplied with the m derived from the X line. This works
recursively, meaning that if a subcircuit contains another subcircuit (a
nested X line), then the latter m parameter will be multiplied by the
former one, and so on.

Example 1:

``` listings
.param madd = 6
X1 a b sub1 m=5
.subckt sub1 a1 b1
   Cs1 a1 b1 C=5p m='madd-2'
.ends
```

In example 1, the capacitance between nodes a and b will be C =
5pF\*(madd-2)\*5 = 100pF.

Example 2:

``` listings
.param madd = 4
X1 a b sub1 m=3
.subckt sub1 a1 b1
   X2 a1 b1 sub2 m='madd-2'
.ends
.subckt sub2 a2 b2
   Cs2 a2 b2 3p m=2
.ends
```

In example 2, the capacitance between nodes a and b is C =
3pF\*2\*(madd-2)\*3 = 36pF.

Using m may fail to correctly describe geometrical properties for real
devices like MOS transistors.

M1 d g s nmos W=0.3u L=0.18u m=20

is probably not be the same as

M1 d g s nmos W=6u L=0.18u

because the former may suffer from small width (or edge) effects,
whereas the latter is simply a wide transistor.

