# Model binning

Binning is a kind of range partitioning for geometry dependent models
like MOSFET's. The purpose is to cover larger geometry ranges (Width and
Length) with higher accuracy then the model built-in geometry formulas.
Each size range described by the additional model parameters LMIN, LMAX,
WMIN and WMAX has its own model parameter set. These model cards are
defined by a number extension, like \`nch.1'. NGSPICE has a algorithm to
choose the right model card by the requested W and L.

This is implemented for BSIM3 ([11.2.10](#subsec_BSIM3_model)) and BSIM4
([11.2.11](#subsec_BSIM4_model)) models.

