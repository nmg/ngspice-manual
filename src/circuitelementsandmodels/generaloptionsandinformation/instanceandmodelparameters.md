# Instance and model parameters

The simple device example below consists of two lines: The device is
defined on the instance line, starting with Lload ...: The first letter
determines the device type (an inductor in this example). Following the
device name are two nodes 1 and 2, then the inductance value 1u is set.
The model name ind1 is a connection to the respective model line.
Finally we have a parameter on the instance line, together with its
value dtemp=5. Parameters on an instance line are called instance
parameters.

The model line starts with the token .model, followed by the model name,
the model type and at leat one model parameter, here tc1=0.001. There
are complex models with more than 100 model parameters.

``` listings
Lload 1 2 1u ind1 dtemp=5 
.MODEL ind1 L tc1=0.001
```

Instance parameters are listed in each of the following device
descriptions. Model parameters sometimes are given below as well, for
complex models like the BSIM transistor models, they are available in
the model makers
[documentation](http://ngspice.sourceforge.net/literature.html).
Instance parameters may also be placed in the .model line. Thus they are
reckognized by each device instance referring to that model. Their
values may be overridden for a specific instance of a device by placing
them additionally onto its instance line.

