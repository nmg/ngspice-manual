# Inductor model

The inductor model contains physical and geometrical information that
may be used to compute the inductance of some common topologies like
solenoids and toroids, wound in air or other material with constant
magnetic permeability.

<table>
<colgroup>
<col width="20%" />
<col width="20%" />
<col width="20%" />
<col width="20%" />
<col width="20%" />
</colgroup>
<tbody>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-3386"></span>Name
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3389"></span>Parameter
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3392"></span>Units
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3395"></span>Default
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3398"></span>Example
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-3401"></span>IND
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3404"></span>model inductance
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3407"></span><span class="math inline"><em>H</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3410"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3413"></span>1e-3
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-3416"></span>CSECT
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3419"></span>cross section
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3422"></span><span class="math inline"><em>m</em><sup>2</sup></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3425"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3428"></span>1e-3
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-3431"></span>LENGTH
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3434"></span>length
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3437"></span><span class="math inline"><em>m</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3440"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3443"></span>1e-2
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-3446"></span>TC1
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3449"></span>first order temperature coeff.
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3452"></span><span class="math inline">$\frac{H}{C}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3455"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3458"></span>0.001
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-3461"></span>TC2
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3464"></span>second order temperature coeff.
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3467"></span><span class="math inline">$\frac{H}{C^{2}}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3470"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3473"></span>0.0001
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-3476"></span>TNOM
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3479"></span>parameter measurement temperature
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3482"></span><span class="math inline"><em>C</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3485"></span>27
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3488"></span>50
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-3491"></span>NT
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3494"></span>number of turns
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3497"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3500"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3503"></span>10
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-3506"></span>MU
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3509"></span>relative magnetic permeability
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3512"></span><span class="math inline">$\frac{H}{m}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3515"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3518"></span>-
</div></td>
</tr>
</tbody>
</table>

The inductor has an inductance computed as:

If **value** is specified on the instance line then

\[\begin{array}{ll}
{L_{nom} = \frac{{\lbrack font\ rm\ \lbrack char\ v\ mathalpha\rbrack\lbrack char\ a\ mathalpha\rbrack\lbrack char\ l\ mathalpha\rbrack\lbrack char\ u\ mathalpha\rbrack\lbrack char\ e\ mathalpha\rbrack\rbrack}{\lbrack font\ rm\ \lbrack char\ s\ mathalpha\rbrack\lbrack char\ c\ mathalpha\rbrack\lbrack char\ a\ mathalpha\rbrack\lbrack char\ l\ mathalpha\rbrack\lbrack char\ e\ mathalpha\rbrack\rbrack}}{m}} & \\
\end{array}\]

If model inductance is specified then

\[\begin{array}{ll}
{L_{nom} = \frac{{\lbrack font\ rm\ \lbrack char\ I\ mathalpha\rbrack\lbrack char\ N\ mathalpha\rbrack\lbrack char\ D\ mathalpha\rbrack\rbrack}{\lbrack font\ rm\ \lbrack char\ s\ mathalpha\rbrack\lbrack char\ c\ mathalpha\rbrack\lbrack char\ a\ mathalpha\rbrack\lbrack char\ l\ mathalpha\rbrack\lbrack char\ e\ mathalpha\rbrack\rbrack}}{m}} & \\
\end{array}\]

If neither **value** nor **IND** are specified, then geometrical and
physical parameters are take into account. In the following formulas

**NT** refers to both instance and model parameter (instance parameter
overrides model parameter):

If **LENGTH** is not zero:

\[\begin{array}{ll}
\begin{cases}
{L_{nom} = \frac{{\lbrack font\ rm\ \lbrack char\ M\ mathalpha\rbrack\lbrack char\ U\ mathalpha\rbrack\rbrack}\mu_{0}{\lbrack font\ rm\ \lbrack char\ N\ mathalpha\rbrack\lbrack char\ T\ mathalpha\rbrack\rbrack}^{2}{\lbrack font\ rm\ \lbrack char\ C\ mathalpha\rbrack\lbrack char\ S\ mathalpha\rbrack\lbrack char\ E\ mathalpha\rbrack\lbrack char\ C\ mathalpha\rbrack\lbrack char\ T\ mathalpha\rbrack\rbrack}}{\lbrack font\ rm\ \lbrack char\ L\ mathalpha\rbrack\lbrack char\ E\ mathalpha\rbrack\lbrack char\ N\ mathalpha\rbrack\lbrack char\ G\ mathalpha\rbrack\lbrack char\ T\ mathalpha\rbrack\lbrack char\ H\ mathalpha\rbrack\rbrack}} & {{if}{\lbrack font\ rm\ \lbrack char\ M\ mathalpha\rbrack\lbrack char\ U\ mathalpha\rbrack\lbrack mathrm\ \lbrack space\ 6\rbrack\ \lbrack char\ i\ mathalpha\rbrack\lbrack char\ s\ mathalpha\rbrack\lbrack space\ 6\rbrack\ \lbrack char\ s\ mathalpha\rbrack\lbrack char\ p\ mathalpha\rbrack\lbrack char\ e\ mathalpha\rbrack\lbrack char\ c\ mathalpha\rbrack\lbrack char\ i\ mathalpha\rbrack\lbrack char\ f\ mathalpha\rbrack\lbrack char\ i\ mathalpha\rbrack\lbrack char\ e\ mathalpha\rbrack\lbrack char\ d\ mathalpha\rbrack\lbrack char\ ,\ mathalpha\rbrack\rbrack\rbrack}} \\
{L_{nom} = \frac{\mu_{0}{\lbrack font\ rm\ \lbrack char\ N\ mathalpha\rbrack\lbrack char\ T\ mathalpha\rbrack\rbrack}^{2}{\lbrack font\ rm\ \lbrack char\ C\ mathalpha\rbrack\lbrack char\ S\ mathalpha\rbrack\lbrack char\ E\ mathalpha\rbrack\lbrack char\ C\ mathalpha\rbrack\lbrack char\ T\ mathalpha\rbrack\rbrack}}{\lbrack font\ rm\ \lbrack char\ L\ mathalpha\rbrack\lbrack char\ E\ mathalpha\rbrack\lbrack char\ N\ mathalpha\rbrack\lbrack char\ G\ mathalpha\rbrack\lbrack char\ T\ mathalpha\rbrack\lbrack char\ H\ mathalpha\rbrack\rbrack}} & {otherwise.} \\
\end{cases} & \\
\end{array}\]

with \(\mu_{0} = 1.25663706143592\frac{\mu H}{m}\). After the nominal
inductance is calculated, it is adjusted for temperature by the formula

\[\begin{array}{ll}
{L\left( T \right) = L\left( {\lbrack font\ rm\ \lbrack char\ T\ mathalpha\rbrack\lbrack char\ N\ mathalpha\rbrack\lbrack char\ O\ mathalpha\rbrack\lbrack char\ M\ mathalpha\rbrack\rbrack} \right)\left( 1 + TC_{1}\left( {T - {\lbrack font\ rm\ \lbrack char\ T\ mathalpha\rbrack\lbrack char\ N\ mathalpha\rbrack\lbrack char\ O\ mathalpha\rbrack\lbrack char\ M\ mathalpha\rbrack\rbrack}} \right) + TC_{2}\left( T - {\lbrack font\ rm\ \lbrack char\ T\ mathalpha\rbrack\lbrack char\ N\ mathalpha\rbrack\lbrack char\ O\ mathalpha\rbrack\lbrack char\ M\ mathalpha\rbrack\rbrack})^{2} \right), \right.} & \\
\end{array}\]

where
\(L\left( {\lbrack font\ rm\ \lbrack char\ T\ mathalpha\rbrack\lbrack char\ N\ mathalpha\rbrack\lbrack char\ O\ mathalpha\rbrack\lbrack char\ M\ mathalpha\rbrack\rbrack} \right) = L_{nom}\).
In the above formula, \`\(T\)' represents the instance temperature,
which can be explicitly set using the **temp** keyword or calculated
using the circuit temperature and **dtemp**, if present.

