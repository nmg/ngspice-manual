# Semiconductor Resistor Model \(R\)

The resistor model consists of process-related device data that allow
the resistance to be calculated from geometric information and to be
corrected for temperature. The parameters available are:

<table>
<colgroup>
<col width="20%" />
<col width="20%" />
<col width="20%" />
<col width="20%" />
<col width="20%" />
</colgroup>
<tbody>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-2470"></span>Name
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2473"></span>Parameter
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2476"></span>Units
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2479"></span>Default
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2482"></span>Example
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-2485"></span>TC1
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2488"></span>first order temperature coeff.
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2491"></span><span class="math inline">$\frac{\Omega}{C}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2494"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2497"></span>-
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-2500"></span>TC2
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2503"></span>second order temperature coeff.
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2506"></span><span class="math inline">$\frac{\Omega}{C^{2}}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2509"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2512"></span>-
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-2515"></span>RSH
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2518"></span>sheet resistance
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2521"></span><span class="math inline">$\frac{\Omega}{\square}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2524"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2527"></span>50
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-2530"></span>DEFW
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2533"></span>default width
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2536"></span><span class="math inline"><em>m</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2539"></span>1e-6
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2542"></span>2e-6
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-2545"></span>NARROW
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2548"></span>narrowing due to side etching
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2551"></span><span class="math inline"><em>m</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2554"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2557"></span>1e-7
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-2560"></span>SHORT
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2563"></span>shortening due to side etching
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2566"></span><span class="math inline"><em>m</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2569"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2572"></span>1e-7
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-2575"></span>TNOM
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2578"></span>parameter measurement temperature
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2581"></span><span class="math inline"><em>C</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2584"></span>27
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2587"></span>50
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-2590"></span>KF
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2593"></span>flicker noise coefficient
</div></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-2599"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2602"></span>1e-25
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-2605"></span>AF
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2608"></span>flicker noise exponent
</div></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-2614"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2617"></span>1.0
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-2620"></span>WF
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2623"></span>flicker noise width exponent
</div></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-2629"></span>1.0
</div></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-2635"></span>LF
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2638"></span>flicker noise length exponent
</div></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-2644"></span>1.0
</div></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-2650"></span>EF
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2653"></span>flicker noise frequency exponent
</div></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-2659"></span>1.0
</div></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-2665"></span>R (RES)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2668"></span>default value if element value not given
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2671"></span><span class="math inline"><em>Ω</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2674"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2677"></span>1000
</div></td>
</tr>
</tbody>
</table>

The sheet resistance is used with the narrowing parameter and **l** and
**w** from the resistor device to determine the nominal resistance by
the formula:

\[\begin{array}{ll}
{R_{nom} = {\lbrack font\ rm\ \lbrack char\ r\ mathalpha\rbrack\lbrack char\ s\ mathalpha\rbrack\lbrack char\ h\ mathalpha\rbrack\rbrack}\frac{l - {\lbrack font\ rm\ \lbrack char\ S\ mathalpha\rbrack\lbrack char\ H\ mathalpha\rbrack\lbrack char\ O\ mathalpha\rbrack\lbrack char\ R\ mathalpha\rbrack\lbrack char\ T\ mathalpha\rbrack\rbrack}}{w - {\lbrack font\ rm\ \lbrack char\ N\ mathalpha\rbrack\lbrack char\ A\ mathalpha\rbrack\lbrack char\ R\ mathalpha\rbrack\lbrack char\ R\ mathalpha\rbrack\lbrack char\ O\ mathalpha\rbrack\lbrack char\ W\ mathalpha\rbrack\rbrack}}} & \\
\end{array}\]

**DEFW** is used to supply a default value for **w** if one is not
specified for the device. If either **rsh** or **l** is not specified,
then the standard default resistance value of 1 mOhm is used. **TNOM**
is used to override the circuit-wide value given on the .options control
line where the parameters of this model have been measured at a
different temperature. After the nominal resistance is calculated, it is
adjusted for temperature by the formula:

\[\begin{array}{ll}
{R\left( T \right) = R\left( {\lbrack font\ rm\ \lbrack char\ T\ mathalpha\rbrack\lbrack char\ N\ mathalpha\rbrack\lbrack char\ O\ mathalpha\rbrack\lbrack char\ M\ mathalpha\rbrack\rbrack} \right)\left( 1 + TC_{1}\left( {T - {\lbrack font\ rm\ \lbrack char\ T\ mathalpha\rbrack\lbrack char\ N\ mathalpha\rbrack\lbrack char\ O\ mathalpha\rbrack\lbrack char\ M\ mathalpha\rbrack\rbrack}} \right) + TC_{2}\left( T - {\lbrack font\ rm\ \lbrack char\ T\ mathalpha\rbrack\lbrack char\ N\ mathalpha\rbrack\lbrack char\ O\ mathalpha\rbrack\lbrack char\ M\ mathalpha\rbrack\rbrack})^{2} \right) \right.} & \\
\end{array}\]

where
\(R\left( {\lbrack font\ rm\ \lbrack char\ T\ mathalpha\rbrack\lbrack char\ N\ mathalpha\rbrack\lbrack char\ O\ mathalpha\rbrack\lbrack char\ M\ mathalpha\rbrack\rbrack} \right) = R_{nom}|R_{acnom}\).
In the above formula, \`\(T\)' represents the instance temperature,
which can be explicitly set using the **temp** keyword or calculated
using the circuit temperature and **dtemp**, if present. If both
**temp** and **dtemp** are specified, the latter is ignored. Ngspice
improves SPICE's resistors noise model, adding flicker noise
(\(\frac{1}{f}\)) to it and the **noisy (or noise)** keyword to simulate
noiseless resistors. The thermal noise in resistors is modeled according
to the equation:

\[\begin{array}{ll}
{\bar{i_{R}^{2}} = \frac{4kT}{R}\Delta f} & \\
\end{array}\]

where \`\(k\)' is the Boltzmann's constant, and \`\(T\)' the instance
temperature.

Flicker noise model is:

\[\begin{array}{ll}
{\bar{i_{Rfn}^{2}} = \frac{{\lbrack font\ rm\ \lbrack char\ K\ mathalpha\rbrack\lbrack char\ F\ mathalpha\rbrack\rbrack}I_{R}^{\lbrack font\ rm\ \lbrack char\ A\ mathalpha\rbrack\lbrack char\ F\ mathalpha\rbrack\rbrack}}{W^{WF}L^{LF}f^{EF}}\Delta f} & \\
\end{array}\]

A small list of sheet resistances (in \(\frac{\Omega}{\square}\)) for
conductors is shown below. The table represents typical values for MOS
processes in the 0.5 - 1 um

range. The table is taken from: *N. Weste, K. Eshraghian - Principles of
CMOS VLSI Design 2nd Edition, Addison Wesley*.

<table>
<colgroup>
<col width="25%" />
<col width="25%" />
<col width="25%" />
<col width="25%" />
</colgroup>
<tbody>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-2732"></span>Material
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2735"></span>Min.
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2738"></span>Typ.
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2741"></span>Max.
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-2744"></span>Inter-metal (metal1 - metal2)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2747"></span>0.005
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2750"></span>0.007
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2753"></span>0.1
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-2756"></span>Top-metal (metal3)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2759"></span>0.003
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2762"></span>0.004
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2765"></span>0.05
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-2768"></span>Polysilicon (poly)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2771"></span>15
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2774"></span>20
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2777"></span>30
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-2780"></span>Silicide
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2783"></span>2
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2786"></span>3
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2789"></span>6
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-2792"></span>Diffusion (n+, p+)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2795"></span>10
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2798"></span>25
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2801"></span>100
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-2804"></span>Silicided diffusion
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2807"></span>2
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2810"></span>4
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2813"></span>10
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-2816"></span>n-well
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2819"></span>1000
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2822"></span>2000
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-2825"></span>5000
</div></td>
</tr>
</tbody>
</table>

