# Inductors

General form:

``` listings
LYYYYYYY n+ n- <value> <mname> <nt=val> <m=val>
+ <scale=val> <temp=val> <dtemp=val> <tc1=val>
+ <tc2=val> <ic=init_condition>
```

Examples:

``` listings
LLINK 42 69 1UH
LSHUNT 23 51 10U IC=15.7MA
```

The inductor device implemented into ngspice has many enhancements over
the original one.**n+** and **n-** are the positive and negative element
nodes, respectively. **value** is the inductance in Henry. Inductance
can be specified in the instance line as in the examples above or in a
.model line, as in the example below:

``` listings
L1 15 5 indmod1
L2 2 7 indmod1
.model indmod1 L ind=3n
```

Both inductors have an inductance of 3nH.

The **nt** is used in conjunction with a .model line, and is used to
specify the number of turns of the inductor. If you want to simulate
temperature dependence of an inductor, you need to specify its
temperature coefficients, using a .model line, like in the example
below:

``` listings
Lload 1 2 1u ind1 dtemp=5 
.MODEL ind1 L tc1=0.001
```

The (optional) initial condition is the initial (time zero) value of
inductor current (in Amps) that flows from **n+**, through the inductor,
to **n-**. Note that the initial conditions (if any) apply only if the
**UIC** option is specified on the .tran analysis line.

Ngspice calculates the nominal inductance as described below:

\[\begin{array}{ll}
{L_{nom} = \frac{{\lbrack font\ rm\ \lbrack char\ v\ mathalpha\rbrack\lbrack char\ a\ mathalpha\rbrack\lbrack char\ l\ mathalpha\rbrack\lbrack char\ u\ mathalpha\rbrack\lbrack char\ e\ mathalpha\rbrack\rbrack}{\lbrack font\ rm\ \lbrack char\ s\ mathalpha\rbrack\lbrack char\ c\ mathalpha\rbrack\lbrack char\ a\ mathalpha\rbrack\lbrack char\ l\ mathalpha\rbrack\lbrack char\ e\ mathalpha\rbrack\rbrack}}{m}} & \\
\end{array}\]

