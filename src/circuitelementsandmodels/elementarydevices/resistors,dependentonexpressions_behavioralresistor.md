# Resistors, dependent on expressions \(behavioral resistor\)

General form:

``` listings
RXXXXXXX n+ n- R = 'expression' <tc1=value> <tc2=value> <noisy=0>
RXXXXXXX n+ n- 'expression' <tc1=value> <tc2=value> <noisy=0>
```

Examples:

``` listings
R1 rr 0 r = 'V(rr) < {Vt} ? {R0} : {2*R0}' tc1=2e-03 tc2=3.3e-06
R2 r2 rr r = {5k + 50*TEMPER}
.param rp1 = 20
R3 no1 no2 r = '5k * rp1' noisy=1
```

**Expression** may be an equation or an expression containing node
voltages or branch currents (in the form of i(vm)) and any other terms
as given for the B source and described in Chapt.
[5.1](#sec_B_source__ASRC_). It may contain parameters
([2.8.1](#subsec__param_line)) and the special variables time, temper,
and hertz ([5.1.2](#subsec_Special_B_Source_Variables)). An example file
is given below. Small signal noise in the resistor
([15.3.4](#subsec__NOISE__Noise_Analysis)) may be evaluated as white
noise, depending on resistance, temperature and tc1, tc2. To enable
noise calculation, add the flag noisy=1 to the instance line. As a
default the behavioral resistor is noiseless.

Example input file for non-linear resistor:

``` listings
Non-linear resistor   
.param R0=1k Vi=1 Vt=0.5
* resistor depending on control voltage V(rr) 
R1 rr 0 r = 'V(rr) < {Vt} ? {R0} : {2*R0}'
* control voltage 
V1 rr 0 PWL(0 0 100u {Vi})
.control 
unset askquit 
tran 100n 100u uic 
plot i(V1) 
.endc   
.end
```

