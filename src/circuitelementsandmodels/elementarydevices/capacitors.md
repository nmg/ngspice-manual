# Capacitors

General form:

``` listings
CXXXXXXX n+ n- <value> <mname> <m=val> <scale=val> <temp=val> 
+ <dtemp=val> <tc1=val> <tc2=val> <ic=init_condition>
```

Examples:

``` listings
CBYP 13 0 1UF
COSC 17 23 10U IC=3V
```

Ngspice provides a detailed model for capacitors. Capacitors in the
netlist can be specified giving their capacitance or their geometrical
and physical characteristics. Following the original SPICE3
\`convention', capacitors specified by their geometrical or physical
characteristics are called \`semiconductor capacitors' and are described
in the next section.

In this first form **n+** and **n-** are the positive and negative
element nodes, respectively and **value** is the capacitance in Farads.

Capacitance can be specified in the instance line as in the examples
above or in a .model line, as in the example below:

``` listings
C1 15 5 cstd
C2 2 7 cstd
.model cstd C cap=3n
```

Both capacitors have a capacitance of 3nF.

If you want to simulate temperature dependence of a capacitor, you need
to specify its temperature coefficients, using a .model line, like in
the example below:

``` listings
CEB 1 2 1u cap1 dtemp=5 
.MODEL cap1 C tc1=0.001
```

The (optional) initial condition is the initial (time zero) value of
capacitor voltage (in Volts). Note that the initial conditions (if any)
apply only if the **uic** option is specified on the .tran control line.

Ngspice calculates the nominal capacitance as described below:

\[\begin{array}{ll}
{C_{nom} = {{{\lbrack font\ rm\ \lbrack char\ v\ mathalpha\rbrack\lbrack char\ a\ mathalpha\rbrack\lbrack char\ l\ mathalpha\rbrack\lbrack char\ u\ mathalpha\rbrack\lbrack char\ e\ mathalpha\rbrack\rbrack} \cdot {\lbrack font\ rm\ \lbrack char\ s\ mathalpha\rbrack\lbrack char\ c\ mathalpha\rbrack\lbrack char\ a\ mathalpha\rbrack\lbrack char\ l\ mathalpha\rbrack\lbrack char\ e\ mathalpha\rbrack\rbrack}} \cdot m}} & \\
\end{array}\]

The temperature coefficients tc1 and tc2 describe a quadratic
temperature dependence (see equation[77](#eq_)) of the capacitance. If
given in the instance line (the C... line) their values will override
the tc1 and tc2 of the .model line
([3.2.7](#subsec_Semiconductor_Capacitor_Model)).

