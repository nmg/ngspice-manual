# Semiconductor Resistors

General form:

``` listings
RXXXXXXX n+ n- <value> <mname> <l=length> <w=width>
+ <temp=val> <dtemp=val> <m=val> <ac=val> <scale=val>
+ <noisy = 0|1>
```

Examples:

``` listings
RLOAD 2 10 10K
RMOD 3 7 RMODEL L=10u W=1u
```

This is the more general form of the resistor presented before
([3.2.1](#subsec_Resistors)) and allows the modeling of temperature
effects and for the calculation of the actual resistance value from
strictly geometric information and the specifications of the process. If
**value** is specified, it overrides the geometric information and
defines the resistance. If **mname** is specified, then the resistance
may be calculated from the process information in the model** mname**
and the given **length** and **width**. If **value** is not specified,
then **mname** and **length** must be specified. If **width** is not
specified, then it is taken from the default width given in the model.

The (optional) **temp** value is the temperature at which this device is
to operate, and overrides the temperature specification on the .option
control line and the value specified in **dtemp**.

