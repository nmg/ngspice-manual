# Inductors, dependent on expressions \(behavioral inductor\)

General form:

``` listings
LXXXXXXX n+ n- L = 'expression' <tc1=value> <tc2=value>
LXXXXXXX n+ n- 'expression' <tc1=value> <tc2=value>
```

Examples:

``` listings
L1 l2 lll L = 'i(Vm) < {It} ? {Ll} : {Lh}' tc1=-4e-03 tc2=6e-05
```

**Expression** may be an equation or an expression containing node
voltages or branch currents (in the form of i(vm)) and any other terms
as given for the B source and described in Chapt.
[5.1](#sec_B_source__ASRC_). It may contain parameters
([2.8.1](#subsec__param_line)) and the special variables time, temper,
and hertz ([5.1.2](#subsec_Special_B_Source_Variables)).

Example input file:

``` listings
Variable inductor
.param Ll=0.5m Lh=5m It=50u Vi=2m 
.ic v(int21) = 0

* variable inductor depending on control current i(Vm) 
L1 l2 lll L = 'i(Vm) < {It} ? {Ll} : {Lh}' 
* measure current through inductor 
vm lll 0 dc 0 
* voltage on inductor 
V1 l2 0 {Vi}

* fixed inductor
L3 33 331 {Ll} 
* measure current through inductor 
vm33 331 0 dc 0 
* voltage on inductor 
V3 33 0 {Vi}

* non linear inductor (discrete setup)
F21 int21 0 B21 -1 
L21 int21 0 1 
B21 n1 n2 V = '(i(Vm21) < {It} ? {Ll} : {Lh})' * v(int21) 
* measure current through inductor 
vm21 n2 0 dc 0 
V21 n1 0 {Vi}

.control 
unset askquit 
tran 1u 100u uic 
plot i(Vm) i(vm33) 
plot i(vm21) i(vm33) 
plot i(vm)-i(vm21) 
.endc
.end
```

