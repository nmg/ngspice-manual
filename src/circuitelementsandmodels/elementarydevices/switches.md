# Switches

Two types of switches are available: a voltage controlled switch (type
SXXXXXX, model SW) and a current controlled switch (type WXXXXXXX, model
CSW). A switching hysteresis may be defined, as well as on- and
off-resistances (\(\left. 0 < R < \infty \right)\).

General form:

``` listings
SXXXXXXX N+ N- NC+ NC- MODEL <ON><OFF>
WYYYYYYY N+ N- VNAM MODEL <ON><OFF>
```

Examples:

``` listings
s1 1 2 3 4 switch1 ON
s2 5 6 3 0 sm2 off
Switch1 1 2 10 0 smodel1
w1 1 2 vclock switchmod1
W2 3 0 vramp sm1 ON
wreset 5 6 vclck lossyswitch OFF
```

Nodes 1 and 2 are the nodes between which the switch terminals are
connected. The model name is mandatory while the initial conditions are
optional. For the voltage controlled switch, nodes 3 and 4 are the
positive and negative controlling nodes respectively. For the current
controlled switch, the controlling current is that through the specified
voltage source. The direction of positive controlling current flow is
from the positive node, through the source, to the negative node.

The instance parameters ON or OFF are required, when the controlling
voltage (current) starts inside the range of the hysteresis loop
(different outputs during forward vs. backward voltage or current ramp).
Then ON or OFF determine the initial state of the switch.

