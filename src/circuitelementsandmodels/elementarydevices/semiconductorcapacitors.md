# Semiconductor Capacitors

General form:

``` listings
CXXXXXXX n+ n- <value> <mname> <l=length> <w=width> <m=val> 
+ <scale=val> <temp=val> <dtemp=val> <ic=init_condition>
```

Examples:

``` listings
CLOAD 2 10 10P
CMOD 3 7 CMODEL L=10u W=1u
```

This is the more general form of the Capacitor presented in section
([3.2.5](#subsec_Capacitors)), and allows for the calculation of the
actual capacitance value from strictly geometric information and the
specifications of the process. If **value** is specified, it defines the
capacitance and both process and geometrical information are discarded.
If **value** is not specified, the capacitance is calculated from
information contained model **mname** and the given length and width
(**l**, **w** keywords, respectively).

It is possible to specify **mname** only, without geometrical dimensions
and set the capacitance in the .model line
([3.2.5](#subsec_Capacitors)).

