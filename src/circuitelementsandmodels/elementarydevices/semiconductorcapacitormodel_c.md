# Semiconductor Capacitor Model \(C\)

The capacitor model contains process information that may be used to
compute the capacitance from strictly geometric information.

<table>
<colgroup>
<col width="20%" />
<col width="20%" />
<col width="20%" />
<col width="20%" />
<col width="20%" />
</colgroup>
<tbody>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-3018"></span>Name
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3021"></span>Parameter
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3024"></span>Units
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3027"></span>Default
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3030"></span>Example
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-3033"></span>CAP
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3036"></span>model capacitance
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3039"></span><span class="math inline"><em>F</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3042"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3045"></span>1e-6
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-3048"></span>CJ
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3051"></span>junction bottom capacitance
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3054"></span><span class="math inline">$\frac{F}{m^{2}}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3057"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3060"></span>5e-5
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-3063"></span>CJSW
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3066"></span>junction sidewall capacitance
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3069"></span><span class="math inline">$\frac{F}{m}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3072"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3075"></span>2e-11
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-3078"></span>DEFW
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3081"></span>default device width
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3084"></span><span class="math inline"><em>m</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3087"></span>1e-6
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3090"></span>2e-6
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-3093"></span>DEFL
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3096"></span>default device length
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3099"></span><span class="math inline"><em>m</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3102"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3105"></span>1e-6
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-3108"></span>NARROW
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3111"></span>narrowing due to side etching
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3114"></span><span class="math inline"><em>m</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3117"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3120"></span>1e-7
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-3123"></span>SHORT
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3126"></span>shortening due to side etching
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3129"></span><span class="math inline"><em>m</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3132"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3135"></span>1e-7
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-3138"></span>TC1
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3141"></span>first order temperature coeff.
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3144"></span><span class="math inline">$\frac{F}{C}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3147"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3150"></span>0.001
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-3153"></span>TC2
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3156"></span>second order temperature coeff.
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3159"></span><span class="math inline">$\frac{F}{C^{2}}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3162"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3165"></span>0.0001
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-3168"></span>TNOM
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3171"></span>parameter measurement temperature
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3174"></span><span class="math inline"><em>C</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3177"></span>27
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3180"></span>50
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-3183"></span>DI
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3186"></span>relative dielectric constant
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3189"></span><span class="math inline">$\frac{F}{m}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3192"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3195"></span>1
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-3198"></span>THICK
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3201"></span>insulator thickness
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3204"></span><span class="math inline"><em>m</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3207"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3210"></span>1e-9
</div></td>
</tr>
</tbody>
</table>

The capacitor has a capacitance computed as:

If **value** is specified on the instance line then

\[\begin{array}{ll}
{C_{nom} = {{{\lbrack font\ rm\ \lbrack char\ v\ mathalpha\rbrack\lbrack char\ a\ mathalpha\rbrack\lbrack char\ l\ mathalpha\rbrack\lbrack char\ u\ mathalpha\rbrack\lbrack char\ e\ mathalpha\rbrack\rbrack} \cdot {\lbrack font\ rm\ \lbrack char\ s\ mathalpha\rbrack\lbrack char\ c\ mathalpha\rbrack\lbrack char\ a\ mathalpha\rbrack\lbrack char\ l\ mathalpha\rbrack\lbrack char\ e\ mathalpha\rbrack\rbrack}} \cdot m}} & \\
\end{array}\]

If model capacitance is specified then

\[\begin{array}{ll}
{C_{nom} = {{{\lbrack font\ rm\ \lbrack char\ C\ mathalpha\rbrack\lbrack char\ A\ mathalpha\rbrack\lbrack char\ P\ mathalpha\rbrack\rbrack} \cdot {\lbrack font\ rm\ \lbrack char\ s\ mathalpha\rbrack\lbrack char\ c\ mathalpha\rbrack\lbrack char\ a\ mathalpha\rbrack\lbrack char\ l\ mathalpha\rbrack\lbrack char\ e\ mathalpha\rbrack\rbrack}} \cdot m}} & \\
\end{array}\]

If neither **value** nor **CAP** are specified, then geometrical and
physical parameters are take into account:

\[\begin{array}{ll}
{{\lbrack font\ rm\ \lbrack sub\ \lbrack char\ C\ mathalpha\rbrack\ \lbrack char\ 0\ mathalpha\rbrack\rbrack\rbrack} = {\lbrack font\ rm\ \lbrack char\ C\ mathalpha\rbrack\lbrack char\ J\ mathalpha\rbrack\rbrack}\left( {l - {\lbrack font\ rm\ \lbrack char\ S\ mathalpha\rbrack\lbrack char\ H\ mathalpha\rbrack\lbrack char\ O\ mathalpha\rbrack\lbrack char\ R\ mathalpha\rbrack\lbrack char\ T\ mathalpha\rbrack\rbrack}} \right)\left( {w - {\lbrack font\ rm\ \lbrack char\ N\ mathalpha\rbrack\lbrack char\ A\ mathalpha\rbrack\lbrack char\ R\ mathalpha\rbrack\lbrack char\ R\ mathalpha\rbrack\lbrack char\ O\ mathalpha\rbrack\lbrack char\ W\ mathalpha\rbrack\rbrack}} \right) + 2{\lbrack font\ rm\ \lbrack char\ C\ mathalpha\rbrack\lbrack char\ J\ mathalpha\rbrack\lbrack char\ S\ mathalpha\rbrack\lbrack char\ W\ mathalpha\rbrack\rbrack}\left( {l - {\lbrack font\ rm\ \lbrack char\ S\ mathalpha\rbrack\lbrack char\ H\ mathalpha\rbrack\lbrack char\ O\ mathalpha\rbrack\lbrack char\ R\ mathalpha\rbrack\lbrack char\ T\ mathalpha\rbrack\rbrack} + w - {\lbrack font\ rm\ \lbrack char\ N\ mathalpha\rbrack\lbrack char\ A\ mathalpha\rbrack\lbrack char\ R\ mathalpha\rbrack\lbrack char\ R\ mathalpha\rbrack\lbrack char\ O\ mathalpha\rbrack\lbrack char\ W\ mathalpha\rbrack\rbrack}} \right)} & \\
\end{array}\]

**CJ** can be explicitly given on the .model line or calculated by
physical parameters. When **CJ** is not given, is calculated as:

If **THICK** is not zero:

\[\begin{array}{ll}
\begin{array}{ll}
{{\lbrack font\ rm\ \lbrack char\ C\ mathalpha\rbrack\lbrack char\ J\ mathalpha\rbrack\rbrack} = \frac{{\lbrack font\ rm\ \lbrack char\ D\ mathalpha\rbrack\lbrack char\ I\ mathalpha\rbrack\rbrack}\varepsilon_{0}}{\lbrack font\ rm\ \lbrack char\ T\ mathalpha\rbrack\lbrack char\ H\ mathalpha\rbrack\lbrack char\ I\ mathalpha\rbrack\lbrack char\ C\ mathalpha\rbrack\lbrack char\ K\ mathalpha\rbrack\rbrack}} & {if DI is specified,} \\
 & \\
{{\lbrack font\ rm\ \lbrack char\ C\ mathalpha\rbrack\lbrack char\ J\ mathalpha\rbrack\rbrack} = \frac{\varepsilon_{SiO_{2}}}{\lbrack font\ rm\ \lbrack char\ T\ mathalpha\rbrack\lbrack char\ H\ mathalpha\rbrack\lbrack char\ I\ mathalpha\rbrack\lbrack char\ C\ mathalpha\rbrack\lbrack char\ K\ mathalpha\rbrack\rbrack}} & {otherwise.} \\
\end{array} & \\
\end{array}\]

If the relative dielectric constant is not specified the one for SiO2 is
used. The values of the constants are:
\(\varepsilon_{0} = 8.854214871e - 12\frac{F}{m}\) and
\(\varepsilon_{SiO_{2}} = 3.4531479969e - 11\frac{F}{m}\). The nominal
capacitance is then computed as:

\[\begin{array}{ll}
{C_{nom} = {C_{0}{\lbrack font\ rm\ \lbrack char\ s\ mathalpha\rbrack\lbrack char\ c\ mathalpha\rbrack\lbrack char\ a\ mathalpha\rbrack\lbrack char\ l\ mathalpha\rbrack\lbrack char\ e\ mathalpha\rbrack\rbrack} m}} & \\
\end{array}\]

After the nominal capacitance is calculated, it is adjusted for
temperature by the formula:

\[\begin{array}{ll}
{C\left( T \right) = C\left( {\lbrack font\ rm\ \lbrack char\ T\ mathalpha\rbrack\lbrack char\ N\ mathalpha\rbrack\lbrack char\ O\ mathalpha\rbrack\lbrack char\ M\ mathalpha\rbrack\rbrack} \right)\left( 1 + TC_{1}\left( {T - {\lbrack font\ rm\ \lbrack char\ T\ mathalpha\rbrack\lbrack char\ N\ mathalpha\rbrack\lbrack char\ O\ mathalpha\rbrack\lbrack char\ M\ mathalpha\rbrack\rbrack}} \right) + TC_{2}\left( T - {\lbrack font\ rm\ \lbrack char\ T\ mathalpha\rbrack\lbrack char\ N\ mathalpha\rbrack\lbrack char\ O\ mathalpha\rbrack\lbrack char\ M\ mathalpha\rbrack\rbrack})^{2} \right) \right.} & \\
\end{array}\]

where
\(C\left( {\lbrack font\ rm\ \lbrack char\ T\ mathalpha\rbrack\lbrack char\ N\ mathalpha\rbrack\lbrack char\ O\ mathalpha\rbrack\lbrack char\ M\ mathalpha\rbrack\rbrack} \right) = C_{nom}\).

In the above formula, \`\(T\)' represents the instance temperature,
which can be explicitly set using the **temp** keyword or calculated
using the circuit temperature and **dtemp**, if present.

