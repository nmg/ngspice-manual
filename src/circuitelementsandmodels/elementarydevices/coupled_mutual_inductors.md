# Coupled \(Mutual\) Inductors

General form:

``` listings
KXXXXXXX LYYYYYYY LZZZZZZZ value
```

Examples:

``` listings
K43 LAA LBB 0.999
KXFRMR L1 L2 0.87
```

LYYYYYYY and LZZZZZZZ are the names of the two coupled inductors, and
**value** is the coefficient of coupling, K, which must be greater than
0 and less than or equal to 1. Using the \`dot' convention, place a
\`dot' on the first node of each inductor.

