# Switch Model \(SW/CSW\)

The switch model allows an almost ideal switch to be described in
ngspice. The switch is not quite ideal, in that the resistance can not
change from 0 to infinity, but must always have a finite positive value.
By proper selection of the on and off resistances, they can be
effectively zero and infinity in comparison to other circuit elements.
The parameters available are:

<table>
<colgroup>
<col width="20%" />
<col width="20%" />
<col width="20%" />
<col width="20%" />
<col width="20%" />
</colgroup>
<tbody>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-3710"></span>Name
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3713"></span>Parameter
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3716"></span>Units
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3719"></span>Default
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3722"></span>Switch model
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-3725"></span>VT
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3728"></span>threshold voltage
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3731"></span>V
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3734"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3737"></span>SW
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-3740"></span>IT
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3743"></span>threshold current
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3746"></span>A
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3749"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3752"></span>CSW
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-3755"></span>VH
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3758"></span>hysteresis voltage
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3761"></span>V
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3764"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3767"></span>SW
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-3770"></span>IH
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3773"></span>hysteresis current
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3776"></span>A
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3779"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3782"></span>CSW
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-3785"></span>RON
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3788"></span>on resistance
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3791"></span><span class="math inline"><em>Ω</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3794"></span>1.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3797"></span>SW,CSW
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-3800"></span>ROFF
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3803"></span>off resistance
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3806"></span><span class="math inline"><em>Ω</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3809"></span>1.0e+12 (*)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-3812"></span>SW,CSW
</div></td>
</tr>
</tbody>
</table>

(\*) Or \(1/GMIN\), if you have set \(GMIN\) to any other value, see the
.OPTIONS control line ([15.1.2](#subsec_DC_Solution_Options)) for a
description of \(GMIN\), its default value results in an off-resistance
of 1.0e+12 ohms.

The use of an ideal element that is highly nonlinear such as a switch
can cause large discontinuities to occur in the circuit node voltages. A
rapid change such as that associated with a switch changing state can
cause numerical round-off or tolerance problems leading to erroneous
results or time step difficulties. The user of switches can improve the
situation by taking the following steps:

  - First, it is wise to set the ideal switch impedance just high or low
    enough to be negligible with respect to other circuit elements.
    Using switch impedances that are close to \`ideal' in all cases
    aggravates the problem of discontinuities mentioned above. Of
    course, when modeling real devices such as MOSFETS, the on
    resistance should be adjusted to a realistic level depending on the
    size of the device being modeled.
  - If a wide range of ON to OFF resistance must be used in the switches
    (ROFF/RON \> 1e+12), then the tolerance on errors allowed during
    transient analysis should be decreased by using the .OPTIONS control
    line and specifying **TRTOL** to be less than the default value of
    7.0.
  - When switches are placed around capacitors, then the option
    **CHGTOL** should also be reduced. Suggested values for these two
    options are 1.0 and 1e-16 respectively. These changes inform ngspice
    to be more careful around the switch points so that no errors are
    made due to the rapid change in the circuit.

Example input file:

``` listings
Switch test
.tran 2us 5ms
*switch control voltage
v1 1 0 DC 0.0 PWL(0 0 2e-3 2 4e-3 0)
*switch control voltage starting inside hysteresis window 
*please note influence of instance parameters ON, OFF
v2 2 0 DC 0.0 PWL(0 0.9 2e-3 2 4e-3 0.4)
*switch control current
i3 3 0 DC 0.0 PWL(0 0 2e-3 2m 4e-3 0) $ <--- switch control current
*load voltage
v4 4 0 DC 2.0
*input load for current source i3
r3 3 33 10k
vm3 33 0 dc 0 $ <--- measure the current
* ouput load resistors
r10 4 10 10k
r20 4 20 10k
r30 4 30 10k
r40 4 40 10k
*
s1 10 0 1 0 switch1 OFF
s2 20 0 2 0 switch1 OFF
s3 30 0 2 0 switch1 ON
.model switch1 sw vt=1 vh=0.2 ron=1 roff=10k
*
w1 40 0 vm3 wswitch1 off
.model wswitch1 csw  it=1m ih=0.2m ron=1 roff=10k
*
.control
run
plot v(1) v(10)
plot v(10) vs v(1) $ <-- get hysteresis loop
plot v(2) v(20) $ <--- different initial values
plot v(20) vs v(2) $ <-- get hysteresis loop
plot v(2) v(30) $ <--- different initial values
plot v(30) vs v(2) $ <-- get hysteresis loop
plot v(40) vs vm3#branch $ <--- current controlled switch hysteresis
.endc
.end
```

