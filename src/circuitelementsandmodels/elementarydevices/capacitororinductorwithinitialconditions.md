# Capacitor or inductor with initial conditions

The simulator supports the specification of voltage and current initial
conditions on capacitor and inductor models, respectively. *These models
are not the standard ones supplied with SPICE3, but are in fact code
models that can be substituted for the SPICE models when realistic
initial conditions are required*. For details please refer to Chapter
[12](#cha_Behavioral_Modeling). A XSPICE deck example using these models
is shown below:

\*

\* This circuit contains a capacitor and an inductor with

\* initial conditions on them. Each of the components

\* has a parallel resistor so that an exponential decay

\* of the initial condition occurs with a time constant of

\* 1 second.

\*

a1 1 0 cap

.model cap capacitor (c=1000uf ic=1)

r1 1 0 1k

\*

a2 2 0 ind

.model ind inductor (l=1H ic=1)

r2 2 0 1.0

\*

.control

tran 0.01 3

plot v(1) v(2)

.endc

.end

