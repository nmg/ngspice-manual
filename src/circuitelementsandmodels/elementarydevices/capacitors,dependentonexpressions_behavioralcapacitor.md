# Capacitors, dependent on expressions \(behavioral capacitor\)

General form:

``` listings
CXXXXXXX n+ n- C = 'expression' <tc1=value> <tc2=value>
CXXXXXXX n+ n- 'expression' <tc1=value> <tc2=value>
```

Examples:

``` listings
C1 cc 0 c = 'V(cc) < {Vt} ? {C1} : {Ch}' tc1=-1e-03 tc2=1.3e-05
```

**Expression** may be an equation or an expression containing node
voltages or branch currents (in the form of i(vm)) and any other terms
as given for the B source and described in Chapt.
[5.1](#sec_B_source__ASRC_). It may contain parameters
([2.8.1](#subsec__param_line)) and the special variables time, temper,
and hertz ([5.1.2](#subsec_Special_B_Source_Variables)).

Example input file:

``` listings
Behavioral Capacitor   
.param Cl=5n Ch=1n Vt=1m Il=100n 
.ic v(cc) = 0    v(cc2) = 0 
* capacitor depending on control voltage V(cc)
C1 cc 0 c = 'V(cc) < {Vt} ? {Cl} : {Ch}' 
*C1 cc 0 c ={Ch} 
I1 0 1 {Il}
Exxx  n1-copy n2  n2 cc2  1 
Cxxx  n1-copy n2  1 
Bxxx  cc2 n2  I = '(V(cc2) < {Vt} ? {Cl} : {Ch})' * i(Exxx) 
I2 n2 22 {Il} 
vn2 n2 0 DC 0
* measure charge by integrating current 
aint1 %id(1 cc) 2 time_count 
aint2 %id(22 cc2) 3 time_count
.model time_count int(in_offset=0.0 gain=1.0 
+ out_lower_limit=-1e12 out_upper_limit=1e12 
+ limit_range=1e-9 out_ic=0.0)   
.control 
unset askquit 
tran 100n 100u 
plot v(2) 
plot v(cc) v(cc2) 
.endc   
.end
```

