# Junction Field-Effect Transistors \(JFETs\)

General form:

``` listings
JXXXXXXX nd ng ns mname <area> <off> <ic=vds,vgs> <temp=t>
```

Examples:

``` listings
J1 7 2 3 JM1 OFF
```

**nd**, **ng**, and **ns** are the drain, gate, and source nodes,
respectively. **mname** is the model name, **area** is the area factor,
and **off** indicates an (optional) initial condition on the device for
dc analysis. If the area factor is omitted, a value of 1.0 is assumed.
The (optional) initial condition specification, using **ic=VDS,VGS** is
intended for use with the **uic** option on the .TRAN control line, when
a transient analysis is desired starting from other than the quiescent
operating point. See the .ic control line for a better way to set
initial conditions. The (optional) temp value is the temperature where
this device is to operate, and overrides the temperature specification
on the .option control line.

