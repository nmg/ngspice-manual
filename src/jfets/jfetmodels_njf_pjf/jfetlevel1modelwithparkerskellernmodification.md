# JFET level 1 model with Parker Skellern modification

The **level 1** JFET model is derived from the FET model of Shichman and
Hodges. The dc characteristics are defined by the parameters VTO and
BETA, which determine the variation of drain current with gate voltage,
LAMBDA, which determines the output conductance, and IS, the saturation
current of the two gate junctions. Two ohmic resistances, RD and RS, are
included.

\[\begin{array}{ll}
{vgst = vgs - VTO} & \\
\end{array}\]

\[\begin{array}{ll}
{\beta_{p} = BETA\left( {1 + LAMBDA vds} \right)} & \\
\end{array}\]

\[\begin{array}{ll}
{bfac = \frac{1 - B}{PB - VTO}} & \\
\end{array}\]

\[\begin{array}{ll}
{I_{Drain} = \begin{cases}
{vds \cdot GMIN,} & {{if} vgst \leq 0} \\
{\beta_{p} vds\left( {vds\left( {bfac vds - B} \right) vgst\left( {2B + 3bfac\left( {vgst - vds} \right)} \right)} \right) + vds \cdot GMIN,} & {{if} vgst \geq vds} \\
{\beta_{p} vgst^{2}\left( {B + vgst bfac} \right) + vds \cdot GMIN,} & {{if} vgst < vds} \\
\end{cases}} & \\
\end{array}\]

Note that in Spice3f and later, the fitting parameter B has been added
by Parker and Skellern. For details, see \[[9](#LyXCite-key_9)\]. If
parameter B is set to 1 equation above simplifies to

\[\begin{array}{ll}
{I_{Drain} = \begin{cases}
{vds \cdot GMIN,} & {{if} vgst \leq 0} \\
{\beta_{p} vds\left( {2vgst - vds} \right) + vds \cdot GMIN,} & {{if} vgst \geq vds} \\
{\beta_{p} vgst^{2} + vds \cdot GMIN,} & {{if} vgst < vds} \\
\end{cases}} & \\
\end{array}\]

Charge storage is modeled by nonlinear depletion layer capacitances for
both gate junctions, which vary as the \(- \frac{1}{2}\) power of
junction voltage and are defined by the parameters CGS, CGD, and PB.

<table style="width:100%;">
<colgroup>
<col width="16%" />
<col width="16%" />
<col width="16%" />
<col width="16%" />
<col width="16%" />
<col width="16%" />
</colgroup>
<tbody>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-10393"></span>Name
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10396"></span>Parameter
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10399"></span>Units
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10402"></span>Default
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10405"></span>Example
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10408"></span>Scaling factor
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-10411"></span>VTO
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10414"></span>Threshold voltage <span class="math inline"><em>V</em><sub><em>T</em>0</sub></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10417"></span><span class="math inline"><em>V</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10420"></span>-2.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10423"></span>-2.0
</div></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-10429"></span>BETA
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10432"></span>Transconductance parameter (<span class="math inline"><em>β</em></span>)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10435"></span><span class="math inline">$\frac{A}{V^{&quot;}}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10438"></span>1.0e-4
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10441"></span>1.0e-3
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10444"></span>area
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-10447"></span>LAMBDA
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10450"></span>Channel-length modulation parameter (<span class="math inline"><em>λ</em></span>)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10453"></span><span class="math inline">$\frac{1}{V}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10456"></span>0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10459"></span>1.0e-4
</div></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-10465"></span>RD
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10468"></span>Drain ohmic resistance
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10471"></span><span class="math inline"><em>Ω</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10474"></span>0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10477"></span>100
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10480"></span>area
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-10483"></span>RS
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10486"></span>Source ohmic resistance
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10489"></span><span class="math inline"><em>Ω</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10492"></span>0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10495"></span>100
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10498"></span>area
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-10501"></span>CGS
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10504"></span>Zero-bias G-S junction capacitance <span class="math inline"><em>C</em><sub><em>g</em><em>s</em></sub></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10507"></span><span class="math inline"><em>F</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10510"></span>0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10513"></span>5pF
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10516"></span>area
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-10519"></span>CGD
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10522"></span>Zero-bias G-D junction capacitance <span class="math inline"><em>C</em><sub><em>g</em><em>d</em></sub></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10525"></span><span class="math inline"><em>F</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10528"></span>0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10531"></span>1pF
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10534"></span>area
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-10537"></span>PB
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10540"></span>Gate junction potential
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10543"></span><span class="math inline"><em>V</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10546"></span>1
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10549"></span>0.6
</div></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-10555"></span>IS
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10558"></span>Gate saturation current <span class="math inline"><em>I</em><sub><em>S</em></sub></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10561"></span><span class="math inline"><em>A</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10564"></span>1.0e-14
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10567"></span>1.0e-14
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10570"></span>area
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-10573"></span>B
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10576"></span>Doping tail parameter
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10579"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10582"></span>1
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10585"></span>1.1
</div></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-10591"></span>KF
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10594"></span>Flicker noise coefficient
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10597"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10600"></span>0
</div></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-10609"></span>AF
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10612"></span>Flicker noise exponent
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10615"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10618"></span>1
</div></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-10627"></span>NLEV
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10630"></span>Noise equation selector
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10633"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10636"></span>1
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10639"></span>3
</div></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-10645"></span>GDSNOI
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10648"></span>Channel noise coefficient for nlev=3
</div></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-10654"></span>1.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10657"></span>2.0
</div></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-10663"></span>FC
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10666"></span>Coefficient for forward-bias depletion capacitance formula
</div></td>
<td></td>
<td><div class="plain_layout">
<span id="magicparlabel-10672"></span>0.5
</div></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-10681"></span>TNOM
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10684"></span>Parameter measurement temperature
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10687"></span><span class="math inline"><em>C</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10690"></span>27
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10693"></span>50
</div></td>
<td></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-10699"></span>TCV
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10702"></span>Threshold voltage temperature coefficient
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10705"></span><span class="math inline">$\frac{1}{C}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10708"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10711"></span>0.1
</div></td>
<td></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-10717"></span>BEX
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10720"></span>Mobility temperature exponent
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10723"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10726"></span>0.0
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10729"></span>1.1
</div></td>
<td></td>
</tr>
</tbody>
</table>

Additional to the standard thermal and flicker noise model an
alternative thermal channel noise model is implemented and is selectable
by setting NLEV parameter to 3. This follows in a correct channel
thermal noise in the linear region.

\[\begin{array}{ll}
{S_{noise} = \frac{2}{3} 4kT \cdot BETA \cdot Vgst\frac{\left( {1 + \alpha + \alpha^{2}} \right)}{1 + \alpha}GDSNOI} & \\
\end{array}\]

with

\[\begin{array}{ll}
{\alpha = \begin{cases}
{1 - \frac{vds}{vgs - VTO},} & {{if} vgs - VTO \geq vds} \\
{0,} & {else} \\
\end{cases}} & \\
\end{array}\]

