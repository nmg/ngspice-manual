# JFET level 2 Parker Skellern model

The level 2 model is an improvement to level 1. Details are available
from [Macquarie
University](http://www.engineering.mq.edu.au/research/groups/cnerf/psmodel/index.htm).
Some important items are:

  - The description maintains strict continuity in its high-order
    derivatives, which is essential for prediction of distortion and
    intermodulation.
  - Frequency dependence of output conductance and transconductance is
    described as a function of bias.
  - Both drain-gate and source-gate potentials modulate the pinch-off
    potential, which is consistent with S-parameter and pulsed-bias
    measurements.
  - Self-heating varies with frequency.
  - Extreme operating regions - subthreshold, forward gate bias,
    controlled resistance, and breakdown regions - are included.
  - Parameters provide independent fitting to all operating regions. It
    is not necessary to compromise one region in favor of another.
  - Strict drain-source symmetry is maintained. The transition during
    drain-source potential reversal is smooth and continuous.

The model equations are described in this [pdf
document](http://www.engineering.mq.edu.au/research/groups/cnerf/psfet.pdf)
and in \[[19](#LyXCite-key_19)\].

<table>
<colgroup>
<col width="25%" />
<col width="25%" />
<col width="25%" />
<col width="25%" />
</colgroup>
<tbody>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-10930"></span>Name
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10933"></span>Description
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10936"></span>Units
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10939"></span>Default
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-10942"></span>ID
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10945"></span>Device IDText
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10948"></span>Text
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10951"></span>PF1
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-10954"></span>ACGAM
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10957"></span>Capacitance modulation
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10960"></span> -
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10963"></span>0
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-10966"></span>BETA
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10969"></span>Linear-region transconductance scale
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10972"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10975"></span><span class="math inline">10<sup>−4</sup></span>
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-10978"></span>CGD
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10981"></span>Zero-bias gate-source capacitance
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10984"></span><span class="math inline"><em>F</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10987"></span>0
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-10990"></span>CGS
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10993"></span>Zero-bias gate-drain capacitance
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10996"></span><span class="math inline"><em>F</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-10999"></span>0
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-11002"></span>DELTA
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11005"></span>Thermal reduction coefficient
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11008"></span><span class="math inline">$\frac{1}{W}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11011"></span>0
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-11014"></span>FC
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11017"></span>Forward bias capacitance parameter
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11020"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11023"></span>0.5
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-11026"></span>HFETA
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11029"></span>High-frequency VGS feedback parameter
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11032"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11035"></span>0
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-11038"></span>HFE1
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11041"></span>HFGAM modulation by VGD
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11044"></span><span class="math inline">$\frac{1}{V}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11047"></span>0
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-11050"></span>HFE2
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11053"></span>HFGAM modulation by VGS
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11056"></span><span class="math inline">$\frac{1}{V}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11059"></span>0
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-11062"></span>HFGAM
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11065"></span>High-frequency VGD feedback parameter
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11068"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11071"></span>0
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-11074"></span>HFG1
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11077"></span>HFGAM modulation by VSG
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11080"></span><span class="math inline">$\frac{1}{V}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11083"></span>0
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-11086"></span>HFG2
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11089"></span>HFGAM modulation by VDG
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11092"></span><span class="math inline">$\frac{1}{V}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11095"></span>0
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-11098"></span>IBD
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11101"></span>Gate-junction breakdown current
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11104"></span><span class="math inline"><em>A</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11107"></span>0
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-11110"></span>IS
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11113"></span>Gate-junction saturation current
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11116"></span><span class="math inline"><em>A</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11119"></span><span class="math inline">10<sup>−14</sup></span>
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-11122"></span>LFGAM
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11125"></span>Low-frequency feedback parameter
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11128"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11131"></span>0
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-11134"></span>LFG1
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11137"></span>LFGAM modulation by VSG
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11140"></span><span class="math inline">$\frac{1}{V}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11143"></span>0
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-11146"></span>LFG2
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11149"></span>LFGAM modulation by VDG
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11152"></span><span class="math inline">$\frac{1}{V}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11155"></span>0
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-11158"></span>MVST
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11161"></span>Subthreshold modulation
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11164"></span><span class="math inline">$\frac{1}{V}$</span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11167"></span>0
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-11170"></span>N
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11173"></span>Gate-junction ideality factor
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11176"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11179"></span>1
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-11182"></span>P
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11185"></span>Linear-region power-law exponent
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11188"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11191"></span>2
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-11194"></span>Q
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11197"></span>Saturated-region power-law exponent
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11200"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11203"></span>2
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-11206"></span>RS
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11209"></span>Source ohmic resistance
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11212"></span><span class="math inline"><em>Ω</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11215"></span>0
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-11218"></span>RD
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11221"></span>Drain ohmic resistance
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11224"></span><span class="math inline"><em>Ω</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11227"></span>0
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-11230"></span>TAUD
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11233"></span>Relaxation time for thermal reduction
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11236"></span><span class="math inline"><em>s</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11239"></span>0
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-11242"></span>TAUG
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11245"></span>Relaxation time for gamma feedback
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11248"></span><span class="math inline"><em>s</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11251"></span>0
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-11254"></span>VBD
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11257"></span>Gate-junction breakdown potential
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11260"></span><span class="math inline"><em>V</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11263"></span>1
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-11266"></span>VBI
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11269"></span>Gate-junction potential
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11272"></span><span class="math inline"><em>V</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11275"></span>1
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-11278"></span>VST
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11281"></span>Subthreshold potential
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11284"></span><span class="math inline"><em>V</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11287"></span>0
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-11290"></span>VTO
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11293"></span>Threshold voltage
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11296"></span><span class="math inline"><em>V</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11299"></span>-2.0
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-11302"></span>XC
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11305"></span>Capacitance pinch-off reduction factor
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11308"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11311"></span>0
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-11314"></span>XI
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11317"></span>Saturation-knee potential factor
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11320"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11323"></span>1000
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-11326"></span>Z
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11329"></span>Knee transition parameter
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11332"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11335"></span>0.5
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-11338"></span>RG
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11341"></span>Gate ohmic resistance
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11344"></span><span class="math inline"><em>Ω</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11347"></span>0
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-11350"></span>LG
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11353"></span>Gate inductance
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11356"></span><span class="math inline"><em>H</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11359"></span>0
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-11362"></span>LS
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11365"></span>Source inductance
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11368"></span><span class="math inline"><em>H</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11371"></span>0
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-11374"></span>LD
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11377"></span>Drain inductance
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11380"></span><span class="math inline"><em>H</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11383"></span>0
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-11386"></span>CDSS
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11389"></span>Fixed Drain-source capacitance
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11392"></span><span class="math inline"><em>F</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11395"></span>0
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-11398"></span>AFAC
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11401"></span>Gate-width scale factor
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11404"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11407"></span>1
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-11410"></span>NFING
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11413"></span>Number of gate fingers scale factor
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11416"></span>-
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11419"></span>1
</div></td>
</tr>
<tr class="even">
<td><div class="plain_layout">
<span id="magicparlabel-11422"></span>TNOM
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11425"></span>Nominal Temperature (Not implemented)
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11428"></span><span class="math inline"><em>K</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11431"></span>300 K
</div></td>
</tr>
<tr class="odd">
<td><div class="plain_layout">
<span id="magicparlabel-11434"></span>TEMP
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11437"></span>Temperature
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11440"></span><span class="math inline"><em>K</em></span>
</div></td>
<td><div class="plain_layout">
<span id="magicparlabel-11443"></span>300 K
</div></td>
</tr>
</tbody>
</table>

