# Transmission Lines

Ngspice implements both the original SPICE3f5 transmission lines models
and the one introduced with KSPICE. The latter provide an improved
transient analysis of lossy transmission lines. Unlike SPICE models that
use the state-based approach to simulate lossy transmission lines,
KSPICE simulates lossy transmission lines and coupled multiconductor
line systems using the recursive convolution method. The impulse
response of an arbitrary transfer function can be determined by deriving
a recursive convolution from the Pade approximations of the function. We
use this approach for simulating each transmission line's
characteristics and each multiconductor line's modal functions. This
method of lossy transmission line simulation has been proved to give a
speedup of one to two orders of magnitude over SPICE3f5.

