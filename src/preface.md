Holger Vogt, Marcel Hendrix, Paolo Nenzi  
  
  
January 1st, 2019

  

## Locations

The project and download pages of ngspice may be found at

Ngspice home page <http://ngspice.sourceforge.net/>

Project page at sourceforge <http://sourceforge.net/projects/ngspice/>

Download page at sourceforge
<http://sourceforge.net/projects/ngspice/files/>

Git source download
[http://sourceforge.net/scm/?type=cvs\&amp;group\_id=38962](http://sourceforge.net/scm/?type=cvs&group_id=38962)

## Status

This manual is a work in progress. Some to-dos are listed in Chapt.
[24.3](#sec_To_Do). More is surely needed. You are invited to report
bugs, missing items, wrongly described items, bad English style etc.

## How to use this manual

The manual is a \`work in progress'. It may accompany a specific ngspice
release, e.g. ngspice-24 as manual version 24. If its name contains
\`Version xxplus', it describes the actual code status, found at the
date of issue in the Git Source Code Management (SCM) tool. The manual
is intended to provide a complete description of the ngspice
functionality, its features, commands, or procedures. It is not a book
about learning SPICE usage, but the novice user may find some hints how
to start using ngspice. Chapter [21.1](#sec_AC_coupled_transistor) gives
a short introduction how to set up and simulate a small circuit. Chapter
[32](#cha_Compilation_notes) is about compiling and installing ngspice
from a tarball or the actual Git source code, which you may find on the
[ngspice web pages](http://ngspice.sourceforge.net/download.html). If
you are running a specific Linux distribution, you may check if it
provides ngspice as part of the package. Some are listed
[here](http://ngspice.sourceforge.net/packages.html).

## License

This document is covered by the [Creative Commons Attribution
Share-Alike (CC-BY-SA)
v4.0.](https://creativecommons.org/licenses/by-sa/4.0/).

Part of chapters 12 and 25-27 are in the public domain.

Chapter 30 is covered by New BSD (chapt.
[33.3.2](#subsec__Modified__BSD_license)).

###   

# Part I Ngspice User Manual

## Table of Contents

[Locations](#magicparlabel-4)

[Status](#magicparlabel-10)

[How to use this manual](#magicparlabel-12)

[License](#magicparlabel-14)

[Part I Ngspice User Manual](#magicparlabel-19)

[Prefaces](#magicparlabel-21)

[Preface to the first edition](#magicparlabel-22)

[Preface to the actual edition (as May 2018)](#magicparlabel-45)

[Acknowledgments](#magicparlabel-49)

[ngspice contributors](#magicparlabel-50)

[Chapter 1 Introduction](#magicparlabel-117)

[1.1 Simulation Algorithms](#magicparlabel-128)

[1.2 Supported Analyses](#magicparlabel-152)

[1.3 Analysis at Different Temperatures](#magicparlabel-192)

[1.4 Convergence](#magicparlabel-235)

[Chapter 2 Circuit Description](#magicparlabel-254)

[2.1 General Structure and Conventions](#magicparlabel-255)

[2.2 Basic lines](#magicparlabel-768)

[2.3 .MODEL Device Models](#magicparlabel-867)

[2.4 .SUBCKT Subcircuits](#magicparlabel-1048)

[2.5 .GLOBAL](#magicparlabel-1129)

[2.6 .INCLUDE](#magicparlabel-1147)

[2.7 .LIB](#magicparlabel-1166)

[2.8 .PARAM Parametric netlists](#magicparlabel-1185)

[2.9 .FUNC](#magicparlabel-1950)

[2.10 .CSPARAM](#magicparlabel-1972)

[2.11 .TEMP](#magicparlabel-2004)

[2.12 .IF Condition-Controlled Netlist](#magicparlabel-2023)

[2.13 Parameters, functions, expressions, and command
scripts](#magicparlabel-2075)

[Chapter 3 Circuit Elements and Models](#magicparlabel-2085)

[3.1 General options and information](#magicparlabel-2087)

[3.2 Elementary Devices](#magicparlabel-2292)

[Chapter 4 Voltage and Current Sources](#magicparlabel-3867)

[4.1 Independent Sources for Voltage or Current](#magicparlabel-3868)

[4.2 Linear Dependent Sources](#magicparlabel-5124)

[Chapter 5 Non-linear Dependent Sources (Behavioral
Sources)](#magicparlabel-5291)

[5.1 Bxxxx: Nonlinear dependent source (ASRC)](#magicparlabel-5293)

[5.2 Exxxx: non-linear voltage source](#magicparlabel-5537)

[5.3 Gxxxx: non-linear current source](#magicparlabel-5643)

[5.4 Debugging a behavioral source](#magicparlabel-5746)

[5.5 POLY Sources](#magicparlabel-5790)

[Chapter 6 Transmission Lines](#magicparlabel-5847)

[6.1 Lossless Transmission Lines](#magicparlabel-5849)

[6.2 Lossy Transmission Lines](#magicparlabel-5870)

[6.3 Uniform Distributed RC Lines](#magicparlabel-6231)

[6.4 KSPICE Lossy Transmission Lines](#magicparlabel-6432)

[Chapter 7 Diodes](#magicparlabel-6748)

[7.1 Junction Diodes](#magicparlabel-6749)

[7.2 Diode Model (D)](#magicparlabel-6769)

[7.3 Diode Equations](#magicparlabel-7895)

[Chapter 8 BJTs](#magicparlabel-7979)

[8.1 Bipolar Junction Transistors (BJTs)](#magicparlabel-7980)

[8.2 BJT Models (NPN/PNP)](#magicparlabel-8002)

[Chapter 9 JFETs](#magicparlabel-10237)

[9.1 Junction Field-Effect Transistors (JFETs)](#magicparlabel-10238)

[9.2 JFET Models (NJF/PJF)](#magicparlabel-10256)

[Chapter 10 MESFETs](#magicparlabel-11444)

[10.1 MESFETs](#magicparlabel-11445)

[10.2 MESFET Models (NMF/PMF)](#magicparlabel-11462)

[Chapter 11 MOSFETs](#magicparlabel-11843)

[11.1 MOSFET devices](#magicparlabel-11845)

[11.2 MOSFET models (NMOS/PMOS)](#magicparlabel-11941)

[11.3 Power MOSFET model (VDMOS)](#magicparlabel-14557)

[Chapter 12 Mixed-Mode and Behavioral Modeling with
XSPICE](#magicparlabel-15434)

[12.1 Code Model Element & .MODEL Cards](#magicparlabel-15438)

[12.2 Analog Models](#magicparlabel-15660)

[12.3 Hybrid Models](#magicparlabel-17518)

[12.4 Digital Models](#magicparlabel-17791)

[12.5 Predefined Node Types for event driven
simulation](#magicparlabel-19304)

[Chapter 13 Verilog A Device models](#magicparlabel-19315)

[13.1 Introduction](#magicparlabel-19316)

[13.2 ADMS](#magicparlabel-19318)

[13.3 How to integrate a Verilog-A model into
ngspice](#magicparlabel-19321)

[Chapter 14 Mixed-Level Simulation (ngspice with
TCAD)](#magicparlabel-19334)

[14.1 Cider](#magicparlabel-19335)

[14.2 GSS, Genius](#magicparlabel-19342)

[Chapter 15 Analyses and Output Control (batch
mode)](#magicparlabel-19344)

[15.1 Simulator Variables (.options)](#magicparlabel-19354)

[15.2 Initial Conditions](#magicparlabel-19447)

[15.3 Analyses](#magicparlabel-19488)

[15.4 Measurements after AC, DC and Transient
Analysis](#magicparlabel-19842)

[15.5 Safe Operating Area (SOA) warning messages](#magicparlabel-20172)

[15.6 Batch Output](#magicparlabel-20193)

[15.7 Measuring current through device terminals](#magicparlabel-20407)

[Chapter 16 Starting ngspice](#magicparlabel-20448)

[16.1 Introduction](#magicparlabel-20449)

[16.2 Where to obtain ngspice](#magicparlabel-20454)

[16.3 Command line options for starting ngspice and
ngnutmeg](#magicparlabel-20463)

[16.4 Starting options](#magicparlabel-20660)

[16.5 Standard configuration file spinit](#magicparlabel-20736)

[16.6 User defined configuration file .spiceinit](#magicparlabel-20778)

[16.7 Environmental variables](#magicparlabel-20794)

[16.8 Memory usage](#magicparlabel-20811)

[16.9 Simulation time](#magicparlabel-20814)

[16.10 Ngspice on multi-core processors using
OpenMP](#magicparlabel-20822)

[16.11 Server mode option -s](#magicparlabel-20979)

[16.12 Ngspice control via input, output fifos](#magicparlabel-21036)

[16.13 Compatibility](#magicparlabel-21105)

[16.14 Tests](#magicparlabel-21215)

[16.15 Reporting bugs and errors](#magicparlabel-21222)

[Chapter 17 Interactive Interpreter](#magicparlabel-21229)

[17.1 Introduction](#magicparlabel-21230)

[17.2 Expressions, Functions, and Constants](#magicparlabel-21237)

[17.3 Plots](#magicparlabel-21942)

[17.4 Command Interpretation](#magicparlabel-21946)

[17.5 Commands](#magicparlabel-21976)

[17.6 Control Structures](#magicparlabel-24730)

[17.7 Internally predefined variables](#magicparlabel-24873)

[17.8 Scripts](#magicparlabel-24967)

[17.9 Scattering parameters (s-parameters)](#magicparlabel-25322)

[17.10 MISCELLANEOUS](#magicparlabel-25369)

[17.11 Bugs](#magicparlabel-25373)

[Chapter 18 Ngspice User Interfaces](#magicparlabel-25376)

[18.1 MS Windows Graphical User Interface](#magicparlabel-25378)

[18.2 MS Windows Console](#magicparlabel-25451)

[18.3 Linux](#magicparlabel-25457)

[18.4 CygWin](#magicparlabel-25459)

[18.5 Error handling](#magicparlabel-25464)

[18.6 Postscript printing options](#magicparlabel-25470)

[18.7 Gnuplot](#magicparlabel-25503)

[18.8 Integration with CAD software and \`third party'
GUIs](#magicparlabel-25505)

[Chapter 19 ngspice as shared library or dynamic link
library](#magicparlabel-25521)

[19.1 Compile options](#magicparlabel-25525)

[19.2 Linking shared ngspice to a calling
application](#magicparlabel-25533)

[19.3 Shared ngspice API](#magicparlabel-25539)

[19.4 General remarks on using the API](#magicparlabel-25738)

[19.5 Example applications](#magicparlabel-25845)

[19.6 ngspice parallel](#magicparlabel-25850)

[Chapter 20 TCLspice](#magicparlabel-25910)

[20.1 tclspice framework](#magicparlabel-25912)

[20.2 tclspice documentation](#magicparlabel-25920)

[20.3 spicetoblt](#magicparlabel-25922)

[20.4 Running TCLspice](#magicparlabel-25937)

[20.5 examples](#magicparlabel-25973)

[20.6 Compiling](#magicparlabel-26445)

[20.7 MS Windows 32 Bit binaries](#magicparlabel-26480)

[Chapter 21 Example Circuits](#magicparlabel-26482)

[21.1 AC coupled transistor amplifier](#magicparlabel-26500)

[21.2 Differential Pair](#magicparlabel-26613)

[21.3 MOSFET Characterization](#magicparlabel-26639)

[21.4 RTL Inverter](#magicparlabel-26661)

[21.5 Four-Bit Binary Adder (Bipolar)](#magicparlabel-26682)

[21.6 Four-Bit Binary Adder (MOS)](#magicparlabel-26768)

[21.7 Transmission-Line Inverter](#magicparlabel-26849)

[Chapter 22 Statistical circuit analysis](#magicparlabel-26874)

[22.1 Introduction](#magicparlabel-26875)

[22.2 Using random param(eters)](#magicparlabel-26880)

[22.3 Behavioral sources (B, E, G, R, L, C) with random
control](#magicparlabel-26978)

[22.4 ngspice scripting language](#magicparlabel-27001)

[22.5 Monte-Carlo Simulation](#magicparlabel-27059)

[22.6 Data evaluation with Gnuplot](#magicparlabel-27137)

[Chapter 23 Circuit optimization with ngspice](#magicparlabel-27174)

[23.1 Optimization of a circuit](#magicparlabel-27175)

[23.2 ngspice optimizer using ngspice scripts](#magicparlabel-27182)

[23.3 ngspice optimizer using tclspice](#magicparlabel-27185)

[23.4 ngspice optimizer using a Python script](#magicparlabel-27187)

[23.5 ngspice optimizer using ASCO](#magicparlabel-27189)

[Chapter 24 Notes](#magicparlabel-27274)

[24.1 Glossary](#magicparlabel-27275)

[24.2 Acronyms and Abbreviations](#magicparlabel-27286)

[24.3 To Do](#magicparlabel-27310)

[Part II XSPICE Software User's Manual](#magicparlabel-27338)

[Chapter 25 XSPICE Basics](#magicparlabel-27339)

[25.1 ngspice with the XSPICE option](#magicparlabel-27340)

[25.2 The XSPICE Code Model Subsystem](#magicparlabel-27345)

[25.3 XSPICE Top-Level Diagram](#magicparlabel-27349)

[Chapter 26 Execution Procedures](#magicparlabel-27361)

[26.1 Simulation and Modeling Overview](#magicparlabel-27363)

[26.2 Circuit Description Syntax](#magicparlabel-27557)

[26.3 How to create code models](#magicparlabel-27608)

[Chapter 27 Example circuits](#magicparlabel-27674)

[27.1 Amplifier with XSPICE model \`gain'](#magicparlabel-27684)

[27.2 XSPICE advanced usage](#magicparlabel-27727)

[Chapter 28 Code Models and User-Defined Nodes](#magicparlabel-27944)

[28.1 Code Model Data Type Definitions](#magicparlabel-27950)

[28.2 Creating Code Models](#magicparlabel-27964)

[28.3 Creating User-Defined Nodes](#magicparlabel-27975)

[28.4 Adding a new code model library](#magicparlabel-27984)

[28.5 Compiling and loading the new code model
(library)](#magicparlabel-27991)

[28.6 Interface Specification File](#magicparlabel-28004)

[28.7 Model Definition File](#magicparlabel-28300)

[28.8 User-Defined Node Definition File](#magicparlabel-29250)

[Chapter 29 Error Messages](#magicparlabel-29633)

[29.1 Preprocessor Error Messages](#magicparlabel-29639)

[29.2 Simulator Error Messages](#magicparlabel-29731)

[29.3 Code Model Error Messages](#magicparlabel-29763)

[Part III CIDER](#magicparlabel-29890)

[Chapter 30 CIDER User’s Manual](#magicparlabel-29891)

[30.1 SPECIFICATION](#magicparlabel-29893)

[30.2 BOUNDARY, INTERFACE](#magicparlabel-29931)

[30.3 COMMENT](#magicparlabel-30217)

[30.4 CONTACT](#magicparlabel-30247)

[30.5 DOMAIN, REGION](#magicparlabel-30319)

[30.6 DOPING](#magicparlabel-30501)

[30.7 ELECTRODE](#magicparlabel-30965)

[30.8 END](#magicparlabel-31146)

[30.9 MATERIAL](#magicparlabel-31160)

[30.10 METHOD](#magicparlabel-31506)

[30.11 Mobility](#magicparlabel-31636)

[30.12 MODELS](#magicparlabel-31899)

[30.13 OPTIONS](#magicparlabel-32079)

[30.14 OUTPUT](#magicparlabel-32342)

[30.15 TITLE](#magicparlabel-32748)

[30.16 X.MESH, Y.MESH](#magicparlabel-32775)

[30.17 NUMD](#magicparlabel-32947)

[30.18 NBJT](#magicparlabel-33169)

[30.19 NUMOS](#magicparlabel-33391)

[30.20 Cider examples](#magicparlabel-33613)

[Part IV Appendices](#magicparlabel-33615)

[Chapter 31 Model and Device Parameters](#magicparlabel-33616)

[31.1 Accessing internal device parameters](#magicparlabel-33618)

[31.2 Elementary Devices](#magicparlabel-33674)

[31.3 Voltage and current sources](#magicparlabel-36144)

[31.4 Transmission Lines](#magicparlabel-38663)

[31.5 BJTs](#magicparlabel-40458)

[31.6 MOSFETs](#magicparlabel-48714)

[Chapter 32 Compilation notes](#magicparlabel-68083)

[32.1 Ngspice Installation under Linux (and other
'UNIXes')](#magicparlabel-68085)

[32.2 Ngspice Compilation under Windows OS](#magicparlabel-68246)

[32.3 Reporting errors](#magicparlabel-68444)

[Chapter 33 Copyrights and licenses](#magicparlabel-68447)

[33.1 Documentation license](#magicparlabel-68448)

[33.2 ngspice license](#magicparlabel-68453)

[33.3 Some license details](#magicparlabel-68455)

[33.4 Some notes on the historical evolvement of the ngspice
licenses](#magicparlabel-68478)

# Prefaces

## Preface to the first edition

This manual has been assembled from different sources:

1.  The spice3f5 manual,
2.  the XSPICE user's manual,
3.  the CIDER user's manual

and some original material needed to describe the new features and the
newly implemented models. This cut and paste approach, while not being
orthodox, allowed ngspice to have a full manual in a fraction of the
time that writing a completely new text would have required. The use of
and LyX instead of info, which was the original encoding for the manual,
further helped to reduce the writing effort and improved the quality of
the result, at the expense of an on-line version of the manual but, due
to the complexity of the software I hardly think that users will ever
want to read an on-line text version.

In writing this text I followed the spice3f5 manual, both in the chapter
sequence and presentation of material, mostly because that was already
the user manual of SPICE.

Ngspice is an open source software, users can download the source code,
compile, and run it. This manual has an entire chapter describing
program compilation and available options to help users in building
ngspice (see Chapt. [32](#cha_Compilation_notes)). The source package
already comes with all \`safe' options enabled by default, and
activating the others can produce unpredictable results and thus is
recommended to expert users only. This is the first ngspice manual and I
have removed all the historical material that described the differences
between ngspice and spice3, since it was of no use for the user and not
so useful for the developer who can look for it in the Changelogs of in
the revision control system.

I want to acknowledge the work done by Emmanuel Rouat and Arno W. Peters
for converting the original spice3f documentation to TeXinfo. Their
effort gave ngspice users the only available documentation that
described the changes for many years. A good source of ideas for this
manual came from the on-line spice3f manual written by Charles D.H.
Williams ([Spice3f5 User
Guide](http://newton.ex.ac.uk/teaching/CDHW/Electronics2/userguide/index.html#toc)),
constantly updated and useful for its many insights.

As always, errors, omissions and unreadable phrases are only my fault.

Paolo Nenzi

Roma, March 24th 2001

> 
> 
> Indeed. At the end of the day, this is engineering, and one learns to
> live
> 
> within the limitations of the tools.

Kevin Aylward, Warden of the King's Ale

## Preface to the actual edition (as May 2018)

Due to the wealth of new material and options in ngspice the actual
order of chapters has been revised. Several new chapters have been
added. The LyX text processor has allowed adding internal cross
references. The PDF format has become the standard format for
distribution of the manual. Within each new ngspice distribution
(starting with ngspice-21) a manual edition is provided reflecting the
ngspice status at the time of distribution. At the same time, located at
[ngspice
manuals](http://ngspice.cvs.sourceforge.net/viewvc/ngspice/ngspice/ng-spice-manuals/),
the manual is constantly updated. Every new ngspice feature should enter
this manual as soon as it has been made available in the Git source code
master branch.

Holger Vogt

Mülheim, 2018

# Acknowledgments

## ngspice contributors

Spice3 and CIDER were originally written at The University of California
at Berkeley (USA).

XSPICE has been provided by Georgia Institute of Technology, Atlanta
(USA).

Since then, there have been many people working on the software, most of
them releasing patches to the original code through the Internet.

The following people have contributed in some way:

> 
> 
> Vera Albrecht,
> 
> Cecil Aswell,
> 
> Giles C. Billingsley,
> 
> Phil Barker,
> 
> Steven Borley,
> 
> Stuart Brorson,
> 
> Mansun Chan,
> 
> Wayne A. Christopher,
> 
> Al Davis,
> 
> Glao S. Dezai,
> 
> Jon Engelbert,
> 
> Daniele Foci,
> 
> Noah Friedman,
> 
> David A. Gates,
> 
> Alan Gillespie,
> 
> John Heidemann,
> 
> Marcel Hendrix,
> 
> Jeffrey M. Hsu,
> 
> JianHui Huang,
> 
> S. Hwang,
> 
> Chris Inbody,
> 
> Gordon M. Jacobs,
> 
> Min-Chie Jeng,
> 
> Beorn Johnson,
> 
> Stefan Jones,
> 
> Kenneth H. Keller,
> 
> Francesco Lannutti,
> 
> Robert Larice,
> 
> Mathew Lew,
> 
> Robert Lindsell,
> 
> Weidong Liu,
> 
> Kartikeya Mayaram,
> 
> Richard D. McRoberts,
> 
> Manfred Metzger,
> 
> Wolfgang Muees,
> 
> Paolo Nenzi,
> 
> Gary W. Ng,
> 
> Hong June Park,
> 
> Stefano Perticaroli,
> 
> Arno Peters,
> 
> Serban-Mihai Popescu,
> 
> Georg Post,
> 
> Thomas L. Quarles,
> 
> Emmanuel Rouat,
> 
> Jean-Marc Routure,
> 
> Jaijeet S. Roychowdhury,
> 
> Lionel Sainte Cluque,
> 
> Takayasu Sakurai,
> 
> Amakawa Shuhei,
> 
> Kanwar Jit Singh,
> 
> Bill Swartz,
> 
> Hitoshi Tanaka,
> 
> Steve Tell,
> 
> Andrew Tuckey,
> 
> Andreas Unger,
> 
> Holger Vogt,
> 
> Dietmar Warning,
> 
> Michael Widlok,
> 
> Charles D.H. Williams,
> 
> Antony Wilson,

and many others...

If someone helped in the development and has not been inserted in this
list then this omission was unintentional. If you feel you should be on
this list then please write to
\<[ngspice-devel@lists.sourceforge.net](ngspice-devel@lists.sourceforge.net)\>.
Do not be shy, we would like to make a list as complete as possible.

