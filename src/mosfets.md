#  MOSFETs

Ngspice supports all the original mosfet models present in SPICE3f5 and
almost all the newer ones that have been published and made open-source.
Both bulk and SOI (Silicon on Insulator) models are available. When
compiled with the cider option, ngspice implements the four terminals
numerical model that can be used to simulate a MOSFET (please refer to
numerical modeling documentation for additional information and
examples).

