# TABLE

A data entry by a tabulated listing is available with syntax similar to
the E-Source (see Chapt. [5.2.3](#subsec_table)).

Syntax for data entry from table:

``` listings
Gxxx n1 n2 TABLE  {expression} =
+  (x0, y0) (x1, y1) (x2, y2) <m=val>
```

Example (simple comparator with current output and voltage control):

``` listings
GCMP 0 11 TABLE {V(10,9)} = (-5MV, 0V) (5MV, 5V)
R 11 0 1k
```

m is an optional multiplier to the output current. val may be a
numerical value or an expression according to
[2.8.5](#subsec_Syntax_of_expressions) containing only references to
other parameters (no node voltages or branch currents\!), because it is
evaluated before the simulation commences. An '=' sign may follow the
keyword TABLE.

