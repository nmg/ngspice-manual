# CUR

General form:

``` listings
GXXXXXXX n+ n- cur='expr' <m=val>
```

Examples:

``` listings
G51 55 225 cur = 'V(3)*V(3)-Offs'
```

**Expression** may be an equation or an expression containing node
voltages or branch currents (in the form of i(vm)) and any other terms
as given for the B source and described in Chapt.
[5.1](#sec_B_source__ASRC_). It may contain parameters
([2.8.1](#subsec__param_line)) and special variables
([5.1.2](#subsec_Special_B_Source_Variables)). m is an optional
multiplier to the output current. val may be a numerical value or an
expression according to [2.8.5](#subsec_Syntax_of_expressions)
containing only references to other parameters (no node voltages or
branch currents\!), because it is evaluated before the simulation
commences.

