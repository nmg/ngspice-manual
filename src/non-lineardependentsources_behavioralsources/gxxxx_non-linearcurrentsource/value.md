# VALUE

Optional syntax:

``` listings
GXXXXXXX n+ n- value='expr' <m=val>
```

Examples:

``` listings
G51 55 225 value = 'V(3)*V(3)-Offs'
```

The '=' sign is optional.

