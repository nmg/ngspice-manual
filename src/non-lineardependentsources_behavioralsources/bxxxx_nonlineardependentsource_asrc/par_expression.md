# par\(*'expression'***\)**

The B source syntax may also be used in output lines like .plot as
algebraic expressions for output (see
Chapt.[15.6.6](#subsec_par__expression____Algebraic_expressions) ).

