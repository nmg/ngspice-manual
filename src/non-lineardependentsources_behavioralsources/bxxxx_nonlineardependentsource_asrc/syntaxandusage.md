# Syntax and usage

General form:

``` listings
BXXXXXXX n+ n- <i=expr> <v=expr> <tc1=value> <tc2=value> 
+ <temp=value> <dtemp=value>
```

Examples:

``` listings
B1 0 1 I=cos(v(1))+sin(v(2))
B2 0 1 V=ln(cos(log(v(1,2)^2)))-v(3)^4+v(2)^v(1)
B3 3 4 I=17
B4 3 4 V=exp(pi^i(vdd))
B5 2 0 V = V(1) < {Vlow} ? {Vlow} :
+  V(1) > {Vhigh} ? {Vhigh} : V(1)
```

**n+** is the positive node, and **n-** is the negative node. The values
of the **V** and **I** parameters determine the voltages and currents
across and through the device, respectively. If **I** is given then the
device is a current source, and if **V** is given the device is a
voltage source. One and only one of these parameters must be given.

A simple model is implemented for temperature behavior by the formula:

\[\begin{array}{ll}
{I\left( T \right) = I\left( {\lbrack font\ rm\ \lbrack char\ T\ mathalpha\rbrack\lbrack char\ N\ mathalpha\rbrack\lbrack char\ O\ mathalpha\rbrack\lbrack char\ M\ mathalpha\rbrack\rbrack} \right)\left( 1 + TC_{1}\left( {T - {\lbrack font\ rm\ \lbrack char\ T\ mathalpha\rbrack\lbrack char\ N\ mathalpha\rbrack\lbrack char\ O\ mathalpha\rbrack\lbrack char\ M\ mathalpha\rbrack\rbrack}} \right) + TC_{2}\left( T - {\lbrack font\ rm\ \lbrack char\ T\ mathalpha\rbrack\lbrack char\ N\ mathalpha\rbrack\lbrack char\ O\ mathalpha\rbrack\lbrack char\ M\ mathalpha\rbrack\rbrack})^{2} \right) \right.} & \\
\end{array}\]

or

\[\begin{array}{ll}
{V\left( T \right) = V\left( {\lbrack font\ rm\ \lbrack char\ T\ mathalpha\rbrack\lbrack char\ N\ mathalpha\rbrack\lbrack char\ O\ mathalpha\rbrack\lbrack char\ M\ mathalpha\rbrack\rbrack} \right)\left( 1 + TC_{1}\left( {T - {\lbrack font\ rm\ \lbrack char\ T\ mathalpha\rbrack\lbrack char\ N\ mathalpha\rbrack\lbrack char\ O\ mathalpha\rbrack\lbrack char\ M\ mathalpha\rbrack\rbrack}} \right) + TC_{2}\left( T - {\lbrack font\ rm\ \lbrack char\ T\ mathalpha\rbrack\lbrack char\ N\ mathalpha\rbrack\lbrack char\ O\ mathalpha\rbrack\lbrack char\ M\ mathalpha\rbrack\rbrack})^{2} \right) \right.} & \\
\end{array}\]

In the above formula, \`\(T\)' represents the instance temperature,
which can be explicitly set using the **temp** keyword or calculated
using the circuit temperature and **dtemp**, if present. If both
**temp** and **dtemp** are specified, the latter is ignored.

The small-signal AC behavior of the nonlinear source is a linear
dependent source (or sources) with a proportionality constant equal to
the derivative (or derivatives) of the source at the DC operating point.
The expressions given for **V** and **I** may be any function of
voltages and currents through voltage sources in the system.

The following functions of a single real variable are defined:

  - Trigonometric functions:  
    cos, sin, tan, acos, asin, atan
  - Hyperbolic functions:  
    cosh, sinh, acosh, asinh, atanh
  - Exponential and logarithmic:  
    exp, ln, log, log10 (ln, log with base e, log10 with base 10)
  - Other:  
    abs, sqrt, u, u2, uramp, floor, ceil, i
  - Functions  
    of two variables are: min, max, pow, \*\*, pwr, ^
  - Functions  
    of three variables are: a ? b:c

The function \`u' is the unit step function, with a value of one for
arguments greater than zero and a value of zero for arguments less than
zero. The function \`u2' returns a value of zero for arguments less than
zero, one for arguments greater than one and assumes the value of the
argument between these limits. The function \`uramp' is the integral of
the unit step: for an input x, the value is zero if x is less than zero,
or if x is greater than zero the value is x. These three functions are
useful in synthesizing piece-wise non-linear functions, though
convergence may be adversely affected.

The function i(xyz) returns the current through the first node of device
instance xyz.

The following standard operators are defined: +, -, \*, /, ^, unary -

Logical operators are \!=, \<\>, \>=, \<=, ==, \>, \<, ||, &&, \! .

A ternary function is defined as a ? b : c , which means IF a, THEN b,
ELSE c. Be sure to place a space in front of \`?' to allow the parser
distinguishing it from other tokens.

The B source functions pow, \*\*, ^, and pwr need some special care.
Avoiding undefined regions in x1, they differ from the common
mathematical usage (and from the functions depicted in chapt.
[2.8.5](#subsec_Syntax_of_expressions)).

The functions y = pow(x1,x2), x1\*\*x2, and x1^x2 , all of them
describing \(y = x1^{x2}\), resolve to the following:

y = pow(fabs(x1), x2)

pow in the preceding line is the standard C math library function.

The function y = pwr(x1,x2) resolves to

if (x1 \< 0.0)

y = (-pow(-x1, x2));

else

y = (pow(x1, x2));

pow here again is the standard C math library function.

Example: Ternary function

``` listings
* B source test Clamped voltage source 
* C. P. Basso "Switched-mode power supplies", New York, 2008
.param Vhigh = 4.6 
.param Vlow = 0.4
Vin1 1 0 DC 0 PWL(0 0 1u 5)
Bcl 2 0 V = V(1) < Vlow ? Vlow : V(1) > Vhigh ? Vhigh : V(1)
.control 
unset askquit 
tran  5n 1u 
plot V(2) vs V(1) 
.endc
.end
```

If the argument of log, ln, or sqrt becomes less than zero, the absolute
value of the argument is used. If a divisor becomes zero or the argument
of log or ln becomes zero, an error will result. Other problems may
occur when the argument for a function in a partial derivative enters a
region where that function is undefined.

Parameters may be used like {Vlow} shown in the example above.
Parameters will be evaluated upon set up of the circuit, vectors like
V(1) will be evaluated during the simulation.

To get time into the expression you can integrate the current from a
constant current source with a capacitor and use the resulting voltage
(don't forget to set the initial voltage across the capacitor).

Non-linear resistors, capacitors, and inductors may be synthesized with
the nonlinear dependent source. Nonlinear resistors, capacitors and
inductors are implemented with their linear counterparts by a change of
variables implemented with the nonlinear dependent source. The following
subcircuit will implement a nonlinear capacitor:

Example: Non linear capacitor

``` listings
.Subckt nlcap pos neg
* Bx: calculate f(input voltage)
Bx 1 0 v = f(v(pos,neg))
* Cx: linear capacitance
Cx 2 0 1
* Vx: Ammeter to measure current into the capacitor
Vx 2 1 DC 0Volts
* Drive the current through Cx back into the circuit
Fx pos neg Vx 1
.ends
```

Example for f(v(pos,neg)):

``` listings
Bx 1 0 V = v(pos,neg)*v(pos,neg)
```

Non-linear resistors or inductors may be described in a similar manner.
An example for a nonlinear resistor using this template is shown below.

Example: Non linear resistor

``` listings
* use of 'hertz' variable in nonlinear resistor
*.param rbase=1k
* some tests
B1  1 0  V = hertz*v(33) 
B2  2 0 V = v(33)*hertz 
b3  3 0 V = 6.283e3/(hertz+6.283e3)*v(33)
V1 33 0 DC 0 AC 1
*** Translate R1 10 0 R='1k/sqrt(HERTZ)' to B source *** 
.Subckt nlres pos neg rb=rbase 
* Bx: calculate f(input voltage) 
Bx   1    0    v = -1 / {rb} / sqrt(HERTZ) * v(pos, neg) 
* Rx: linear resistance 
Rx   2    0    1 
```

Example: Non linear resistor (continued)

``` listings
* Vx: Ammeter to measure current into the resistor 
Vx   2    1    DC 0Volts 
* Drive the current through Rx back into the circuit 
Fx   pos  neg  Vx 1 
.ends
Xres 33 10 nlres rb=1k 
*Rres 33 10 1k 
Vres 10 0 DC 0
.control 
define check(a,b) vecmax(abs(a - b))
ac lin 10 100 1k
* some checks
print v(1) v(2) v(3) 
if check(v(1), frequency) < 1e-12  
echo "INFO: ok" 
end
plot vres#branch 
.endc
.end
```

