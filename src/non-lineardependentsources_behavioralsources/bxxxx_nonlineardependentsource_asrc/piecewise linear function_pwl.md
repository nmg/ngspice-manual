# Piecewise Linear Function: pwl

Both B source types may contain a piece-wise linear dependency of one
network variable:

Example: pwl\_current

``` listings
Bdio 1 0 I = pwl(v(A), 0,0, 33,10m, 100,33m, 200,50m)
```

v(A) is the independent variable x. Each pair of values following
describes the x,y functional relation: In this example at node A voltage
of 0V the current of 0A is generated - next pair gives 10mA flowing from
ground to node 1 at 33V on node A and so forth.

The same is possible for voltage sources:

Example: pwl\_voltage

``` listings
Blimit b 0 V = pwl(v(1), -4,0, -2,2, 2,4, 4,5, 6,5)
```

Monotony of the independent variable in the pwl definition is checked -
non-monotonic x entries will stop the program execution. v(1) may be
replaced by a controlling current source. v(1) may also be replaced by
an expression, e.g. \(- 2 i\left( V_{in} \right)\). The value pairs may
also be parameters, and have to be predefined by a .param statement. An
example for the pwl function using all of these options is shown below.

Example: pwl function in B source

``` listings
Demonstrates usage of the pwl function in an B source (ASRC)
* Also emulates the TABLE function with limits

.param x0=-4 y0=0
.param x1=-2 y1=2 
.param x2=2 y2=-2
.param x3=4 y3=1 
.param xx0=x0-1
.param xx3=x3+1

Vin   1 0   DC=0V
R 1 0 2

* no limits outside of the tabulated x values
* (continues linearily)
Btest2 2 0  I = pwl(v(1),'x0','y0','x1','y1','x2','y2','x3','y3')

* like TABLE function with limits:
Btest3 3 0   I = (v(1) < 'x0') ? 'y0' :  (v(1) < 'x3') ? 
+ pwl(v(1),'x0','y0','x1','y1','x2','y2','x3','y3') : 'y3'

* more efficient and elegant TABLE function with limits
*(voltage controlled):
Btest4 4 0   I = pwl(v(1), 
+ 'xx0','y0', 'x0','y0', 
+             'x1','y1',
+             'x2','y2',
+             'x3','y3', 'xx3','y3')
*
* more efficient and elegant TABLE function with limits
* (controlled by current):
Btest5 5 0   I = pwl(-2*i(Vin), 
+ 'xx0','y0', 'x0','y0', 
+             'x1','y1',
+             'x2','y2',
+             'x3','y3', 'xx3','y3')

Rint2 2 0 1
Rint3 3 0 1
Rint4 4 0 1
Rint5 5 0 1
.control
dc  Vin  -6 6  0.2
plot v(2) v(3) v(4)-0.5 v(5)+0.5
.endc

.end
```

