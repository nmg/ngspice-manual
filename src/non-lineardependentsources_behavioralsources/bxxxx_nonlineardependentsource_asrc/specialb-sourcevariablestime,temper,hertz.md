# Special B-Source Variables time, temper, hertz

The special variables **time** and **temper** are available in a
transient analysis, reflecting the actual simulation time and circuit
temperature. **temper** returns the circuit temperature, given in degree
C (see [2.11](#sec__temp)). The variable **hertz** is available in an AC
analysis. **time** is zero in the AC analysis, **hertz** is zero during
transient analysis. Using the variable **hertz** may cost some CPU time
if you have a large circuit, because for each frequency the operating
point has to be determined before calculating the AC response.

