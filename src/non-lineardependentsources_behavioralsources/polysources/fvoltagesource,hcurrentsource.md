# F voltage source, H current source

General form:

``` listings
FXXXX N+ N- POLY(ND) V1 (V2 V3 ...) P0 (P1...)
```

Example:

``` listings
FNONLIN 100 101 POLY(2) VDD Vxx 0 0.0 13.6 0.2 0.005 
```

POLY(ND) Specifies the number of dimensions of the polynomial. The
number of controlling sources must be equal to the number of dimensions.

(N+) and (N-) nodes are output nodes. Positive current flows from the
(+) node through the source to the (-) node.

V1 (V2 V3 ...) are the controlling voltage sources. Control variable is
the current through these sources.

P0 (P1...) are the coefficients, as have been described in
[5.5.1](#subsec_E_voltage_source).

