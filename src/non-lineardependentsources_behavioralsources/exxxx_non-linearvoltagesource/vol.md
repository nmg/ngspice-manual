# VOL

General form:

``` listings
EXXXXXXX n+ n- vol='expr'
```

Examples:

``` listings
E41 4 0 vol = 'V(3)*V(3)-Offs'
```

**Expression** may be an equation or an expression containing node
voltages or branch currents (in the form of i(vm)) and any other terms
as given for the B source and described in Chapt.
[5.1](#sec_B_source__ASRC_). It may contain parameters
([2.8.1](#subsec__param_line)) and the special variables time, temper,
hertz ([5.1.2](#subsec_Special_B_Source_Variables)). ' or { } may be
used to delimit the function.

