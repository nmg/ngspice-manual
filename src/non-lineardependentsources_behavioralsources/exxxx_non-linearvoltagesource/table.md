# TABLE

Data may be entered from the listings of a data table similar to the pwl
B-Source ([5.1.4](#subsec_PiecewiseLinearFunction__pwl)). Data are
grouped into x, y pairs. **Expression** may be an equation or an
expression containing node voltages or branch currents (in the form of
i(vm)) and any other terms as given for the B source and described in
Chapt. [5.1](#sec_B_source__ASRC_). It may contain parameters
([2.8.1](#subsec__param_line)). ' or { } may be used to delimit the
function. **Expression** delivers the x-value, which is used to generate
a corresponding y-value according to the tabulated value pairs, using
linear interpolation. If the x-value is below x0 , y0 is returned, above
x2 y2 is returned (limiting function). The value pairs have to be real
numbers, parameters are *not* allowed.

Syntax for data entry from table:

``` listings
Exxx n1 n2 TABLE {expression} = (x0, y0) (x1, y1) (x2, y2)
```

Example (simple comparator):

``` listings
ECMP 11 0 TABLE {V(10,9)} = (-5mV, 0V) (5mV, 5V) 
```

An '=' sign may follow the keyword TABLE.

