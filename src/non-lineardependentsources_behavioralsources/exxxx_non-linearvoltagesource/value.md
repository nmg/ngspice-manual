# VALUE

Optional syntax:

``` listings
EXXXXXXX n+ n- value={expr}
```

Examples:

``` listings
E41 4 0 value = {V(3)*V(3)-Offs}
```

The '=' sign is optional.

