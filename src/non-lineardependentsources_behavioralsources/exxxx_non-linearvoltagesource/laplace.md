# LAPLACE

Currently ngspice does not offer a direct E-Source element with the
LAPLACE option. There is however a XSPICE code model equivalent called
**s\_xfer** (see Chapt. [12.2.16](#subsec_S_Domain_Transfer_Function)),
which you may invoke manually. The XSPICE option has to be enabled
([32.1](#sec_Ngspice_Installation_under)). AC
([15.3.1](#subsec__AC__Small_Signal_AC)) and transient analysis
([15.3.9](#subsec__TRAN__Transient_Analysis)) is supported.

The following E-Source:

``` listings
ELOPASS 4 0 LAPLACE {V(1)}
+                   {5 * (s/100 + 1) / (s^2/42000 + s/60 + 1)}
```

may be replaced by:

``` listings
AELOPASS 1 int_4 filter1
.model filter1 s_xfer(gain=5
+                    num_coeff=[{1/100} 1]
+                    den_coeff=[{1/42000} {1/60} 1]
+                    int_ic=[0 0])
ELOPASS 4 0 int_4 0 1
```

where you have the voltage of node 1 as input, an intermediate output
node int\_4 and an E-source as buffer to keep the name \`ELOPASS'
available if further processing is required.

If the controlling expression is more complex than just a voltage node,
you may add a B-Source ([5.1](#sec_B_source__ASRC_)) for evaluating the
expression before entering the A-device.

E-Source with complex controlling expression:

``` listings
ELOPASS 4 0 LAPLACE {V(1)*v(2)} {10 / (s/6800 + 1)}
```

may be replaced by:

``` listings
BELOPASS int_1 0 V=V(1)*v(2)
AELOPASS int_1 int_4 filter1
.model filter1 s_xfer(gain=10
+                    num_coeff=[1]
+                    den_coeff=[{1/6800} 1]
+                    int_ic=[0])
ELOPASS 4 0 int_4 0 1
```

