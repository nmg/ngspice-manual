# Introduction

New compact device models today are released as Verilog-A code. Ngspice
applies ADMS to translate the va code into ngspice C syntax. Currently a
limited number of Verilog-A models is supported: HICUM level0 and level2
([HICUM model web
page](http://www.iee.et.tu-dresden.de/iee/eb/hic_new/hic_intro.html)),
MEXTRAM ([MEXTRAM model web page](http://mextram.ewi.tudelft.nl/)), EKV
([EKV model web page](http://ekv.epfl.ch/)) and PSP ([NXP PSP web
site](https://www.nxp.com/pages/model-psp:MODELPSP)).

