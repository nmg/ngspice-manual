# ADMS

ADMS is a code generator that converts electrical compact device models
specified in high-level description language into ready-to-compile C
code for the API of spice simulators. Based on transformations specified
in XML language, ADMS transforms Verilog-AMS code into other target
languages. Here we use it to to translate the va code into ngspice C
syntax.

To make use of it, a set of ngspice specific XML files is distributed
with ngspice in ngspice\\src\\spicelib\\devices\\adms\\admst. Their
translation is done by the code generator executable admsXml (see
below).

