# How to setup a \*.va model for ngspice

Unfortunately most of the above named models' licenses are not
compatible to free software rules as defined by
[DFSG](https://wiki.debian.org/DFSGLicense). Therefore since ngspice-28
the va model files are no longer part of the standard ngspice
distribution. They may however be downloaded as a 7z archive from the
[ngspice-28 file distribution
folder](https://sourceforge.net/projects/ngspice/files/ng-spice-rework/28/).
After downloading, you may expand the zipped files into your ngspice top
level folder. The models enable dc, ac, and tran simulations. Noise
simulation is not supported.

Other (foreign) va model files will not compile without code tweaking,
due to the limited capabilities of our ADMS installation.

