# Adding admsXml to your build environment

The actual admsXml code is maintained by the
[QUCS](http://qucs.sourceforge.net/) project and is available at
[GitHub](https://github.com/Qucs/ADMS).

Information on how to compile and install admsXml for Linux or Cygwin is
available on the GitHub page. For MS Windows users admsXml.exe is
available for download
[here](https://sourceforge.net/projects/mot-adms/). You may copy
admsXml.exe to your MSYS2 setup into the folder msys64\\mingw64\\bin, if
64 bit compilation is intended.

More information, though partially outdated, is obtainable from the
[ngspice web pages](http://ngspice.sourceforge.net/admshowto.html).

