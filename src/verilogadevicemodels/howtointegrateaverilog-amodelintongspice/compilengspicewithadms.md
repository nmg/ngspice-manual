# Compile ngspice with ADMS

In the top level ngspice folder there are two compile scripts
compile\_min.sh and compile\_linux.sh. They contain information how to
compile ngspice with ADMS. You will have to run autogen.sh with the adms
flag

./autogen.sh - -adms

In addition you have to add - -enable-adms to the ./configure command.
Please check [32.1](#sec_Ngspice_Installation_under) for perequisites
and further details.

Compiling ngspice with ADMS with MS Visual Studio is not supported.

