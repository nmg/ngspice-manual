# Analysis at Different Temperatures

Temperature, in ngspice, is a property associated to the entire circuit,
rather than an analysis option. Circuit temperature has a default
(nominal) value of 27°C (300.15 K) that can be changed using the
**TEMP** option in an .option control line (see
[15.1.1](#subsec_General_Options)) or by the .TEMP line (see
[2.11](#sec__temp)), which has precedence over the .option TEMP line.
All analyses are, thus, performed at circuit temperature, and if you
want to simulate circuit behavior at different temperatures you should
prepare a netlist for each temperature.

All input data for ngspice is assumed to have been measured at the
circuit nominal temperature. This value can further be overridden for
any device that models temperature effects by specifying the **TNOM**
parameter on the .model itself. Individual instances may further
override the circuit temperature through the specification of **TEMP**
and **DTEMP** parameters on the instance. The two options are not
independent even if you can specify both on the instance line, the
**TEMP** option overrides **DTEMP**. The algorithm to compute instance
temperature is described below:

IF TEMP is specified THEN

instance\_temperature = TEMP

ELSE IF

instance\_temperature = circuit\_temperature + DTEMP

END IF

Algorithm 1.1: Instance temperature computation

Temperature dependent support is provided for all devices except voltage
and current sources (either independent and controlled) and BSIM models.
BSIM MOSFETs have an alternate temperature dependency scheme that
adjusts all of the model parameters before input to ngspice.

For details of the BSIM temperature adjustment, see
\[[6](#LyXCite-key_6)\] and \[[7](#LyXCite-key_7)\]. Temperature appears
explicitly in the exponential terms of the BJT and diode model
equations. In addition, saturation currents have a built-in temperature
dependence. The temperature dependence of the saturation current in the
BJT models is determined by:

\[\begin{array}{ll}
{I_{S}\left( T_{1} \right) = I_{S}\left( T_{0} \right)\left( \frac{T_{1}}{T_{0}} \right)^{XTI}\exp\left( \frac{E_{g}q\left( {T_{1}T_{0}} \right)}{k\left( {T_{1} - T_{0}} \right)} \right)} & \\
\end{array}\]

where \(k\) is Boltzmann's constant, \(q\) is the electronic charge,
\(E_{g}\) is the energy gap model parameter, and \(XTI\) is the
saturation current temperature exponent (also a model parameter, and
usually equal to 3).

The temperature dependence of forward and reverse beta is according to
the formula:

\[\begin{array}{ll}
{B\left( T_{1} \right) = B\left( T_{0} \right)\left( \frac{T_{1}}{T_{0}} \right)^{XTB}} & \\
\end{array}\]

where \(T_{0}\) and \(T_{1}\) are in degrees Kelvin, and \(XTB\) is a
user-supplied model parameter. Temperature effects on beta are carried
out by appropriate adjustment to the values of \(B_{F}\), \(I_{SE}\),
\(B_{R}\), and \(I_{SC}\) (SPICE model parameters BF, ISE, BR, and ISC,
respectively).

Temperature dependence of the saturation current in the junction diode
model is determined by:

\[\begin{array}{ll}
{I_{S}\left( T_{1} \right) = I_{S}\left( T_{0} \right)\left( \frac{T_{1}}{T_{0}} \right)^{\frac{XTI}{N}}\exp\left( \frac{E_{g}q\left( {T_{1}T_{0}} \right)}{Nk\left( {T_{1} - T_{0}} \right)} \right)} & \\
\end{array}\]

where \(N\) is the emission coefficient model parameter, and the other
symbols have the same meaning as above. Note that for Schottky barrier
diodes, the value of the saturation current temperature exponent,
\(XTI\), is usually 2. Temperature appears explicitly in the value of
junction potential, U (in Ngspice PHI), for all the device models.

The temperature dependence is determined by:

\[\begin{array}{ll}
{U\left( T \right) = \frac{kT}{q}\ln\left( \frac{N_{a}N_{d}}{N_{i}\left( T \right)^{2}} \right)} & \\
\end{array}\]

where \(k\) is Boltzmann's constant, \(q\) is the electronic charge,
\(N_{a}\) is the acceptor impurity density, \(N_{d}\) is the donor
impurity density, \(N_{i}\) is the intrinsic carrier concentration, and
\(E_{g}\) is the energy gap. Temperature appears explicitly in the value
of surface mobility, \(M_{0}\)(or \(U_{0}\)), for the MOSFET model.

The temperature dependence is determined by:

\[\begin{array}{ll}
{M_{0}\left( T \right) = \frac{M_{0}\left( T_{0} \right)}{\left( \frac{T}{T_{0}} \right)^{1.5}}} & \\
\end{array}\]

The effects of temperature on resistors, capacitor and inductors is
modeled by the formula:

\[\begin{array}{ll}
{R\left( T \right) = R\left( T_{0} \right)\left\lbrack {1 + TC_{1}\left( {T - T_{0}} \right) + TC_{2}\left( {T - T_{0}} \right)^{2}} \right\rbrack} & \\
\end{array}\]

where \(T\) is the circuit temperature, \(T_{0}\) is the nominal
temperature, and \(TC_{1}\) and \(TC_{2}\) are the first and second
order temperature coefficients.

