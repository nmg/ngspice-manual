# Convergence

Ngspice uses the Newton-Raphson algorithm to solve nonlinear equations
arising from circuit description. The NR algorithm is interactive and
terminates when both of the following conditions hold:

1.  The nonlinear branch currents converge to within a tolerance of 0.1%
    or 1 picoamp (1.0e-12 Amp), whichever is larger.
2.  The node voltages converge to within a tolerance of 0.1% or 1
    microvolt (1.0e-6 Volt), whichever is larger.

