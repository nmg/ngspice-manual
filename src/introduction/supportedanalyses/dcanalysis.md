# DC Analysis

The dc analysis portion of ngspice determines the dc operating point of
the circuit with inductors shorted and capacitors opened. The dc
analysis options are specified on the .DC, .TF, and .OP control lines.

There is assumed to be no time dependence on any of the sources within
the system description. The simulator algorithm subdivides the circuit
into those portions that require the analog simulator algorithm and such
that require the event-driven algorithm. Each subsystem block is then
iterated to solution, with the interfaces between analog nodes and
event-driven nodes iterated for consistency across the entire system.

Once stable values are obtained for all nodes in the system, the
analysis halts and the results may be displayed or printed out as you
request them.

A dc analysis is automatically performed prior to a transient analysis
to determine the transient initial conditions, and prior to an ac
small-signal analysis to determine the linearized, small-signal models
for nonlinear devices. If requested, the dc small-signal value of a
transfer function (ratio of output variable to input source), input
resistance, and output resistance is also computed as a part of the dc
solution. The dc analysis can also be used to generate dc transfer
curves: a specified independent voltage, current source, resistor or
temperature is stepped over a user-specified range and the dc output
variables are stored for each sequential source value.

