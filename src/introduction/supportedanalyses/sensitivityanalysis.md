# Sensitivity Analysis

Ngspice will calculate either the DC operating-point sensitivity or the
AC small-signal sensitivity of an output variable with respect to all
circuit variables, including model parameters. Ngspice calculates the
difference in an output variable (either a node voltage or a branch
current) by perturbing each parameter of each device independently.
Since the method is a numerical approximation, the results may
demonstrate second order effects in highly sensitive parameters, or may
fail to show very low but non-zero sensitivity. Further, since each
variable is perturb by a small fraction of its value, zero-valued
parameters are not analyzed (this has the benefit of reducing what is
usually a very large amount of data).

