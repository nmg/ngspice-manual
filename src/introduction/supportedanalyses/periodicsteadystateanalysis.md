# Periodic Steady State Analysis

*Experimental code.*

PSS is a radio frequency periodical large-signal dedicated analysis. The
implementation is based on a time domain shooting method that make use
of transient analysis. As it is in early development stage, PSS performs
analysis only on autonomous circuits, meaning that it is able to predict
fundamental frequency and (harmonic) amplitude(s) for oscillators, VCOs,
etc.. The algorithm is based on a search of the minimum error vector
defined as the difference of RHS vectors between two occurrences of an
estimated period. Convergence is reached when the mean of this error
vector decreases below a given threshold parameter. Results of PSS are
the basis of periodical large-signal analyses like PAC or PNoise.

