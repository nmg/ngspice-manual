# Pole-Zero Analysis

The pole-zero analysis portion of Ngspice computes the poles and/or
zeros in the small-signal ac transfer function. The program first
computes the dc operating point and then determines the linearized,
small-signal models for all the nonlinear devices in the circuit. This
circuit is then used to find the poles and zeros of the transfer
function. Two types of transfer functions are allowed: one of the form
(output voltage)/(input voltage) and the other of the form (output
voltage)/(input current). These two types of transfer functions cover
all the cases and one can find the poles/zeros of functions like
input/output impedance and voltage gain. The input and output ports are
specified as two pairs of nodes. The pole-zero analysis works with
resistors, capacitors, inductors, linear-controlled sources, independent
sources, BJTs, MOSFETs, JFETs and diodes. Transmission lines are not
supported. The method used in the analysis is a sub-optimal numerical
search. For large circuits it may take a considerable time or fail to
find all poles and zeros. For some circuits, the method becomes \`lost'
and finds an excessive number of poles or zeros.

