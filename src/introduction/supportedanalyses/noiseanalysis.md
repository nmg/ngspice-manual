# Noise Analysis

The noise analysis portion of Ngspice gives the device-generated noise
for a given circuit. When provided with an input source and an output
port, the analysis calculates the noise contributions of each device,
and each noise generator within the device, to the output port voltage.
It also calculates the equivalent input noise of the circuit, based on
the output noise. This is done for every frequency point in a specified
range - the calculated value of the noise corresponds to the spectral
density of the circuit variable viewed as a stationary Gaussian
stochastic process. After calculating the spectral densities, noise
analysis integrates these values over the specified frequency range to
arrive at the total noise voltage and current over this frequency range.
The calculated values correspond to the variance of the circuit
variables viewed as stationary Gaussian processes.

