# Convergence failure

Although the algorithm used in ngspice has been found to be very
reliable, in some cases it fails to converge to a solution. When this
failure occurs, the program terminates the job. Failure to converge in
dc analysis is usually due to an error in specifying circuit
connections, element values, or model parameter values. Regenerative
switching circuits or circuits with positive feedback probably will not
converge in the dc analysis unless the **OFF** option is used for some
of the devices in the feedback path, .nodeset control line is used to
force the circuit to converge to the desired state.

