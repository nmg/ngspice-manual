# Voltage convergence criterion

The algorithm has reached convergence when the difference between the
last iteration \(k\) and the current one (\(\left. k + 1 \right)\)

\[\begin{array}{ll}
{\left| {v_{n}^{({k + 1})} - v_{n}^{(k)}} \right| \leq \mathtt{RELTOL} v_{n_{max}} + \mathtt{VNTOL},} & \\
\end{array}\]

where

\[\begin{array}{ll}
{v_{n_{max}} = \max\left( {\left| v_{n}^{({k + 1})} \right|,\left| v_{n}^{(k)} \right|} \right).} & \\
\end{array}\]

The RELTOL (RELative TOLerance) parameter, which default value is
\(10^{- 3}\), specifies how small the solution update must be, relative
to the node voltage, to consider the solution to have converged. The
VNTOL (absolute convergence) parameter, which has \(1\mu V\) as default
value, becomes important when node voltages have near zero values. The
relative parameter alone, in such case, would need too strict
tolerances, perhaps lower than computer round-off error, and thus
convergence would never be achieved. VNTOL forces the algorithm to
consider as converged any node whose solution update is lower than its
value.

