# Current convergence criterion

Ngspice checks the convergence on the non-linear functions that describe
the non-linear branches in circuit elements. In semiconductor devices
the functions defines currents through the device and thus the name of
the criterion.

Ngspice computes the difference between the value of the nonlinear
function computed for the last voltage and the linear approximation of
the same current computed with the actual voltage

\[\begin{array}{ll}
{\left| {\hat{i_{branch}^{({k + 1})}} - i_{branch}^{(k)}} \right| \leq \mathtt{RELTOL} i_{br_{max}} + \mathtt{ABSTOL},} & \\
\end{array}\]

where

\[\begin{array}{ll}
{i_{br_{max}} = \max\left( {\hat{i_{branch}^{({k + 1})}},i_{branch}^{(k)}} \right).} & \\
\end{array}\]

In the two expressions above, the \(\hat{i_{branch}}\) indicates the
linear approximation of the current.

