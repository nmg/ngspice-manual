# Mixed-Level Simulation

Ngspice can simulate numerical device models for diodes and transistors
in two different ways, either through the integrated DSIM simulator or
interfacing to GSS TCAD system. DSIM is an internal C-based device
simulator that is part of the CIDER simulator, the mixed-level simulator
based on SPICE3f5. CIDER within ngspice provides circuit analyses,
compact models for semiconductor devices, and one- or two-dimensional
numerical device models.

