# Mixed-Signal Simulation

Modern circuits often contain a mix of analog and digital circuits. To
simulate such circuits efficiently and accurately a mix of analog and
digital simulation techniques is required. When analog simulation
algorithms are combined with digital simulation algorithms, the result
is termed \`mixed-mode simulation'.

Two basic methods of implementing mixed-mode simulation used in practice
are the \`native mode' and \`glued mode' approaches. Native mode
simulators implement both an analog algorithm and a digital algorithm in
the same executable. Glued mode simulators actually use two simulators,
one of which is analog and the other digital. This type of simulator
must define an input/output protocol so that the two executables can
communicate with each other effectively. The communication constraints
tend to reduce the speed, and sometimes the accuracy, of the complete
simulator. On the other hand, the use of a glued mode simulator allows
the component models developed for the separate executables to be used
without modification.

Ngspice is a native mode simulator providing both analog and event-based
simulation in the same executable. The underlying algorithms of ngspice
(coming from XSPICE and its Code Model Subsystem) allow use of all the
standard SPICE models, provide a pre-defined collection of the most
common analog and digital functions, and provide an extensible base on
which to build additional models.

