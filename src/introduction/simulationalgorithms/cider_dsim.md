# CIDER \(DSIM\)

CIDER integrates the DSIM simulator with Spice3. It provides accurate,
one- and two-dimensional numerical device models based on the solution
of Poisson's equation, and the electron and hole current-continuity
equations. DSIM incorporates many of the same basic physical models
found in the Stanford two-dimensional device simulator PISCES. Input to
CIDER consists of a SPICE-like description of the circuit and its
compact models, and PISCES-like descriptions of the structures of
numerically modeled devices. As a result, CIDER should seem familiar to
designers already accustomed to these two tools. The CIDER input format
has great flexibility and allows access to physical model parameters.
New physical models have been added to allow simulation of
state-of-the-art devices. These include transverse field mobility
degradation important in scaled-down MOSFETs and a polysilicon model for
poly-emitter bipolar transistors. Temperature dependence has been
included over the range from -50C to 150C. The numerical models can be
used to simulate all the basic types of semiconductor devices:
resistors, MOS capacitors, diodes, BJTs, JFETs and MOSFETs. BJTs and
JFETs can be modeled with or without a substrate contact. Support has
been added for the management of device internal states. Post-processing
of device states can be performed using the ngnutmeg user interface.

