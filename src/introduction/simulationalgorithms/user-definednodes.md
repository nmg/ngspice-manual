# User-Defined Nodes

Ngspice supports creation of \`User-Defined Node' types. User-Defined
Node types allow you to specify nodes that propagate data other than
voltages, currents, and digital states. Like digital nodes, User-Defined
Nodes use event-driven simulation, but the state value may be an
arbitrary data type. A simple example application of User-Defined Nodes
is the simulation of a digital signal processing filter algorithm. In
this application, each node could assume a real or integer value. More
complex applications may define types that involve complex data such as
digital data vectors or even non-electronic data.

Ngspice digital simulation is actually implemented as a special case of
this User-Defined Node capability where the digital state is defined by
a data structure that holds a Boolean logic state and a strength value.

