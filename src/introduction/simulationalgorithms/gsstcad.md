# GSS TCAD

GSS is a TCAD software that enables two-dimensional numerical simulation
of semiconductor device with well-known drift-diffusion and hydrodynamic
method. GSS has Basic DDM (drift-diffusion method) solver, Lattice
Temperature Corrected DDM solver, EBM (energy balance method) solver and
Quantum corrected DDM solver based on density-gradient theory. The GSS
program is directed via input statements by a user specified disk file.
Supports triangle mesh generation and adaptive mesh refinement. Employs
PMI (physical model interface) to support various materials, including
compound semiconductor materials such as SiGe and AlGaAs. Supports DC
sweep, transient and AC sweep calculations. The device can be stimulated
by voltage or current source(s).

GSS is no longer updated, but is still available as open source as a
limited edition of the commercial GENIUS TCAD tool. This interface has
not been tested with actual ngspice versions and may need some
maintainance efforts.

