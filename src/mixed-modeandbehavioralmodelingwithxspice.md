#  Mixed-Mode and Behavioral Modeling with XSPICE

Ngspice implements XSPICE extensions for behavioral and mixed-mode
(analog and digital) modeling. In the XSPICE framework this is referred
to as code level modeling. Behavioral modeling may benefit dramatically
because XSPICE offers a means to add analog functionality programmed in
C. Many examples (amplifiers, oscillators, filters ...) are presented in
the following. Even more flexibility is available because you may define
your own models and use them in addition and in combination with all the
already existing ngspice functionality. Digital and mixed mode
simulation is speeded up significantly by simulating the digital part in
an event driven manner, in that state equations use only a few allowed
states and are evaluated only during switching, and not continuously in
time and signal as in a pure analog simulator.

This chapter describes the predefined models available in ngspice,
stemming from the original XSPICE simulator or being added to enhance
the usability. The instructions for writing new code models are given in
Chapt. [28](#cha_Code_Models_and).

To make use of the XSPICE extensions, you need to compile them in.
Linux, CYGWIN, MINGW and other users may add the flag --enable-xspice to
their ./configure command and then recompile. The pre-built ngspice for
Windows distribution has XSPICE already enabled. For detailed compiling
instructions see Chapt. [32.1](#sec_Ngspice_Installation_under).

