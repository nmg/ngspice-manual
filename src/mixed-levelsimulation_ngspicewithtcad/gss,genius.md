# GSS, Genius

For Linux users the cooperation of the TCAD software GSS with ngspice
might be of interest, see <http://ngspice.sourceforge.net/gss.html>.
This project is no longer maintained however, but has moved into the
Genius simulator, still available as open source [cogenda
genius](http://www.cogenda.com/article/download).

